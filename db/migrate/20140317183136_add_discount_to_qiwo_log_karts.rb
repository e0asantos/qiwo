class AddDiscountToQiwoLogKarts < ActiveRecord::Migration
  def change
    add_column :qiwo_log_karts, :discount, :decimal
  end
end
