class AddImageOneFileNameToQiwoProducts < ActiveRecord::Migration
  def change
    add_column :qiwo_products, :image_one_file_name, :string
  end
end
