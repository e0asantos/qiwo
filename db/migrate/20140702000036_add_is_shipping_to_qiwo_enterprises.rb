class AddIsShippingToQiwoEnterprises < ActiveRecord::Migration
  def change
    add_column :qiwo_enterprises, :is_shipping, :integer
  end
end
