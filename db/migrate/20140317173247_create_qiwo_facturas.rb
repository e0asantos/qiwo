class CreateQiwoFacturas < ActiveRecord::Migration
  def change
    create_table :qiwo_facturas do |t|
      t.integer :id
      t.string :folio
      t.integer :status
      t.integer :qiwo_log_kart_id
      t.integer :qiwo_order_id

      t.timestamps
    end
  end
end
