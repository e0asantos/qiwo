class CreateTelegrams < ActiveRecord::Migration
  def change
    create_table :telegrams do |t|
      t.string :phone
      t.string :auth_code

      t.timestamps
    end
  end
end
