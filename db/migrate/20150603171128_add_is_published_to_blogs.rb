class AddIsPublishedToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :is_published, :integer
  end
end
