class CreateYos < ActiveRecord::Migration
  def change
    create_table :yos do |t|
      t.integer :userid

      t.timestamps
    end
  end
end
