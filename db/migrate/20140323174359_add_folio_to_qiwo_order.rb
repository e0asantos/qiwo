class AddFolioToQiwoOrder < ActiveRecord::Migration
  def change
    add_column :qiwo_orders, :folio, :integer
  end
end
