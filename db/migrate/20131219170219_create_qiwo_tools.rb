class CreateQiwoTools < ActiveRecord::Migration
  def change
    create_table :qiwo_tools do |t|
      t.integer :id
      t.integer :user_id
      t.string :toolname
      t.string :apikey
      t.string :apisecret
      t.string :description

      t.timestamps
    end
  end
end
