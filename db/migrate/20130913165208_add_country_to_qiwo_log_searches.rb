class AddCountryToQiwoLogSearches < ActiveRecord::Migration
  def change
    add_column :qiwo_log_searches, :country, :string
  end
end
