class AddTipOptionToQiwoOrders < ActiveRecord::Migration
  def change
    add_column :qiwo_orders, :tip_option, :integer
  end
end
