class CreateQlogs < ActiveRecord::Migration
  def change
    create_table :qlogs do |t|
      t.string :modulo
      t.string :id_ref
      t.string :id_status
      t.string :id_value

      t.timestamps
    end
  end
end
