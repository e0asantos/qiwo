class CreateQiwoProductCategories < ActiveRecord::Migration
  def change
    create_table :qiwo_product_categories do |t|
      t.integer :id
      t.integer :category_id
      t.integer :product_id
      t.integer :store_id

      t.timestamps
    end
  end
end
