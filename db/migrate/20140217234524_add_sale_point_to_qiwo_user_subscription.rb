class AddSalePointToQiwoUserSubscription < ActiveRecord::Migration
  def change
    add_column :qiwo_user_subscriptions, :sale_point, :integer
  end
end
