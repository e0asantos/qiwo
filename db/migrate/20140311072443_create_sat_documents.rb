class CreateSatDocuments < ActiveRecord::Migration
  def change
    create_table :sat_documents do |t|
      t.integer :id
      t.string :key_file
      t.string :cer_file
      t.integer :user_id
      t.integer :enterprise_id

      t.timestamps
    end
  end
end
