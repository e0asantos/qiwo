class AddCityToQiwoLogSearches < ActiveRecord::Migration
  def change
    add_column :qiwo_log_searches, :city, :string
  end
end
