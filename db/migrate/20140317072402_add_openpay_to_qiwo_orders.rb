class AddOpenpayToQiwoOrders < ActiveRecord::Migration
  def change
    add_column :qiwo_orders, :openpay, :string
  end
end
