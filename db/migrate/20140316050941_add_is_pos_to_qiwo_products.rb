class AddIsPosToQiwoProducts < ActiveRecord::Migration
  def change
    add_column :qiwo_products, :is_pos, :integer
  end
end
