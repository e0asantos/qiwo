class CreateRedpacks < ActiveRecord::Migration
  def change
    create_table :redpacks do |t|
      t.string :originated_from
      t.string :delivered_to

      t.timestamps
    end
  end
end
