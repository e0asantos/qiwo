class CreateQiwoCounters < ActiveRecord::Migration
  def change
    create_table :qiwo_counters do |t|
      t.integer :id
      t.integer :folio_factura
      t.integer :ordenes_pos
      t.integer :facturas_disponibles
      t.integer :qiwo_enterprise_id

      t.timestamps
    end
  end
end
