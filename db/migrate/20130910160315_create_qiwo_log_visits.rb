class CreateQiwoLogVisits < ActiveRecord::Migration
  def change
    create_table :qiwo_log_visits do |t|
      t.integer :id
      t.string :ip_user
      t.integer :user_id
      t.integer :facebook_id

      t.timestamps
    end
  end
end
