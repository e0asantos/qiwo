class CreateQiwoEnterprises < ActiveRecord::Migration
  def change
    create_table :qiwo_enterprises do |t|
      t.string :name
      t.string :location
      t.string :location_lat
      t.string :location_lon
      t.string :schedule
      t.datetime :founded_date
      t.string :telephones
      t.integer :point_sale
      t.string :paypal_email
      t.string :bank_clabe
      t.string :comercial_name

      t.timestamps
    end
  end
end
