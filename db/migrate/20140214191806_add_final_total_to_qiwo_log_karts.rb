class AddFinalTotalToQiwoLogKarts < ActiveRecord::Migration
  def change
    add_column :qiwo_log_karts, :final_total, :string
  end
end
