class CreateQiwoProducts < ActiveRecord::Migration
  def change
    create_table :qiwo_products do |t|
      t.integer :id
      t.string :name
      t.integer :location_store
      t.string :raw_price
      t.string :sale_price
      t.integer :stock_items
      t.integer :image_id
      t.string :description

      t.timestamps
    end
  end
end
