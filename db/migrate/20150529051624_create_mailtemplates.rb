class CreateMailtemplates < ActiveRecord::Migration
  def change
    create_table :mailtemplates do |t|
      t.string :content
      t.string :usage

      t.timestamps
    end
  end
end
