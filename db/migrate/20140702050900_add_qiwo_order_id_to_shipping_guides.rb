class AddQiwoOrderIdToShippingGuides < ActiveRecord::Migration
  def change
    add_column :shipping_guides, :qiwo_order_id, :integer
  end
end
