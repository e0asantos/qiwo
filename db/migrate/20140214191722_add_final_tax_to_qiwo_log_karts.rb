class AddFinalTaxToQiwoLogKarts < ActiveRecord::Migration
  def change
    add_column :qiwo_log_karts, :final_tax, :integer
  end
end
