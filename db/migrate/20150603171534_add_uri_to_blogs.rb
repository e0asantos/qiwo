class AddUriToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :uri, :string
  end
end
