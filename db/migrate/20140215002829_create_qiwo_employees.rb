class CreateQiwoEmployees < ActiveRecord::Migration
  def change
    create_table :qiwo_employees do |t|
      t.integer :employee_type
      t.string :employee_fb_id
      t.integer :employee_sale_point
      t.integer :employee_enterprise
      t.integer :employee_products_permission
      t.integer :employee_enterprise_permission
      t.integer :employee_clients_permission
      t.integer :employee_quotes_permission

      t.timestamps
    end
  end
end
