class AddUseEmployeesToQiwoSubscriptions < ActiveRecord::Migration
  def change
    add_column :qiwo_subscriptions, :use_employees, :integer
  end
end
