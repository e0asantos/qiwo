class CreateCarteraClientes < ActiveRecord::Migration
  def change
    create_table :cartera_clientes do |t|
      t.string :negocio
      t.string :contacto_email
      t.string :contacto_name
      t.string :telefono
      t.string :location_lat
      t.string :location_lon
      t.string :observaciones
      t.integer :visitado
      t.integer :is_client
      t.datetime :next_visit

      t.timestamps
    end
  end
end
