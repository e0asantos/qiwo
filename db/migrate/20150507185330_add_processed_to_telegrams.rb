class AddProcessedToTelegrams < ActiveRecord::Migration
  def change
    add_column :telegrams, :processed, :integer
  end
end
