class AddPdfToQiwoFacturas < ActiveRecord::Migration
  def change
    add_column :qiwo_facturas, :pdf, :text
  end
end
