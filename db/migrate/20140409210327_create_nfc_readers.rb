class CreateNfcReaders < ActiveRecord::Migration
  def change
    create_table :nfc_readers do |t|
      t.integer :id
      t.integer :nfc_type
      t.string :last_token
      t.string :static_token
      t.integer :blocked

      t.timestamps
    end
  end
end
