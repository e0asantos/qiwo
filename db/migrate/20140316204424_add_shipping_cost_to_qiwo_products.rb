class AddShippingCostToQiwoProducts < ActiveRecord::Migration
  def change
    add_column :qiwo_products, :shipping_cost, :decimal
  end
end
