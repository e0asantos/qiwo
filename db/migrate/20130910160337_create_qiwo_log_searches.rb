class CreateQiwoLogSearches < ActiveRecord::Migration
  def change
    create_table :qiwo_log_searches do |t|
      t.integer :id
      t.integer :ip_user
      t.integer :user_id
      t.integer :facebook_id
      t.string :search_term

      t.timestamps
    end
  end
end
