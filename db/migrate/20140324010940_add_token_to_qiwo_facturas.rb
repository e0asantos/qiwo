class AddTokenToQiwoFacturas < ActiveRecord::Migration
  def change
    add_column :qiwo_facturas, :token, :string
  end
end
