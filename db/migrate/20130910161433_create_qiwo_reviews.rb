class CreateQiwoReviews < ActiveRecord::Migration
  def change
    create_table :qiwo_reviews do |t|
      t.integer :id
      t.integer :store_id
      t.integer :product_id
      t.integer :user_id
      t.text :review

      t.timestamps
    end
  end
end
