class CreateQiwoCoupons < ActiveRecord::Migration
  def change
    create_table :qiwo_coupons do |t|
      t.integer :id
      t.string :coupon_name
      t.integer :porcentage
      t.datetime :start_date
      t.datetime :end_date
      t.integer :times_used

      t.timestamps
    end
  end
end
