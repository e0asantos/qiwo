class CreateQiwoRegimen < ActiveRecord::Migration
  def change
    create_table :qiwo_regimen do |t|
      t.integer :id
      t.string :name
      t.integer :type

      t.timestamps
    end
  end
end
