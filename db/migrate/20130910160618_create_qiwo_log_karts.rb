class CreateQiwoLogKarts < ActiveRecord::Migration
  def change
    create_table :qiwo_log_karts do |t|
      t.integer :id
      t.integer :ip_user
      t.integer :facebook_id
      t.integer :user_id
      t.integer :cotizacion_number
      t.integer :product_id
      t.integer :store_id
      t.integer :coupon_id

      t.timestamps
    end
  end
end
