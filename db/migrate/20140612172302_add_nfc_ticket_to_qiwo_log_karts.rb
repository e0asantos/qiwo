class AddNfcTicketToQiwoLogKarts < ActiveRecord::Migration
  def change
    add_column :qiwo_log_karts, :nfcTicket, :integer
  end
end
