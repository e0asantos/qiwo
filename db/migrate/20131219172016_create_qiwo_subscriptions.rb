class CreateQiwoSubscriptions < ActiveRecord::Migration
  def change
    create_table :qiwo_subscriptions do |t|
      t.integer :id
      t.string :subscription_name
      t.string :price
      t.integer :places_allowed
      t.integer :products_allowed
      t.string :days_active
      t.string :months_active

      t.timestamps
    end
  end
end
