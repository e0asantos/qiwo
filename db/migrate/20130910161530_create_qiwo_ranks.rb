class CreateQiwoRanks < ActiveRecord::Migration
  def change
    create_table :qiwo_ranks do |t|
      t.integer :id
      t.integer :store_id
      t.integer :product_id
      t.integer :user_id
      t.integer :rank

      t.timestamps
    end
  end
end
