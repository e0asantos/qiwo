class AddCfdiToQiwoFacturas < ActiveRecord::Migration
  def change
    add_column :qiwo_facturas, :cfdi, :text
  end
end
