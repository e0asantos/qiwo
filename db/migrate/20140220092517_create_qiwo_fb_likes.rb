class CreateQiwoFbLikes < ActiveRecord::Migration
  def change
    create_table :qiwo_fb_likes do |t|
      t.string :fb_object_id
      t.integer :qiwo_product_id
      t.integer :qiwo_enterprise_id

      t.timestamps
    end
  end
end
