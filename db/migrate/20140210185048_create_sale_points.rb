class CreateSalePoints < ActiveRecord::Migration
  def change
    create_table :sale_points do |t|
      t.integer :product_id
      t.string :client_ref
      t.integer :client_ref_num
      t.integer :order_ref

      t.timestamps
    end
  end
end
