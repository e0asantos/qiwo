class AddRollbackopToQiwoFacturas < ActiveRecord::Migration
  def change
    add_column :qiwo_facturas, :rollbackop, :text
  end
end
