class AddTaxOptionToQiwoOrders < ActiveRecord::Migration
  def change
    add_column :qiwo_orders, :tax_option, :integer
  end
end
