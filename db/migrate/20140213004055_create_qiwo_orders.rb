class CreateQiwoOrders < ActiveRecord::Migration
  def change
    create_table :qiwo_orders do |t|
      t.integer :owner_user_id
      t.integer :salesforce_user_id
      t.integer :order_state
      t.integer :factura_id
      t.integer :qiwo_enterprise_id

      t.timestamps
    end
  end
end
