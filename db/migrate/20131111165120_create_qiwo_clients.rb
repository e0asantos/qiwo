class CreateQiwoClients < ActiveRecord::Migration
  def change
    create_table :qiwo_clients do |t|
      t.integer :id
      t.integer :user_id
      t.integer :cotizacion_number

      t.timestamps
    end
  end
end
