class AddTechIdToQiwoMeasureUnits < ActiveRecord::Migration
  def change
    add_column :qiwo_measure_units, :tech_id, :string
  end
end
