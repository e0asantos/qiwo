class AddCerFileFileNameToSatDocuments < ActiveRecord::Migration
  def change
    add_column :sat_documents, :cer_file_file_name, :string
  end
end
