class CreateQiwoTags < ActiveRecord::Migration
  def change
    create_table :qiwo_tags do |t|
      t.integer :id
      t.string :tag_name

      t.timestamps
    end
  end
end
