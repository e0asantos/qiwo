class AddSalePointToQiwoSubscriptions < ActiveRecord::Migration
  def change
    add_column :qiwo_subscriptions, :sale_point, :integer
  end
end
