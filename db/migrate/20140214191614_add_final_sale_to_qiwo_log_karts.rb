class AddFinalSaleToQiwoLogKarts < ActiveRecord::Migration
  def change
    add_column :qiwo_log_karts, :final_sale, :string
  end
end
