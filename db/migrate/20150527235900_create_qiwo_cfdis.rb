class CreateQiwoCfdis < ActiveRecord::Migration
  def change
    create_table :qiwo_cfdis do |t|
      t.integer :originated_from
      t.integer :originated_to

      t.timestamps
    end
  end
end
