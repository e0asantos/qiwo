class AddFinalGainToQiwoLogKart < ActiveRecord::Migration
  def change
    add_column :qiwo_log_karts, :final_gain, :float
  end
end
