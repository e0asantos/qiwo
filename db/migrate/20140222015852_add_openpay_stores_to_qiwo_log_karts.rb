class AddOpenpayStoresToQiwoLogKarts < ActiveRecord::Migration
  def change
    add_column :qiwo_log_karts, :openpay_stores, :string
  end
end
