class AddCityToQiwoLogVisits < ActiveRecord::Migration
  def change
    add_column :qiwo_log_visits, :city, :string
  end
end
