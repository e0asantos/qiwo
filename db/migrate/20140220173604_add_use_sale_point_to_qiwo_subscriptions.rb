class AddUseSalePointToQiwoSubscriptions < ActiveRecord::Migration
  def change
    add_column :qiwo_subscriptions, :use_sale_point, :integer
  end
end
