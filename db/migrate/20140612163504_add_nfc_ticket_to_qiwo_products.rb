class AddNfcTicketToQiwoProducts < ActiveRecord::Migration
  def change
    add_column :qiwo_products, :nfcTicket, :integer
  end
end
