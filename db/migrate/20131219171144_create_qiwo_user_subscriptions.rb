class CreateQiwoUserSubscriptions < ActiveRecord::Migration
  def change
    create_table :qiwo_user_subscriptions do |t|
      t.integer :id
      t.integer :user_id
      t.integer :subscription_type
      t.date :ends_on
      t.string :payment_gateway
      t.string :amount_paid
      t.string :currency

      t.timestamps
    end
  end
end
