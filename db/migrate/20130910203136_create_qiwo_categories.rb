class CreateQiwoCategories < ActiveRecord::Migration
  def change
    create_table :qiwo_categories do |t|
      t.integer :id
      t.integer :category_name
      t.integer :parent_category

      t.timestamps
    end
  end
end
