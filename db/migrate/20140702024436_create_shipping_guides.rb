class CreateShippingGuides < ActiveRecord::Migration
  def change
    create_table :shipping_guides do |t|
      t.integer :id
      t.integer :id_from
      t.integer :id_to
      t.string :real_guide
      t.string :status_guide
      t.string :label_guide

      t.timestamps
    end
  end
end
