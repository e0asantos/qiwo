class AddApitokenToSatDocuments < ActiveRecord::Migration
  def change
    add_column :sat_documents, :apitoken, :string
  end
end
