class AddTaxToQiwoProducts < ActiveRecord::Migration
  def change
    add_column :qiwo_products, :tax, :integer
  end
end
