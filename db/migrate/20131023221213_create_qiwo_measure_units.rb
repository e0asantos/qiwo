class CreateQiwoMeasureUnits < ActiveRecord::Migration
  def change
    create_table :qiwo_measure_units do |t|
      t.integer :id
      t.string :label_es

      t.timestamps
    end
  end
end
