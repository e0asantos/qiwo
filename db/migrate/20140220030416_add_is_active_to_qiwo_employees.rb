class AddIsActiveToQiwoEmployees < ActiveRecord::Migration
  def change
    add_column :qiwo_employees, :is_active, :integer
  end
end
