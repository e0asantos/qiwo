class AddAttachmentImageUrlToQiwoImages < ActiveRecord::Migration
  def self.up
    change_table :qiwo_images do |t|
      t.attachment :image_url
    end
  end

  def self.down
    drop_attached_file :qiwo_images, :image_url
  end
end
