class CreateQiwoItemTags < ActiveRecord::Migration
  def change
    create_table :qiwo_item_tags do |t|
      t.integer :id
      t.integer :store_id
      t.integer :product_id
      t.integer :tag_id

      t.timestamps
    end
  end
end
