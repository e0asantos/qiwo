class AddCountryToQiwoLogVisits < ActiveRecord::Migration
  def change
    add_column :qiwo_log_visits, :country, :string
  end
end
