class AddAccessProvidersToQiwoSubscriptions < ActiveRecord::Migration
  def change
    add_column :qiwo_subscriptions, :access_providers, :integer
  end
end
