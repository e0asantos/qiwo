class CreateQiwoImages < ActiveRecord::Migration
  def change
    create_table :qiwo_images do |t|
      t.integer :id
      t.string :image_url
      t.integer :fron_page
      t.integer :user_id
      t.integer :allowed

      t.timestamps
    end
  end
end
