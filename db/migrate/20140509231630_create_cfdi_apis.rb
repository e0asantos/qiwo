class CreateCfdiApis < ActiveRecord::Migration
  def change
    create_table :cfdi_apis do |t|
      t.integer :id
      t.string :cfdi_status
      t.string :incoming_params
      t.string :found_error
      t.string :ip_address

      t.timestamps
    end
  end
end
