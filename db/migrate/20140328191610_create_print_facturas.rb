class CreatePrintFacturas < ActiveRecord::Migration
  def change
    create_table :print_facturas do |t|
      t.integer :id
      t.string :factura

      t.timestamps
    end
  end
end
