class CreateQiwoProfiles < ActiveRecord::Migration
  def change
    create_table :qiwo_profiles do |t|
      t.integer :id

      t.timestamps
    end
  end
end
