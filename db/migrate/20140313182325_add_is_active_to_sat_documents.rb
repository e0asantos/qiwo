class AddIsActiveToSatDocuments < ActiveRecord::Migration
  def change
    add_column :sat_documents, :is_active, :integer
  end
end
