class CreateFacturacions < ActiveRecord::Migration
  def change
    create_table :facturacions do |t|
      t.integer :id
      t.integer :sys_user_id

      t.timestamps
    end
  end
end
