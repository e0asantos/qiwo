class CreateNfcRequests < ActiveRecord::Migration
  def change
    create_table :nfc_requests do |t|
      t.integer :id
      t.integer :qiwo_product_id
      t.integer :qiwo_enterprise_id
      t.integer :sys_user_id

      t.timestamps
    end
  end
end
