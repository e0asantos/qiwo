class AddQiwoLogKartIdToShippingGuides < ActiveRecord::Migration
  def change
    add_column :shipping_guides, :qiwo_log_kart_id, :integer
  end
end
