# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20201105174539) do

  create_table "accounts", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "anet_commands", :force => true do |t|
    t.integer  "arduino_id"
    t.string   "command"
    t.integer  "is_only_question"
    t.string   "command_data"
    t.integer  "is_ondemand"
    t.integer  "invoke_every"
    t.time     "on_time"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "anet_devices", :force => true do |t|
    t.text     "description"
    t.text     "location"
    t.text     "object"
    t.text     "token"
    t.text     "secret_token"
    t.integer  "id_owner"
    t.integer  "is_public"
    t.integer  "likes",          :default => 0
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "notify_hangout"
    t.integer  "notify_fb"
    t.integer  "notify_email"
    t.integer  "notify_twitter"
    t.integer  "notify_fbinbox"
    t.string   "mip"
    t.integer  "cliente"
  end

  create_table "anet_logs", :force => true do |t|
    t.integer  "arduino_id",      :null => false
    t.integer  "connection_type"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "mip"
  end

  add_index "anet_logs", ["arduino_id"], :name => "anet_devices_id"

  create_table "anet_values", :force => true do |t|
    t.integer  "arduino_id"
    t.integer  "command_id"
    t.string   "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "api_interfaces", :force => true do |t|
    t.string  "name",           :limit => 45
    t.string  "command",        :limit => 45
    t.string  "gema"
    t.string  "url"
    t.boolean "removed",                      :default => false, :null => false
    t.text    "token"
    t.text    "token_secret"
    t.integer "visible"
    t.text    "icon"
    t.text    "pattern"
    t.integer "usersNeedToken"
  end

  create_table "api_methods", :force => true do |t|
    t.integer "api_interface",                                  :null => false
    t.string  "name",          :limit => 45
    t.string  "params",        :limit => 45
    t.string  "oral",          :limit => 30
    t.string  "oralSpanish",   :limit => 30
    t.integer "ischat",                      :default => 0
    t.boolean "removed",                     :default => false, :null => false
  end

  add_index "api_methods", ["api_interface"], :name => "fk_api_method_1_idx"

  create_table "api_metric_method", :force => true do |t|
    t.integer "sys_user",                      :null => false
    t.integer "api_method",                    :null => false
    t.boolean "removed",    :default => false, :null => false
  end

  add_index "api_metric_method", ["api_method"], :name => "fk_api_metric_method_2_idx"
  add_index "api_metric_method", ["sys_user"], :name => "fk_api_metric_method_1_idx"

  create_table "api_metric_words", :force => true do |t|
    t.integer "sys_user",                    :null => false
    t.integer "api_word",                    :null => false
    t.boolean "removed",  :default => false, :null => false
  end

  add_index "api_metric_words", ["sys_user"], :name => "fk_sys_user"

  create_table "api_token_user_interfaces", :force => true do |t|
    t.integer "sys_user",                         :null => false
    t.integer "api_interface",                    :null => false
    t.string  "token"
    t.boolean "removed",       :default => false, :null => false
    t.text    "token_secret"
    t.integer "expires"
  end

  add_index "api_token_user_interfaces", ["api_interface"], :name => "fk_api_token_user_interface_2_idx"
  add_index "api_token_user_interfaces", ["sys_user"], :name => "fk_api_token_user_interface_1_idx"

  create_table "api_words", :force => true do |t|
    t.string  "word",    :limit => 25, :default => "",    :null => false
    t.boolean "removed",               :default => false, :null => false
  end

  create_table "authentications", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "blogs", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.string   "authors"
    t.integer  "likes"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "is_published"
    t.string   "uri"
  end

  create_table "cartera_clientes", :force => true do |t|
    t.string   "negocio"
    t.string   "contacto_email"
    t.string   "contacto_name"
    t.string   "telefono"
    t.string   "location_lat"
    t.string   "location_lon"
    t.string   "observaciones"
    t.integer  "visitado"
    t.integer  "is_client"
    t.datetime "next_visit"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "cfdi_apis", :force => true do |t|
    t.string   "cfdi_status"
    t.string   "incoming_params"
    t.string   "found_error"
    t.string   "ip_address"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "earthquakes", :force => true do |t|
    t.string   "latitude"
    t.string   "longitude"
    t.string   "location"
    t.string   "reportedTime"
    t.string   "magnitude"
    t.text     "rawData"
    t.string   "gateway",      :limit => 11
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "facturacions", :force => true do |t|
    t.integer  "sys_user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "feed_readers", :force => true do |t|
    t.string   "url_source"
    t.string   "last_notification"
    t.string   "titulo"
    t.string   "descripcion"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "is_active"
    t.integer  "event_typer"
  end

  create_table "geo_settings", :force => true do |t|
    t.integer  "sys_user_id"
    t.integer  "earthquakes"
    t.integer  "volcano"
    t.integer  "water"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "add_distancia"
  end

  add_index "geo_settings", ["sys_user_id"], :name => "sys_user_id"

  create_table "glasses", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "likes", :force => true do |t|
    t.integer  "command_id"
    t.integer  "device_id"
    t.integer  "user_liked"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "mailtemplates", :force => true do |t|
    t.string   "content"
    t.string   "usage"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "nfc_readers", :force => true do |t|
    t.integer  "nfc_type"
    t.string   "last_token",   :default => "c2FsbW85MQ=="
    t.string   "static_token"
    t.integer  "blocked",      :default => 0
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "user_id"
  end

  create_table "nfc_requests", :force => true do |t|
    t.integer  "qiwo_product_id"
    t.integer  "qiwo_enterprise_id"
    t.integer  "sys_user_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "payments", :force => true do |t|
    t.integer  "amount"
    t.string   "token"
    t.string   "identifier"
    t.string   "payer_id"
    t.boolean  "recurring"
    t.boolean  "digital"
    t.boolean  "popup"
    t.boolean  "completed"
    t.boolean  "canceled"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "personal_accounts", :force => true do |t|
    t.string   "note"
    t.string   "sound_file"
    t.string   "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "print_facturas", :force => true do |t|
    t.string   "factura"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "qiwo_categories", :force => true do |t|
    t.string   "category_name",   :limit => 50
    t.integer  "parent_category"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "qiwo_cfdis", :force => true do |t|
    t.integer  "originated_from"
    t.integer  "originated_to"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "qiwo_clients", :force => true do |t|
    t.integer  "user_id"
    t.integer  "cotizacion_number"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "qiwo_counters", :force => true do |t|
    t.integer  "folio_factura",        :default => 1
    t.integer  "ordenes_pos",          :default => 1
    t.integer  "facturas_disponibles", :default => 0
    t.integer  "qiwo_enterprise_id"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

  create_table "qiwo_coupons", :force => true do |t|
    t.string   "coupon_name"
    t.integer  "porcentage"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "times_used"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "qiwo_employees", :force => true do |t|
    t.integer  "employee_type"
    t.string   "employee_fb_id"
    t.integer  "employee_sale_point"
    t.integer  "employee_enterprise"
    t.integer  "employee_products_permission"
    t.integer  "employee_enterprise_permission"
    t.integer  "employee_clients_permission"
    t.integer  "employee_quotes_permission"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.integer  "is_active",                      :default => 1
  end

  create_table "qiwo_enterprises", :force => true do |t|
    t.integer  "user_owner"
    t.string   "name"
    t.string   "location"
    t.string   "location_lat"
    t.string   "location_lon"
    t.string   "schedule"
    t.datetime "founded_date"
    t.string   "telephones"
    t.integer  "point_sale"
    t.string   "paypal_email"
    t.string   "email_sales"
    t.string   "email_buy"
    t.string   "bank_clabe"
    t.string   "comercial_name"
    t.string   "rfc",            :limit => 50
    t.text     "description"
    t.string   "unique_uri"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "no_certificado"
    t.string   "calle"
    t.string   "numexterior"
    t.string   "numinterior"
    t.string   "colonia"
    t.string   "localidad"
    t.string   "municipio"
    t.string   "estado"
    t.string   "pais"
    t.string   "mycp"
    t.integer  "regimen"
    t.integer  "is_shipping"
  end

  create_table "qiwo_facturas", :force => true do |t|
    t.string   "folio"
    t.integer  "status"
    t.integer  "qiwo_log_kart_id"
    t.integer  "qiwo_order_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.text     "cfdi"
    t.text     "pdf"
    t.string   "token"
    t.string   "sat_folio"
    t.integer  "store_from"
    t.integer  "store_to"
    t.text     "rollbackop"
  end

  add_index "qiwo_facturas", ["qiwo_log_kart_id"], :name => "compra_unica"

  create_table "qiwo_fb_likes", :force => true do |t|
    t.string   "fb_object_id"
    t.integer  "qiwo_product_id"
    t.integer  "qiwo_enterprise_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "qiwo_images", :force => true do |t|
    t.string   "image_url"
    t.integer  "fron_page"
    t.integer  "user_id"
    t.integer  "allowed",                :default => 0
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.string   "image_url_file_name"
    t.string   "image_url_content_type"
    t.integer  "image_url_file_size"
    t.datetime "image_url_updated_at"
  end

  create_table "qiwo_item_tags", :force => true do |t|
    t.integer  "store_id"
    t.integer  "product_id"
    t.integer  "tag_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "qiwo_log_karts", :force => true do |t|
    t.string   "ip_user",           :limit => 20
    t.integer  "facebook_id"
    t.integer  "user_id"
    t.integer  "cotizacion_number"
    t.integer  "qiwo_product_id"
    t.integer  "store_id"
    t.integer  "coupon_id"
    t.integer  "kart_state"
    t.integer  "qiwo_order_id"
    t.integer  "quantity"
    t.string   "paypal"
    t.string   "conekta"
    t.string   "oxxo"
    t.string   "banorte"
    t.integer  "shipping_cost",                                                  :default => 0
    t.string   "shipping_guide",                                                 :default => "SIN GUIA"
    t.datetime "created_at",                                                                             :null => false
    t.datetime "updated_at",                                                                             :null => false
    t.float    "final_sale",                                                     :default => 0.0
    t.float    "final_tax",                                                      :default => 0.0
    t.float    "final_total",                                                    :default => 0.0
    t.float    "final_gain",                                                     :default => 0.0,        :null => false
    t.string   "openpay_stores"
    t.decimal  "discount",                        :precision => 10, :scale => 0, :default => 0
    t.string   "inreference"
    t.integer  "nfcTicket"
  end

  create_table "qiwo_log_searches", :force => true do |t|
    t.string   "ip_user",     :limit => 20
    t.integer  "user_id"
    t.integer  "facebook_id"
    t.string   "search_term"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.string   "country"
    t.string   "city"
  end

  create_table "qiwo_log_visits", :force => true do |t|
    t.string   "ip_user"
    t.integer  "user_id"
    t.integer  "facebook_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "country"
    t.string   "city"
  end

  create_table "qiwo_measure_units", :force => true do |t|
    t.string   "label_es",   :limit => 30
    t.datetime "updated_at"
    t.datetime "created_at"
    t.string   "tech_id"
  end

  create_table "qiwo_orders", :force => true do |t|
    t.integer  "owner_user_id"
    t.integer  "salesforce_user_id"
    t.integer  "order_state",        :default => 1, :null => false
    t.integer  "factura_id"
    t.integer  "qiwo_enterprise_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "tip_option",         :default => 0
    t.integer  "tax_option",         :default => 0
    t.string   "conekta"
    t.string   "openpay"
    t.integer  "folio",              :default => 1
    t.string   "inreference"
  end

  create_table "qiwo_product_categories", :force => true do |t|
    t.integer  "category_id"
    t.integer  "product_id"
    t.integer  "store_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "qiwo_products", :force => true do |t|
    t.string   "name"
    t.integer  "location_store"
    t.float    "raw_price"
    t.float    "sale_price"
    t.integer  "stock_items"
    t.string   "image_id",              :limit => 200
    t.string   "image_id_file_name",    :limit => 250
    t.text     "description"
    t.integer  "business_source"
    t.string   "sku",                   :limit => 50
    t.string   "warehouse",             :limit => 250,                                :default => ""
    t.string   "measure_units",         :limit => 20
    t.integer  "measure_deep"
    t.integer  "measure_width"
    t.integer  "measure_height"
    t.text     "keywords"
    t.float    "major_price"
    t.integer  "major_quantity"
    t.integer  "weight_number"
    t.datetime "ends_on"
    t.integer  "search_enabled",                                                      :default => 1
    t.integer  "discount_amount",                                                     :default => 0
    t.datetime "created_at",                                                                          :null => false
    t.datetime "updated_at",                                                                          :null => false
    t.integer  "tax",                                                                 :default => 1
    t.integer  "is_pos",                                                              :default => 0
    t.decimal  "shipping_cost",                        :precision => 10, :scale => 0
    t.string   "image_one"
    t.string   "image_two"
    t.string   "image_three"
    t.string   "image_one_file_name"
    t.string   "image_two_file_name"
    t.string   "image_three_file_name"
    t.integer  "nfcTicket"
  end

  create_table "qiwo_profiles", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "qiwo_ranks", :force => true do |t|
    t.integer  "store_id"
    t.integer  "product_id"
    t.integer  "user_id"
    t.integer  "rank"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "qiwo_regimen", :force => true do |t|
    t.string   "name"
    t.integer  "typer"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "num_impuestos"
  end

  create_table "qiwo_reviews", :force => true do |t|
    t.integer  "store_id"
    t.integer  "sale_id"
    t.integer  "product_id"
    t.integer  "user_id"
    t.text     "review"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "qiwo_subscriptions", :force => true do |t|
    t.string   "subscription_name"
    t.float    "price",             :default => 0.0
    t.text     "description"
    t.integer  "places_allowed"
    t.integer  "products_allowed"
    t.string   "days_active"
    t.string   "months_active"
    t.integer  "touched"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "sale_point",        :default => 0
    t.integer  "access_providers"
    t.integer  "use_employees"
    t.integer  "use_sale_point"
  end

  create_table "qiwo_tags", :force => true do |t|
    t.string   "tag_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "qiwo_tools", :force => true do |t|
    t.integer  "user_id"
    t.string   "toolname"
    t.string   "apikey"
    t.string   "signature"
    t.string   "apisecret"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "store_id"
  end

  create_table "qiwo_user_subscriptions", :force => true do |t|
    t.integer  "user_id"
    t.integer  "subscription_type"
    t.datetime "ends_on"
    t.string   "payment_gateway"
    t.string   "amount_paid"
    t.string   "currency"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.string   "payment_ref"
    t.integer  "touched",           :default => 0
    t.integer  "sale_point"
  end

  create_table "qlogs", :force => true do |t|
    t.string   "modulo"
    t.string   "id_ref"
    t.string   "id_status"
    t.string   "id_value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "redpacks", :force => true do |t|
    t.string   "originated_from"
    t.string   "delivered_to"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "sale_points", :force => true do |t|
    t.integer  "product_id"
    t.string   "client_ref"
    t.integer  "client_ref_num"
    t.integer  "order_ref"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "sat_documents", :force => true do |t|
    t.string   "key_file"
    t.string   "cer_file"
    t.integer  "user_id"
    t.integer  "qiwo_enterprise_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.string   "cer_file_file_name"
    t.string   "key_file_file_name"
    t.integer  "is_active",          :default => 1
    t.string   "sat_key"
    t.string   "apitoken"
  end

  add_index "sat_documents", ["qiwo_enterprise_id"], :name => "mke"

  create_table "searches", :force => true do |t|
    t.string   "seed"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "shipping_guides", :force => true do |t|
    t.integer  "id_from"
    t.integer  "id_to"
    t.string   "real_guide"
    t.string   "status_guide"
    t.text     "label_guide"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "gtype"
    t.integer  "qiwo_log_kart_id"
    t.integer  "qiwo_order_id"
  end

  create_table "smartwatches", :force => true do |t|
    t.integer  "watch_type"
    t.string   "token"
    t.integer  "sys_user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "subscriptions", :force => true do |t|
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "summaries", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "texto"
    t.integer  "is_active"
  end

  create_table "sys_access_type", :force => true do |t|
    t.string  "type",    :limit => 15
    t.boolean "removed",               :default => false
  end

  create_table "sys_event", :force => true do |t|
    t.string  "name",    :limit => 45
    t.boolean "removed",               :default => false, :null => false
  end

  create_table "sys_ignored_words", :force => true do |t|
    t.string  "word",    :limit => 45
    t.boolean "removed",               :default => false, :null => false
  end

  create_table "sys_logs", :force => true do |t|
    t.integer "sys_user",                                        :null => false
    t.integer "sys_event",                                       :null => false
    t.date    "date_event"
    t.time    "time_event"
    t.string  "before_event", :limit => 6144
    t.string  "after_event",  :limit => 6144
    t.boolean "removed",                      :default => false, :null => false
  end

  add_index "sys_logs", ["sys_event"], :name => "fk_sys_log_2_idx"
  add_index "sys_logs", ["sys_user"], :name => "fk_sys_log_1_idx"

  create_table "sys_messages", :force => true do |t|
    t.text    "message"
    t.string  "lang",       :limit => 30
    t.integer "id_message"
  end

  create_table "sys_metric_accesses", :force => true do |t|
    t.string   "phoneNumber",     :limit => 30
    t.string   "voipAlias",       :limit => 30
    t.integer  "sys_access_type",                                   :null => false
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "closeReason",     :limit => 200
    t.boolean  "removed",                        :default => false
  end

  add_index "sys_metric_accesses", ["sys_access_type"], :name => "fk_sys_metric_access_2_idx"

  create_table "sys_notes", :force => true do |t|
    t.integer "sys_user",                                        :null => false
    t.string  "phrase",        :limit => 512, :default => "",    :null => false
    t.boolean "removed",                      :default => false, :null => false
    t.integer "api_interface",                :default => 1,     :null => false
  end

  add_index "sys_notes", ["api_interface"], :name => "fk_sys_note2_idx"
  add_index "sys_notes", ["sys_user"], :name => "fk_sys_note1"

  create_table "sys_notes_logs", :id => false, :force => true do |t|
    t.integer "id",                                    :null => false
    t.integer "sys_notes",                             :null => false
    t.integer "sys_log",                               :null => false
    t.integer "removed",   :limit => 1, :default => 0, :null => false
  end

  add_index "sys_notes_logs", ["sys_log"], :name => "fk_sys_notes_log_2_idx"
  add_index "sys_notes_logs", ["sys_notes"], :name => "fk_sys_notes_log_1_idx"

  create_table "sys_option", :force => true do |t|
    t.string  "name",    :limit => 45
    t.string  "url",     :limit => 45
    t.boolean "removed",               :default => false, :null => false
  end

  create_table "sys_phone_types", :force => true do |t|
    t.string  "ptype",       :limit => 35, :default => "",    :null => false
    t.boolean "removed",                   :default => false, :null => false
    t.text    "description"
  end

  create_table "sys_phrases", :force => true do |t|
    t.string   "phrase",             :limit => 300
    t.string   "phone",              :limit => 13
    t.string   "soundfile",          :limit => 50
    t.integer  "interface_executed",                :default => 4
    t.text     "translate"
    t.string   "callid",             :limit => 100
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sys_region_langs", :force => true do |t|
    t.string "country_code", :limit => 10
    t.string "lang",         :limit => 10
  end

  create_table "sys_regions", :force => true do |t|
    t.integer "country_code"
    t.integer "area_code"
    t.string  "region_name",  :limit => 100
    t.string  "access_point", :limit => 100
  end

  create_table "sys_rol", :force => true do |t|
    t.string  "name",    :limit => 50, :default => "",    :null => false
    t.boolean "removed",               :default => false, :null => false
  end

  create_table "sys_rol_interface", :force => true do |t|
    t.integer "sys_rol",                          :null => false
    t.integer "api_interface",                    :null => false
    t.boolean "removed",       :default => false, :null => false
  end

  add_index "sys_rol_interface", ["api_interface"], :name => "fk_sys_rol_interface_2_idx"
  add_index "sys_rol_interface", ["sys_rol"], :name => "fk_sys_rol_interface_1_idx"

  create_table "sys_rol_option", :force => true do |t|
    t.integer "sys_rol",                       :null => false
    t.integer "sys_option",                    :null => false
    t.boolean "removed",    :default => false, :null => false
  end

  add_index "sys_rol_option", ["sys_option"], :name => "fk_sys_rol_option_2_idx"
  add_index "sys_rol_option", ["sys_rol"], :name => "fk_sys_rol_option_1_idx"

  create_table "sys_start_commands", :force => true do |t|
    t.text "startCommand"
  end

  create_table "sys_user_audio", :force => true do |t|
    t.integer "sys_user",                                   :null => false
    t.string  "url",      :limit => 128
    t.boolean "removed",                 :default => false, :null => false
  end

  add_index "sys_user_audio", ["sys_user"], :name => "fk_sys_user_audio_1_idx"

  create_table "sys_user_phones", :force => true do |t|
    t.integer "sys_user",                                        :null => false
    t.integer "sys_phone_type",                                  :null => false
    t.string  "number",         :limit => 30
    t.boolean "removed",                      :default => false, :null => false
  end

  add_index "sys_user_phones", ["sys_phone_type"], :name => "fk_sys_phone_type"
  add_index "sys_user_phones", ["sys_user"], :name => "fk_sys_user_phone"

  create_table "sys_users", :force => true do |t|
    t.integer  "sys_rol",                      :default => 2,     :null => false
    t.string   "name",          :limit => 20
    t.string   "last_name",     :limit => 50
    t.string   "user",          :limit => 20
    t.string   "password",      :limit => 20
    t.boolean  "removed",                      :default => false, :null => false
    t.string   "user_id",       :limit => 220
    t.string   "email",         :limit => 220
    t.text     "avatar"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "lang",          :limit => 11,  :default => "es"
    t.integer  "is_technical",                 :default => 0
    t.string   "gps_location"
    t.string   "lat"
    t.string   "lon"
    t.string   "blogfrontpage"
    t.string   "blogname"
  end

  add_index "sys_users", ["id"], :name => "fk_geo_settings"
  add_index "sys_users", ["sys_rol"], :name => "fk_sys_rol"

  create_table "telegrams", :force => true do |t|
    t.string   "phone"
    t.text     "telegram_img"
    t.string   "auth_code"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.text     "alias"
    t.integer  "processed"
  end

  create_table "users", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "password"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "email"
    t.string   "lastname"
    t.string   "token"
    t.string   "phonenumber"
    t.string   "avatar"
  end

  create_table "yos", :force => true do |t|
    t.integer  "userid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
