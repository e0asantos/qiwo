Qiwo::Application.routes.draw do
  
  
  
  get '/articulo/:nombre', to: 'blogs#show'
  get '/portada/:frontpage', to: 'blogs#frontpage'
  resources :blogs


  resources :mailtemplates


  resources :qiwo_cfdis

  match '/redpacks/allowGuide', :controller => 'redpacks', :action => 'allowGuide'
  resources :redpacks


  resources :qiwo_profiles


  resources :shipping_guides


  resources :nfc_requests


  resources :cfdi_apis

  match '/facturacions/populateRollBack', :controller => 'facturacions', :action => 'populateRollBack'
  match '/facturacions/execRollBack', :controller => 'facturacions', :action => 'execRollBack'
  resources :facturacions


  match '/telegrams/validateTelegram', :controller => 'telegrams', :action => 'validateTelegram'
  match '/telegrams/login', :controller => 'telegrams', :action => 'emailLogin'
  match '/telegrams/ping', :controller => 'telegrams', :action => 'ping'
  match '/telegrams/telegram_actions', :controller => 'telegrams', :action => 'telegram_actions'
  resources :telegrams


  resources :nfc_readers


  resources :print_facturas


  resources :qiwo_counters


  resources :sat_documents


  match '/qiwo_employees/asociadoBuscar', :controller => 'qiwo_employees', :action => 'lookInFacebookEmployees'
  match '/qiwo_employees/misEmpleados', :controller => 'qiwo_employees', :action => 'listEmployees'
  match '/qiwo_employees/contratarEmpleado', :controller => 'qiwo_employees', :action => 'assignEmployee'
  match '/qiwo_employees/despedirEmpleado', :controller => 'qiwo_employees', :action => 'fireEmployee'
  resources :qiwo_employees


  resources :qiwo_orders


  match '/sale_points/puntoDeVentaProducto', :controller => 'sale_points', :action => 'searchByProduct'
  match '/sale_points/puntoDeVentaSKU', :controller => 'sale_points', :action => 'searchBySKU'
  match '/sale_points/puntoDeVentaNuevaOrden', :controller => 'sale_points', :action => 'createNewOrder'
  match '/sale_points/puntoDeVentaItems', :controller => 'sale_points', :action => 'getOrderItems'
  match '/sale_points/puntoDeVentaAlterItems', :controller => 'sale_points', :action => 'addProductToOrder'
  match '/sale_points/puntoDeVentaAlterGetTax', :controller => 'sale_points', :action => 'getTaxAddons'
  match '/sale_points/puntoDeVentaAlterSetTax', :controller => 'sale_points', :action => 'setTaxAddons'
  match '/sale_points/puntoDeVentaCloseOrder', :controller => 'sale_points', :action => 'closeOrder'
  match '/sale_points/puntoDeVentaValidar', :controller => 'sale_points', :action => 'validateOrderPayment'
  match '/sale_points/puntoDeVentaCerrarCuenta', :controller => 'sale_points', :action => 'closeWithNoInvoice'
  match '/sale_points/fd_card', :controller => 'sale_points', :action => 'first_data_card'
  resources :sale_points


  resources :cartera_clientes


  resources :payments


  resources :qiwo_subscriptions

  match '/qiwo_user_subscriptions/paySubscription', :controller => 'qiwo_user_subscriptions', :action => 'paySubscription'
  match '/qiwo_user_subscriptions/incomingFromPaypal', :controller => 'qiwo_user_subscriptions', :action => 'incomingFromPaypal'
  match '/gateway/incomingFromConekta', :controller => 'qiwo_user_subscriptions', :action => 'incomingFromConekta'
  match '/gateway/incomingFromOpenPay', :controller => 'qiwo_user_subscriptions', :action => 'incomingFromOpenPay'
  resources :qiwo_user_subscriptions

  match '/qiwo_tools/updateMerchantTools', :controller => 'qiwo_tools', :action => 'updateMerchantTools'
  resources :qiwo_tools

  


  resources :qiwo_measure_units

  match '/yo/sendMessageToAll', :controller => 'yo', :action => 'sendMessageToAll'
  match '/yo/getCalendar', :controller => 'yo', :action => 'getCalendar'
  match '/yo/qiwoLogOut', :controller => 'yo', :action => 'qiwoLogOut'
  match '/yo/createGuides', :controller => 'yo', :action => 'createGuides'
  match '/yo/reassignPlan', :controller => 'yo', :action => 'reassignPlan'
  match '/yo/reassignCFDI', :controller => 'yo', :action => 'reassignCFDI'
  match '/yo/qtinbo', :controller => 'yo', :action => 'qtinbo'
#  resources :yo
  get '/yo', to: redirect('/dashboard')
  get 'dashboard', to: 'yo#index'


  resources :qiwo_categories

  match '/suscribir/me', :controller => 'welcome', :action => 'suscribir'
  root :to => 'welcome#index'

  resources :qiwo_item_tags


  resources :qiwo_tags


  resources :qiwo_product_categories


  resources :qiwo_coupons


  resources :qiwo_ranks


  resources :qiwo_reviews

  match '/qiwo_products/applyDiscount', :controller => 'qiwo_products', :action => 'applyDiscount'
  match '/qiwo_products/compartir', :controller => 'qiwo_products', :action => 'shareProduct'
  match '/qiwo_products/getProductsAsToken', :controller => 'qiwo_products', :action => 'getProductsAsToken'
  resources :qiwo_products


  resources :qiwo_images


  match '/qiwo_log_karts/oxxoPayment', :controller => 'qiwo_log_karts', :action => 'oxxoPayment'
  match '/qiwo_log_karts/banortePayment', :controller => 'qiwo_log_karts', :action => 'banortePayment'
  match '/qiwo_log_karts/thirdPartySale', :controller => 'qiwo_log_karts', :action => 'thirdPartySale'
  match '/qiwo_log_karts/basicPayments', :controller => 'qiwo_log_karts', :action => 'pagosPorOpenPay'
  match '/qiwo_log_karts/descargarPDF', :controller => 'qiwo_log_karts', :action => 'downloadPDF'
  match '/qiwo_log_karts/descargarXML', :controller => 'qiwo_log_karts', :action => 'downloadXML'
  resources :qiwo_log_karts


  resources :qiwo_log_searches 


  resources :qiwo_log_visits

  match '/h2h/direccionFacturacion', :controller => 'qiwo_enterprises', :action => 'getFiscalAddress'
  resources :qiwo_enterprises

  get '/tienda/:id', to: 'searches#show'
  resources :searches do 
      post :sendToInbox, on: :collection 
      # or you may prefer to call this route on: :member
  end

  match '/qiwo_clients/cancelarFactura', :controller => 'qiwo_clients', :action => 'cancelFactura'
  match '/qiwo_clients/markAsCompleted', :controller => 'qiwo_clients', :action => 'markAsCompleted'
  match '/qiwo_clients/changeShippingPrice', :controller => 'qiwo_clients', :action => 'changeShippingPrice'
  match '/qiwo_clients/changeShippingGuide', :controller => 'qiwo_clients', :action => 'changeShippingGuide'
  match '/qiwo_clients/attachGuide', :controller => 'qiwo_clients', :action => 'attachGuide'
  resources :qiwo_clients do
    post :markAsCompleted, on: :collection 
  end
  
  match '/auth/:provider/callback' => 'authentications#create'
  match '*path' => redirect('/')


  # get 'search/:seed', to: 'welcome#search'
  # post 'search/:seed', to: 'welcome#search'


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
