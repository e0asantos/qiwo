CKEDITOR.plugins.add( 'qiwo',
{   
   requires : ['richcombo'], //, 'styles' ],
   init : function( editor )
   {
      var config = editor.config,
         lang = editor.lang.format;

      // Gets the list of tags from the settings.
      var tags = []; //new Array();
      //this.add('value', 'drop_text', 'drop_label');
      tags[0]=["[qiwo-product=345]", "Name", "Name"];
      tags[1]=["[qiwo-product=347]", "email", "email"];
      tags[2]=["[qiwo-product=34]", "User name", "User name"];      
      // Create style objects for all defined styles.
      $.get( "/qiwo_products/getProductsAsToken.json", function( data ) {
  			tags=data;
	});
      editor.ui.addRichCombo( 'tokens',
         {
            label : "Tienda Qiwo",
            title :"Tienda Qiwo",
            voiceLabel : "Tienda Qiwo",
            className : 'cke_format',
            multiSelect : false,

            panel :
            {
               css : [ config.contentsCss, CKEDITOR.getUrl( editor.skinPath + 'editor.css' ) ],
               voiceLabel : lang.panelVoiceLabel
            },

            init : function()
            {
               this.startGroup( "Tienda Qiwo" );
               //this.add('value', 'drop_text', 'drop_label');
               for (var this_tag in tags){
                  this.add(tags[this_tag][0], tags[this_tag][1], tags[this_tag][2]);
               }
            },

            onClick : function( value )
            {         
               editor.focus();
               editor.fire( 'saveSnapshot' );
               editor.insertHtml(value);
               editor.fire( 'saveSnapshot' );
            }
         });
   }
});