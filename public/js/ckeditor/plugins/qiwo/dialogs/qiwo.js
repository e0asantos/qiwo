CKEDITOR.dialog.add( 'qiwoDialog', function( editor ) {
    return {
        title: 'Abbreviation Properties',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: 'Productos de mi tienda',
                elements: [
                    {
                        type: 'text',
                        id: 'abbr',
                        label: 'Abbreviation',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Abbreviation field cannot be empty." )
                    },
                    {
                        type: 'text',
                        id: 'title',
                        label: 'Explanation',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Explanation field cannot be empty." )
                    }
                ]
            }
        ],
        onOk: function() {
            var dialog = this;

            var qiwo = editor.document.createElement( 'qiwo' );
            qiwo.setAttribute( 'title', dialog.getValueOf( 'tab-basic', 'title' ) );
            qiwo.setText( dialog.getValueOf( 'tab-basic', 'qiwo' ) );

            var id = dialog.getValueOf( 'tab-adv', 'id' );
            if ( id )
                qiwo.setAttribute( 'id', id );

            editor.insertElement( qiwo );
        }
    };
});