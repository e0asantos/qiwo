require 'test_helper'

class QiwoEmployeesControllerTest < ActionController::TestCase
  setup do
    @qiwo_employee = qiwo_employees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_employees)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_employee" do
    assert_difference('QiwoEmployee.count') do
      post :create, qiwo_employee: { employee_clients_permission: @qiwo_employee.employee_clients_permission, employee_enterprise: @qiwo_employee.employee_enterprise, employee_enterprise_permission: @qiwo_employee.employee_enterprise_permission, employee_fb_id: @qiwo_employee.employee_fb_id, employee_products_permission: @qiwo_employee.employee_products_permission, employee_quotes_permission: @qiwo_employee.employee_quotes_permission, employee_sale_point: @qiwo_employee.employee_sale_point, employee_type: @qiwo_employee.employee_type }
    end

    assert_redirected_to qiwo_employee_path(assigns(:qiwo_employee))
  end

  test "should show qiwo_employee" do
    get :show, id: @qiwo_employee
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_employee
    assert_response :success
  end

  test "should update qiwo_employee" do
    put :update, id: @qiwo_employee, qiwo_employee: { employee_clients_permission: @qiwo_employee.employee_clients_permission, employee_enterprise: @qiwo_employee.employee_enterprise, employee_enterprise_permission: @qiwo_employee.employee_enterprise_permission, employee_fb_id: @qiwo_employee.employee_fb_id, employee_products_permission: @qiwo_employee.employee_products_permission, employee_quotes_permission: @qiwo_employee.employee_quotes_permission, employee_sale_point: @qiwo_employee.employee_sale_point, employee_type: @qiwo_employee.employee_type }
    assert_redirected_to qiwo_employee_path(assigns(:qiwo_employee))
  end

  test "should destroy qiwo_employee" do
    assert_difference('QiwoEmployee.count', -1) do
      delete :destroy, id: @qiwo_employee
    end

    assert_redirected_to qiwo_employees_path
  end
end
