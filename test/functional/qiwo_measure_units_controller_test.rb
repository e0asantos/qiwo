require 'test_helper'

class QiwoMeasureUnitsControllerTest < ActionController::TestCase
  setup do
    @qiwo_measure_unit = qiwo_measure_units(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_measure_units)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_measure_unit" do
    assert_difference('QiwoMeasureUnit.count') do
      post :create, qiwo_measure_unit: { id: @qiwo_measure_unit.id, label_es: @qiwo_measure_unit.label_es }
    end

    assert_redirected_to qiwo_measure_unit_path(assigns(:qiwo_measure_unit))
  end

  test "should show qiwo_measure_unit" do
    get :show, id: @qiwo_measure_unit
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_measure_unit
    assert_response :success
  end

  test "should update qiwo_measure_unit" do
    put :update, id: @qiwo_measure_unit, qiwo_measure_unit: { id: @qiwo_measure_unit.id, label_es: @qiwo_measure_unit.label_es }
    assert_redirected_to qiwo_measure_unit_path(assigns(:qiwo_measure_unit))
  end

  test "should destroy qiwo_measure_unit" do
    assert_difference('QiwoMeasureUnit.count', -1) do
      delete :destroy, id: @qiwo_measure_unit
    end

    assert_redirected_to qiwo_measure_units_path
  end
end
