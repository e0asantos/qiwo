require 'test_helper'

class QiwoLogVisitsControllerTest < ActionController::TestCase
  setup do
    @qiwo_log_visit = qiwo_log_visits(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_log_visits)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_log_visit" do
    assert_difference('QiwoLogVisit.count') do
      post :create, qiwo_log_visit: { date_visit: @qiwo_log_visit.date_visit, facebook_id: @qiwo_log_visit.facebook_id, id: @qiwo_log_visit.id, ip_user: @qiwo_log_visit.ip_user, user_id: @qiwo_log_visit.user_id }
    end

    assert_redirected_to qiwo_log_visit_path(assigns(:qiwo_log_visit))
  end

  test "should show qiwo_log_visit" do
    get :show, id: @qiwo_log_visit
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_log_visit
    assert_response :success
  end

  test "should update qiwo_log_visit" do
    put :update, id: @qiwo_log_visit, qiwo_log_visit: { date_visit: @qiwo_log_visit.date_visit, facebook_id: @qiwo_log_visit.facebook_id, id: @qiwo_log_visit.id, ip_user: @qiwo_log_visit.ip_user, user_id: @qiwo_log_visit.user_id }
    assert_redirected_to qiwo_log_visit_path(assigns(:qiwo_log_visit))
  end

  test "should destroy qiwo_log_visit" do
    assert_difference('QiwoLogVisit.count', -1) do
      delete :destroy, id: @qiwo_log_visit
    end

    assert_redirected_to qiwo_log_visits_path
  end
end
