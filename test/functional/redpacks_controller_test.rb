require 'test_helper'

class RedpacksControllerTest < ActionController::TestCase
  setup do
    @redpack = redpacks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:redpacks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create redpack" do
    assert_difference('Redpack.count') do
      post :create, redpack: { delivered_to: @redpack.delivered_to, originated_from: @redpack.originated_from }
    end

    assert_redirected_to redpack_path(assigns(:redpack))
  end

  test "should show redpack" do
    get :show, id: @redpack
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @redpack
    assert_response :success
  end

  test "should update redpack" do
    put :update, id: @redpack, redpack: { delivered_to: @redpack.delivered_to, originated_from: @redpack.originated_from }
    assert_redirected_to redpack_path(assigns(:redpack))
  end

  test "should destroy redpack" do
    assert_difference('Redpack.count', -1) do
      delete :destroy, id: @redpack
    end

    assert_redirected_to redpacks_path
  end
end
