require 'test_helper'

class ShippingGuidesControllerTest < ActionController::TestCase
  setup do
    @shipping_guide = shipping_guides(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:shipping_guides)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create shipping_guide" do
    assert_difference('ShippingGuide.count') do
      post :create, shipping_guide: { id: @shipping_guide.id, id_from: @shipping_guide.id_from, id_to: @shipping_guide.id_to, label_guide: @shipping_guide.label_guide, real_guide: @shipping_guide.real_guide, status_guide: @shipping_guide.status_guide }
    end

    assert_redirected_to shipping_guide_path(assigns(:shipping_guide))
  end

  test "should show shipping_guide" do
    get :show, id: @shipping_guide
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @shipping_guide
    assert_response :success
  end

  test "should update shipping_guide" do
    put :update, id: @shipping_guide, shipping_guide: { id: @shipping_guide.id, id_from: @shipping_guide.id_from, id_to: @shipping_guide.id_to, label_guide: @shipping_guide.label_guide, real_guide: @shipping_guide.real_guide, status_guide: @shipping_guide.status_guide }
    assert_redirected_to shipping_guide_path(assigns(:shipping_guide))
  end

  test "should destroy shipping_guide" do
    assert_difference('ShippingGuide.count', -1) do
      delete :destroy, id: @shipping_guide
    end

    assert_redirected_to shipping_guides_path
  end
end
