require 'test_helper'

class QiwoCfdisControllerTest < ActionController::TestCase
  setup do
    @qiwo_cfdi = qiwo_cfdis(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_cfdis)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_cfdi" do
    assert_difference('QiwoCfdi.count') do
      post :create, qiwo_cfdi: { originated_from: @qiwo_cfdi.originated_from, originated_to: @qiwo_cfdi.originated_to }
    end

    assert_redirected_to qiwo_cfdi_path(assigns(:qiwo_cfdi))
  end

  test "should show qiwo_cfdi" do
    get :show, id: @qiwo_cfdi
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_cfdi
    assert_response :success
  end

  test "should update qiwo_cfdi" do
    put :update, id: @qiwo_cfdi, qiwo_cfdi: { originated_from: @qiwo_cfdi.originated_from, originated_to: @qiwo_cfdi.originated_to }
    assert_redirected_to qiwo_cfdi_path(assigns(:qiwo_cfdi))
  end

  test "should destroy qiwo_cfdi" do
    assert_difference('QiwoCfdi.count', -1) do
      delete :destroy, id: @qiwo_cfdi
    end

    assert_redirected_to qiwo_cfdis_path
  end
end
