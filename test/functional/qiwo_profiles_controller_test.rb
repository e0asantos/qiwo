require 'test_helper'

class QiwoProfilesControllerTest < ActionController::TestCase
  setup do
    @qiwo_profile = qiwo_profiles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_profiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_profile" do
    assert_difference('QiwoProfile.count') do
      post :create, qiwo_profile: { id: @qiwo_profile.id }
    end

    assert_redirected_to qiwo_profile_path(assigns(:qiwo_profile))
  end

  test "should show qiwo_profile" do
    get :show, id: @qiwo_profile
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_profile
    assert_response :success
  end

  test "should update qiwo_profile" do
    put :update, id: @qiwo_profile, qiwo_profile: { id: @qiwo_profile.id }
    assert_redirected_to qiwo_profile_path(assigns(:qiwo_profile))
  end

  test "should destroy qiwo_profile" do
    assert_difference('QiwoProfile.count', -1) do
      delete :destroy, id: @qiwo_profile
    end

    assert_redirected_to qiwo_profiles_path
  end
end
