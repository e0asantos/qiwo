require 'test_helper'

class CarteraClientesControllerTest < ActionController::TestCase
  setup do
    @cartera_cliente = cartera_clientes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cartera_clientes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cartera_cliente" do
    assert_difference('CarteraCliente.count') do
      post :create, cartera_cliente: { contacto_email: @cartera_cliente.contacto_email, contacto_name: @cartera_cliente.contacto_name, is_client: @cartera_cliente.is_client, location_lat: @cartera_cliente.location_lat, location_lon: @cartera_cliente.location_lon, negocio: @cartera_cliente.negocio, next_visit: @cartera_cliente.next_visit, observaciones: @cartera_cliente.observaciones, telefono: @cartera_cliente.telefono, visitado: @cartera_cliente.visitado }
    end

    assert_redirected_to cartera_cliente_path(assigns(:cartera_cliente))
  end

  test "should show cartera_cliente" do
    get :show, id: @cartera_cliente
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cartera_cliente
    assert_response :success
  end

  test "should update cartera_cliente" do
    put :update, id: @cartera_cliente, cartera_cliente: { contacto_email: @cartera_cliente.contacto_email, contacto_name: @cartera_cliente.contacto_name, is_client: @cartera_cliente.is_client, location_lat: @cartera_cliente.location_lat, location_lon: @cartera_cliente.location_lon, negocio: @cartera_cliente.negocio, next_visit: @cartera_cliente.next_visit, observaciones: @cartera_cliente.observaciones, telefono: @cartera_cliente.telefono, visitado: @cartera_cliente.visitado }
    assert_redirected_to cartera_cliente_path(assigns(:cartera_cliente))
  end

  test "should destroy cartera_cliente" do
    assert_difference('CarteraCliente.count', -1) do
      delete :destroy, id: @cartera_cliente
    end

    assert_redirected_to cartera_clientes_path
  end
end
