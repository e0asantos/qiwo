require 'test_helper'

class QiwoToolsControllerTest < ActionController::TestCase
  setup do
    @qiwo_tool = qiwo_tools(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_tools)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_tool" do
    assert_difference('QiwoTool.count') do
      post :create, qiwo_tool: { apikey: @qiwo_tool.apikey, apisecret: @qiwo_tool.apisecret, description: @qiwo_tool.description, id: @qiwo_tool.id, toolname: @qiwo_tool.toolname, user_id: @qiwo_tool.user_id }
    end

    assert_redirected_to qiwo_tool_path(assigns(:qiwo_tool))
  end

  test "should show qiwo_tool" do
    get :show, id: @qiwo_tool
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_tool
    assert_response :success
  end

  test "should update qiwo_tool" do
    put :update, id: @qiwo_tool, qiwo_tool: { apikey: @qiwo_tool.apikey, apisecret: @qiwo_tool.apisecret, description: @qiwo_tool.description, id: @qiwo_tool.id, toolname: @qiwo_tool.toolname, user_id: @qiwo_tool.user_id }
    assert_redirected_to qiwo_tool_path(assigns(:qiwo_tool))
  end

  test "should destroy qiwo_tool" do
    assert_difference('QiwoTool.count', -1) do
      delete :destroy, id: @qiwo_tool
    end

    assert_redirected_to qiwo_tools_path
  end
end
