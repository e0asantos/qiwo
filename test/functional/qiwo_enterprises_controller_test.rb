require 'test_helper'

class QiwoEnterprisesControllerTest < ActionController::TestCase
  setup do
    @qiwo_enterprise = qiwo_enterprises(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_enterprises)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_enterprise" do
    assert_difference('QiwoEnterprise.count') do
      post :create, qiwo_enterprise: { bank_clabe: @qiwo_enterprise.bank_clabe, comercial_name: @qiwo_enterprise.comercial_name, founded_date: @qiwo_enterprise.founded_date, location: @qiwo_enterprise.location, location_lat: @qiwo_enterprise.location_lat, location_lon: @qiwo_enterprise.location_lon, name: @qiwo_enterprise.name, paypal_email: @qiwo_enterprise.paypal_email, point_sale: @qiwo_enterprise.point_sale, schedule: @qiwo_enterprise.schedule, telephones: @qiwo_enterprise.telephones }
    end

    assert_redirected_to qiwo_enterprise_path(assigns(:qiwo_enterprise))
  end

  test "should show qiwo_enterprise" do
    get :show, id: @qiwo_enterprise
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_enterprise
    assert_response :success
  end

  test "should update qiwo_enterprise" do
    put :update, id: @qiwo_enterprise, qiwo_enterprise: { bank_clabe: @qiwo_enterprise.bank_clabe, comercial_name: @qiwo_enterprise.comercial_name, founded_date: @qiwo_enterprise.founded_date, location: @qiwo_enterprise.location, location_lat: @qiwo_enterprise.location_lat, location_lon: @qiwo_enterprise.location_lon, name: @qiwo_enterprise.name, paypal_email: @qiwo_enterprise.paypal_email, point_sale: @qiwo_enterprise.point_sale, schedule: @qiwo_enterprise.schedule, telephones: @qiwo_enterprise.telephones }
    assert_redirected_to qiwo_enterprise_path(assigns(:qiwo_enterprise))
  end

  test "should destroy qiwo_enterprise" do
    assert_difference('QiwoEnterprise.count', -1) do
      delete :destroy, id: @qiwo_enterprise
    end

    assert_redirected_to qiwo_enterprises_path
  end
end
