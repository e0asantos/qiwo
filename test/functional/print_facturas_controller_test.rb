require 'test_helper'

class PrintFacturasControllerTest < ActionController::TestCase
  setup do
    @print_factura = print_facturas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:print_facturas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create print_factura" do
    assert_difference('PrintFactura.count') do
      post :create, print_factura: { factura: @print_factura.factura, id: @print_factura.id }
    end

    assert_redirected_to print_factura_path(assigns(:print_factura))
  end

  test "should show print_factura" do
    get :show, id: @print_factura
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @print_factura
    assert_response :success
  end

  test "should update print_factura" do
    put :update, id: @print_factura, print_factura: { factura: @print_factura.factura, id: @print_factura.id }
    assert_redirected_to print_factura_path(assigns(:print_factura))
  end

  test "should destroy print_factura" do
    assert_difference('PrintFactura.count', -1) do
      delete :destroy, id: @print_factura
    end

    assert_redirected_to print_facturas_path
  end
end
