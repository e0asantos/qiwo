require 'test_helper'

class QiwoLogKartsControllerTest < ActionController::TestCase
  setup do
    @qiwo_log_kart = qiwo_log_karts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_log_karts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_log_kart" do
    assert_difference('QiwoLogKart.count') do
      post :create, qiwo_log_kart: { cotizacion_number: @qiwo_log_kart.cotizacion_number, coupon_id: @qiwo_log_kart.coupon_id, facebook_id: @qiwo_log_kart.facebook_id, id: @qiwo_log_kart.id, ip_user: @qiwo_log_kart.ip_user, product_id: @qiwo_log_kart.product_id, store_id: @qiwo_log_kart.store_id, user_id: @qiwo_log_kart.user_id }
    end

    assert_redirected_to qiwo_log_kart_path(assigns(:qiwo_log_kart))
  end

  test "should show qiwo_log_kart" do
    get :show, id: @qiwo_log_kart
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_log_kart
    assert_response :success
  end

  test "should update qiwo_log_kart" do
    put :update, id: @qiwo_log_kart, qiwo_log_kart: { cotizacion_number: @qiwo_log_kart.cotizacion_number, coupon_id: @qiwo_log_kart.coupon_id, facebook_id: @qiwo_log_kart.facebook_id, id: @qiwo_log_kart.id, ip_user: @qiwo_log_kart.ip_user, product_id: @qiwo_log_kart.product_id, store_id: @qiwo_log_kart.store_id, user_id: @qiwo_log_kart.user_id }
    assert_redirected_to qiwo_log_kart_path(assigns(:qiwo_log_kart))
  end

  test "should destroy qiwo_log_kart" do
    assert_difference('QiwoLogKart.count', -1) do
      delete :destroy, id: @qiwo_log_kart
    end

    assert_redirected_to qiwo_log_karts_path
  end
end
