require 'test_helper'

class QiwoReviewsControllerTest < ActionController::TestCase
  setup do
    @qiwo_review = qiwo_reviews(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_reviews)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_review" do
    assert_difference('QiwoReview.count') do
      post :create, qiwo_review: { id: @qiwo_review.id, product_id: @qiwo_review.product_id, review: @qiwo_review.review, store_id: @qiwo_review.store_id, user_id: @qiwo_review.user_id }
    end

    assert_redirected_to qiwo_review_path(assigns(:qiwo_review))
  end

  test "should show qiwo_review" do
    get :show, id: @qiwo_review
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_review
    assert_response :success
  end

  test "should update qiwo_review" do
    put :update, id: @qiwo_review, qiwo_review: { id: @qiwo_review.id, product_id: @qiwo_review.product_id, review: @qiwo_review.review, store_id: @qiwo_review.store_id, user_id: @qiwo_review.user_id }
    assert_redirected_to qiwo_review_path(assigns(:qiwo_review))
  end

  test "should destroy qiwo_review" do
    assert_difference('QiwoReview.count', -1) do
      delete :destroy, id: @qiwo_review
    end

    assert_redirected_to qiwo_reviews_path
  end
end
