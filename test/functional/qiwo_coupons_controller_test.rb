require 'test_helper'

class QiwoCouponsControllerTest < ActionController::TestCase
  setup do
    @qiwo_coupon = qiwo_coupons(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_coupons)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_coupon" do
    assert_difference('QiwoCoupon.count') do
      post :create, qiwo_coupon: { coupon_name: @qiwo_coupon.coupon_name, end_date: @qiwo_coupon.end_date, id: @qiwo_coupon.id, porcentage: @qiwo_coupon.porcentage, start_date: @qiwo_coupon.start_date, times_used: @qiwo_coupon.times_used }
    end

    assert_redirected_to qiwo_coupon_path(assigns(:qiwo_coupon))
  end

  test "should show qiwo_coupon" do
    get :show, id: @qiwo_coupon
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_coupon
    assert_response :success
  end

  test "should update qiwo_coupon" do
    put :update, id: @qiwo_coupon, qiwo_coupon: { coupon_name: @qiwo_coupon.coupon_name, end_date: @qiwo_coupon.end_date, id: @qiwo_coupon.id, porcentage: @qiwo_coupon.porcentage, start_date: @qiwo_coupon.start_date, times_used: @qiwo_coupon.times_used }
    assert_redirected_to qiwo_coupon_path(assigns(:qiwo_coupon))
  end

  test "should destroy qiwo_coupon" do
    assert_difference('QiwoCoupon.count', -1) do
      delete :destroy, id: @qiwo_coupon
    end

    assert_redirected_to qiwo_coupons_path
  end
end
