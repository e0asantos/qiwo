require 'test_helper'

class QiwoProductCategoriesControllerTest < ActionController::TestCase
  setup do
    @qiwo_product_category = qiwo_product_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_product_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_product_category" do
    assert_difference('QiwoProductCategory.count') do
      post :create, qiwo_product_category: { category_id: @qiwo_product_category.category_id, id: @qiwo_product_category.id, product_id: @qiwo_product_category.product_id, store_id: @qiwo_product_category.store_id }
    end

    assert_redirected_to qiwo_product_category_path(assigns(:qiwo_product_category))
  end

  test "should show qiwo_product_category" do
    get :show, id: @qiwo_product_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_product_category
    assert_response :success
  end

  test "should update qiwo_product_category" do
    put :update, id: @qiwo_product_category, qiwo_product_category: { category_id: @qiwo_product_category.category_id, id: @qiwo_product_category.id, product_id: @qiwo_product_category.product_id, store_id: @qiwo_product_category.store_id }
    assert_redirected_to qiwo_product_category_path(assigns(:qiwo_product_category))
  end

  test "should destroy qiwo_product_category" do
    assert_difference('QiwoProductCategory.count', -1) do
      delete :destroy, id: @qiwo_product_category
    end

    assert_redirected_to qiwo_product_categories_path
  end
end
