require 'test_helper'

class QiwoUserSubscriptionsControllerTest < ActionController::TestCase
  setup do
    @qiwo_user_subscription = qiwo_user_subscriptions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_user_subscriptions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_user_subscription" do
    assert_difference('QiwoUserSubscription.count') do
      post :create, qiwo_user_subscription: { amount_paid: @qiwo_user_subscription.amount_paid, currency: @qiwo_user_subscription.currency, ends_on: @qiwo_user_subscription.ends_on, id: @qiwo_user_subscription.id, payment_gateway: @qiwo_user_subscription.payment_gateway, subscription_type: @qiwo_user_subscription.subscription_type, user_id: @qiwo_user_subscription.user_id }
    end

    assert_redirected_to qiwo_user_subscription_path(assigns(:qiwo_user_subscription))
  end

  test "should show qiwo_user_subscription" do
    get :show, id: @qiwo_user_subscription
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_user_subscription
    assert_response :success
  end

  test "should update qiwo_user_subscription" do
    put :update, id: @qiwo_user_subscription, qiwo_user_subscription: { amount_paid: @qiwo_user_subscription.amount_paid, currency: @qiwo_user_subscription.currency, ends_on: @qiwo_user_subscription.ends_on, id: @qiwo_user_subscription.id, payment_gateway: @qiwo_user_subscription.payment_gateway, subscription_type: @qiwo_user_subscription.subscription_type, user_id: @qiwo_user_subscription.user_id }
    assert_redirected_to qiwo_user_subscription_path(assigns(:qiwo_user_subscription))
  end

  test "should destroy qiwo_user_subscription" do
    assert_difference('QiwoUserSubscription.count', -1) do
      delete :destroy, id: @qiwo_user_subscription
    end

    assert_redirected_to qiwo_user_subscriptions_path
  end
end
