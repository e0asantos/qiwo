require 'test_helper'

class QiwoImagesControllerTest < ActionController::TestCase
  setup do
    @qiwo_image = qiwo_images(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_images)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_image" do
    assert_difference('QiwoImage.count') do
      post :create, qiwo_image: { allowed: @qiwo_image.allowed, fron_page: @qiwo_image.fron_page, id: @qiwo_image.id, image_url: @qiwo_image.image_url, user_id: @qiwo_image.user_id }
    end

    assert_redirected_to qiwo_image_path(assigns(:qiwo_image))
  end

  test "should show qiwo_image" do
    get :show, id: @qiwo_image
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_image
    assert_response :success
  end

  test "should update qiwo_image" do
    put :update, id: @qiwo_image, qiwo_image: { allowed: @qiwo_image.allowed, fron_page: @qiwo_image.fron_page, id: @qiwo_image.id, image_url: @qiwo_image.image_url, user_id: @qiwo_image.user_id }
    assert_redirected_to qiwo_image_path(assigns(:qiwo_image))
  end

  test "should destroy qiwo_image" do
    assert_difference('QiwoImage.count', -1) do
      delete :destroy, id: @qiwo_image
    end

    assert_redirected_to qiwo_images_path
  end
end
