require 'test_helper'

class QiwoCategoriesControllerTest < ActionController::TestCase
  setup do
    @qiwo_category = qiwo_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_category" do
    assert_difference('QiwoCategory.count') do
      post :create, qiwo_category: { category_name: @qiwo_category.category_name, id: @qiwo_category.id, parent_category: @qiwo_category.parent_category }
    end

    assert_redirected_to qiwo_category_path(assigns(:qiwo_category))
  end

  test "should show qiwo_category" do
    get :show, id: @qiwo_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_category
    assert_response :success
  end

  test "should update qiwo_category" do
    put :update, id: @qiwo_category, qiwo_category: { category_name: @qiwo_category.category_name, id: @qiwo_category.id, parent_category: @qiwo_category.parent_category }
    assert_redirected_to qiwo_category_path(assigns(:qiwo_category))
  end

  test "should destroy qiwo_category" do
    assert_difference('QiwoCategory.count', -1) do
      delete :destroy, id: @qiwo_category
    end

    assert_redirected_to qiwo_categories_path
  end
end
