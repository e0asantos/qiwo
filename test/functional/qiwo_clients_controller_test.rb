require 'test_helper'

class QiwoClientsControllerTest < ActionController::TestCase
  setup do
    @qiwo_client = qiwo_clients(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_clients)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_client" do
    assert_difference('QiwoClient.count') do
      post :create, qiwo_client: { cotizacion_number: @qiwo_client.cotizacion_number, id: @qiwo_client.id, user_id: @qiwo_client.user_id }
    end

    assert_redirected_to qiwo_client_path(assigns(:qiwo_client))
  end

  test "should show qiwo_client" do
    get :show, id: @qiwo_client
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_client
    assert_response :success
  end

  test "should update qiwo_client" do
    put :update, id: @qiwo_client, qiwo_client: { cotizacion_number: @qiwo_client.cotizacion_number, id: @qiwo_client.id, user_id: @qiwo_client.user_id }
    assert_redirected_to qiwo_client_path(assigns(:qiwo_client))
  end

  test "should destroy qiwo_client" do
    assert_difference('QiwoClient.count', -1) do
      delete :destroy, id: @qiwo_client
    end

    assert_redirected_to qiwo_clients_path
  end
end
