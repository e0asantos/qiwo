require 'test_helper'

class QiwoRanksControllerTest < ActionController::TestCase
  setup do
    @qiwo_rank = qiwo_ranks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_ranks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_rank" do
    assert_difference('QiwoRank.count') do
      post :create, qiwo_rank: { id: @qiwo_rank.id, product_id: @qiwo_rank.product_id, rank: @qiwo_rank.rank, store_id: @qiwo_rank.store_id, user_id: @qiwo_rank.user_id }
    end

    assert_redirected_to qiwo_rank_path(assigns(:qiwo_rank))
  end

  test "should show qiwo_rank" do
    get :show, id: @qiwo_rank
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_rank
    assert_response :success
  end

  test "should update qiwo_rank" do
    put :update, id: @qiwo_rank, qiwo_rank: { id: @qiwo_rank.id, product_id: @qiwo_rank.product_id, rank: @qiwo_rank.rank, store_id: @qiwo_rank.store_id, user_id: @qiwo_rank.user_id }
    assert_redirected_to qiwo_rank_path(assigns(:qiwo_rank))
  end

  test "should destroy qiwo_rank" do
    assert_difference('QiwoRank.count', -1) do
      delete :destroy, id: @qiwo_rank
    end

    assert_redirected_to qiwo_ranks_path
  end
end
