require 'test_helper'

class QiwoCountersControllerTest < ActionController::TestCase
  setup do
    @qiwo_counter = qiwo_counters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_counters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_counter" do
    assert_difference('QiwoCounter.count') do
      post :create, qiwo_counter: { facturas_disponibles: @qiwo_counter.facturas_disponibles, folio_factura: @qiwo_counter.folio_factura, id: @qiwo_counter.id, ordenes_pos: @qiwo_counter.ordenes_pos, qiwo_enterprise_id: @qiwo_counter.qiwo_enterprise_id }
    end

    assert_redirected_to qiwo_counter_path(assigns(:qiwo_counter))
  end

  test "should show qiwo_counter" do
    get :show, id: @qiwo_counter
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_counter
    assert_response :success
  end

  test "should update qiwo_counter" do
    put :update, id: @qiwo_counter, qiwo_counter: { facturas_disponibles: @qiwo_counter.facturas_disponibles, folio_factura: @qiwo_counter.folio_factura, id: @qiwo_counter.id, ordenes_pos: @qiwo_counter.ordenes_pos, qiwo_enterprise_id: @qiwo_counter.qiwo_enterprise_id }
    assert_redirected_to qiwo_counter_path(assigns(:qiwo_counter))
  end

  test "should destroy qiwo_counter" do
    assert_difference('QiwoCounter.count', -1) do
      delete :destroy, id: @qiwo_counter
    end

    assert_redirected_to qiwo_counters_path
  end
end
