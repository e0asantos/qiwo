require 'test_helper'

class NfcReadersControllerTest < ActionController::TestCase
  setup do
    @nfc_reader = nfc_readers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:nfc_readers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create nfc_reader" do
    assert_difference('NfcReader.count') do
      post :create, nfc_reader: { blocked: @nfc_reader.blocked, id: @nfc_reader.id, last_token: @nfc_reader.last_token, nfc_type: @nfc_reader.nfc_type, static_token: @nfc_reader.static_token }
    end

    assert_redirected_to nfc_reader_path(assigns(:nfc_reader))
  end

  test "should show nfc_reader" do
    get :show, id: @nfc_reader
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @nfc_reader
    assert_response :success
  end

  test "should update nfc_reader" do
    put :update, id: @nfc_reader, nfc_reader: { blocked: @nfc_reader.blocked, id: @nfc_reader.id, last_token: @nfc_reader.last_token, nfc_type: @nfc_reader.nfc_type, static_token: @nfc_reader.static_token }
    assert_redirected_to nfc_reader_path(assigns(:nfc_reader))
  end

  test "should destroy nfc_reader" do
    assert_difference('NfcReader.count', -1) do
      delete :destroy, id: @nfc_reader
    end

    assert_redirected_to nfc_readers_path
  end
end
