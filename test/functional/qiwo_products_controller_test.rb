require 'test_helper'

class QiwoProductsControllerTest < ActionController::TestCase
  setup do
    @qiwo_product = qiwo_products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_product" do
    assert_difference('QiwoProduct.count') do
      post :create, qiwo_product: { description: @qiwo_product.description, id: @qiwo_product.id, image_id: @qiwo_product.image_id, location_store: @qiwo_product.location_store, name: @qiwo_product.name, raw_price: @qiwo_product.raw_price, sale_price: @qiwo_product.sale_price, stock_items: @qiwo_product.stock_items }
    end

    assert_redirected_to qiwo_product_path(assigns(:qiwo_product))
  end

  test "should show qiwo_product" do
    get :show, id: @qiwo_product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_product
    assert_response :success
  end

  test "should update qiwo_product" do
    put :update, id: @qiwo_product, qiwo_product: { description: @qiwo_product.description, id: @qiwo_product.id, image_id: @qiwo_product.image_id, location_store: @qiwo_product.location_store, name: @qiwo_product.name, raw_price: @qiwo_product.raw_price, sale_price: @qiwo_product.sale_price, stock_items: @qiwo_product.stock_items }
    assert_redirected_to qiwo_product_path(assigns(:qiwo_product))
  end

  test "should destroy qiwo_product" do
    assert_difference('QiwoProduct.count', -1) do
      delete :destroy, id: @qiwo_product
    end

    assert_redirected_to qiwo_products_path
  end
end
