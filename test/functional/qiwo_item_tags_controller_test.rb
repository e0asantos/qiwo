require 'test_helper'

class QiwoItemTagsControllerTest < ActionController::TestCase
  setup do
    @qiwo_item_tag = qiwo_item_tags(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_item_tags)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_item_tag" do
    assert_difference('QiwoItemTag.count') do
      post :create, qiwo_item_tag: { id: @qiwo_item_tag.id, product_id: @qiwo_item_tag.product_id, store_id: @qiwo_item_tag.store_id, tag_id: @qiwo_item_tag.tag_id }
    end

    assert_redirected_to qiwo_item_tag_path(assigns(:qiwo_item_tag))
  end

  test "should show qiwo_item_tag" do
    get :show, id: @qiwo_item_tag
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_item_tag
    assert_response :success
  end

  test "should update qiwo_item_tag" do
    put :update, id: @qiwo_item_tag, qiwo_item_tag: { id: @qiwo_item_tag.id, product_id: @qiwo_item_tag.product_id, store_id: @qiwo_item_tag.store_id, tag_id: @qiwo_item_tag.tag_id }
    assert_redirected_to qiwo_item_tag_path(assigns(:qiwo_item_tag))
  end

  test "should destroy qiwo_item_tag" do
    assert_difference('QiwoItemTag.count', -1) do
      delete :destroy, id: @qiwo_item_tag
    end

    assert_redirected_to qiwo_item_tags_path
  end
end
