require 'test_helper'

class NfcRequestsControllerTest < ActionController::TestCase
  setup do
    @nfc_request = nfc_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:nfc_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create nfc_request" do
    assert_difference('NfcRequest.count') do
      post :create, nfc_request: { id: @nfc_request.id, qiwo_enterprise_id: @nfc_request.qiwo_enterprise_id, qiwo_product_id: @nfc_request.qiwo_product_id, sys_user_id: @nfc_request.sys_user_id }
    end

    assert_redirected_to nfc_request_path(assigns(:nfc_request))
  end

  test "should show nfc_request" do
    get :show, id: @nfc_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @nfc_request
    assert_response :success
  end

  test "should update nfc_request" do
    put :update, id: @nfc_request, nfc_request: { id: @nfc_request.id, qiwo_enterprise_id: @nfc_request.qiwo_enterprise_id, qiwo_product_id: @nfc_request.qiwo_product_id, sys_user_id: @nfc_request.sys_user_id }
    assert_redirected_to nfc_request_path(assigns(:nfc_request))
  end

  test "should destroy nfc_request" do
    assert_difference('NfcRequest.count', -1) do
      delete :destroy, id: @nfc_request
    end

    assert_redirected_to nfc_requests_path
  end
end
