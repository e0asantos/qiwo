require 'test_helper'

class CfdiApisControllerTest < ActionController::TestCase
  setup do
    @cfdi_api = cfdi_apis(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cfdi_apis)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cfdi_api" do
    assert_difference('CfdiApi.count') do
      post :create, cfdi_api: { cfdi_status: @cfdi_api.cfdi_status, found_error: @cfdi_api.found_error, id: @cfdi_api.id, incoming_params: @cfdi_api.incoming_params, ip_address: @cfdi_api.ip_address }
    end

    assert_redirected_to cfdi_api_path(assigns(:cfdi_api))
  end

  test "should show cfdi_api" do
    get :show, id: @cfdi_api
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cfdi_api
    assert_response :success
  end

  test "should update cfdi_api" do
    put :update, id: @cfdi_api, cfdi_api: { cfdi_status: @cfdi_api.cfdi_status, found_error: @cfdi_api.found_error, id: @cfdi_api.id, incoming_params: @cfdi_api.incoming_params, ip_address: @cfdi_api.ip_address }
    assert_redirected_to cfdi_api_path(assigns(:cfdi_api))
  end

  test "should destroy cfdi_api" do
    assert_difference('CfdiApi.count', -1) do
      delete :destroy, id: @cfdi_api
    end

    assert_redirected_to cfdi_apis_path
  end
end
