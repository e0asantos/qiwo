require 'test_helper'

class QiwoLogSearchesControllerTest < ActionController::TestCase
  setup do
    @qiwo_log_search = qiwo_log_searches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_log_searches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_log_search" do
    assert_difference('QiwoLogSearch.count') do
      post :create, qiwo_log_search: { date_search: @qiwo_log_search.date_search, facebook_id: @qiwo_log_search.facebook_id, id: @qiwo_log_search.id, ip_user: @qiwo_log_search.ip_user, search_term: @qiwo_log_search.search_term, user_id: @qiwo_log_search.user_id }
    end

    assert_redirected_to qiwo_log_search_path(assigns(:qiwo_log_search))
  end

  test "should show qiwo_log_search" do
    get :show, id: @qiwo_log_search
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_log_search
    assert_response :success
  end

  test "should update qiwo_log_search" do
    put :update, id: @qiwo_log_search, qiwo_log_search: { date_search: @qiwo_log_search.date_search, facebook_id: @qiwo_log_search.facebook_id, id: @qiwo_log_search.id, ip_user: @qiwo_log_search.ip_user, search_term: @qiwo_log_search.search_term, user_id: @qiwo_log_search.user_id }
    assert_redirected_to qiwo_log_search_path(assigns(:qiwo_log_search))
  end

  test "should destroy qiwo_log_search" do
    assert_difference('QiwoLogSearch.count', -1) do
      delete :destroy, id: @qiwo_log_search
    end

    assert_redirected_to qiwo_log_searches_path
  end
end
