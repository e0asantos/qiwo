require 'test_helper'

class QiwoOrdersControllerTest < ActionController::TestCase
  setup do
    @qiwo_order = qiwo_orders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_orders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_order" do
    assert_difference('QiwoOrder.count') do
      post :create, qiwo_order: { factura_id: @qiwo_order.factura_id, order_state: @qiwo_order.order_state, owner_user_id: @qiwo_order.owner_user_id, qiwo_enterprise_id: @qiwo_order.qiwo_enterprise_id, salesforce_user_id: @qiwo_order.salesforce_user_id }
    end

    assert_redirected_to qiwo_order_path(assigns(:qiwo_order))
  end

  test "should show qiwo_order" do
    get :show, id: @qiwo_order
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_order
    assert_response :success
  end

  test "should update qiwo_order" do
    put :update, id: @qiwo_order, qiwo_order: { factura_id: @qiwo_order.factura_id, order_state: @qiwo_order.order_state, owner_user_id: @qiwo_order.owner_user_id, qiwo_enterprise_id: @qiwo_order.qiwo_enterprise_id, salesforce_user_id: @qiwo_order.salesforce_user_id }
    assert_redirected_to qiwo_order_path(assigns(:qiwo_order))
  end

  test "should destroy qiwo_order" do
    assert_difference('QiwoOrder.count', -1) do
      delete :destroy, id: @qiwo_order
    end

    assert_redirected_to qiwo_orders_path
  end
end
