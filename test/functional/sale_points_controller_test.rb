require 'test_helper'

class SalePointsControllerTest < ActionController::TestCase
  setup do
    @sale_point = sale_points(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sale_points)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sale_point" do
    assert_difference('SalePoint.count') do
      post :create, sale_point: { client_ref: @sale_point.client_ref, client_ref_num: @sale_point.client_ref_num, order_ref: @sale_point.order_ref, product_id: @sale_point.product_id }
    end

    assert_redirected_to sale_point_path(assigns(:sale_point))
  end

  test "should show sale_point" do
    get :show, id: @sale_point
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sale_point
    assert_response :success
  end

  test "should update sale_point" do
    put :update, id: @sale_point, sale_point: { client_ref: @sale_point.client_ref, client_ref_num: @sale_point.client_ref_num, order_ref: @sale_point.order_ref, product_id: @sale_point.product_id }
    assert_redirected_to sale_point_path(assigns(:sale_point))
  end

  test "should destroy sale_point" do
    assert_difference('SalePoint.count', -1) do
      delete :destroy, id: @sale_point
    end

    assert_redirected_to sale_points_path
  end
end
