require 'test_helper'

class SatDocumentsControllerTest < ActionController::TestCase
  setup do
    @sat_document = sat_documents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sat_documents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sat_document" do
    assert_difference('SatDocument.count') do
      post :create, sat_document: { cer_file: @sat_document.cer_file, enterprise_id: @sat_document.enterprise_id, id: @sat_document.id, key_file: @sat_document.key_file, user_id: @sat_document.user_id }
    end

    assert_redirected_to sat_document_path(assigns(:sat_document))
  end

  test "should show sat_document" do
    get :show, id: @sat_document
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sat_document
    assert_response :success
  end

  test "should update sat_document" do
    put :update, id: @sat_document, sat_document: { cer_file: @sat_document.cer_file, enterprise_id: @sat_document.enterprise_id, id: @sat_document.id, key_file: @sat_document.key_file, user_id: @sat_document.user_id }
    assert_redirected_to sat_document_path(assigns(:sat_document))
  end

  test "should destroy sat_document" do
    assert_difference('SatDocument.count', -1) do
      delete :destroy, id: @sat_document
    end

    assert_redirected_to sat_documents_path
  end
end
