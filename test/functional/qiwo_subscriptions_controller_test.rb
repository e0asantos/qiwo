require 'test_helper'

class QiwoSubscriptionsControllerTest < ActionController::TestCase
  setup do
    @qiwo_subscription = qiwo_subscriptions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_subscriptions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_subscription" do
    assert_difference('QiwoSubscription.count') do
      post :create, qiwo_subscription: { days_active: @qiwo_subscription.days_active, id: @qiwo_subscription.id, months_active: @qiwo_subscription.months_active, places_allowed: @qiwo_subscription.places_allowed, price: @qiwo_subscription.price, products_allowed: @qiwo_subscription.products_allowed, subscription_name: @qiwo_subscription.subscription_name }
    end

    assert_redirected_to qiwo_subscription_path(assigns(:qiwo_subscription))
  end

  test "should show qiwo_subscription" do
    get :show, id: @qiwo_subscription
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_subscription
    assert_response :success
  end

  test "should update qiwo_subscription" do
    put :update, id: @qiwo_subscription, qiwo_subscription: { days_active: @qiwo_subscription.days_active, id: @qiwo_subscription.id, months_active: @qiwo_subscription.months_active, places_allowed: @qiwo_subscription.places_allowed, price: @qiwo_subscription.price, products_allowed: @qiwo_subscription.products_allowed, subscription_name: @qiwo_subscription.subscription_name }
    assert_redirected_to qiwo_subscription_path(assigns(:qiwo_subscription))
  end

  test "should destroy qiwo_subscription" do
    assert_difference('QiwoSubscription.count', -1) do
      delete :destroy, id: @qiwo_subscription
    end

    assert_redirected_to qiwo_subscriptions_path
  end
end
