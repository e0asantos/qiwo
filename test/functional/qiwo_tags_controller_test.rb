require 'test_helper'

class QiwoTagsControllerTest < ActionController::TestCase
  setup do
    @qiwo_tag = qiwo_tags(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qiwo_tags)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qiwo_tag" do
    assert_difference('QiwoTag.count') do
      post :create, qiwo_tag: { id: @qiwo_tag.id, tag_name: @qiwo_tag.tag_name }
    end

    assert_redirected_to qiwo_tag_path(assigns(:qiwo_tag))
  end

  test "should show qiwo_tag" do
    get :show, id: @qiwo_tag
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qiwo_tag
    assert_response :success
  end

  test "should update qiwo_tag" do
    put :update, id: @qiwo_tag, qiwo_tag: { id: @qiwo_tag.id, tag_name: @qiwo_tag.tag_name }
    assert_redirected_to qiwo_tag_path(assigns(:qiwo_tag))
  end

  test "should destroy qiwo_tag" do
    assert_difference('QiwoTag.count', -1) do
      delete :destroy, id: @qiwo_tag
    end

    assert_redirected_to qiwo_tags_path
  end
end
