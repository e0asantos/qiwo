require 'fm_timbrado_cfdi'
require "base64"

FmTimbradoCfdi.configurar do |config|
  config.user_id = 'UsuarioPruebasWS'
  config.user_pass = 'b9ec2afa3361a59af4b4d102d3f704eabdf097d4'
  config.namespace = 'https://t2demo.facturacionmoderna.com/timbrado/soap'
  config.endpoint = 'https://t2demo.facturacionmoderna.com/timbrado/soap'
  config.fm_wsdl = 'https://t2demo.facturacionmoderna.com/timbrado/wsdl'
  config.log = 'logme'
  config.ssl_verify_mode = false
end # configurar

#hay que cargar el contenido del archivo en base64, este archivo o lo que sea es un texto plano
# serie|folio|Fecha|Lugar expedición|tipo documento|Forma de pago|Método de pago|condicion pago|Num cuenta pago|subtotal|descuento|total|moneda|tipo de cambio|no. certificado|
# RFC emisor|Razon social emisor|Régimen fiscal
# calle|numero exterior|numero interior|colonia|localidad|municipio|estado|pais|codigo postal
# calle sucursal|numero exterior sucursal|numero interior sucursal|colonia sucursal|localidad sucursal|municipio|estado|pais|codigo postal
# RFC receptor|razon social receptor
# calle|numero exterior|numero interior|colonia|localidad|municipio|estado|pais|cp
# CONCEPTOS|numero de conceptos
# |unidad medida|descripcion|cantidad|precio unitario|importe
# IMPUESTOS_TRASLADADOS|numero de impuestos traslados
# nombre impuesto|tasa|importe
# IMPUESTOS_RETENIDOS|numero de impuesto retenidos
# nombre|importe

fileToSend="AA|21|asignarFecha|Nuevo León, México|ingreso|Contado|Cheque|Pago en una sola Exhibición|No identificado|1498.00|0.00|1737.68|MXN|0.00|20001000000200000278|
TUMG620310R95|FACTURACION MODERNA SA DE CV|PERSONA MORAL REGIMEN GENERAL DE LEY.
RIO GUADALQUIVIR|238||ORIENTE DEL VALLE||San Pedro Garza Garcia|Nuevo León|México|66220
||||||||
XAXX010101000|PUBLICO EN GENERAL
CERRADA DE AZUCENAS|109||REFORMA||OAXACA DE JUAREZ|OAXACA|México|68050
CONCEPTOS|2
|PIEZA|CAJA DE HOJAS BLANCAS TAMAÑO CARTA|3|450.00|1350.00
|PIEZA|RECOPILADOR PASTA DURA 3 ARILLOS|8|18.50|148.00
IMPUESTOS_TRASLADADOS|1
IVA|16.00|239.68"
enc   = Base64.encode64(fileToSend)
#openssl x509 -inform DER -in "esi920427886_1210221507s.cer" -noout -serial
# respuesta = FmTimbradoCfdi.timbra_cfdi_layout 'ESI920427886', enc, false
client = Savon.client(:wsdl=> "https://t2demo.facturacionmoderna.com/timbrado/wsdl",:ssl_verify_mode=>:none,ssl_version: :TLSv1)
my_hash = { :first_name => 'Joe', :last_name => 'Blow', :email => 'joe@example.com'}
respuesta=client.call(:request_timbrar_cfdi,:message=>{:request=>{'emisorRFC'=>'ESI920427886','UserID'=>'UsuarioPruebasWS','UserPass'=>'b9ec2afa3361a59af4b4d102d3f704eabdf097d4','text2CFDI'=>enc}})
puts respuesta.inspect