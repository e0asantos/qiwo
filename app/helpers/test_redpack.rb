require 'savon'




# respuesta=client.call(:valida_usuario,:message=>{'sUsuario'=>'andressantos.consultware','sPwd'=>'ndrs2014'})

guia={'auxiliar'=>nil,
	'claveDex'=>nil,
	'consignatario'=>{
			'calle'=>'Camino Real',
			'ciudad'=>'San Luis Potosi',
			'codigoPostal'=>'78437',
			'colonia_Asentamiento'=>'Jardines del Valle',
			'contacto'=>'Jonathan Santos',
			'email'=>'asb.studios@gmail.com',
			'estado'=>'San Luis Potosi',
			'iata'=>'',
			'nombre_Compania'=>'Particula',
			'numeroExterior'=>'317',
			'numeroInterior'=>'',
			'pais'=>'Mexico',
			'referenciaUbicacion'=>'Cerca de punto A',
			'telefonos'=>nil
		},
	'costoSeguro'=>nil,
	'cotizaciones'=>nil,
	'fechaDocumentacion'=>nil,
	'fechaSituacion'=>nil,
	'flag'=>nil,
	'guiaAsegurada'=>nil,
	'moneda'=>'MXN',
	'numeroDeGuia'=>'237751979',
	'observaciones'=>nil,
	'paquetes'=>[{
			'alto'=>'20',
			'ancho'=>'20',
			'auxiliar'=>'',
			'consecutivo'=>'1',
			'descripcion'=>'un paquete',
			'detallesRastreo'=>'',
			'formatoEtiqueta'=>'',
			'largo'=>'20',
			'peso'=>'1.5'
		}],
	'personaRecibio'=>'',
	'pruebaDeEnterga'=>'',
	'referencia'=>nil,
	'remitente'=>{
			'calle'=>'Diaz Ordaz',
			'ciudad'=>'San Pedro Garza Garcia',
			'codigoPostal'=>'66215',
			'colonia_Asentamiento'=>'Unidad San Pedro',
			'contacto'=>'Andres Santos',
			'email'=>'andres.santos@gmail.com',
			'estado'=>'Nuevo Leon',
			'iata'=>'',
			'nombre_Compania'=>'Particular',
			'numeroExterior'=>'333',
			'numeroInterior'=>'',
			'pais'=>'Mexico',
			'referenciaUbicacion'=>'A un lado de axtel',
			'telefonos'=>nil
		},
	'situacion'=>nil,
	'tipoCambio'=>nil,
	'tipoEntrega'=>{'auxiliar'=>'',
			'descripcion'=>'',
			'equivalencia'=>'DOM',
			'id'=>'2'},
	'tipoEnvio'=>{'auxiliar'=>'',
			'descripcion'=>'',
			'equivalencia'=>'PAQ',
			'id'=>'1'},
	'tipoIdentificacion'=>nil,
	'tipoServicio'=>{'auxiliar'=>'',
			'descripcion'=>'',
			'equivalencia'=>'',
			'id'=>'1'},
	'valorDeclarado'=>nil}

typeIdDesc={'auxiliar'=>'',
			'descripcion'=>'',
			'equivalencia'=>'',
			'id'=>''}
typeDireccion={
	'calle'=>'',
	'ciudad'=>'',
	'codigoPostal'=>'',
	'colonia_Asentamiento'=>'',
	'contacto'=>'',
	'email'=>'',
	'estado'=>'',
	'iata'=>'',
	'nombre_Compania'=>'',
	'numeroExterior'=>'',
	'numeroInterior'=>'',
	'pais'=>'',
	'referenciaUbicacion'=>'',
	'telefonos'=>''
}
typeTelefono={
	'LADA'=>'',
	'extension'=>'',
	'telefono'=>''
}
typeCotizacion={
	'descripcion'=>'',
	'detallesCotizacion'=>'',
	'tarifa'=>'',
	'tiempoEntrega'=>'',
	'tiempoSobre'=>'',
	'tipoServicio'=>'',
}
typeDetalleCotizacion={
	'costoBase'=>'',
	'descripcion'=>''
}

typePaquete={
	'alto'=>'',
	'ancho'=>'',
	'auxiliar'=>'',
	'consecutivo'=>'',
	'descripcion'=>'',
	'detallesRastreo'=>'',
	'formatoEtiqueta'=>'',
	'largo'=>'',
	'peso'=>''
}

typeDetalleRastreo={
	'descripcionEvento'=>'',
	'evento'=>'',
	'fechaEvento'=>'',
	'localizacion'=>'',
	'observaciones'=>''
}
typeResultado={
	'descripcion'=>'',
	'estatus'=>'',
	'gravedad'=>''
}
guiaotr={'auxiliar'=>nil,
	'claveDex'=>nil,
	'consignatario'=>nil,
	'costoSeguro'=>nil,
	'cotizaciones'=>nil,
	'fechaDocumentacion'=>nil,
	'fechaSituacion'=>nil,
	'flag'=>nil,
	'guiaAsegurada'=>nil,
	'moneda'=>'MXN',
	'numeroDeGuia'=>'33333333',
	'observaciones'=>nil,
	'paquetes'=>nil,
	'personaRecibio'=>'',
	'pruebaDeEnterga'=>'',
	'referencia'=>nil,
	'remitente'=>nil,
	'situacion'=>nil,
	'tipoCambio'=>nil,
	'tipoEntrega'=>nil,
	'tipoEnvio'=>nil,
	'tipoIdentificacion'=>nil,
	'tipoServicio'=>nil,
	'valorDeclarado'=>nil}

# respuesta=client.call(:predocumentacion,:message=>{'PIN'=>'QA tf+edKE4WTniXVrBpeEhnqgjNdTDKnGAuEPsF8zJzzk=','idUsuario'=>85,'guias'=>guiaotr})

client = Savon.client(:wsdl=> "http://ws.redpack.com.mx/RedpackAPI_WS/services/RedpackWS?wsdl",:log=>true)
respuesta=client.call(:obtener_formato_de_impresion,xml:'<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> 
	<SOAP-ENV:Body> 
		<ns:predocumentacion xmlns:ns="http://ws.redpack.com"> 
			<ns:PIN>QA tf+edKE4WTniXVrBpeEhnqgjNdTDKnGAuEPsF8zJzzk=</ns:PIN> 
			<ns:idUsuario>85</ns:idUsuario> 
			<ns:guias> 
				<ax21:auxiliar xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:claveDex xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:consignatario xmlns:ax21="http://vo.redpack.com/xsd"> 
					<ax21:calle>camino real</ax21:calle> 
					<ax21:ciudad>san luis potosi</ax21:ciudad> 
					<ax21:codigoPostal>78437</ax21:codigoPostal> 
					<ax21:colonia_Asentamiento>jardines del valle</ax21:colonia_Asentamiento> 
					<ax21:contacto>contacto1 apellido</ax21:contacto> 
					<ax21:email>andres.santos@consultware.mx</ax21:email> 
					<ax21:estado>san luis potosi</ax21:estado> 
					<ax21:iata xsi:nil="true" /> 
					<ax21:nombre_Compania>ninguna</ax21:nombre_Compania> 
					<ax21:numeroExterior>317</ax21:numeroExterior> 
					<ax21:numeroInterior xsi:nil="true" /> 
					<ax21:pais>Mexico</ax21:pais> 
					<ax21:referenciaUbicacion xsi:nil="true" /> 
					<ax21:telefonos xsi:nil="true" /> 
				</ax21:consignatario> 
				<ax21:costoSeguro xmlns:ax21="http://vo.redpack.com/xsd">NaN</ax21:costoSeguro> 
				<ax21:cotizaciones xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:fechaDocumentacion xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:fechaSituacion xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:flag xmlns:ax21="http://vo.redpack.com/xsd">0</ax21:flag> 
				<ax21:guiaAsegurada xmlns:ax21="http://vo.redpack.com/xsd">false</ax21:guiaAsegurada> 
				<ax21:moneda xmlns:ax21="http://vo.redpack.com/xsd">MXN</ax21:moneda> 
				<ax21:numeroDeGuia xmlns:ax21="http://vo.redpack.com/xsd">22111111</ax21:numeroDeGuia> 
				<ax21:observaciones xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:paquetes xmlns:ax21="http://vo.redpack.com/xsd"> 
					<ax21:alto>30</ax21:alto> 
					<ax21:ancho>30</ax21:ancho> 
					<ax21:auxiliar xsi:nil="true" /> 
					<ax21:consecutivo>1</ax21:consecutivo> 
					<ax21:descripcion>un paquete</ax21:descripcion> 
					<ax21:detallesRastreo xsi:nil="true" /> 
					<ax21:formatoEtiqueta xsi:nil="true" /> 
					<ax21:largo>30</ax21:largo> 
					<ax21:peso>1.5</ax21:peso> 
				</ax21:paquetes> 
				<ax21:personaRecibio xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:pruebaDeEnterga xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:referencia xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:remitente xmlns:ax21="http://vo.redpack.com/xsd"> 
					<ax21:calle>Diaz Ordaz</ax21:calle> 
					<ax21:ciudad>san pedro garza garcia</ax21:ciudad> 
					<ax21:codigoPostal>66215</ax21:codigoPostal> 
					<ax21:colonia_Asentamiento>unidad san pedro</ax21:colonia_Asentamiento> 
					<ax21:contacto>contacto2 apellido</ax21:contacto> 
					<ax21:email>asb.studios@gmail.com</ax21:email> 
					<ax21:estado>nuevo leon</ax21:estado> 
					<ax21:iata xsi:nil="true" /> 
					<ax21:nombre_Compania>ninguna</ax21:nombre_Compania> 
					<ax21:numeroExterior>333</ax21:numeroExterior> 
					<ax21:numeroInterior xsi:nil="true" /> 
					<ax21:pais>Mexico</ax21:pais> 
					<ax21:referenciaUbicacion xsi:nil="true" /> 
					<ax21:telefonos xsi:nil="true" /> 
				</ax21:remitente> 
				<ax21:resultadoConsumoWS xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:situacion xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:tipoCambio xmlns:ax21="http://vo.redpack.com/xsd">NaN</ax21:tipoCambio> 
				<ax21:tipoEntrega xmlns:ax21="http://vo.redpack.com/xsd"> 
					<ax21:auxiliar xsi:nil="true" /> 
					<ax21:descripcion xsi:nil="true" /> 
					<ax21:equivalencia>DOM</ax21:equivalencia> 
					<ax21:id>2</ax21:id> 
				</ax21:tipoEntrega> 
				<ax21:tipoEnvio xmlns:ax21="http://vo.redpack.com/xsd"> 
					<ax21:auxiliar xsi:nil="true" /> 
					<ax21:descripcion xsi:nil="true" /> 
					<ax21:equivalencia>PAQ</ax21:equivalencia> 
					<ax21:id>1</ax21:id> 
				</ax21:tipoEnvio> 
				<ax21:tipoIdentificacion xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
				<ax21:tipoServicio xmlns:ax21="http://vo.redpack.com/xsd"> 
					<ax21:auxiliar xsi:nil="true" /> 
					<ax21:descripcion>EXPRESS</ax21:descripcion>
					<ax21:equivalencia xsi:nil="true" /> 
					<ax21:id>1</ax21:id> 
				</ax21:tipoServicio> 
				<ax21:valorDeclarado xmlns:ax21="http://vo.redpack.com/xsd">NaN</ax21:valorDeclarado> 
			</ns:guias> 
		</ns:predocumentacion> 
	</SOAP-ENV:Body> 
</SOAP-ENV:Envelope>')

# ahora regresamos el resultado
puts "------------"
# puts respuesta.body[:predocumentacion_response][:return][:resultado_consumo_ws]
puts respuesta.body[:obtener_formato_de_impresion_response][:return][:paquetes][:formato_etiqueta]
# puts respuesta.body[:obtenerFormatoDeImpresionResponse][:return][:resultado_consumo_ws]

# cuando hay error es status
# {:descripcion=>"Error en ejecuci\u00F3n de base de datos", :estatus=>"2", :gravedad=>"1", :"@xsi:type"=>"ax21:Resultado"}
# cuando ya esta hecha es
# {:descripcion=>"La guia ya esta predocumentada", :estatus=>"58", :gravedad=>"1", :"@xsi:type"=>"ax21:Resultado"}
# cuando es correcta es
# {:descripcion=>"Generaci\u00F3n correcta", :estatus=>"1", :gravedad=>"3", :"@xsi:type"=>"ax21:Resultado"}

puts "------------"

# respuesta=client.call(:obtiene_catalogo_tipo_servicio,:message=>{'PIN'=>'QA tf+edKE4WTniXVrBpeEhnqgjNdTDKnGAuEPsF8zJzzk=','idUsuario'=>85})