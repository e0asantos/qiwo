class RedpacksController < ApplicationController
  # GET /redpacks
  # GET /redpacks.json
  def index
    @redpacks = []
    if session[:activeUser].sys_rol==1
        @redpacks = ShippingGuide.where("label_guide is not null").paginate(:page => params[:page], :per_page => 10).order("created_at DESC")
      else
        @redpacks = ShippingGuide.where("label_guide is not null and (id_from="+session[:activeUser].id.to_s+" or id_to="+session[:activeUser].id.to_s+")").paginate(:page => params[:page], :per_page => 10).order("created_at DESC")
      end
    
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @redpacks }
    end
  end

  # GET /redpacks/1
  # GET /redpacks/1.json
  def show
    @redpack = Redpack.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @redpack }
    end
  end

  # GET /redpacks/new
  # GET /redpacks/new.json
  def new
    @redpack = Redpack.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @redpack }
    end
  end

  # GET /redpacks/1/edit
  def edit
    @redpack = Redpack.find(params[:id])
  end

  # POST /redpacks
  # POST /redpacks.json
  def allowGuide
    mensajes=Hash.new
    mensajes["type"]="Error"
    mensajes["mensaje"]="Ocurrió un error inesperado"
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    # verificar primero que tenga direccion de envio
    if QiwoEnterprise.where(:user_owner=>session[:activeUser].id,:is_shipping=>1).last==nil
      # no tiene direccion de envio
      mensajes["mensaje"]="Primero registra tu dirección de recolección en el menú de la izquierda antes de generar un número de rastreo."
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: mensajes.to_json }
      end
      return
    else 
      # puede que tenga ya una dirección, hay que ver si aun tiene folios
      foliosDeRastreo=ShippingGuide.where(:id_from=>session[:activeUser].id,:id_to=>nil,:gtype=>isInTestModeNumber()).count
      if foliosDeRastreo>0
        mensajes["type"]="OK"
        mensajes["mensaje"]="continue"  
      else
        mensajes["type"]="Error"
        mensajes["mensaje"]="No cuentas con suficientes folios para generar un número de guía, puedes adquirir los folios con un representante de Qiwo o comprandolos directamente en la tienda."
      end

      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: mensajes.to_json }
      end
      

    end

  end
  def create
    @redpack = Redpack.new(params[:redpack])

    respond_to do |format|
      if @redpack.save
        format.html { redirect_to @redpack, notice: 'Redpack was successfully created.' }
        format.json { render json: @redpack, status: :created, location: @redpack }
      else
        format.html { render action: "new" }
        format.json { render json: @redpack.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /redpacks/1
  # PUT /redpacks/1.json
  def update
    @redpack = Redpack.find(params[:id])

    respond_to do |format|
      if @redpack.update_attributes(params[:redpack])
        format.html { redirect_to @redpack, notice: 'Redpack was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @redpack.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /redpacks/1
  # DELETE /redpacks/1.json
  def destroy
    @redpack = Redpack.find(params[:id])
    @redpack.destroy

    respond_to do |format|
      format.html { redirect_to redpacks_url }
      format.json { head :no_content }
    end
  end
end
