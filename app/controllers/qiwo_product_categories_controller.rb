#!/bin/env ruby
# encoding: utf-8
class QiwoProductCategoriesController < ApplicationController
  # GET /qiwo_product_categories
  # GET /qiwo_product_categories.json
  def index
    @qiwo_product_categories = QiwoProductCategory.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_product_categories }
    end
  end

  # GET /qiwo_product_categories/1
  # GET /qiwo_product_categories/1.json
  def show
    @qiwo_product_category = QiwoProductCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_product_category }
    end
  end

  # GET /qiwo_product_categories/new
  # GET /qiwo_product_categories/new.json
  def new
    @qiwo_product_category = QiwoProductCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_product_category }
    end
  end

  # GET /qiwo_product_categories/1/edit
  def edit
    @qiwo_product_category = QiwoProductCategory.find(params[:id])
  end

  # POST /qiwo_product_categories
  # POST /qiwo_product_categories.json
  def create
    @qiwo_product_category = QiwoProductCategory.new(params[:qiwo_product_category])

    respond_to do |format|
      if @qiwo_product_category.save
        format.html { redirect_to @qiwo_product_category, notice: 'Qiwo product category was successfully created.' }
        format.json { render json: @qiwo_product_category, status: :created, location: @qiwo_product_category }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_product_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_product_categories/1
  # PUT /qiwo_product_categories/1.json
  def update
    @qiwo_product_category = QiwoProductCategory.find(params[:id])

    respond_to do |format|
      if @qiwo_product_category.update_attributes(params[:qiwo_product_category])
        format.html { redirect_to @qiwo_product_category, notice: 'Qiwo product category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_product_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_product_categories/1
  # DELETE /qiwo_product_categories/1.json
  def destroy
    @qiwo_product_category = QiwoProductCategory.find(params[:id])
    @qiwo_product_category.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_product_categories_url }
      format.json { head :no_content }
    end
  end
end
