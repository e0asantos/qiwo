class MailtemplatesController < ApplicationController
  # GET /mailtemplates
  # GET /mailtemplates.json
  def index
    @mailtemplates = Mailtemplate.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @mailtemplates }
    end
  end

  # GET /mailtemplates/1
  # GET /mailtemplates/1.json
  def show
    @mailtemplate = Mailtemplate.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @mailtemplate }
    end
  end

  # GET /mailtemplates/new
  # GET /mailtemplates/new.json
  def new
    @mailtemplate = Mailtemplate.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @mailtemplate }
    end
  end

  # GET /mailtemplates/1/edit
  def edit
    @mailtemplate = Mailtemplate.find(params[:id])
  end

  # POST /mailtemplates
  # POST /mailtemplates.json
  def create
    @mailtemplate = Mailtemplate.new(params[:mailtemplate])

    respond_to do |format|
      if @mailtemplate.save
        format.html { redirect_to @mailtemplate, notice: 'Mailtemplate was successfully created.' }
        format.json { render json: @mailtemplate, status: :created, location: @mailtemplate }
      else
        format.html { render action: "new" }
        format.json { render json: @mailtemplate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /mailtemplates/1
  # PUT /mailtemplates/1.json
  def update
    @mailtemplate = Mailtemplate.find(params[:id])

    respond_to do |format|
      if @mailtemplate.update_attributes(params[:mailtemplate])
        format.html { redirect_to @mailtemplate, notice: 'Mailtemplate was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @mailtemplate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mailtemplates/1
  # DELETE /mailtemplates/1.json
  def destroy
    @mailtemplate = Mailtemplate.find(params[:id])
    @mailtemplate.destroy

    respond_to do |format|
      format.html { redirect_to mailtemplates_url }
      format.json { head :no_content }
    end
  end
end
