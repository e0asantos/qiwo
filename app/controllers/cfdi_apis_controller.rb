#!/bin/env ruby
# encoding: utf-8
class CfdiApisController < ApplicationController
  # GET /cfdi_apis
  # GET /cfdi_apis.json
  def index
    @cfdi_apis = CfdiApi.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @cfdi_apis }
    end
  end

  # GET /cfdi_apis/1
  # GET /cfdi_apis/1.json
  def show
    @cfdi_api = CfdiApi.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @cfdi_api }
    end
  end

  # GET /cfdi_apis/new
  # GET /cfdi_apis/new.json
  def new
    @cfdi_api = CfdiApi.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @cfdi_api }
    end
  end

  # GET /cfdi_apis/1/edit
  def edit
    @cfdi_api = CfdiApi.find(params[:id])
  end

  # POST /cfdi_apis
  # POST /cfdi_apis.json
  def create
    @cfdi_api = CfdiApi.new(params[:cfdi_api])

    respond_to do |format|
      if @cfdi_api.save
        format.html { redirect_to @cfdi_api, notice: 'Cfdi api was successfully created.' }
        format.json { render json: @cfdi_api, status: :created, location: @cfdi_api }
      else
        format.html { render action: "new" }
        format.json { render json: @cfdi_api.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /cfdi_apis/1
  # PUT /cfdi_apis/1.json
  def update
    @cfdi_api = CfdiApi.find(params[:id])

    respond_to do |format|
      if @cfdi_api.update_attributes(params[:cfdi_api])
        format.html { redirect_to @cfdi_api, notice: 'Cfdi api was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @cfdi_api.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cfdi_apis/1
  # DELETE /cfdi_apis/1.json
  def destroy
    @cfdi_api = CfdiApi.find(params[:id])
    @cfdi_api.destroy

    respond_to do |format|
      format.html { redirect_to cfdi_apis_url }
      format.json { head :no_content }
    end
  end
end
