class QiwoCfdisController < ApplicationController
  # GET /qiwo_cfdis
  # GET /qiwo_cfdis.json
  def index
    @qiwo_cfdis = QiwoCfdi.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_cfdis }
    end
  end

  # GET /qiwo_cfdis/1
  # GET /qiwo_cfdis/1.json
  def show
    @qiwo_cfdi = QiwoCfdi.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_cfdi }
    end
  end

  # GET /qiwo_cfdis/new
  # GET /qiwo_cfdis/new.json
  def new
    @qiwo_cfdi = QiwoCfdi.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_cfdi }
    end
  end

  # GET /qiwo_cfdis/1/edit
  def edit
    @qiwo_cfdi = QiwoCfdi.find(params[:id])
  end

  # POST /qiwo_cfdis
  # POST /qiwo_cfdis.json
  def create
    @qiwo_cfdi = QiwoCfdi.new(params[:qiwo_cfdi])

    respond_to do |format|
      if @qiwo_cfdi.save
        format.html { redirect_to @qiwo_cfdi, notice: 'Qiwo cfdi was successfully created.' }
        format.json { render json: @qiwo_cfdi, status: :created, location: @qiwo_cfdi }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_cfdi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_cfdis/1
  # PUT /qiwo_cfdis/1.json
  def update
    @qiwo_cfdi = QiwoCfdi.find(params[:id])

    respond_to do |format|
      if @qiwo_cfdi.update_attributes(params[:qiwo_cfdi])
        format.html { redirect_to @qiwo_cfdi, notice: 'Qiwo cfdi was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_cfdi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_cfdis/1
  # DELETE /qiwo_cfdis/1.json
  def destroy
    @qiwo_cfdi = QiwoCfdi.find(params[:id])
    @qiwo_cfdi.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_cfdis_url }
      format.json { head :no_content }
    end
  end
end
