#!/bin/env ruby
# encoding: utf-8
class QiwoRanksController < ApplicationController
  # GET /qiwo_ranks
  # GET /qiwo_ranks.json
  def index
    @qiwo_ranks = QiwoRank.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_ranks }
    end
  end

  # GET /qiwo_ranks/1
  # GET /qiwo_ranks/1.json
  def show
    @qiwo_rank = QiwoRank.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_rank }
    end
  end

  # GET /qiwo_ranks/new
  # GET /qiwo_ranks/new.json
  def new
    @qiwo_rank = QiwoRank.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_rank }
    end
  end

  # GET /qiwo_ranks/1/edit
  def edit
    @qiwo_rank = QiwoRank.find(params[:id])
  end

  # POST /qiwo_ranks
  # POST /qiwo_ranks.json
  def create
    @qiwo_rank = QiwoRank.new(params[:qiwo_rank])

    respond_to do |format|
      if @qiwo_rank.save
        format.html { redirect_to @qiwo_rank, notice: 'Qiwo rank was successfully created.' }
        format.json { render json: @qiwo_rank, status: :created, location: @qiwo_rank }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_rank.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_ranks/1
  # PUT /qiwo_ranks/1.json
  def update
    @qiwo_rank = QiwoRank.find(params[:id])

    respond_to do |format|
      if @qiwo_rank.update_attributes(params[:qiwo_rank])
        format.html { redirect_to @qiwo_rank, notice: 'Qiwo rank was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_rank.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_ranks/1
  # DELETE /qiwo_ranks/1.json
  def destroy
    @qiwo_rank = QiwoRank.find(params[:id])
    @qiwo_rank.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_ranks_url }
      format.json { head :no_content }
    end
  end
end
