#!/bin/env ruby
# encoding: utf-8
require 'rubygems'
require 'json'
require "uuidtools"
require 'net/http'
class NfcReadersController < ApplicationController
  # GET /nfc_readers
  # GET /nfc_readers.json
  def index
    @nfc_readers = NfcReader.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @nfc_readers }
    end
  end

  # GET /nfc_readers/1
  # GET /nfc_readers/1.json
  def show
    @nfc_reader = NfcReader.where(:id=>1).last
    fbtoken=params[:token]
    mclient=params[:mclient]
    #por aqui solo entran los telefonos
    response = open('https://graph.facebook.com/me?fields=id%2Cname&method=GET&format=json&suppress_http_code=1&access_token='+fbtoken)
    jsonr= JSON.parse(response.read)
    if jsonr["error"]==nil
      #si no hay error entonces hay que obtener el id del usuario
      uid=jsonr["id"].to_s
      if mclient=="ax"
         uri = URI('http://servicio.axesovip.com.mx/ServiceAxeso.svc/InicioSesion?usuario=none&pass=none&fb='+uid+'&tipo=true')
         from_service=Net::HTTP.get(uri) # => String
         jparse=JSON.parse(from_service)
         # {"InicioSesionResult":[]}
         if jparse['InicioSesionResult'].length>0
            # send_data "NOT_FOUND_SORRY\n"
         mynfc=NfcReader.new
          sttoken=UUIDTools::UUID.random_create.to_s.delete("-")
          mynfc.static_token=jparse['InicioSesionResult'][0]["user_uid"]
          mynfc.nfc_type=1
          mynfc.blocked=0
          respond_to do |format|
            format.json { render json: mynfc }  
          end
         end
         
      end
      #en este punto vamos a buscar que ese usuario tenga un token de nfc registrado
      usuario=Sys_User.find_by_user_id(uid)
      if usuario!=nil
        #hay que verificar que si tiene token
        if NfcReader.where(:user_id=>usuario.id).last!=nil
          #regresamos su id
          respond_to do |format|
            format.json { render json: NfcReader.where(:user_id=>usuario.id).last }
          end
          return
        else
          #el usuario no tiene NFC, se lo creamos
          mynfc=NfcReader.new
          mynfc.user_id=usuario.id
          sttoken=UUIDTools::UUID.random_create.to_s.delete("-")
          mynfc.static_token=sttoken
          mynfc.nfc_type=1
          mynfc.blocked=0
          mynfc.save
          respond_to do |format|
          format.json { render json: mynfc }  
        end
        
        return
        end
      else 
        msgerror="No existe el usuario en Qiwo"
        respond_to do |format|
          format.json { render json: msgerror }  
        end
        
        return
      end
      #si no tiene token registrado le creamos uno y se lo regresamos
      

    else
       msgerror="No se pudo corroborar tu identidar, vuelve a intentarlo"
        respond_to do |format|
          format.json { render json: msgerror }  
        end
        
        return
    end

    respond_to do |format|
      format.json { render json: @nfc_reader }
    end
  end

  # GET /nfc_readers/new
  # GET /nfc_readers/new.json
  def new
    @nfc_reader = NfcReader.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @nfc_reader }
    end
  end

  # GET /nfc_readers/1/edit
  def edit
    @nfc_reader = NfcReader.find(params[:id])
  end

  # POST /nfc_readers
  # POST /nfc_readers.json
  def create
    @nfc_reader = NfcReader.new(params[:nfc_reader])

    respond_to do |format|
      if @nfc_reader.save
        format.html { redirect_to @nfc_reader, notice: 'Nfc reader was successfully created.' }
        format.json { render json: @nfc_reader, status: :created, location: @nfc_reader }
      else
        format.html { render action: "new" }
        format.json { render json: @nfc_reader.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /nfc_readers/1
  # PUT /nfc_readers/1.json
  def update
    @nfc_reader = NfcReader.find(params[:id])

    respond_to do |format|
      if @nfc_reader.update_attributes(params[:nfc_reader])
        format.html { redirect_to @nfc_reader, notice: 'Nfc reader was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @nfc_reader.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nfc_readers/1
  # DELETE /nfc_readers/1.json
  def destroy
    @nfc_reader = NfcReader.find(params[:id])
    @nfc_reader.destroy

    respond_to do |format|
      format.html { redirect_to nfc_readers_url }
      format.json { head :no_content }
    end
  end
end
