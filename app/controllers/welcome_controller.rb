#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/sys_user.rb'
require '/usr/src/qiwo/app/models/qiwo_log_visit.rb'
require '/usr/src/qiwo/app/models/qiwo_log_search.rb'
require '/usr/src/qiwo/app/models/qiwo_category.rb'
require '/usr/src/qiwo/app/models/qiwo_image.rb'
require '/usr/src/qiwo/app/models/qiwo_log_kart.rb'
require 'open-uri'
require 'json'
require 'timeout'
class WelcomeController < ApplicationController

  def suscribir
    nemail=Subscription.new
    nemail.email=params[:email]
    nemail.save
    respond_to do |format|
      format.html { redirect_to "/", notice: 'Gracias por suscribirte.' }
    end
    
  end
  def index
  	puts "---opening index--"
  	qiwoUser=session[:line_access_user]
    begin
    status = Timeout::timeout(3) {
  	regionService = JSON.parse(open("http://freegeoip.net/json/"+request.remote_ip.to_s).read)
  	puts regionService
  	puts regionService[:country_name]
  	puts regionService["country_name"]

  	if qiwoUser!=nil
  		#if the user is not empty save the data
  		newVisit=QiwoLogVisit.new(:ip_user=>request.remote_ip,:facebook_id=>qiwoUser.user_id,:user_id=>qiwoUser.id,:country=>regionService["country_name"],:city=>regionService["region_name"]+","+regionService["city"])
  		newVisit.save
  	else
  		newVisit=QiwoLogVisit.new(:ip_user=>request.remote_ip,:country=>regionService["country_name"],:city=>regionService["region_name"]+","+regionService["city"])
  		newVisit.save
  	end
    }
     rescue Exception => e
      
    end
    render layout: false
  end

  def search
  	puts "---defining search---"
  	#insertamos la busqueda
  	regionService = JSON.parse(open("http://freegeoip.net/json/"+request.remote_ip.to_s).read)
  	qiwoUser=session[:line_access_user]
  	if qiwoUser!=nil
  		#if the user is not empty save the data
  		@qiwoSearch=QiwoLogSearch.new(:ip_user=>request.remote_ip.to_s,:facebook_id=>qiwoUser.user_id,:user_id=>qiwoUser.id,:search_term=>params[:seed],:country=>regionService["country_name"],:city=>regionService["region_name"]+","+regionService["city"])
  		@qiwoSearch.save
  	else
  		@qiwoSearch=QiwoLogSearch.new(:ip_user=>request.remote_ip.to_s,:search_term=>params[:seed],:country=>regionService["country_name"],:city=>regionService["region_name"]+","+regionService["city"])
  		@qiwoSearch.save
  	end
  	@qiwo_visitors=QiwoLogVisit.all
  	respond_to do |format|
      format.html { render action: "index" }
      format.json { render json: @qiwo_visitors }
    end
  end
end
