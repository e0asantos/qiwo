#!/bin/env ruby
# encoding: utf-8
class QiwoCouponsController < ApplicationController
  # GET /qiwo_coupons
  # GET /qiwo_coupons.json
  def index
    @qiwo_coupons = QiwoCoupon.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_coupons }
    end
  end

  # GET /qiwo_coupons/1
  # GET /qiwo_coupons/1.json
  def show
    @qiwo_coupon = QiwoCoupon.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_coupon }
    end
  end

  # GET /qiwo_coupons/new
  # GET /qiwo_coupons/new.json
  def new
    @qiwo_coupon = QiwoCoupon.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_coupon }
    end
  end

  # GET /qiwo_coupons/1/edit
  def edit
    @qiwo_coupon = QiwoCoupon.find(params[:id])
  end

  # POST /qiwo_coupons
  # POST /qiwo_coupons.json
  def create
    @qiwo_coupon = QiwoCoupon.new(params[:qiwo_coupon])

    respond_to do |format|
      if @qiwo_coupon.save
        format.html { redirect_to @qiwo_coupon, notice: 'Qiwo coupon was successfully created.' }
        format.json { render json: @qiwo_coupon, status: :created, location: @qiwo_coupon }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_coupon.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_coupons/1
  # PUT /qiwo_coupons/1.json
  def update
    @qiwo_coupon = QiwoCoupon.find(params[:id])

    respond_to do |format|
      if @qiwo_coupon.update_attributes(params[:qiwo_coupon])
        format.html { redirect_to @qiwo_coupon, notice: 'Qiwo coupon was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_coupon.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_coupons/1
  # DELETE /qiwo_coupons/1.json
  def destroy
    @qiwo_coupon = QiwoCoupon.find(params[:id])
    @qiwo_coupon.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_coupons_url }
      format.json { head :no_content }
    end
  end
end
