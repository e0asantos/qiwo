#!/bin/env ruby
# encoding: utf-8
class QiwoImagesController < ApplicationController
  # GET /qiwo_images.
  # GET /qiwo_images.json
  def index
    
    @qiwo_images = QiwoImage.all
    redirect_to "/"
    # respond_to do |format|
    #   format.html # index.html.erb
    #   format.json { render json: @qiwo_images }
    # end
  end

  # GET /qiwo_images/1
  # GET /qiwo_images/1.json
  def show
    #checar a quien pertenece el codigo de barras
    @proveedor="oxxo"
    if QiwoLogKart.where(:openpay_stores=>params[:id]).last!=nil
      #entonces es de openpay
      @proveedor="openpay"
    end
    @fake_id=params[:id]
    @qiwo_image = QiwoImage.find(1)
    render layout: false
  end

  # GET /qiwo_images/new
  # GET /qiwo_images/new.json
  def new
    redirect_to "/"
    # @qiwo_image = QiwoImage.new

    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @qiwo_image }
    # end
  end

  # GET /qiwo_images/1/edit
  def edit
    redirect_to "/"
    # @qiwo_image = QiwoImage.find(params[:id])
  end

  # POST /qiwo_images
  # POST /qiwo_images.json
  def create
    @qiwo_image = QiwoImage.new(params[:qiwo_image])
    @qiwo_image.save
    redirect_to "/"
    # respond_to do |format|
    #   if @qiwo_image.save
    #     # format.html { redirect_to @qiwo_image, notice: 'Qiwo image was successfully created.' }
    #     # format.json { render json: @qiwo_image, status: :created, location: @qiwo_image }
    #     redirect_to "/"
    #   else
    #     # format.html { render action: "new" }
    #     # format.json { render json: @qiwo_image.errors, status: :unprocessable_entity }
    #     redirect_to "/"
    #   end
    
  end

  # PUT /qiwo_images/1
  # PUT /qiwo_images/1.json
  def update
    redirect_to "/"
    # @qiwo_image = QiwoImage.find(params[:id])

    # respond_to do |format|
    #   if @qiwo_image.update_attributes(params[:qiwo_image])
    #     format.html { redirect_to @qiwo_image, notice: 'Qiwo image was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: "edit" }
    #     format.json { render json: @qiwo_image.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /qiwo_images/1
  # DELETE /qiwo_images/1.json
  def destroy
    redirect_to "/"
    # @qiwo_image = QiwoImage.find(params[:id])
    # @qiwo_image.destroy

    # respond_to do |format|
    #   format.html { redirect_to qiwo_images_url }
    #   format.json { head :no_content }
    # end
  end
end
