#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/qiwo_product.rb'
require '/usr/src/qiwo/app/models/qiwo_log_kart.rb'
require '/usr/src/qiwo/app/models/qiwo_review.rb'
require '/usr/src/qiwo/app/models/qiwo_enterprise.rb'
require '/usr/src/qiwo/app/models/sys_user.rb'
require '/usr/src/qiwo/app/models/qiwo_log_visit.rb'
require '/usr/src/qiwo/app/models/qiwo_log_search.rb'
require '/usr/src/qiwo/app/models/qiwo_category.rb'
require '/usr/src/qiwo/app/models/qiwo_image.rb'
require '/usr/src/qiwo/app/controllers/notifications.rb'
require 'open-uri'
require 'json'
require 'xmpp4r_facebook'
require 'xmpp4r/roster'
require 'fb_graph'
require 'timeout'
class SearchesController < ApplicationController
  # GET /searches
  # GET /searches.json
  def index
    redirect_to "/"
    return
    @searches = Search.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @searches }
    end
  end

  # GET /searches/1.
  # GET /searches/1.json
  def show
    qiwoUser=session[:activeUser]
    begin
    status = Timeout::timeout(3) {
    regionService = JSON.parse(open("http://freegeoip.net/json/"+request.remote_ip.to_s).read)
    if qiwoUser!=nil
      #if the user is not empty save the data
      @qiwoSearch=QiwoLogSearch.new(:ip_user=>request.remote_ip.to_s,:facebook_id=>qiwoUser.user_id,:user_id=>qiwoUser.id,:search_term=>params[:id],:country=>regionService["country_name"],:city=>regionService["region_name"]+","+regionService["city"])
      @qiwoSearch.save
    else
        #modificar la semilla
        
        @qiwoSearch=QiwoLogSearch.new(:ip_user=>request.remote_ip.to_s,:search_term=>params[:id],:country=>regionService["country_name"],:city=>regionService["region_name"]+","+regionService["city"])
      @qiwoSearch.save
      
   
    end
  }
   rescue Exception => e
      
    end
    @productSearch=globalQiwoSearch(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @productSearch }
    end
  end

  # GET /searches/new
  # GET /searches/new.json
  def new
    redirect_to "/"
    return
    @search = Search.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @search }
    end
  end

  # GET /searches/1/edit
  def edit
      @search=QiwoProduct.find_by_id(params[:id])
      render layout: false
  end

  # POST /searches
  # POST /searches.json
  def create
    redirect_to "/"
    return
    @search = Search.new(params[:search])

    respond_to do |format|
      if @search.save
        format.html { redirect_to @search, notice: 'Search was successfully created.' }
        format.json { render json: @search, status: :created, location: @search }
      else
        format.html { render action: "new" }
        format.json { render json: @search.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /searches/1
  # PUT /searches/1.json
  def update
    redirect_to "/"
    return
    @search = Search.find(params[:id])

    respond_to do |format|
      if @search.update_attributes(params[:search])
        format.html { redirect_to @search, notice: 'Search was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @search.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /searches/1
  # DELETE /searches/1.json
  def destroy
    redirect_to "/"
    return
    @search = Search.find(params[:id])
    @search.destroy

    respond_to do |format|
      format.html { redirect_to searches_url }
      format.json { head :no_content }
    end
  end
    
    
  def sendToInbox
      puts params.inspect
      puts "sendToInbox was executed"
      qiwoUser=session[:activeUser]
      if qiwoUser!=nil
        Notifications.sendInfoToInbox(session,params[:prodid])
          puts "enviando mensaje"
      end
      render :nothing => true, :status => 200
  end
end
