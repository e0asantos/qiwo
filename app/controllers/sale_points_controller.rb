#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/qiwo_product.rb'
require '/usr/src/qiwo/app/models/qiwo_enterprise.rb'
require '/usr/src/qiwo/app/models/qiwo_log_kart.rb'
require '/usr/src/qiwo/app/models/qiwo_order.rb'
require "conekta"
require "uuidtools"
class SalePointsController < ApplicationController
  protect_from_forgery :except => [:searchByProduct, :searchBySKU,:createNewOrder,:getOrderItems,:addProductToOrder,:getTaxAddons,:setTaxAddons,:closeOrder,:create,:validateOrderPayment,:closeWithNoInvoice]

  attr_accessor:qiwo_products

  def closeWithNoInvoice
    #aqui cerramos la orden 
    #primero verificar que la orden sea del usuario
    ordenID=params[:orderadd]
    orden=QiwoOrder.where(:salesforce_user_id=>session[:activeUser].id,:id=>ordenID)
    if orden.length>0
      #ahora cerrar las ordenes
      orden.each do |singleOrden|
        singleOrden.order_state=13
        singleOrden.save
        itemsInside=QiwoLogKart.where(:qiwo_order_id=>singleOrden.id)
          itemsInside.each do |singleItem|
            singleItem.kart_state=3
            singleItem.save
            #hay que calcular el stock restante
            # comentamos la siguiente linea por que no se pago entonces no hace falta cerrar el stock
            # thisProduct.save
        end
        #buscamos los karts y los avanzamos a status 3
      end
    end
    render :nothing => true, :status => 200
  end
  def validateOrderPayment
    orderId=params[:orderaddp]
    destino=params[:cardd]
    openOrders=nil
    if destino!=-1 and destino!="-1"
      openOrders=QiwoLogKart.select("kart_state as order_state").where(:user_id=>session[:activeUser].id,:id=>orderId).last
    else
      openOrders=QiwoOrder.select("order_state").where(:salesforce_user_id=>session[:activeUser].id,:id=>orderId).last
    end
    respond_to do |format|
      format.html { render :json => openOrders.to_json }
      format.xml  { render :xml => openOrders.to_json }
      format.json { render :json => openOrders.to_json }
    end
  end

  def closeOrder
    orderId=params[:orderadd]
    action=params[:closeType]
    openOrdersByEnterprise=QiwoOrder.where(:salesforce_user_id=>session[:activeUser].id,:id=>orderId).last
    if action=="cancel"
      #en lugar de cancelar una cuenta solo la cerramos usando otro status de venta
      #buscamos items de la orden
      itemsInside=QiwoLogKart.where(:qiwo_order_id=>openOrdersByEnterprise.id)
      itemsInside.each do |singleItem|
        singleItem.kart_state=10
        singleItem.save
      end
      #y cerramos la orden
      openOrdersByEnterprise.order_state=10
      openOrdersByEnterprise.save
    elsif action=="closeCash"
      itemsInside=QiwoLogKart.where(:qiwo_order_id=>openOrdersByEnterprise.id)
      itemsInside.each do |singleItem|
        fuenteProducto=QiwoProduct.where(:id=>singleItem.qiwo_product_id).last
        singleItem.final_sale=finalProductPriceNoTax(singleItem.id)*singleItem.quantity
        singleItem.final_tax=getTax(singleItem.final_sale)
        singleItem.final_total=finalProductPrice(singleItem.id)*singleItem.quantity
        singleItem.final_gain=(singleItem.final_sale-(fuenteProducto.raw_price*singleItem.quantity))
        singleItem.discount=fuenteProducto.discount_amount
        fuenteProducto.stock_items=fuenteProducto.stock_items-singleItem.quantity
        if fuenteProducto.stock_items<=0
          fuenteProducto.stock_items=0
          fuenteProducto.save  
          #notificar si llegamos a cero
          Notifications.notificateNoStock(session,singleItem.store_id)
        end
        fuenteProducto.save
        singleItem.kart_state=3
        singleItem.save
        #hay que calcular el stock restante
        # thisProduct=QiwoProduct.where(:id=>singleItem.qiwo_product_id).last
        # thisProduct.stock_items=thisProduct.stock_items-singleItem.quantity
        # if thisProduct.stock_items<0
        #   thisProduct.stock_items=0
        # end
        # thisProduct.save
      end
      openOrdersByEnterprise.order_state=11
      openOrdersByEnterprise.save

    elsif action=="closeCard"
        
    end
      
    res="LOL"
    respond_to do |format|
      format.html { render :json => res.to_json }
      format.xml  { render :xml => res.to_json }
      format.json { render :json => res.to_json }
    end
  end

  def getTaxAddons
    orderId=params[:orderadd]
    openOrdersByEnterprise=QiwoOrder.select("tip_option,tax_option").where(:salesforce_user_id=>session[:activeUser].id,:id=>orderId).last
    #actualizar opciones de la orden
    puts openOrdersByEnterprise.to_json
    respond_to do |format|
      format.html { render :json => openOrdersByEnterprise.to_json }
      format.xml  { render :xml => openOrdersByEnterprise.to_json }
      format.json { render :json => openOrdersByEnterprise.to_json }
    end
  end

  def setTaxAddons
    orderId=params[:orderadd]
    tipOption=params[:tipSelected]
    taxOption=params[:taxSelected]
    openOrdersByEnterprise=QiwoOrder.where(:salesforce_user_id=>session[:activeUser].id,:id=>orderId).last
    
    openOrdersByEnterprise.tip_option=tipOption;
    openOrdersByEnterprise.tax_option=taxOption;
    openOrdersByEnterprise.save
    puts "------------------"
    puts openOrdersByEnterprise.inspect
    puts "------------------"

    respond_to do |format|
      format.html { render :json => openOrdersByEnterprise.to_json }
      format.xml  { render :xml => openOrdersByEnterprise.to_json }
      format.json { render :json => openOrdersByEnterprise.to_json }
    end
  end
  def addProductToOrder
    orderId=params[:orderadd]
    newItemId=params[:productadd]
    action=params[:moreless]
    producto=QiwoProduct.find_by_id(newItemId)
    #antes de proceder asegurar que la orden no este cerrada
    if QiwoOrder.where(:id=>orderId,:order_state=>1).last!=nil
      #primero buscamos si ese item ya esta ahi
      itemsInOrder=QiwoLogKart.where(:qiwo_product_id=>newItemId,:qiwo_order_id=>orderId).last
      if itemsInOrder==nil
        #creamos el item de venta
        itemsInOrder=QiwoLogKart.new(:qiwo_product_id=>newItemId,:qiwo_order_id=>orderId,:user_id=>session[:activeUser].id,:store_id=>producto.business_source)
        itemsInOrder.save
        itemsInOrder.quantity=0
        itemsInOrder.kart_state=1
        itemsInOrder.save
      end
      ##hay que checar si tenemos stock
      if action=="0" or action==0
        itemsInOrder.quantity=itemsInOrder.quantity-1
        itemsInOrder.save
        if itemsInOrder.quantity<=0
          itemsInOrder.destroy
        end
      else
        if producto.stock_items>=itemsInOrder.quantity+1
          itemsInOrder.quantity=itemsInOrder.quantity+1
          itemsInOrder.save  
        end
        
      end
    end
    res="LOL"
    respond_to do |format|
      format.html { render :json => res.to_json }
      format.xml  { render :xml => res.to_json }
      format.json { render :json => res.to_json }
    end
  end
  def searchByProduct
    #productos={:movies=>[{:id=> 1, :title=> "option1 de qiwo"},{:id=>2, :title=> "option2 de qiwo"},{:id=> 3, :title=> "option3 qiwo"}],:total=>3}
    productos={:movies=>[],:total=>3}
    searchParam=params[:q]
    peliculas=productos[:movies]
    misNegocios=QiwoEnterprise.where(:user_owner=>session[:activeUser].id)
    misNegociosIndex=misNegocios.index_by(&:id).keys
    @qiwo_products=QiwoProduct.where(:business_source=>misNegociosIndex).where("name like '%"+searchParam+"%' or description like '%"+searchParam+"%'")
    @qiwo_products.each do |singleProduct|
      peliculas.push({:id=>singleProduct.id,:title=>singleProduct.name})
    end
    productos[:movies]=peliculas
    respond_to do |format|
      format.html { render :json => productos.to_json }
      format.xml  { render :xml => productos.to_json }
      format.json { render :json => productos.to_json }
    end
  end

  def searchBySKU
    searchParam=params[:q]
    orderId=params[:orderadd]
    misNegocios=QiwoEnterprise.where(:user_owner=>session[:activeUser].id)
    misNegociosIndex=misNegocios.index_by(&:id).keys
    @qiwo_products=QiwoProduct.where(:business_source=>misNegociosIndex).where(:sku=>searchParam.to_i)
    lastProduct=@qiwo_products.last
    itemsInOrder=nil
    if lastProduct!=nil
      itemsInOrder=QiwoLogKart.where(:qiwo_product_id=>lastProduct.id,:qiwo_order_id=>orderId).last  
       if itemsInOrder==nil
        #creamos el item de venta
        itemsInOrder=QiwoLogKart.new(:qiwo_product_id=>lastProduct.id,:qiwo_order_id=>orderId,:user_id=>session[:activeUser].id,:store_id=>lastProduct.business_source)
        itemsInOrder.save
        itemsInOrder.quantity=1
        itemsInOrder.kart_state=1
        itemsInOrder.save
      else
        itemsInOrder.quantity=itemsInOrder.quantity+1
        itemsInOrder.save
      end
    end
    

   

    respond_to do |format|
      format.html { render :json => @qiwo_products.to_json }
      format.xml  { render :xml => @qiwo_products.to_json }
      format.json { render :json => @qiwo_products.to_json }
    end
  end

  def createNewOrder
    #order_state = 10 cancelado
    #order_state = 12 pagado con tarjeta
    #order_state = 11 pagado con efectivo
    #order_state = 13 cerrado (quiere decir ya no la vemos) 
    #creamos orden nueva y regresamos todas las ordenes abiertas
    newOrder=nil
    if params[:cprocess]=="start"
      newOrder=QiwoOrder.new
      newOrder.inreference=UUIDTools::UUID.random_create.to_s
      newOrder.salesforce_user_id=session[:activeUser].id
      newOrder.order_state=1
      #consultamos el folio y lo ponemos aqui
      # contadorFolio=QiwoCounter.where(:qiwo_enterprise_id=>)
      # contadorFolio.folio=
      newOrder.save
      newOrder.tax_option=0
      newOrder.tip_option=0
      newOrder.save  
    end
    
    misNegocios=QiwoEnterprise.where(:user_owner=>session[:activeUser].id)
    misNegociosIndex=misNegocios.index_by(&:id).keys
    openOrdersByEnterprise=QiwoOrder.where(:qiwo_enterprise_id=>misNegociosIndex)
    openOrdersBySalesForce=QiwoOrder.where(:salesforce_user_id=>session[:activeUser].id)
    openOrdersByEnterprise=openOrdersByEnterprise.where_values.reduce(:and)
    openOrdersBySalesForce=openOrdersBySalesForce.where_values.reduce(:and)
    openOrders=QiwoOrder.where(openOrdersByEnterprise.or(openOrdersBySalesForce)).where("order_state!=10 and order_state!=13")
    #alojamos la nueva orden y la regresamos

    respond_to do |format|
      format.html { render :json => openOrders.to_json }
      format.xml  { render :xml => openOrders.to_json }
      format.json { render :json => openOrders.to_json }
    end
  end
  # GET /sale_points
  # GET /sale_points.json
  def index
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_enterprise=QiwoEnterprise.new
    misNegocios=QiwoEnterprise.where(:user_owner=>session[:activeUser].id)
    misNegociosIndex=misNegocios.index_by(&:id).keys
    #hay que buscar si tiene forma de cobrar con tarjeta
    @public_conekta_key=QiwoTool.where(:toolname=>"conekta",:user_id=>session[:activeUser].id).last
    @public_firstdata=QiwoTool.where(:toolname=>"firstdata",:user_id=>session[:activeUser].id).last
    #buscar los negocios en los que tiene permiso cobrar por punto de venta
    @negociosPuntoDeVenta=[]
    #primero ver el permiso si acepta el punto de venta
    planWithSalePoint=QiwoUserSubscription.where(:user_id=>session[:activeUser].id).where(['ends_on >= ?', DateTime.now])
    if planWithSalePoint.length>0
      #si tiene un plan activo, ahora determinar si el plan o los planes tienen negocios con punto de venta
      planWithSalePoint.each do |singlePlanWithSalePoint|
        currentPlant=QiwoSubscription.where(:id=>singlePlanWithSalePoint.subscription_type).where("sale_point>0").last
        if currentPlant!=nil
          #entonces si tiene permisos de punto de venta, agregar el objeto de empresa
          @negociosPuntoDeVenta.concat(misNegociosIndex)
          puts "TIENE PUNTO DE VENTA 1"
          puts @negociosPuntoDeVenta.inspect
          puts "-----------------------"
        end
      end
    end
    #ahora verificar si tiene permisos de otra tienda para cobrar como empleado
    empleadoDe=QiwoEmployee.where(:employee_fb_id=>session[:activeUser].user_id)
    #trae los objetos de tienda, ahora verificar que tenga un plan activo con el punto de venta
    miPatronIndice=empleadoDe.index_by(&:employee_enterprise).keys
    #cargadas las empresas verificar que sus dueños tengan plan activo
    ownerEnterprise=QiwoEnterprise.where(:id=>miPatronIndice)
    miPatronEmpresaIndice=ownerEnterprise.index_by(&:user_owner).keys
    planesConSalePoint=QiwoSubscription.where("sale_point>0")
    planesConSalePointIndice=planesConSalePoint.index_by(&:id).keys
    miPatronPlanWithSalePoint=QiwoUserSubscription.where(:user_id=>miPatronEmpresaIndice,:subscription_type=>planesConSalePointIndice).where(['ends_on >= ?', DateTime.now])
    miPatronPlanWithSalePointIndice=miPatronPlanWithSalePoint.index_by(&:user_id).keys
    finalMiPatronWithSalePoint=ownerEnterprise.where(:user_owner=>miPatronPlanWithSalePointIndice)
    @negociosPuntoDeVenta.concat(finalMiPatronWithSalePoint.index_by(&:id).keys).uniq!
    puts "TIENE PUNTO DE VENTA 2"
    puts @negociosPuntoDeVenta.inspect
    puts "-----------------------"
    if @public_conekta_key!=nil
      @public_conekta_key=@public_conekta_key.apikey
    end
    if @public_firstdata!=nil
      @public_firstdata=@public_firstdata.apikey
    end
    
    @mayoresVentas=QiwoProduct.where(:business_source=>misNegociosIndex).joins(:qiwo_log_karts).salesMade.last(10)
    qiwoProductsIndex=@mayoresVentas.index_by(&:id).keys
    openOrdersByEnterprise=QiwoOrder.where(:qiwo_enterprise_id=>misNegociosIndex)
    openOrdersBySalesForce=QiwoOrder.where(:salesforce_user_id=>session[:activeUser].id)
    openOrdersByEnterprise=openOrdersByEnterprise.where_values.reduce(:and)
    openOrdersBySalesForce=openOrdersBySalesForce.where_values.reduce(:and)
    @ordenesAbiertas=QiwoOrder.where(openOrdersByEnterprise.or(openOrdersBySalesForce)).where("order_state!=10 and order_state!=13")
    #con los indices de los productos que poseemos, podemos determinar la cantidad de ventas
   
    
    @sale_point=SalePoint.new
    @sale_points = SalePoint.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sale_points }
    end
  end

  def getOrderItems
    idOrder=params[:orderView]
    #traer el objeto de venta y de producto
    misNegocios=QiwoEnterprise.where(:user_owner=>session[:activeUser].id)
    misNegociosIndex=misNegocios.index_by(&:id).keys
    itemsInOrder=QiwoProduct.select("qiwo_log_karts.qiwo_product_id,qiwo_log_karts.id,qiwo_log_karts.qiwo_order_id,qiwo_log_karts.quantity,qiwo_products.name,qiwo_products.sale_price").where(:business_source=>misNegociosIndex).joins(:qiwo_log_karts).where("qiwo_log_karts.qiwo_order_id='"+idOrder.to_s+"'")
    #calcular iva en caso que la orden lo requiera
    #necesitaIva=QiwoOrder.where(:salesforce_user_id=>session[:activeUser].id,:id=>idOrder).last
    itemsInOrder.each do |singleItemsInOrder|
      singleItemsInOrder.sale_price=sprintf('%.2f', singleItemsInOrder.sale_price+getTax(singleItemsInOrder.sale_price))
    end
    #QiwoLogKart.where(:qiwo_order_id=>idOrder.to_i)
    respond_to do |format|
      format.html { render :json => itemsInOrder.to_json }
      format.xml  { render :xml => itemsInOrder.to_json }
      format.json { render :json => itemsInOrder.to_json }
    end
  end

  # GET /sale_points/1
  # GET /sale_points/1.json
  def show
    redirect_to "/grant_permission.html"
    return
    @sale_point = SalePoint.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sale_point }
    end
  end

  # GET /sale_points/new
  # GET /sale_points/new.json
  def new
    redirect_to "/grant_permission.html"
    return
    @sale_point = SalePoint.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sale_point }
    end
  end

  # GET /sale_points/1/edit
  def edit
    redirect_to "/grant_permission.html"
    return
    @sale_point = SalePoint.find(params[:id])
  end

  # POST /sale_points
  # POST /sale_points.json
  def create
    orderId=params[:orderaddp]
    customPayment=params[:cashtype]
    storeId=params[:cardd]
    cargo=params[:cardch]
    #en lugar de crear puntos de venta lo que hacemos es crear cargos
    #cargamos el primer producto de la orden, si la orden viene vacia la saltamos
    #el primer producto de la orden nos va a dar el key de conekta
    primerProducto=QiwoLogKart.where(:qiwo_order_id=>orderId)
    tienda=nil
    if customPayment=="closecard"
      
    end
    if primerProducto.length>0 and orderId!=-1
        openOrdersByEnterprise=QiwoOrder.where(:salesforce_user_id=>session[:activeUser].id,:id=>orderId).last
        tienda=QiwoEnterprise.where(:id=>primerProducto[0].store_id).last
        puts ".............."
        puts tienda.inspect
        puts ".............."
        conektaKey=QiwoTool.where(:toolname=>'conekta',:user_id=>tienda.user_owner).last
        puts ".............."
        puts conektaKey.inspect
        puts ".............."
        totalACargar=0
        primerProducto.each do |singleProduct|
          #primero ver si tiene precio mayoritario
          totalACargar+=finalProductPrice(singleProduct.id)*singleProduct.quantity
        end
        Conekta.api_key=conektaKey.apisecret
        charge=nil
        begin
          
         charge = Conekta::Charge.create({
        "currency"=>"MXN",
        "amount"=> conektaFormat(totalACargar).to_s,
        "description"=>"Qiwo third party payment",
        "reference_id"=>openOrdersByEnterprise.inreference,
        "card"=> params[:conektaTokenId]
      })
         
         rescue Exception => e
          puts "ERRRRRRRRRRRROOOOOORR==>"+e.message+" "+e.backtrace.inspect
        end
        puts charge.inspect
        if charge!=nil
          
          openOrdersByEnterprise.conekta=charge[:id]
          openOrdersByEnterprise.save
          primerProducto.each do |singleProduct|
            puts ">>>changing single product with id:"+charge[:id]
            singleProduct.conekta=charge[:id]
            singleProduct.inreference=openOrdersByEnterprise.inreference
            singleProduct.save
          end  
        end
        
      
    #esperando a que notifiquen la tarjeta
    elsif orderId==-1
      #se esta realizando un cargo con tarjeta
    end
      
    # #por el momento redirigimos a la parte de punto de venta
    # redirect_to "/sale_points"
    # return
    openOrders=nil
    if customPayment=="closecard"
      openOrders=QiwoOrder.select("order_state").where(:salesforce_user_id=>session[:activeUser].id,:id=>orderId).last  
    else
       tienda=QiwoEnterprise.where(:id=>storeId).last
        puts ".......cobro......."
        puts tienda.inspect
        puts ".............."
        conektaKey=QiwoTool.where(:toolname=>'conekta',:user_id=>tienda.user_owner).last
        puts ".......cobro......."
        puts conektaKey.inspect
        puts ".............."
        Conekta.api_key=conektaKey.apisecret
         charge = Conekta::Charge.create({
        "currency"=>"MXN",
        "amount"=> conektaFormat(cargo),
        "description"=>"Qiwo third party payment",
        "reference_id"=>orderId,
        "card"=> params[:conektaTokenId]
      })
      #creamos un log_kart con el cargo
      cargoNuevo=QiwoLogKart.new
      cargoNuevo.save
      cargoNuevo.quantity=1
      cargoNuevo.kart_state=1
      cargoNuevo.user_id=session[:activeUser].id
      cargoNuevo.qiwo_product_id=-1 #con este -1 identificamos si es un cargo
      cargoNuevo.store_id=storeId
      cargoNuevo.final_total=cargo.to_s
      cargoNuevo.conekta=charge[:id]
      cargoNuevo.save
      openOrders=QiwoLogKart.select("kart_state as order_state,id as payment_ref").where(:id=>cargoNuevo.id).last
    end
    
    respond_to do |format|
      format.html { render :json => openOrders.to_json }
      format.xml  { render :xml => openOrders.to_json }
      format.json { render :json => openOrders.to_json }
    end
  end

  # POST
  def first_data_card
    orderId=params[:orderaddp]
    customPayment=params[:cashtype]
    storeId=params[:cardd]
    cargo=params[:cardch]
    #en lugar de crear puntos de venta lo que hacemos es crear cargos
    #cargamos el primer producto de la orden, si la orden viene vacia la saltamos
    #el primer producto de la orden nos va a dar el key de conekta
    primerProducto=QiwoLogKart.where(:qiwo_order_id=>orderId)
    tienda=nil
    if customPayment=="closecard"
      
    end
    if primerProducto.length>0 and orderId!=-1
      openOrdersByEnterprise=QiwoOrder.where(:salesforce_user_id=>session[:activeUser].id,:id=>orderId).last
      tienda=QiwoEnterprise.where(:id=>primerProducto[0].store_id).last
      puts ".............."
      puts tienda.inspect
      puts ".............."
      conektaKey=QiwoTool.where(:toolname=>'firstdata',:user_id=>tienda.user_owner).last
      puts ".............."
      puts conektaKey.inspect
      puts ".............."
      totalACargar=0
      primerProducto.each do |singleProduct|
        #primero ver si tiene precio mayoritario
        totalACargar+=finalProductPrice(singleProduct.id)*singleProduct.quantity
      end
    end
    #     Conekta.api_key=conektaKey.apisecret
        charge=nil
        # begin
          # POST request with modified headers
          # uri = URI("https://cert.api.firstdata.com/gateway/v2/payments")
          # http = Net::HTTP.new(uri.host, uri.port)
          # http.use_ssl = false

          # request = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json', 'Api-Key' => 'TxyDdgsyXTA7aMCZ4veuDxTYhN5LMkLS', 'Client-Request-Id' => '75e1e130-f786-4281-b52f-fe9df6f14669', 'Message-Signature' => 'ZWU5OWYzOWViYmQzZWE1MGIxZDAwYzU3NWM0ZjFhNDU1Y2JjMzJlNzk2NzkzNjJkNjNmNTVlOWZkMjMwZDc5MQ==', 'Timestamp' => Time.now.to_s })
          # request.body = {
          #   :requestType => 'PaymentCardSaleTransaction',
          #   :transactionAmount => {:total => "12.20", :currency => "MXN"},
          #   :paymentMethod => { :paymentCard => {
          #     'number' => '5424180279791732',
          #     'securityCode' => '977',
          #     'expiryDate' => {
          #       'month' => '12',
          #       'year' => '24'
          #     }
          #   }}
          #  }
          #  response = http.request(request)
          #  body = JSON.parse(response.body)
          #  puts body.inspect
        charge = RestClient.post 'https://cert.api.firstdata.com/gateway/v2/payments', {
          :requestType => 'PaymentCardSaleTransaction',
          :transactionAmount => {:total => "12.20", :currency => "MXN"},
          :paymentMethod => { :paymentCard => {
            'number' => '5424180279791732',
            'securityCode' => '977',
            'expiryDate' => {
              'month' => '12',
              'year' => '24'
            }
          }}
         }, {'Api-Key' => 'TxyDdgsyXTA7aMCZ4veuDxTYhN5LMkLS', 'Client-Request-Id' => 
         '75e1e130-f786-4281-b52f-fe9df6f14669', 'Message-Signature' =>  
         'ZWU5OWYzOWViYmQzZWE1MGIxZDAwYzU3NWM0ZjFhNDU1Y2JjMzJlNzk2NzkzNjJkNjNmNTVlOWZkMjMwZDc5MQ==', :Timestamp => Time.now.to_i.to_s}
         puts charge.inspect
      #    charge = Conekta::Charge.create({
      #   "currency"=>"MXN",
      #   "amount"=> conektaFormat(totalACargar).to_s,
      #   "description"=>"Qiwo third party payment",
      #   "reference_id"=>openOrdersByEnterprise.inreference,
      #   "card"=> params[:conektaTokenId]
      # })
         
      #    rescue Exception => e
      #     puts "ERRRRRRRRRRRROOOOOORR==>"+e.message+" "+e.backtrace.inspect
      #   end
    #     puts charge.inspect
    #     if charge!=nil
          
    #       openOrdersByEnterprise.conekta=charge[:id]
    #       openOrdersByEnterprise.save
    #       primerProducto.each do |singleProduct|
    #         puts ">>>changing single product with id:"+charge[:id]
    #         singleProduct.conekta=charge[:id]
    #         singleProduct.inreference=openOrdersByEnterprise.inreference
    #         singleProduct.save
    #       end  
    #     end
        
      
    # #esperando a que notifiquen la tarjeta
    # elsif orderId==-1
    #   #se esta realizando un cargo con tarjeta
    # end
      
    # # #por el momento redirigimos a la parte de punto de venta
    # # redirect_to "/sale_points"
    # # return
    # openOrders=nil
    # if customPayment=="closecard"
    #   openOrders=QiwoOrder.select("order_state").where(:salesforce_user_id=>session[:activeUser].id,:id=>orderId).last  
    # else
    #    tienda=QiwoEnterprise.where(:id=>storeId).last
    #     puts ".......cobro......."
    #     puts tienda.inspect
    #     puts ".............."
    #     conektaKey=QiwoTool.where(:toolname=>'conekta',:user_id=>tienda.user_owner).last
    #     puts ".......cobro......."
    #     puts conektaKey.inspect
    #     puts ".............."
    #     Conekta.api_key=conektaKey.apisecret
    #      charge = Conekta::Charge.create({
    #     "currency"=>"MXN",
    #     "amount"=> conektaFormat(cargo),
    #     "description"=>"Qiwo third party payment",
    #     "reference_id"=>orderId,
    #     "card"=> params[:conektaTokenId]
    #   })
    #   #creamos un log_kart con el cargo
    #   cargoNuevo=QiwoLogKart.new
    #   cargoNuevo.save
    #   cargoNuevo.quantity=1
    #   cargoNuevo.kart_state=1
    #   cargoNuevo.user_id=session[:activeUser].id
    #   cargoNuevo.qiwo_product_id=-1 #con este -1 identificamos si es un cargo
    #   cargoNuevo.store_id=storeId
    #   cargoNuevo.final_total=cargo.to_s
    #   cargoNuevo.conekta=charge[:id]
    #   cargoNuevo.save
    #   openOrders=QiwoLogKart.select("kart_state as order_state,id as payment_ref").where(:id=>cargoNuevo.id).last
    # end
    
    respond_to do |format|
      format.html { render :json => openOrders.to_json }
      format.xml  { render :xml => openOrders.to_json }
      format.json { render :json => openOrders.to_json }
    end
  end

  # PUT /sale_points/1
  # PUT /sale_points/1.json
  def update
    redirect_to "/grant_permission.html"
    return
    @sale_point = SalePoint.find(params[:id])

    respond_to do |format|
      if @sale_point.update_attributes(params[:sale_point])
        format.html { redirect_to @sale_point, notice: 'Sale point was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sale_point.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sale_points/1
  # DELETE /sale_points/1.json
  def destroy
    redirect_to "/grant_permission.html"
    return
    @sale_point = SalePoint.find(params[:id])
    @sale_point.destroy

    respond_to do |format|
      format.html { redirect_to sale_points_url }
      format.json { head :no_content }
    end
  end
end
