#!/bin/env ruby
# encoding: utf-8
class QiwoLogSearchesController < ApplicationController
  # GET /qiwo_log_searches
  # GET /qiwo_log_searches.json
  def index
    @qiwo_log_searches = QiwoLogSearch.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_log_searches }
    end
  end

  # GET /qiwo_log_searches/1
  # GET /qiwo_log_searches/1.json
  def show
    @qiwo_log_search = QiwoLogSearch.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_log_search }
    end
  end

  # GET /qiwo_log_searches/new
  # GET /qiwo_log_searches/new.json
  def new
    @qiwo_log_search = QiwoLogSearch.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_log_search }
    end
  end

  # GET /qiwo_log_searches/1/edit
  def edit
    @qiwo_log_search = QiwoLogSearch.find(params[:id])
  end

  # POST /qiwo_log_searches
  # POST /qiwo_log_searches.json
  def create
    @qiwo_log_search = QiwoLogSearch.new(params[:qiwo_log_search])

    respond_to do |format|
      if @qiwo_log_search.save
        format.html { redirect_to @qiwo_log_search, notice: 'Qiwo log search was successfully created.' }
        format.json { render json: @qiwo_log_search, status: :created, location: @qiwo_log_search }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_log_search.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_log_searches/1
  # PUT /qiwo_log_searches/1.json
  def update
    @qiwo_log_search = QiwoLogSearch.find(params[:id])

    respond_to do |format|
      if @qiwo_log_search.update_attributes(params[:qiwo_log_search])
        format.html { redirect_to @qiwo_log_search, notice: 'Qiwo log search was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_log_search.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_log_searches/1
  # DELETE /qiwo_log_searches/1.json
  def destroy
    @qiwo_log_search = QiwoLogSearch.find(params[:id])
    @qiwo_log_search.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_log_searches_url }
      format.json { head :no_content }
    end
  end
end
