#!/bin/env ruby
# encoding: utf-8
class QiwoMeasureUnitsController < ApplicationController
  # GET /qiwo_measure_units
  # GET /qiwo_measure_units.json
  def index
    @qiwo_measure_units = QiwoMeasureUnit.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_measure_units }
    end
  end

  # GET /qiwo_measure_units/1
  # GET /qiwo_measure_units/1.json
  def show
    @qiwo_measure_unit = QiwoMeasureUnit.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_measure_unit }
    end
  end

  # GET /qiwo_measure_units/new
  # GET /qiwo_measure_units/new.json
  def new
    @qiwo_measure_unit = QiwoMeasureUnit.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_measure_unit }
    end
  end

  # GET /qiwo_measure_units/1/edit
  def edit
    @qiwo_measure_unit = QiwoMeasureUnit.find(params[:id])
  end

  # POST /qiwo_measure_units
  # POST /qiwo_measure_units.json
  def create
    @qiwo_measure_unit = QiwoMeasureUnit.new(params[:qiwo_measure_unit])

    respond_to do |format|
      if @qiwo_measure_unit.save
        format.html { redirect_to @qiwo_measure_unit, notice: 'Qiwo measure unit was successfully created.' }
        format.json { render json: @qiwo_measure_unit, status: :created, location: @qiwo_measure_unit }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_measure_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_measure_units/1
  # PUT /qiwo_measure_units/1.json
  def update
    @qiwo_measure_unit = QiwoMeasureUnit.find(params[:id])

    respond_to do |format|
      if @qiwo_measure_unit.update_attributes(params[:qiwo_measure_unit])
        format.html { redirect_to @qiwo_measure_unit, notice: 'Qiwo measure unit was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_measure_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_measure_units/1
  # DELETE /qiwo_measure_units/1.json
  def destroy
    @qiwo_measure_unit = QiwoMeasureUnit.find(params[:id])
    @qiwo_measure_unit.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_measure_units_url }
      format.json { head :no_content }
    end
  end
end
