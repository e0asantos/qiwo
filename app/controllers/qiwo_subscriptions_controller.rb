#!/bin/env ruby
# encoding: utf-8
class QiwoSubscriptionsController < ApplicationController
  # GET /qiwo_subscriptions
  # GET /qiwo_subscriptions.json
  def index

     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_subscriptions = QiwoSubscription.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_subscriptions }
    end
  end

  # GET /qiwo_subscriptions/1
  # GET /qiwo_subscriptions/1.json
  def show
    if session[:activeUser].sys_rol!=1
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_subscription = QiwoSubscription.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_subscription }
    end
  end

  # GET /qiwo_subscriptions/new
  # GET /qiwo_subscriptions/new.json
  def new
    if session[:activeUser].sys_rol!=1
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_subscription = QiwoSubscription.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_subscription }
    end
  end

  # GET /qiwo_subscriptions/1/edit
  def edit
    if session[:activeUser].sys_rol==1
      @qiwo_subscription = QiwoSubscription.find(params[:id])
    end
      render layout: false 
  end

  # POST /qiwo_subscriptions
  # POST /qiwo_subscriptions.json
  def create
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    if session[:activeUser].sys_rol!=1
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_subscription = QiwoSubscription.new(params[:qiwo_subscription])
    #creamos una plantilla de plan en conekta

    respond_to do |format|
      if @qiwo_subscription.save
        Conekta.api_key=conektaKeyPrivate()
        plan = Conekta::Plan.create({
          :id => "qiwo-plan-"+@qiwo_subscription.id.to_s,
          :name => @qiwo_subscription.subscription_name,
          :amount => conektaFormat(@qiwo_subscription.price).to_s,
          :currency => "MXN",
          :interval => "month",
          :frequency => @qiwo_subscription.months_active.to_i,
          :trial_period_days => 0,
          :expiry_count => @qiwo_subscription.months_active.to_i})
        puts "CONEKTA PLAN>>>>:"+plan.inspect
        format.html { redirect_to "/qiwo_user_subscriptions", notice: 'Qiwo subscription was successfully created.' }
        format.json { render json: @qiwo_subscription, status: :created, location: @qiwo_subscription }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_subscriptions/1
  # PUT /qiwo_subscriptions/1.json
  def update
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    if session[:activeUser].sys_rol!=1
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_subscription = QiwoSubscription.find(params[:id])

    respond_to do |format|
      if @qiwo_subscription.update_attributes(params[:qiwo_subscription])
        format.html { redirect_to "/qiwo_user_subscriptions", notice: 'Suscripción actualizada.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_subscriptions/1
  # DELETE /qiwo_subscriptions/1.json
  def destroy
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    if session[:activeUser].sys_rol==1
      @qiwo_subscription = QiwoSubscription.find(params[:id])
      @qiwo_subscription.destroy

      respond_to do |format|
        format.html { redirect_to "/qiwo_user_subscriptions" }
        format.json { head :no_content }
      end
    end
  end
end
