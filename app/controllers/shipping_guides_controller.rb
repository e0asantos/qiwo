#!/bin/env ruby
# encoding: utf-8
class ShippingGuidesController < ApplicationController
  # GET /shipping_guides
  # GET /shipping_guides.json
  def index
    @shipping_guides = ShippingGuide.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @shipping_guides }
    end
  end

  # GET /shipping_guides/1
  # GET /shipping_guides/1.json
  def show
    @shipping_guide = ShippingGuide.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @shipping_guide }
    end
  end

  # GET /shipping_guides/new
  # GET /shipping_guides/new.json
  def new
    @shipping_guide = ShippingGuide.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @shipping_guide }
    end
  end

  # GET /shipping_guides/1/edit
  def edit
    @shipping_guide = ShippingGuide.find(params[:id])
  end

  # POST /shipping_guides
  # POST /shipping_guides.json
  def create
    @shipping_guide = ShippingGuide.new(params[:shipping_guide])

    respond_to do |format|
      if @shipping_guide.save
        format.html { redirect_to @shipping_guide, notice: 'Shipping guide was successfully created.' }
        format.json { render json: @shipping_guide, status: :created, location: @shipping_guide }
      else
        format.html { render action: "new" }
        format.json { render json: @shipping_guide.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /shipping_guides/1
  # PUT /shipping_guides/1.json
  def update
    @shipping_guide = ShippingGuide.find(params[:id])

    respond_to do |format|
      if @shipping_guide.update_attributes(params[:shipping_guide])
        format.html { redirect_to @shipping_guide, notice: 'Shipping guide was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @shipping_guide.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shipping_guides/1
  # DELETE /shipping_guides/1.json
  def destroy
    @shipping_guide = ShippingGuide.find(params[:id])
    @shipping_guide.destroy

    respond_to do |format|
      format.html { redirect_to shipping_guides_url }
      format.json { head :no_content }
    end
  end
end
