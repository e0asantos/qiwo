#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/qiwo_log_kart.rb'
require '/usr/src/qiwo/app/models/qiwo_product.rb'
require '/usr/src/qiwo/app/models/qiwo_enterprise.rb'
require '/usr/src/qiwo/app/models/qiwo_review.rb'
require "base64"
require 'xmlsimple'
require 'aftership'
class QiwoClientsController < ApplicationController
  protect_from_forgery :except => [:changeShippingPrice,:changeShippingGuide,:cancelFactura,:attachGuide]

  
  def attachGuide
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    mensajeRespuesta="No se pudo completar la asignación del folio."
    # el delta es el id de la compra, necesitamos saber si el usuario actual es el dueño sino lo mandamos a la verga
    delta=params[:delta].to_i
    empresas=QiwoEnterprise.select(:id).where(:user_owner=>session[:activeUser].id)
    mis_negocios=empresas.index_by(&:id).keys
    venta=QiwoLogKart.find_by_id(delta)
    if venta!=nil
      alreadyGuide=ShippingGuide.where(:qiwo_log_kart_id=>venta.id,:gtype=>isInTestModeNumber()).last
      if alreadyGuide==nil
        # hay que comprobar si tiene dirección de envio
        direccionDeRecoleccion=QiwoEnterprise.where(:user_owner=>session[:activeUser].id,:is_shipping=>1).last
        
        if direccionDeRecoleccion!=nil
          if direccionDeRecoleccion.telephones!=nil and 
                direccionDeRecoleccion.comercial_name!=nil and 
                direccionDeRecoleccion.email_sales!=nil and 
                direccionDeRecoleccion.email_buy!=nil and 
                direccionDeRecoleccion.description!=nil and 
                direccionDeRecoleccion.calle!=nil and 
                direccionDeRecoleccion.numexterior!=nil and 
                direccionDeRecoleccion.colonia!=nil and
                direccionDeRecoleccion.municipio!=nil and
                direccionDeRecoleccion.estado!=nil and
                direccionDeRecoleccion.pais!=nil and
                direccionDeRecoleccion.mycp!=nil
                  # hay que verificar si el cliente tiene direccion de envio
                  direccionDeEnvio=QiwoEnterprise.where(:user_owner=>venta.user_id,:is_shipping=>1).last
                  if direccionDeEnvio!=nil
                    # hay que revisar que se tengan todos los datos
                    if direccionDeEnvio.telephones!=nil and 
                      direccionDeEnvio.comercial_name!=nil and 
                      direccionDeEnvio.email_sales!=nil and 
                      direccionDeEnvio.email_buy!=nil and 
                      direccionDeEnvio.description!=nil and 
                      direccionDeEnvio.calle!=nil and 
                      direccionDeEnvio.numexterior!=nil and 
                      direccionDeEnvio.colonia!=nil and
                      direccionDeEnvio.municipio!=nil and
                      direccionDeEnvio.estado!=nil and
                      direccionDeEnvio.pais!=nil and
                      direccionDeEnvio.mycp!=nil
                      ultimaGuiaDisponible=ShippingGuide.where(:id_from=>session[:activeUser].id,:id_to=>nil,:gtype=>isInTestModeNumber()).first
                      if ultimaGuiaDisponible!=nil
                        resp=superAttachGuide(direccionDeRecoleccion,direccionDeEnvio,ultimaGuiaDisponible.real_guide,QiwoProduct.where(:id=>venta.qiwo_product_id).last.name,QiwoProduct.where(:id=>venta.qiwo_product_id).last.sale_price*3,venta.id,-1)  
                        mensajeRespuesta=resp
                      else
                        mensajeRespuesta="Al parecer no cuentas con guias de rastreo para enviar este pedido, puedes adquirir nuevas guias de rastreo de redpack dando click en la parte superior donde aparece tu contador de folios de rastreo."
                      end
                      
                      
                    else 
                      mensajeRespuesta="Tu cliente no ha ingresado su dirección de recepción en el menú de Entrega/recolección. Pidele que complete estos datos para proceder con la asignación del número de rastreo."
                      Notifications.sendInboxToId([venta.user_id]," el vendedor del artículo "+QiwoProduct.where(:id=>venta.qiwo_product_id).last.name+" intentó generar un número de rastreo para tu compra pero aun te falta completar la información de Entrega/recolección en el menú, atte Qiwo MX")    
                    end
                  else
                    mensajeRespuesta="Tu cliente no ha ingresado su dirección de recepción en el menú de Entrega/recolección. Pidele que complete estos datos para proceder con la asignación del número de rastreo."
                    Notifications.sendInboxToId([venta.user_id]," el vendedor del artículo "+QiwoProduct.where(:id=>venta.qiwo_product_id).last.name+" intentó generar un número de rastreo para tu compra pero aun te falta completar la información de Entrega/recolección en el menú, atte Qiwo MX")
                  end
                
              else 
                mensajeRespuesta="No has ingresado tu dirección de recolección en el menú de Entrega/recolección. Completalo antes de proceder a asignarle una guia de rastreo."
              end
          else
            mensajeRespuesta="No has ingresado tu dirección de recolección en el menú de Entrega/recolección. Completalo antes de proceder a asignarle una guia de rastreo."  
          end

        else
          mensajeRespuesta="Parece que esta compra ya tiene un número de guia asociado."
        end


    else
        mensajeRespuesta="Ups!"
        hackAttempt("qiwo_clients::attachGuide")
    end

    respond_to do |format|
      
        
        format.json { render json: mensajeRespuesta.to_json }
      
    end
  end

  def changeShippingPrice
    puts params.inspect
    puts "changeShippingPrice was executed"
    #primero ver si trae el nombre del campo por shipping-cost
    if params[:name].index("shipping-cost")!=nil
      #obtener los numeros
      indiceDeVenta=params[:name][14..-1]
      puts "INDICE:"+indiceDeVenta
      #ahora accesar a la session actual
      #con la sesion determinamos si este usuario
      #es propietario de ese producto
      empresas=QiwoEnterprise.select(:id).where(:user_owner=>session[:activeUser].id)
      mis_negocios=empresas.index_by(&:id).keys
      venta=QiwoLogKart.find_by_id(indiceDeVenta.to_i)
      if venta!=nil
        puts "HAY VENTA"
        #verificar si pertenece
        if mis_negocios.index(venta.store_id)!=nil
          puts "SI PERTENECE AL USUARIO"
          #ahora cambiamos el valor en la venta
          venta.shipping_cost=params[:value]
          venta.save
        end
      end
      puts session[:activeUser].name
    end
    render :nothing => true, :status => 200
  end

  def changeShippingGuide
    puts params.inspect
    puts "changeShippingGuide was executed"
    if params[:name].index("shipping-guide")!=nil
      #obtener los numeros
      indiceDeVenta=params[:name][15..-1]
      puts "INDICE:"+indiceDeVenta
      #ahora accesar a la session actual
      #con la sesion determinamos si este usuario
      #es propietario de ese producto
      empresas=QiwoEnterprise.select(:id).where(:user_owner=>session[:activeUser].id)
      mis_negocios=empresas.index_by(&:id).keys
      venta=QiwoLogKart.find_by_id(indiceDeVenta.to_i)
      if venta!=nil
        puts "HAY VENTA"
        #verificar si pertenece
        if mis_negocios.index(venta.store_id)!=nil
          puts "SI PERTENECE AL USUARIO"
          #ahora cambiamos el valor en la venta
          venta.shipping_guide=params[:value]
          venta.save
          #en esta parte vemos si el usuario tiene aftership
          aftership=QiwoTool.where(:user_id=>session[:activeUser].id,:toolname=>'aftership').last
          if aftership!=nil
            if aftership.apisecret!=nil
              if aftership.apisecret.length>10
                #si tiene aftership
                AfterShip.api_key = aftership.apisecret
                AfterShip::V3::Courier.get
                algo=AfterShip::V3::Courier.detect(params[:value])
                #obtenemosel mail del cliente y del vendedor
                correos=[]
                if session[:activeUser].email.index("@")!=nil
                  correos.push(session[:activeUser].email)
                end
                #ahora los mails del cliente
                if Sys_User.find_by_id(venta.user_id).email.index("@")!=nil
                  correos.push(Sys_User.find_by_id(venta.user_id).email)
                end
                algo2=AfterShip::V3::Tracking.create(params[:value], {"emails"=>correos})
              end
              
            end
          end
        end
      end
      puts session[:activeUser].name
    end
    render :nothing => true, :status => 200
  end

  def cancelFactura
  	puts params.inspect
  	#hay que ver si quien pide la cancelacion es el dueño de la venta
  	
	mis_negocios=currentUserEnterprises()
	venta=QiwoLogKart.find_by_id(params[:prodid].to_i)
    if venta!=nil
    	if mis_negocios.index(venta.store_id)!=nil
          puts "SI PERTENECE AL USUARIO"
          # necesitamos reintegrar al stock
          #ahora recuperamos el xml y procedemos a cancelarlo
          @print_factura=nil
          productosACancelar=[]
          if venta.qiwo_order_id!=nil
            # entonces la factura es una factura de orden y debemos buscarla como orden
            @print_factura = QiwoFacturas.where(:qiwo_order_id=>venta.qiwo_order_id,:status=>1).last
            # buscamos los productos
            productosACancelar=QiwoLogKart.where(:qiwo_order_id=>venta.qiwo_order_id)
            
          else 
            @print_factura = QiwoFacturas.where(:qiwo_log_kart_id=>params[:prodid],:status=>1).last
            productosACancelar=QiwoLogKart.where(:id=>params[:prodid])
          end
          if @print_factura!=nil
            empresaQueFactura=QiwoCounter.where(:qiwo_enterprise_id=>venta.store_id).last
            if empresaQueFactura!=nil
              #en caso de que lo encontremos hay que verificar que el usuario tenga folios disponibles
              if empresaQueFactura.facturas_disponibles<1
                redirect_to "/facturacions", notice: 'No se ha podido efectuar la operación parece ser que no hay folios de facturación electronica disponibles para proceder con el SAT.' 
                return
              end
            end
          end
          if venta.qiwo_order_id!=nil
            # cancelamos la orden
            ordernACancelar=QiwoOrder.where(:id=>venta.qiwo_order_id).last
            ordernACancelar.order_state=10
            ordernACancelar.save
          end
          # regresamos a todos al stock
          productosACancelar.each do |singleToStock|
            singleToStock.kart_state=4;
            singleToStock.save
            productoParticular=QiwoProduct.where(:id=>singleToStock.qiwo_product_id).last
            productoParticular.stock_items=productoParticular.stock_items+singleToStock.quantity
            productoParticular.save
          end
          if @print_factura!=nil
        	#primero vamos a abrir el xml
	    	doc=Base64.decode64(@print_factura.cfdi)
	    	@xml=XmlSimple.xml_in(doc)
	    	uid=@xml["Complemento"][0]["TimbreFiscalDigital"][0]["UUID"]
	    	
            begin
              client = Savon.client(:wsdl=> facturacionWSDL(),:ssl_verify_mode=>:none)
              puts client.operations
            	respuesta=client.call(:request_cancelar_cfdi,:message=>{:request=>{'emisorRFC'=>@xml["Emisor"][0]["rfc"],'UserID'=>facturacionUser(),'UserPass'=>facturacionPwd(),'uuid'=>uid}})	
            	#cancelamos
            	@print_factura.status=2
            	@print_factura.save
            rescue Exception => e
            	notice=e.message
            end
            actualizarPuntoDeVenta=listadoPuntoDeVenta()
            actualizarPuntoDeVenta.each do |singlePoint|
              pcontador=QiwoCounter.where(:qiwo_enterprise_id=>singlePoint).last
              if pcontador!=nil
                pcontador.facturas_disponibles=pcontador.facturas_disponibles-1
                if pcontador.facturas_disponibles<0
                  pcontador.facturas_disponibles=0
                end
                pcontador.save
              end
            end
              
          end
	    
        end
    end
  	
  	respond_to do |format|
      
        format.html { redirect_to "/yo", notice: 'Nothing to see.' }
        format.json { render json: "OK".to_json }
      
    end
  end

  def markAsCompleted
      puts params.inspect
      puts "sendToInbox was executed"
      qiwoUser=session[:activeUser]
      if qiwoUser!=nil
        #hay que buscar las compras, por que alguien puede cerrarlas sin ser el propietario del producto
          venta=QiwoLogKart.find_by_id(params[:prodid])
          #hay que revisar  que sea parte de
          misNegocios=currentUserEnterprises()
          puts misNegocios.inspect
          puts venta.inspect
          if misNegocios.index(venta.store_id)==nil and qiwoUser.sys_rol!=1
            #si alguien quiere terminar una venta que no le pertenece
            hackAttempt("qiwo_client_controller::markAsCompleted")
            render :nothing => true, :status => 200
            return
          end
          #hay que tener cuidado aqui por que si esta en status 1 de venta hay que hacer todo el proceso de ganancias
          #en caso contrario solo es moverlo de status
          if venta.kart_state==1
            fuenteProducto=QiwoProduct.where(:id=>venta.qiwo_product_id).last
            venta.final_sale=finalProductPriceNoTax(venta.id)*venta.quantity
            venta.final_tax=getTax(venta.final_sale)
            venta.final_total=finalProductPrice(venta.id)*venta.quantity
            venta.final_gain=(venta.final_sale-(fuenteProducto.raw_price*venta.quantity))
            venta.discount=fuenteProducto.discount_amount
            fuenteProducto.stock_items=fuenteProducto.stock_items-venta.quantity
            if fuenteProducto.stock_items<=0
              fuenteProducto.stock_items=0
              fuenteProducto.save  
              #notificar si llegamos a cero
              Notifications.notificateNoStock(session,venta.store_id)
            end
            fuenteProducto.save
          end
          #para activar los folios hay que ver si no estaban pagados para no duplicarlos
          if venta.kart_state==1 and foliosId().index(venta.qiwo_product_id)!=nil
            agregarFolios(venta.id)  
          end
          if venta.kart_state==1 and foliosEnviosId().index(venta.qiwo_product_id)!=nil
            agregarFoliosGuia(venta.id)
          end
          
          venta.kart_state=3           
          venta.save
      end
      render :nothing => true, :status => 200
  end

  # GET /qiwo_clients
  # GET /qiwo_clients.json
  def index
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    #buscar todos los del usuario
    if session[:activeUser].sys_rol!=1
        #buscar por los productos del usuario
        empresas=QiwoEnterprise.select(:id).where(:user_owner=>session[:activeUser].id)
        indicesEmpresas=empresas.index_by(&:id).keys
        productos=QiwoProduct.select(:id).where(:business_source=>indicesEmpresas).index_by(&:id).keys
        @qiwo_clients = QiwoLogKart.where(:qiwo_product_id=>productos).where("qiwo_product_id>0").paginate(:page => params[:page], :per_page => 20).order("created_at DESC")
    else
        @qiwo_clients=QiwoLogKart.where("qiwo_product_id>0").paginate(:page => params[:page], :per_page => 20).order("created_at DESC")
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_clients }
    end
  end
    
  def index_by
      inject({}) do |accum, elem|
        accum[yield(elem)] = elem
        accum
      end
  end
  # GET /qiwo_clients/1
  # GET /qiwo_clients/1.json
  def show
    @qiwo_client = QiwoClient.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_client }
    end
  end

  # GET /qiwo_clients/new
  # GET /qiwo_clients/new.json
  def new
    @qiwo_client = QiwoClient.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_client }
    end
  end

  # GET /qiwo_clients/1/edit
  def edit
    @qiwo_client = QiwoClient.find(params[:id])
  end

  # POST /qiwo_clients
  # POST /qiwo_clients.json
  def create
    @qiwo_client = QiwoClient.new(params[:qiwo_client])

    respond_to do |format|
      if @qiwo_client.save
        format.html { redirect_to @qiwo_client, notice: 'Qiwo client was successfully created.' }
        format.json { render json: @qiwo_client, status: :created, location: @qiwo_client }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_clients/1
  # PUT /qiwo_clients/1.json
  def update
    @qiwo_client = QiwoClient.find(params[:id])

    respond_to do |format|
      if @qiwo_client.update_attributes(params[:qiwo_client])
        format.html { redirect_to @qiwo_client, notice: 'Qiwo client was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_clients/1
  # DELETE /qiwo_clients/1.json
  def destroy
    @qiwo_client = QiwoClient.find(params[:id])
    @qiwo_client.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_clients_url }
      format.json { head :no_content }
    end
  end

  

end
