#!/bin/env ruby
# encoding: utf-8
class QiwoLogVisitsController < ApplicationController
  # GET /qiwo_log_visits
  # GET /qiwo_log_visits.json
  def index
    @qiwo_log_visits = QiwoLogVisit.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_log_visits }
    end
  end

  # GET /qiwo_log_visits/1
  # GET /qiwo_log_visits/1.json
  def show
    @qiwo_log_visit = QiwoLogVisit.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_log_visit }
    end
  end

  # GET /qiwo_log_visits/new
  # GET /qiwo_log_visits/new.json
  def new
    @qiwo_log_visit = QiwoLogVisit.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_log_visit }
    end
  end

  # GET /qiwo_log_visits/1/edit
  def edit
    @qiwo_log_visit = QiwoLogVisit.find(params[:id])
  end

  # POST /qiwo_log_visits
  # POST /qiwo_log_visits.json
  def create
    @qiwo_log_visit = QiwoLogVisit.new(params[:qiwo_log_visit])

    respond_to do |format|
      if @qiwo_log_visit.save
        format.html { redirect_to @qiwo_log_visit, notice: 'Qiwo log visit was successfully created.' }
        format.json { render json: @qiwo_log_visit, status: :created, location: @qiwo_log_visit }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_log_visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_log_visits/1
  # PUT /qiwo_log_visits/1.json
  def update
    @qiwo_log_visit = QiwoLogVisit.find(params[:id])

    respond_to do |format|
      if @qiwo_log_visit.update_attributes(params[:qiwo_log_visit])
        format.html { redirect_to @qiwo_log_visit, notice: 'Qiwo log visit was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_log_visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_log_visits/1
  # DELETE /qiwo_log_visits/1.json
  def destroy
    @qiwo_log_visit = QiwoLogVisit.find(params[:id])
    @qiwo_log_visit.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_log_visits_url }
      format.json { head :no_content }
    end
  end
end
