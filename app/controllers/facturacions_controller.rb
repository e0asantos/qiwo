#!/bin/env ruby
# encoding: utf-8
require "base64"
require 'xmlsimple'
class FacturacionsController < ApplicationController
  protect_from_forgery :except => [:populateRollBack,:execRollBack]

  def execRollBack
    # busca las compras con status menor a 4 y a 12 estos status son de ordenes devueltas
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    semilla=params[:delta]
    cantidad=params[:am].to_i
    # cargamos las empresas a las que tenemos permiso
    productos=currentUserProducts()
    puts productos.inspect
    filtrado=[]
    facturaSuper=nil
    ordenID=-1
    ventaID=-1
    productoParticular=nil
    filtroErase=nil
    if semilla.downcase.index("a-")!=nil
      secondSemilla=semilla[2..-1]
      preBusqueda=QiwoFacturas.where(:folio=>secondSemilla).where("qiwo_log_kart_id IS NOT NULL")
      preBusquedaIndex=preBusqueda.index_by(&:qiwo_log_kart_id).keys

      if preBusquedaIndex.length>0
        filtrado=QiwoProduct.where(:id=>productos).joins(:qiwo_log_karts).where("qiwo_log_karts.id="+preBusquedaIndex.join(" or qiwo_log_karts.id=")).where("qiwo_log_karts.kart_state>1 and qiwo_log_karts.kart_state<4").select("qiwo_products.business_source,qiwo_log_karts.id,qiwo_products.name,qiwo_log_karts.quantity,qiwo_log_karts.final_total,qiwo_log_karts.qiwo_order_id")#QiwoLogKart.where(:qiwo_product_id=>productos,:id=>semilla).joins(:qiwo_products)  
      end
      
      if filtrado.length==0
        # hay que probar si la factura es de una orden,
        preBusqueda=QiwoFacturas.where(:folio=>secondSemilla).where("qiwo_order_id IS NOT NULL")
        preBusquedaIndex=preBusqueda.index_by(&:qiwo_order_id).keys
        
        # entonces ya tenemos las ordenes con ese folio ahora buscamos en las ventas cuales productos se vendieron bajo esa orden
        if preBusquedaIndex.length>0
          tiendas=currentUserEnterprises()
          # ventasDeOrden=QiwoLogKart.where(:qiwo_order_id=>preBusquedaIndex,:store_id=>tiendas)
          filtrado=QiwoProduct.where(:id=>productos).joins(:qiwo_log_karts).where("qiwo_log_karts.qiwo_order_id= "+preBusquedaIndex.join(" or qiwo_log_karts.qiwo_order_id=")).where("qiwo_log_karts.kart_state>1 and qiwo_log_karts.kart_state<4").select("qiwo_products.business_source,qiwo_log_karts.id,qiwo_products.name,qiwo_log_karts.quantity,qiwo_log_karts.final_total,qiwo_log_karts.qiwo_order_id")#QiwoLogKart.where(:qiwo_product_id=>productos,:id=>semilla).joins(:qiwo_products)
          
        end
        
      end
      puts "borrando----------->"
      filtrado.each do |singleFactura|
        facturaToCancel=nil
        if singleFactura.qiwo_order_id!=nil
          # buscar la factura por numero de orden
          facturaToCancel=QiwoFacturas.where(:qiwo_order_id=>singleFactura.qiwo_order_id,:status=>1).last
          ordernACancelar=QiwoOrder.where(:id=>singleFactura.qiwo_order_id).last
          ordernACancelar.order_state=10
          ordernACancelar.save
          ordenID=singleFactura.qiwo_order_id
        else
          facturaToCancel=QiwoFacturas.where(:qiwo_log_kart_id=>singleFactura.id,:status=>1).last
          ventaID=singleFactura.id
        end
        # ponemos en status 4
        ventaACancelar=QiwoLogKart.where(:id=>singleFactura.id).last
        if ventaACancelar!=nil
          ventaACancelar.kart_state=4;
          ventaACancelar.save
          productoParticular=QiwoProduct.where(:id=>ventaACancelar.qiwo_product_id).last
          puts "PRODUCTO_PARTICULAR--->"+productoParticular.inspect
          productoParticular.stock_items=productoParticular.stock_items+ventaACancelar.quantity
          productoParticular.save
        end
        if facturaToCancel!=nil
          facturaSuper=facturaToCancel
          doc=Base64.decode64(facturaToCancel.cfdi)
          @xml=XmlSimple.xml_in(doc)
          uid=@xml["Complemento"][0]["TimbreFiscalDigital"][0]["UUID"]
          client = Savon.client(:wsdl=> facturacionWSDL(),:ssl_verify_mode=>:none)
          puts client.operations
          begin
            respuesta=client.call(:request_cancelar_cfdi,:message=>{:request=>{'emisorRFC'=>@xml["Emisor"][0]["rfc"],'UserID'=>facturacionUser(),'UserPass'=>facturacionPwd(),'uuid'=>uid}})  
            #cancelamos
            facturaToCancel.status=2
            facturaToCancel.save
          rescue Exception => e
            
          end 
        end
      end
      puts "borrando----------->"
    else
    # filtrado=QiwoProduct.where(:id=>productos).joins(:qiwo_log_karts).where("qiwo_log_karts.id="+semilla.to_s).select("qiwo_products.business_source,qiwo_log_karts.id,qiwo_products.name,qiwo_log_karts.quantity,qiwo_log_karts.final_total")#QiwoLogKart.where(:qiwo_product_id=>productos,:id=>semilla).joins(:qiwo_products)
        filtroErase=QiwoLogKart.where(:qiwo_product_id=>productos,:id=>semilla).last
        
        # buscamos y devolvemos a stock la cantidad
        if filtroErase!=nil
          
          # filtroErase.kart_state=4
          # filtroErase.save 
          # ahora necesitamos reintegrar la cantidad al stock
          if cantidad<=filtroErase.quantity
            productoParticular=QiwoProduct.where(:id=>filtroErase.qiwo_product_id).last
            productoParticular.stock_items=productoParticular.stock_items+cantidad
            
            if filtroErase.qiwo_order_id==nil
              facturaSat=QiwoFacturas.where(:qiwo_log_kart_id=>filtroErase.id,:status=>1).last
              if facturaSat!=nil
                ventaID=filtroErase.id
                facturaSuper=facturaSat
                empresaQueFactura=QiwoCounter.where(:qiwo_enterprise_id=>filtroErase.store_id).last
                if empresaQueFactura!=nil
                  #en caso de que lo encontremos hay que verificar que el usuario tenga folios disponibles
                  if empresaQueFactura.facturas_disponibles<1
                    redirect_to "/facturacions", notice: 'No se ha podido efectuar la operación parece ser que no hay folios de facturación electronica disponibles para proceder con el SAT.' 
                    return
                  else
                    productoParticular.save
                  end
                end
              else
                productoParticular.save
              end
            else
              facturaSat=QiwoFacturas.where(:qiwo_order_id=>filtroErase.qiwo_order_id,:status=>1).last
              if facturaSat!=nil
                ordenID=filtroErase.qiwo_order_id
                facturaSuper=facturaSat
                empresaQueFactura=QiwoCounter.where(:qiwo_enterprise_id=>filtroErase.store_id).last
                if empresaQueFactura!=nil
                  #en caso de que lo encontremos hay que verificar que el usuario tenga folios disponibles
                  if empresaQueFactura.facturas_disponibles<1
                    redirect_to "/facturacions", notice: 'No se ha podido efectuar la operación parece ser que no hay folios de facturación electronica disponibles para proceder con el SAT.' 
                    return
                  else
                    productoParticular.save
                  end
                end
              else
                productoParticular.save
              end


            end
            
          end
          
          # ahora hay que revisar si tiene factura y cancelarla
          
            # doc=Base64.decode64(facturaSat.cfdi)
            # @xml=XmlSimple.xml_in(doc)
            # uid=@xml["Complemento"][0]["TimbreFiscalDigital"][0]["UUID"]
            # client = Savon.client(:wsdl=> facturacionWSDL())
            # puts client.operations
            # begin
            #   respuesta=client.call(:request_cancelar_cfdi,:message=>{:request=>{'emisorRFC'=>@xml["Emisor"][0]["rfc"],'UserID'=>facturacionUser(),'UserPass'=>facturacionPwd(),'uuid'=>uid}})  
            #   #cancelamos
            #   facturaSat.status=2
            #   facturaSat.save
            # rescue Exception => e
              
            # end 
            
          
        end
    end
    puts "FACTURA SUPER---->"+facturaSuper.inspect
    # ahora hay que generar la nota de credito usando el campo de rollbackoperation
    if facturaSuper!=nil
      fileToSend=facturaSuper.rollbackop
      client = Savon.client(:wsdl=> facturacionWSDL(),:ssl_verify_mode=>:none)
      begin
        # completamos el cfdi de nota de credito
        fileToSend=fileToSend+cantidad.to_s+"
"
        
            if productoParticular.measure_units=="" or productoParticular.measure_units==nil
              fileToSend+="|No Aplica|"
            else
              fileToSend+="|"+QiwoMeasureUnit.find_by_id(productoParticular.measure_units).label_es.upcase+"|"
            end
            descriptionCorrected=productoParticular.description.gsub("\n"," ")
            descriptionCorrected=descriptionCorrected.gsub("\r"," ")
            # aqui hay que realizar la modificacion del numero de elementos comprados en realidad
            # lo que hacemos es dividir la venta en el numero de elementos comprados y restarle los que se estan devolviendo
            finalSale=filtroErase.final_sale/filtroErase.quantity
            finalTax=filtroErase.final_tax/filtroErase.quantity
            finalTotal=filtroErase.final_total/filtroErase.quantity
            finalGain=filtroErase.final_gain/filtroErase.quantity
            # ahora multiplicamos lo que nos de por la cantidad devuelta y los reales
            filtroErase.quantity=filtroErase.quantity-cantidad
            filtroErase.final_sale=finalSale*filtroErase.quantity
            filtroErase.final_tax=finalTax*filtroErase.quantity
            filtroErase.final_total=finalTotal*filtroErase.quantity
            filtroErase.final_gain=finalGain*filtroErase.quantity
            filtroErase.save
            fileToSend+=descriptionCorrected+"|"+cantidad.to_s+"|"+finalSale.to_s+"|"+(finalSale*cantidad).to_s+"
"
            fileToSend+="IMPUESTOS_TRASLADADOS|"+QiwoRegimen.find_by_typer(QiwoEnterprise.find_by_id(facturaSuper.store_from).regimen).num_impuestos.to_s+"
IVA|16.00|"+(finalTax*cantidad).to_s
        fileToSend=Base64.encode64(fileToSend)
        respuesta=client.call(:request_timbrar_cfdi,:message=>{:request=>{'emisorRFC'=>QiwoEnterprise.where(:id=>facturaSuper.store_from).last.rfc,'UserID'=>facturacionUser(),'UserPass'=>facturacionPwd(),'text2CFDI'=>fileToSend}})
        #se genero la factura y hay que guardar que compra tiene factura y descontar los folios
        # puts "XML"
        # puts respuesta.body[:request_timbrar_cfdi_response][:return][:xml]
        # puts "XML"
        # puts "PDF"
        # puts respuesta.body[:request_timbrar_cfdi_response][:return][:pdf]
        # puts "PDF"
        empresaQueFactura=QiwoCounter.where(:qiwo_enterprise_id=>facturaSuper.store_from).last
        nuevaFactura=QiwoFacturas.new
        nuevaFactura.store_from=facturaSuper.store_from
        nuevaFactura.store_to=facturaSuper.store_to
        nuevaFactura.folio=empresaQueFactura.folio_factura
        nuevaFactura.status=3
        nuevaFactura.token=UUIDTools::UUID.random_create.to_s()
        if ventaID!=-1
          nuevaFactura.qiwo_log_kart_id=ventaID
        else
          nuevaFactura.qiwo_order_id=ordenID
          #modificar estado de la orden
        end
        #en esta parte sacamos totalmente el string 
        # facturaSeparada=respuesta.to_s[respuesta.to_s.index("string")+8,respuesta.to_s.index("</xml>")]
        # facturaSeparada=facturaSeparada[0,facturaSeparada.to_s.index("</xml>")]

        nuevaFactura.cfdi=respuesta.body[:request_timbrar_cfdi_response][:return][:xml]
        #nuevaFactura.pdf=respuesta.body[:request_timbrar_cfdi_response][:return][:pdf]
        nuevaFactura.save
        #una vez que tenemos la factura podemos convertirla en pdf
        # kit = PDFKit.new('http://qiwo.mx/print_facturas/'+nuevaFactura.token)
        # pdf = kit.to_pdf
        #ahora guardar en base64
        
        empresaQueFactura.folio_factura=empresaQueFactura.folio_factura+1
        empresaQueFactura.save
        actualizarPuntoDeVenta=listadoPuntoDeVenta()
        actualizarPuntoDeVenta.each do |singlePoint|
          pcontador=QiwoCounter.where(:qiwo_enterprise_id=>singlePoint).last
          if pcontador!=nil
            pcontador.facturas_disponibles=pcontador.facturas_disponibles-1
            if pcontador.facturas_disponibles<0
              pcontador.facturas_disponibles=0
            end
            pcontador.save
          end
        end
        rescue Exception => e
          puts e.inspect
        end

      else
        # si la venta no tiene factura, entonces solo lo regresamos al stock
        # aqui hay que realizar la modificacion del numero de elementos comprados en realidad
        # lo que hacemos es dividir la venta en el numero de elementos comprados y restarle los que se estan devolviendo
        finalSale=filtroErase.final_sale/filtroErase.quantity
        finalTax=filtroErase.final_tax/filtroErase.quantity
        finalTotal=filtroErase.final_total/filtroErase.quantity
        finalGain=filtroErase.final_gain/filtroErase.quantity
        # ahora multiplicamos lo que nos de por la cantidad devuelta y los reales
        filtroErase.quantity=filtroErase.quantity-cantidad
        filtroErase.final_sale=finalSale*filtroErase.quantity
        filtroErase.final_tax=finalTax*filtroErase.quantity
        filtroErase.final_total=finalTotal*filtroErase.quantity
        filtroErase.final_gain=finalGain*filtroErase.quantity
        filtroErase.save
      end
    respond_to do |format|
      # format.html # index.html.erb
      format.json { render json: "OK".to_json }
    end
  end



  def populateRollBack
    # busca las compras con status menor a 4 y a 12 estos status son de ordenes devueltas
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    semilla=params[:delta]
    productos=currentUserProducts()
    filtrado=[]
    # en caso que la semilla tenga A- entonces es una factura
    if semilla.downcase.index("a-")!=nil
      # hay que extraer la factura
      secondSemilla=semilla[2..-1]
      preBusqueda=QiwoFacturas.where(:folio=>secondSemilla).where("qiwo_log_kart_id IS NOT NULL")
      preBusquedaIndex=preBusqueda.index_by(&:qiwo_log_kart_id).keys
      # debido a que hay muchos folios con el mismo numero, hay que encontrar el correcto
      # usamos el kartID para
      # filtrado=QiwoLogKart.where(:id=>preBusquedaIndex,:qiwo_product_id=>productos)
      if preBusquedaIndex.length>0
        filtrado=QiwoProduct.where(:id=>productos).joins(:qiwo_log_karts).where("qiwo_log_karts.id="+preBusquedaIndex.join(" or qiwo_log_karts.id=")).where("qiwo_log_karts.kart_state>1 and qiwo_log_karts.kart_state<4 and qiwo_log_karts.quantity>0").select("qiwo_products.business_source,qiwo_log_karts.id,qiwo_products.name,qiwo_log_karts.quantity,qiwo_log_karts.final_total,qiwo_log_karts.qiwo_order_id")#QiwoLogKart.where(:qiwo_product_id=>productos,:id=>semilla).joins(:qiwo_products)  
      end
      
      if filtrado.length==0
        # hay que probar si la factura es de una orden,
        preBusqueda=QiwoFacturas.where(:folio=>secondSemilla).where("qiwo_order_id IS NOT NULL")
        preBusquedaIndex=preBusqueda.index_by(&:qiwo_order_id).keys
        puts "<---------------"
        puts preBusquedaIndex.inspect
        puts "<---------------"
        # entonces ya tenemos las ordenes con ese folio ahora buscamos en las ventas cuales productos se vendieron bajo esa orden
        if preBusquedaIndex.length>0
          tiendas=currentUserEnterprises()
          # ventasDeOrden=QiwoLogKart.where(:qiwo_order_id=>preBusquedaIndex,:store_id=>tiendas)
          filtrado=QiwoProduct.where(:id=>productos).joins(:qiwo_log_karts).where("qiwo_log_karts.qiwo_order_id= "+preBusquedaIndex.join(" or qiwo_log_karts.qiwo_order_id=")).where("qiwo_log_karts.kart_state>1 and qiwo_log_karts.kart_state<4 and qiwo_log_karts.quantity>0").select("qiwo_products.business_source,qiwo_log_karts.id,qiwo_products.name,qiwo_log_karts.quantity,qiwo_log_karts.final_total,qiwo_log_karts.qiwo_order_id")#QiwoLogKart.where(:qiwo_product_id=>productos,:id=>semilla).joins(:qiwo_products)
          # finalTotalHash=Hash.new
          # finalTotalHash[:final_total]=0
          # finalTotalHash[:name]="Factura con "+pfiltrado.length.to_s+" productos"
          # finalTotalHash[:quantity]=0
          # finalTotalHash[:id]=params[:delta]
          # finalTotalHash[:business_source]=0
          # finalTotalHash[:qiwo_order_id]=1
          # pfiltrado.each do |singlePre|
          #   finalTotalHash[:final_total]=finalTotalHash[:final_total]+singlePre.final_total
          #   finalTotalHash[:quantity]=finalTotalHash[:quantity]+singlePre.quantity
          #   finalTotalHash[:business_source]=singlePre.business_source
          # end
          # if pfiltrado.length>0
          #   filtrado=[finalTotalHash]
          # end
        end
        
      end
      puts filtrado.inspect
    else

    # cargamos las empresas a las que tenemos permiso
    
      puts productos.inspect
      filtrado=QiwoProduct.where(:id=>productos).joins(:qiwo_log_karts).where("qiwo_log_karts.id="+semilla.to_s+" and qiwo_log_karts.kart_state>1 and qiwo_log_karts.kart_state<4").select("qiwo_products.business_source,qiwo_log_karts.id,qiwo_products.name,qiwo_log_karts.quantity,qiwo_log_karts.final_total,qiwo_log_karts.qiwo_order_id")#QiwoLogKart.where(:qiwo_product_id=>productos,:id=>semilla).joins(:qiwo_products)
    end
    # recorremos el filtro para ponerle la tienda
    conjunto=[]
    filtrado.each do |singleFilter|
      if singleFilter.is_a? Hash
        mh=Hash.new
        mh[:enterprise]=QiwoEnterprise.where(:id=>singleFilter[:business_source]).last.name
        mh[:product]=singleFilter[:name]
        mh[:quantity]=singleFilter[:quantity]
        mh[:total]=sprintf('%.2f', singleFilter[:final_total])
        mh[:delta]=singleFilter[:id]
        conjunto.push(mh)
      else
        mh=Hash.new
        mh[:enterprise]=QiwoEnterprise.where(:id=>singleFilter.business_source).last.name
        mh[:product]=singleFilter.name
        mh[:quantity]=singleFilter.quantity
        mh[:total]=sprintf('%.2f', singleFilter.final_total)
        mh[:delta]=singleFilter.id
        conjunto.push(mh)
      end
    end
    puts "--------------------"
    # puts filtrado.inspect
    puts "--------------------"
    puts conjunto.to_json.to_s
    puts "--------------------"

    # render :nothing => true, :status => 200
    respond_to do |format|
      # format.html # index.html.erb
      if conjunto.length>0
        format.json { render json: conjunto.to_json }  
      else
        format.json { render json: "NONE".to_json }
      end
    end
  end
  # GET /facturacions
  # GET /facturacions.json
  def index
    @facturacions = Facturacion.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @facturacions }
    end
  end

  # GET /facturacions/1
  # GET /facturacions/1.json
  def show
    @facturacion = Facturacion.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @facturacion }
    end
  end

  # GET /facturacions/new
  # GET /facturacions/new.json
  def new
    @facturacion = Facturacion.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @facturacion }
    end
  end

  # GET /facturacions/1/edit
  def edit
    @facturacion = Facturacion.find(params[:id])
  end

  # POST /facturacions
  # POST /facturacions.json
  def create
    @facturacion = Facturacion.new(params[:facturacion])

    respond_to do |format|
      if @facturacion.save
        format.html { redirect_to @facturacion, notice: 'Facturacion was successfully created.' }
        format.json { render json: @facturacion, status: :created, location: @facturacion }
      else
        format.html { render action: "new" }
        format.json { render json: @facturacion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /facturacions/1
  # PUT /facturacions/1.json
  def update
    @facturacion = Facturacion.find(params[:id])

    respond_to do |format|
      if @facturacion.update_attributes(params[:facturacion])
        format.html { redirect_to @facturacion, notice: 'Facturacion was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @facturacion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacions/1
  # DELETE /facturacions/1.json
  def destroy
    @facturacion = Facturacion.find(params[:id])
    @facturacion.destroy

    respond_to do |format|
      format.html { redirect_to facturacions_url }
      format.json { head :no_content }
    end
  end
end
