class QiwoProfilesController < ApplicationController
  # GET /qiwo_profiles
  # GET /qiwo_profiles.json
  def index
    @qiwo_profiles = QiwoProfile.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_profiles }
    end
  end

  # GET /qiwo_profiles/1
  # GET /qiwo_profiles/1.json
  def show
    @qiwo_profile = QiwoProfile.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_profile }
    end
  end

  # GET /qiwo_profiles/new
  # GET /qiwo_profiles/new.json
  def new
    @qiwo_profile = QiwoProfile.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_profile }
    end
  end

  # GET /qiwo_profiles/1/edit
  def edit
    @qiwo_profile = QiwoProfile.find(params[:id])
  end

  # POST /qiwo_profiles
  # POST /qiwo_profiles.json
  def create
    @qiwo_profile = QiwoProfile.new(params[:qiwo_profile])

    respond_to do |format|
      if @qiwo_profile.save
        format.html { redirect_to @qiwo_profile, notice: 'Qiwo profile was successfully created.' }
        format.json { render json: @qiwo_profile, status: :created, location: @qiwo_profile }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_profiles/1
  # PUT /qiwo_profiles/1.json
  def update
    @qiwo_profile = QiwoProfile.find(params[:id])

    respond_to do |format|
      if @qiwo_profile.update_attributes(params[:qiwo_profile])
        format.html { redirect_to @qiwo_profile, notice: 'Qiwo profile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_profiles/1
  # DELETE /qiwo_profiles/1.json
  def destroy
    @qiwo_profile = QiwoProfile.find(params[:id])
    @qiwo_profile.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_profiles_url }
      format.json { head :no_content }
    end
  end
end
