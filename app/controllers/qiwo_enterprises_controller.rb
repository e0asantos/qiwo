#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/qiwo_log_kart.rb'
require "base64"
require "uuidtools"

require 'mail'
require 'mandrill'

class QiwoEnterprisesController < ApplicationController

  def getFiscalAddress
    notEnough=[]
        respond_to do |format|
          format.html { render :json => notEnough.to_json }
          format.xml  { render :xml => notEnough.to_json }
          format.json { render :json => notEnough.to_json }
      end
  end
  # GET /qiwo_enterprises
  # GET /qiwo_enterprises.json
  def index
      if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
      if session[:activeUser].sys_rol==1
        @qiwo_enterprises = QiwoEnterprise.where("founded_date IS NOT NULL").paginate(:page => params[:page], :per_page => 10).order("created_at DESC")
      else
        @qiwo_enterprises = QiwoEnterprise.where("founded_date IS NOT NULL").paginate(:page => params[:page], :per_page => 10).order("created_at DESC").find_all_by_user_owner(session[:activeUser].id)
      end
    @qiwo_enterprise = QiwoEnterprise.new

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_enterprises }
    end
  end

  # GET /qiwo_enterprises/1
  # GET /qiwo_enterprises/1.json
  def show
    # hay que ver si estamos buscando un shipping address
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    
    
    @qiwo_enterprise=nil
    if params[:id]=="shipping_delivery"
      
    @qiwo_enterprise=QiwoEnterprise.new
    elsif params[:id]=="shipping"
      # hay que buscar si tiene un shipping address sino, crear uno
      @qiwo_enterprise=QiwoEnterprise.where(:user_owner=>session[:activeUser].id,:is_shipping=>1).last
      if @qiwo_enterprise==nil
        @qiwo_enterprise=QiwoEnterprise.new
        # creamos un shipping type para la direccion
        @qiwo_enterprise.is_shipping=1
        @qiwo_enterprise.user_owner=session[:activeUser].id
        @qiwo_enterprise.save
      end
    else
      if currentUserEnterprises().index(params[:id].to_i)==nil
      redirect_to "/yo"
        return
    end
      @qiwo_enterprise = QiwoEnterprise.find(params[:id])
    end

    respond_to do |format|
      format.html {render layout: false}# show.html.erb
      format.json { render json: @qiwo_enterprise}
    end
  end

  # GET /qiwo_enterprises/new
  # GET /qiwo_enterprises/new.json
  def new
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    
    @qiwo_enterprise = QiwoEnterprise.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_enterprise }
    end
  end

  # GET /qiwo_enterprises/1/edit
  def edit
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end

    if params[:id]=="shipping_delivery"
      
    @qiwo_enterprise=QiwoEnterprise.new
    elsif params[:id]=="shipping"
      # hay que buscar si tiene un shipping address sino, crear uno
      @qiwo_enterprise=QiwoEnterprise.where(:user_owner=>session[:activeUser].id,:is_shipping=>1).last
      if @qiwo_enterprise==nil
        @qiwo_enterprise=QiwoEnterprise.new
        # creamos un shipping type para la direccion
        @qiwo_enterprise.is_shipping=1
        @qiwo_enterprise.user_owner=session[:activeUser].id
        @qiwo_enterprise.save
      end
    else
      if currentUserEnterprises().index(params[:id].to_i)==nil and session[:activeUser].sys_rol!=1
        redirect_to "/yo"
        return
      end

    
    @qiwo_enterprise = QiwoEnterprise.find(params[:id])

    end
    
    render layout: false
  end

  # POST /qiwo_enterprises
  # POST /qiwo_enterprises.json
  def create
    puts "====>"
    puts request.env['PATH_INFO']
    puts "<===="
    #antes de crear la empresa verificar si tenemos plan activo
    if QiwoUserSubscriptionsController.howManyEnterpriseAllowed(session)>0 or params[:delta]==1 or params[:delta]=="1" or params[:delta]==2 or params[:delta]=="2" or params[:delta]==3 or params[:delta]=="3" or params[:delta]=="4" or params[:delta]==4
      
    
      @qiwo_enterprise = QiwoEnterprise.new(params[:qiwo_enterprise])
      @qiwo_enterprise.user_owner=session[:activeUser].id
      if params[:delta]=="4" or params[:delta]==4
        @qiwo_enterprise.is_shipping=1
        # si entramos aqui entonces se esta buscando generar numero de guia
      end
      #crear un unique id de tienda
      if @qiwo_enterprise.unique_uri==nil or @qiwo_enterprise.unique_uri==""
        #reemplazar espacios
        #primero eliminar puntuaciones
        @qiwo_enterprise.unique_uri=@qiwo_enterprise.comercial_name.gsub(/[^0-9a-z ]/i, '').downcase
        @qiwo_enterprise.unique_uri=@qiwo_enterprise.unique_uri.gsub(" ", '-')
        #ahora quitar espacios
      end
      #cuando se crea uno nuevo hay que verificar que solo tengamos un solo RFC y verificar de donde vienen las peticiones revisando el -delta-
      if params[:delta]==1 or params[:delta]=="1" or params[:delta]==2 or params[:delta]=="2" or params[:delta]==3 or params[:delta]=="3"
        #guardamos la empresa a facturar
        @qiwo_enterprise.save
        #lo primero que debemos hacer es verificar si la empresa puede facturar, nos basamos en la compra para saber la empresa
        ventaAFacturar=[]
        if params[:delta]==1 or params[:delta]=="1"
          ventaAFacturar=QiwoLogKart.where(:id=>params[:purchase],:user_id=>session[:activeUser].id)
        elsif params[:delta]==2 or params[:delta]=="2"
          ventaAFacturar=QiwoLogKart.where(:qiwo_order_id=>params[:purchase],:user_id=>session[:activeUser].id)
        else 
          ventaAFacturar=QiwoLogKart.where(:qiwo_order_id=>params[:purchase],:user_id=>session[:activeUser].id)
        end
        if ventaAFacturar.length==0
          if params[:delta]==1 or params[:delta]=="1"
            redirect_to "/qiwo_log_karts", notice: 'No se ha podido facturar, parece ser que existe un error con la venta.' 
          elsif params[:delta]==2 or params[:delta]=="2"
            redirect_to "/sale_points", notice: 'No se ha podido facturar, parece ser que existe un error con la venta.' 
          else 
            redirect_to "/qiwo_orders", notice: 'No se ha podido facturar, parece ser que existe un error con la venta.' 
          end
          hackAttempt("qiwo_enterprise_controller::create")
          return
        end
        #ahora asegurarse que la compra no este previamente facturada
        if QiwoFacturas.where(:qiwo_log_kart_id=>params[:purchase]).last!=nil and (params[:delta]==1 or params[:delta]=="1")
            redirect_to "/qiwo_log_karts", notice: 'El artículo seleccionado ya cuenta con factura, primero cancele la factura y vuelva a intentarlo (ref1).' 
            return
        elsif QiwoFacturas.where(:qiwo_order_id=>params[:purchase]).last!=nil and (params[:delta]==2 or params[:delta]=="2")
          redirect_to "/sale_points", notice: 'El artículo seleccionado ya cuenta con factura, primero cancele la factura y vuelva a intentarlo (ref2).' 
          return
        elsif QiwoFacturas.where(:qiwo_order_id=>params[:purchase]).last!=nil and (params[:delta]==3 or params[:delta]=="3")
          redirect_to "/qiwo_orders", notice: 'El artículo seleccionado ya cuenta con factura, primero cancele la factura y vuelva a intentarlo (ref3).' 
          return
        end

        empresaQueFactura=QiwoCounter.where(:qiwo_enterprise_id=>ventaAFacturar[0].store_id).last
        if empresaQueFactura!=nil
          #en caso de que lo encontremos hay que verificar que el usuario tenga folios disponibles
          if empresaQueFactura.facturas_disponibles<1
             if params[:delta]==1 or params[:delta]=="1"
              redirect_to "/qiwo_log_karts", notice: 'No se ha podido facturar, parece ser que el vendedor se ha quedado sin folios para emitir factura electronica.' 
            elsif params[:delta]==2 or params[:delta]=="2"
              redirect_to "/sale_points", notice: 'No se ha podido facturar, parece ser que te has quedado sin folios para emitir factura electronica.' 
            elsif params[:delta]==3 or params[:delta]=="3"
              redirect_to "/qiwo_orders", notice: 'No se ha podido facturar, parece ser que te has quedado sin folios para emitir factura electronica.' 
            end
             
            return
          else 
            #en esta parte si hay folios y hay que verificar que el vendedor tenga todos los datos de facturacion
            origenDatosFactura=QiwoEnterprise.where(:id=>ventaAFacturar[0].store_id).last
            if origenDatosFactura.calle=="" or origenDatosFactura.name=="" or origenDatosFactura.telephones=="" or origenDatosFactura.email_sales=="" or origenDatosFactura.email_buy=="" or origenDatosFactura.comercial_name=="" or origenDatosFactura.rfc=="" or origenDatosFactura.numexterior=="" or origenDatosFactura.colonia=="" or origenDatosFactura.municipio=="" or origenDatosFactura.estado=="" or origenDatosFactura.pais=="" or origenDatosFactura.mycp=="" or origenDatosFactura.regimen==nil or origenDatosFactura.no_certificado==""
              if params[:delta]==1 or params[:delta]=="1"
                redirect_to "/qiwo_log_karts", notice: 'Los datos del vendedor estan incompletos para poder generar la factura, ponte en contacto con la contraparte.' 
              elsif params[:delta]==2 or params[:delta]=="2"
                redirect_to "/sale_points", notice: 'Los datos de tu negocio estan incompletos para poder generar la factura.' 
              elsif params[:delta]==3 or params[:delta]=="3"
                redirect_to "/sale_points", notice: 'Los datos de tu negocio estan incompletos para poder generar la factura.' 
              end
              return
            end
            if @qiwo_enterprise.calle=="" or @qiwo_enterprise.name=="" or @qiwo_enterprise.telephones=="" or @qiwo_enterprise.email_sales=="" or @qiwo_enterprise.email_buy=="" or @qiwo_enterprise.comercial_name=="" or @qiwo_enterprise.rfc=="" or @qiwo_enterprise.numexterior=="" or @qiwo_enterprise.colonia=="" or @qiwo_enterprise.municipio=="" or @qiwo_enterprise.estado=="" or @qiwo_enterprise.pais=="" or @qiwo_enterprise.mycp==""
              if params[:delta]==1 or params[:delta]=="1"
                redirect_to "/qiwo_log_karts", notice: 'Los datos que proporcionaste no están completos para generar la factura electronica, por favor vuelve a intentarlo.' 
              elsif params[:delta]==2 or params[:delta]=="2"
                redirect_to "/sale_points", notice: 'Los datos que proporcionaste no están completos para generar la factura electronica, por favor vuelve a intentarlo.' 
              elsif params[:delta]==3 or params[:delta]=="3"
                redirect_to "/qiwo_orders", notice: 'Los datos que proporcionaste no están completos para generar la factura electronica, por favor vuelve a intentarlo.' 
              end
                
              return
            end
            #aqui se realiza la peticion de facturacion
            #hay que cargar el contenido del archivo en base64, este archivo o lo que sea es un texto plano
            # serie|folio|Fecha|Lugar expedición|tipo documento|Forma de pago|Método de pago|condicion pago|Num cuenta pago|subtotal|descuento|total|moneda|tipo de cambio|no. certificado|
            # RFC emisor|Razon social emisor|Régimen fiscal
            # calle|numero exterior|numero interior|colonia|localidad|municipio|estado|pais|codigo postal
            # calle sucursal|numero exterior sucursal|numero interior sucursal|colonia sucursal|localidad sucursal|municipio|estado|pais|codigo postal
            # RFC receptor|razon social receptor
            # calle|numero exterior|numero interior|colonia|localidad|municipio|estado|pais|cp
            # CONCEPTOS|numero de conceptos
            # |unidad medida|descripcion|cantidad|precio unitario|importe
            # IMPUESTOS_TRASLADADOS|numero de impuestos traslados
            # nombre impuesto|tasa|importe
            # IMPUESTOS_RETENIDOS|numero de impuesto retenidos
            # nombre|importe
            sumaSubtotal=0
            sumaDescuento=0
            sumaTotal=0
            sumaTaxes=0
            ventaAFacturar.each do |singleOneSale|
              sumaSubtotal+=singleOneSale.final_sale
              sumaDescuento+=singleOneSale.discount
              sumaTotal+=singleOneSale.final_total
              sumaTaxes+=singleOneSale.final_tax
            end
            conceptoDinamico="Pago en una sola exhibición"
            if params[:delta]==2
              conceptoDinamico=params[:tipopago]
            end
            puts "=====)"+empresaQueFactura.inspect
            puts "=====)"+origenDatosFactura.inspect
            # correccion de numeros con digitos grandes

            cfdiReal="A|"+empresaQueFactura.folio_factura.to_s+"|asignarFecha|"+origenDatosFactura.estado+", "+origenDatosFactura.pais+"|ingreso|contado|No Aplica|"+conceptoDinamico+"||"+sprintf('%.2f', sumaSubtotal)+"|"+sprintf('%.2f', sumaDescuento)+"|"+sprintf('%.2f', sumaTotal)+"|MXN|0.0|"+origenDatosFactura.no_certificado+"|
"+origenDatosFactura.rfc.upcase+"|"+origenDatosFactura.name+"|"+QiwoRegimen.where(:typer=>origenDatosFactura.regimen).last.name+"
"+origenDatosFactura.calle+"|"+origenDatosFactura.numexterior+"|"+origenDatosFactura.numinterior+"|"+origenDatosFactura.colonia+"|"+origenDatosFactura.localidad+"|"+origenDatosFactura.municipio+"|"+origenDatosFactura.estado+"|"+origenDatosFactura.pais+"|"+origenDatosFactura.mycp+"
||||||||
"+@qiwo_enterprise.rfc.upcase+"|"+@qiwo_enterprise.name+"
"+@qiwo_enterprise.calle+"|"+@qiwo_enterprise.numexterior+"|"+@qiwo_enterprise.numinterior+"|"+@qiwo_enterprise.colonia+"|"+@qiwo_enterprise.localidad+"|"+@qiwo_enterprise.municipio+"|"+@qiwo_enterprise.estado+"|"+@qiwo_enterprise.pais+"|"+@qiwo_enterprise.mycp+"
CONCEPTOS|"
              
              cfdiReal=cfdiReal+ventaAFacturar.length.to_s+"
"

            #aqui vamos por los productos comprado
            ventaAFacturar.each do |singleVentaAFacturar|
            realProduct=QiwoProduct.where(:id=>singleVentaAFacturar.qiwo_product_id).last
            if realProduct.measure_units=="" or realProduct.measure_units==nil
              cfdiReal+="|No Aplica|"
            else
              cfdiReal+="|"+QiwoMeasureUnit.find_by_id(realProduct.measure_units).label_es.upcase+"|"
            end
            descriptionCorrected=realProduct.description.gsub("\n"," ")
            descriptionCorrected=descriptionCorrected.gsub("\r"," ")
            cfdiReal+=descriptionCorrected+"|"+singleVentaAFacturar.quantity.to_s+"|"+sprintf('%.2f', realProduct.sale_price)+"|"+sprintf('%.2f', singleVentaAFacturar.final_sale)+"
"
          end
            cfdiReal+="IMPUESTOS_TRASLADADOS|"+QiwoRegimen.find_by_typer(origenDatosFactura.regimen).num_impuestos.to_s+"
IVA|16.00|"+sprintf('%.2f', sumaTaxes)
            
            puts cfdiReal.inspect
            fileToSend=Base64.encode64(cfdiReal)
            rollbackOperationFile=cfdiReal
            rollbackOperationFile["|ingreso|"]="|egreso|"
            client = Savon.client(:wsdl=> facturacionWSDL(),:ssl_verify_mode=>:none,ssl_version: :TLSv1)
            begin
              puts "---------------------FACTURA--------------------1"
              respuesta=client.call(:request_timbrar_cfdi,:message=>{:request=>{'emisorRFC'=>origenDatosFactura.rfc,'UserID'=>facturacionUser(),'UserPass'=>facturacionPwd(),'text2CFDI'=>fileToSend}})
              #se genero la factura y hay que guardar que compra tiene factura y descontar los folios
              # puts "XML"
              puts "---------------------FACTURA--------------------2"
              puts respuesta.inspect
              puts "---------------------FACTURA--------------------3"
              #puts respuesta.body[:request_timbrar_cfdi_response][:return][:xml]
              # puts "XML"
              # puts "PDF"
              # puts respuesta.body[:request_timbrar_cfdi_response][:return][:pdf]
              # puts "PDF"
              
              nuevaFactura=QiwoFacturas.new
              nuevaFactura.store_from=empresaQueFactura.qiwo_enterprise_id
              nuevaFactura.store_to=@qiwo_enterprise.id

              nuevaFactura.rollbackop=rollbackOperationFile
              nuevaFactura.folio=empresaQueFactura.folio_factura
              nuevaFactura.status=1
              nuevaFactura.token=UUIDTools::UUID.random_create.to_s()
              if params[:delta]==1 or params[:delta]=="1"
                nuevaFactura.qiwo_log_kart_id=ventaAFacturar[0].id
              else
                nuevaFactura.qiwo_order_id=ventaAFacturar[0].qiwo_order_id
                #modificar estado de la orden
                ventaDePuntoDeVenta=QiwoOrder.where(:id=>ventaAFacturar[0].qiwo_order_id).last
                ventaDePuntoDeVenta.order_state=13
                ventaDePuntoDeVenta.save
              end
              #en esta parte sacamos totalmente el string
              # facturaSeparada=respuesta.to_s[respuesta.to_s.index("string")+8,respuesta.to_s.index("</xml>")]
              # facturaSeparada=facturaSeparada[0,facturaSeparada.to_s.index("</xml>")]

              nuevaFactura.cfdi=respuesta.body[:request_timbrar_cfdi_response][:return][:xml]
              #nuevaFactura.pdf=respuesta.body[:request_timbrar_cfdi_response][:return][:pdf]
              nuevaFactura.save
              #una vez que tenemos la factura podemos convertirla en pdf
              # kit = PDFKit.new('http://qiwo.mx/print_facturas/'+nuevaFactura.token)
              # pdf = kit.to_pdf
              #ahora guardar en base64
              
              empresaQueFactura.folio_factura=empresaQueFactura.folio_factura+1
              empresaQueFactura.save
              #enviamos mail ------------------------>
              mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
              message = {"html"=>'<title>Qiwo facturación</title>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#eb4d41;height:52px;">
<tbody><tr>
<td align="center">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="600px" style="height:100%;">
<tbody><tr>
<td align="left" valign="middle" style="padding-left:20px;color:#ffffff;">

<strong>CONSULTWARE</strong>

</td>
<td align="right" valign="middle" style="padding-right:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="200px" style="height:100%;">
<tbody><tr>
<td width="30px">&nbsp;</td>

<td style="padding-left:15px;">
<a href="http://www.facebook.com/qiwo.mx">
<img src="http://keenthemes.com/assets/img/emailtemplate/social_facebook.png" height="30px" alt="">
</a>
<a href="https://twitter.com/qiwomx">
<img src="http://keenthemes.com/assets/img/emailtemplate/social_twitter.png" height="30px" alt="">
</a>
</td>

</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</center>
</td>
</tr>
</tbody></table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-bottom:1px solid #e7e7e7;">
<tbody><tr>
<td>
<center>
<table border="0" cellpadding="0" cellspacing="0" width="600px" style="height:100%;">
<tbody><tr>
<td colspan="2" height="20px"></td>
</tr>
<tr>
<td align="left" valign="bottom" style="padding-left:20px;padding-bottom:20px;">
<h1>Hola!</h1>
<br>
<div class="ecxtextdark">Ha recibido su factura electronica, por favor haga click <a href="http://qiwo.mx/qiwo_log_karts/descargarPDF?facturaId='+nuevaFactura.token+'">aquí</a> para descargarla en PDF o <a href="http://qiwo.mx/qiwo_log_karts/descargarXML?facturaId='+nuevaFactura.token+'">aquí</a> para descargarla en XML.</div>
<br>
</td>

</tr>
</tbody></table>
</center>
</td>
</tr>
</tbody></table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#f8f8f8;border-bottom:1px solid #e7e7e7;">
<tbody><tr>
<td>
<center>
<table border="0" cellpadding="0" cellspacing="0" width="600px" style="height:100%;">
<tbody><tr>
<td valign="top" style="padding:20px;">
<h2>Qiwo facturación</h2>
<br>
<div class="ecxtextdark">
Qiwo puede ayudarle a administrar sus facturas.
</div>
<br>
</td>
</tr>
</tbody></table>
</center>
</td>
</tr>
</tbody></table>


<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#eb4d41;">
<tbody><tr>
<td align="center">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="600px" style="height:100%;">
<tbody><tr>

© 2014 Consultware de Mexico.
</td>
</tr>
</tbody></table>
</center>
</td>
</tr>
</tbody></table>',
                   "text"=>"Se genero su factura electronica, la puede descargar en dos formatos",
                   "subject"=>"Factura electronica",
                   "from_email"=>"enviofactura@cfdi.ws",
                   "from_name"=>"Qiwo - factura",
                   "to"=>
                      [{"email"=>@qiwo_enterprise.email_sales,
                          "name"=>"Cliente Qiwo Factura",
                          "type"=>"to"},{"email"=>@qiwo_enterprise.email_buy,
                          "name"=>"Cliente Qiwo Factura",
                          "type"=>"to"}],
                   "headers"=>{"Reply-To"=>"message.reply@example.com"},
                   "important"=>false,
                   "track_opens"=>nil,
                   "track_clicks"=>nil,
                   "auto_text"=>nil,
                   "auto_html"=>nil,
                   "inline_css"=>nil,
                   "url_strip_qs"=>nil,
                   "preserve_recipients"=>nil,
                   "view_content_link"=>nil,
                   "tracking_domain"=>nil,
                   "signing_domain"=>nil,
                   "return_path_domain"=>nil,
                   "merge"=>true,
                   "global_merge_vars"=>[{"name"=>"merge1", "content"=>"merge1 content"}],
                   "merge_vars"=>
                      [{"rcpt"=>"recipient.email@example.com",
                          "vars"=>[{"name"=>"merge2", "content"=>"merge2 content"}]}],
                   "tags"=>["password-resets"],
                   "attachments"=>
                      [],
                   "images"=>
                      []}
                  async = false
                  ip_pool = "Main Pool"
                  send_at = DateTime.now-2.hours
                  result = mandrill.messages.send message, async, ip_pool, send_at
                      # [{"email"=>"recipient.email@example.com",
                      #     "status"=>"sent",
                      #     "reject_reason"=>"hard-bounce",
                      #     "_id"=>"abc123abc123abc123abc123abc123"}]
                  
              
              # mail enviado ------------------------>
              #descontamos una unidad
              if params[:delta]==2 or params[:delta]=="2" or params[:delta]=="3" or params[:delta]==3
                actualizarPuntoDeVenta=listadoPuntoDeVenta()
                actualizarPuntoDeVenta.each do |singlePoint|
                  pcontador=QiwoCounter.where(:qiwo_enterprise_id=>singlePoint).last
                  if pcontador!=nil
                    pcontador.facturas_disponibles=pcontador.facturas_disponibles-1
                    if pcontador.facturas_disponibles<0
                      pcontador.facturas_disponibles=0
                    end
                    pcontador.save
                  end
                end
              else
                #cuando no es punto de venta hay que descontar los folios del vendedor
                #obtenemos el usuario propietario de la tienda
                tiendasUsuario=QiwoEnterprise.where(:user_owner=>origenDatosFactura.user_owner).where("regimen is not null")
                #ahora las buscamos en los contadores y modificamos
                oldestAvailable=0
                tiendasUsuario.each do |singleTiendaUsuario|
                  contador=QiwoCounter.where(:qiwo_enterprise_id=>singleTiendaUsuario.id).last
                  if contador==nil
                    contador=QiwoCounter.new
                    contador.qiwo_enterprise_id=singleTiendaUsuario.id
                    contador.save
                  end
                  if contador.facturas_disponibles>oldestAvailable
                    oldestAvailable=contador.facturas_disponibles
                  end
                end
                tiendasUsuario.each do |singleTiendaUsuario|
                  contador=QiwoCounter.where(:qiwo_enterprise_id=>singleTiendaUsuario.id).last
                  contador.facturas_disponibles=oldestAvailable-1
                  if contador.facturas_disponibles<0
                    contador.facturas_disponibles=0
                  end
                  contador.save
                end
              end
              if params[:delta]==1 or params[:delta]=="1"
                redirect_to "/qiwo_log_karts", notice: 'La factura electronica se genero correctamente, ya puede descargarla de la compra.' 
              elsif params[:delta]==2 or params[:delta]=="2"
                
                redirect_to "/sale_points", notice: 'La factura electronica se genero correctamente y fue enviada al email del cliente.' 
              elsif params[:delta]==3 or params[:delta]=="3"
                redirect_to "/qiwo_orders", notice: 'La factura electronica se genero correctamente y fue enviada al email del cliente.' 
              end
            return
            rescue Exception => e
              if params[:delta]==1 or params[:delta]=="1"
                redirect_to "/qiwo_log_karts", notice: e.message
              elsif params[:delta]==2 or params[:delta]=="2"
                
                redirect_to "/sale_points", notice: e.message
              elsif params[:delta]==3 or params[:delta]=="3"
                redirect_to "/qiwo_orders", notice: e.message
              end
              return
            end
            
          end
        else
          #es nil entonces no han comprado facturas, comentar que no tiene para facturar
           if params[:delta]==1 or params[:delta]=="1"
            redirect_to "/qiwo_log_karts", notice: 'No se ha podido facturar, parece ser que el vendedor agotó sus folios CFDI.' 
            elsif params[:delta]==2 or params[:delta]=="2"  
                redirect_to "/sale_points", notice: 'No se ha podido facturar, parece ser que el vendedor agotó sus folios CFDI.' 
            elsif params[:delta]==3 or params[:delta]=="3"
              redirect_to "/qiwo_orders", notice: 'No se ha podido facturar, parece ser que el vendedor agotó sus folios CFDI.' 
            end
          return
        end
      end
      respond_to do |format|
        if @qiwo_enterprise.save
          if params[:delta]==1 or params[:delta]=="1"
            format.html { redirect_to "/qiwo_enterprises", notice: 'Negocio Qiwo agregado con éxito.' }
          elsif params[:delta]==2 or params[:delta]=="2"
            format.html { redirect_to "/sale_points", notice: 'Negocio Qiwo agregado con éxito.' }
          elsif params[:delta]==3 or params[:delta]=="3"
            format.html { redirect_to "/qiwo_orders", notice: 'Negocio Qiwo agregado con éxito.' }
          elsif params[:delta]==4 or params[:delta]=="4"

            responseFromGuide=attachGuideToClient(QiwoEnterprise.where(:user_owner=>session[:activeUser].id,:is_shipping=>1).first,@qiwo_enterprise)
            puts ">>>>>>>>>"+responseFromGuide
            format.html { redirect_to "/redpacks", notice: responseFromGuide }
          end
            
          # superAttachGuide(addressSource,addressDestination,shippingGuide,contenido,valor,logKart,logOrder)
          #format.json { render json: @qiwo_enterprise, status: :created, location: @qiwo_enterprise }
        else
          format.html { render action: "qiwo_enterprises" }
          #format.json { render json: @qiwo_enterprise.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
          format.html { redirect_to "/qiwo_enterprises", notice: 'No agregado debido a tu plan de suscripción actual.' }
          format.json { render json: @qiwo_enterprise, status: :created, location: @qiwo_enterprise }
      end
    end
  end

  def attachGuideToClient(direccionDeRecoleccionInput,direccionDeEnvioInput)
    mensajeRespuesta="No se pudo completar la asignación del folio."

    
        # hay que comprobar si tiene dirección de envio, esta es la direccion de quien envia
        direccionDeRecoleccion=direccionDeRecoleccionInput
        
        if direccionDeRecoleccion!=nil
          if direccionDeRecoleccion.telephones!=nil and 
                direccionDeRecoleccion.comercial_name!=nil and 
                direccionDeRecoleccion.email_sales!=nil and 
                direccionDeRecoleccion.email_buy!=nil and 
                direccionDeRecoleccion.description!=nil and 
                direccionDeRecoleccion.calle!=nil and 
                direccionDeRecoleccion.numexterior!=nil and 
                direccionDeRecoleccion.colonia!=nil and
                direccionDeRecoleccion.municipio!=nil and
                direccionDeRecoleccion.estado!=nil and
                direccionDeRecoleccion.pais!=nil and
                direccionDeRecoleccion.mycp!=nil
                  # hay que verificar si el cliente tiene direccion de envio
                  direccionDeEnvio=direccionDeEnvioInput
                  if direccionDeEnvio!=nil
                    # hay que revisar que se tengan todos los datos
                    if direccionDeEnvio.telephones!=nil and 
                      direccionDeEnvio.comercial_name!=nil and 
                      direccionDeEnvio.email_sales!=nil and 
                      direccionDeEnvio.email_buy!=nil and 
                      direccionDeEnvio.description!=nil and 
                      direccionDeEnvio.calle!=nil and 
                      direccionDeEnvio.numexterior!=nil and 
                      direccionDeEnvio.colonia!=nil and
                      direccionDeEnvio.municipio!=nil and
                      direccionDeEnvio.estado!=nil and
                      direccionDeEnvio.pais!=nil and
                      direccionDeEnvio.mycp!=nil
                      ultimaGuiaDisponible=ShippingGuide.where(:id_from=>session[:activeUser].id,:id_to=>nil,:gtype=>isInTestModeNumber()).first
                      if ultimaGuiaDisponible!=nil
                        resp=superAttachGuide(direccionDeRecoleccion,direccionDeEnvio,ultimaGuiaDisponible.real_guide,"Productos de electronica",1500,-1,-1)  
                        mensajeRespuesta=resp
                      else
                        mensajeRespuesta="Al parecer no cuentas con guias de rastreo para enviar este pedido, puedes adquirir nuevas guias de rastreo de redpack dando click en la parte superior donde aparece tu contador de folios de rastreo."
                      end
                      
                      
                    else 
                      mensajeRespuesta="Tu cliente no ha ingresado su dirección de recepción en el menú de Entrega/recolección. Pidele que complete estos datos para proceder con la asignación del número de rastreo."
                      # Notifications.sendInboxToId([venta.user_id]," el vendedor del artículo "+QiwoProduct.where(:id=>venta.qiwo_product_id).last.name+" intentó generar un número de rastreo para tu compra pero aun te falta completar la información de Entrega/recolección en el menú, atte Qiwo MX")    
                    end
                  else
                    mensajeRespuesta="Tu cliente no ha ingresado su dirección de recepción en el menú de Entrega/recolección. Pidele que complete estos datos para proceder con la asignación del número de rastreo."
                    # Notifications.sendInboxToId([venta.user_id]," el vendedor del artículo "+QiwoProduct.where(:id=>venta.qiwo_product_id).last.name+" intentó generar un número de rastreo para tu compra pero aun te falta completar la información de Entrega/recolección en el menú, atte Qiwo MX")
                  end
                
              else 
                mensajeRespuesta="No has ingresado tu dirección de recolección en el menú de Entrega/recolección. Completalo antes de proceder a asignarle una guia de rastreo."
              end
          else
            mensajeRespuesta="No has ingresado tu dirección de recolección en el menú de Entrega/recolección. Completalo antes de proceder a asignarle una guia de rastreo."  
          end
        return mensajeRespuesta
  end

  # PUT /qiwo_enterprises/1
  # PUT /qiwo_enterprises/1.json
  def update
    if session[:activeUser]==nil 
        redirect_to "/grant_permission.html"
        return
    end
   if currentUserEnterprises().index(params[:id].to_i)==nil  and session[:activeUser].sys_rol!=1
      redirect_to "/yo"
        return
    end
    
    @qiwo_enterprise = QiwoEnterprise.find(params[:id])
     if @qiwo_enterprise.unique_uri==nil or @qiwo_enterprise.unique_uri==""
      #reemplazar espacios
      #primero eliminar puntuaciones
      if @qiwo_enterprise.is_shipping!=1
        @qiwo_enterprise.unique_uri=@qiwo_enterprise.comercial_name.gsub(/[^0-9a-z ]/i, '').downcase
      @qiwo_enterprise.unique_uri=@qiwo_enterprise.unique_uri.gsub(" ", '-')  
      end
      
      #ahora quitar espacios
      
    end
    respond_to do |format|
      if @qiwo_enterprise.update_attributes(params[:qiwo_enterprise])
        if @qiwo_enterprise.is_shipping==1
          format.html { redirect_to "/qiwo_enterprises", notice: 'La dirección de recolección/entrega se modificó correctamente.' }
          format.json { head :no_content }
        else
          format.html { redirect_to "/qiwo_enterprises", notice: 'Negocio Qiwo correctamente modificado.' }
          format.json { head :no_content }
        end
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_enterprise.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_enterprises/1
  # DELETE /qiwo_enterprises/1.json
  def destroy
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    #detectar si tiene productos esa empresa, de ser asi, anunciar que se deben borrar
    productos=QiwoProduct.where(:business_source=>params[:id])
    if productos.length>0
      respond_to do |format|
        format.html { redirect_to qiwo_enterprises_url,notice:'El negocio que intentaste borrar tiene productos, primero borra los productos antes de eliminar el negocio.' }
        format.json { head :no_content }
      end
    else
       if currentUserEnterprises().index(params[:id].to_i)==nil
          redirect_to "/yo"
          return
        end
      @qiwo_enterprise = QiwoEnterprise.find(params[:id])
      @qiwo_enterprise.destroy

      respond_to do |format|
        format.html { redirect_to qiwo_enterprises_url }
        format.json { head :no_content }
      end
    end
  end
end
