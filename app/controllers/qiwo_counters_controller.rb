#!/bin/env ruby
# encoding: utf-8
class QiwoCountersController < ApplicationController
  # GET /qiwo_counters
  # GET /qiwo_counters.json
  def index
    @qiwo_counters = QiwoCounter.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_counters }
    end
  end

  # GET /qiwo_counters/1
  # GET /qiwo_counters/1.json
  def show
    @qiwo_counter = QiwoCounter.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_counter }
    end
  end

  # GET /qiwo_counters/new
  # GET /qiwo_counters/new.json
  def new
    @qiwo_counter = QiwoCounter.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_counter }
    end
  end

  # GET /qiwo_counters/1/edit
  def edit
    @qiwo_counter = QiwoCounter.find(params[:id])
  end

  # POST /qiwo_counters
  # POST /qiwo_counters.json
  def create
    @qiwo_counter = QiwoCounter.new(params[:qiwo_counter])

    respond_to do |format|
      if @qiwo_counter.save
        format.html { redirect_to @qiwo_counter, notice: 'Qiwo counter was successfully created.' }
        format.json { render json: @qiwo_counter, status: :created, location: @qiwo_counter }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_counter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_counters/1
  # PUT /qiwo_counters/1.json
  def update
    @qiwo_counter = QiwoCounter.find(params[:id])

    respond_to do |format|
      if @qiwo_counter.update_attributes(params[:qiwo_counter])
        format.html { redirect_to @qiwo_counter, notice: 'Qiwo counter was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_counter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_counters/1
  # DELETE /qiwo_counters/1.json
  def destroy
    @qiwo_counter = QiwoCounter.find(params[:id])
    @qiwo_counter.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_counters_url }
      format.json { head :no_content }
    end
  end
end
