#!/bin/env ruby
# encoding: utf-8
class QiwoTagsController < ApplicationController
  # GET /qiwo_tags
  # GET /qiwo_tags.json
  def index
    @qiwo_tags = QiwoTag.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_tags }
    end
  end

  # GET /qiwo_tags/1
  # GET /qiwo_tags/1.json
  def show
    @qiwo_tag = QiwoTag.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_tag }
    end
  end

  # GET /qiwo_tags/new
  # GET /qiwo_tags/new.json
  def new
    @qiwo_tag = QiwoTag.new

    respond_to do |format|
      format.html {render layout: false} # new.html.erb
      format.json { render json: @qiwo_tag }
    end
  end

  # GET /qiwo_tags/1/edit
  def edit
    searchResult=QiwoTag.find_by_id(params[:id])
    if searchResult==nil
        #entonces creamos un tag descriptor con ese id de usuario
        @qiwo_tag=QiwoTag.new
        @qiwo_tag.id=params[:id]
        @qiwo_tag.save
    end  
    @qiwo_tag = QiwoTag.find(params[:id])
    render layout: false
  end

  # POST /qiwo_tags
  # POST /qiwo_tags.json
  def create
    @qiwo_tag = QiwoTag.new(params[:qiwo_tag])

    respond_to do |format|
      if @qiwo_tag.save
        format.html { redirect_to @qiwo_tag, notice: 'Qiwo tag was successfully created.' }
        format.json { render json: @qiwo_tag, status: :created, location: @qiwo_tag }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_tags/1
  # PUT /qiwo_tags/1.json
  def update
    @qiwo_tag = QiwoTag.find(params[:id])

    respond_to do |format|
      if @qiwo_tag.update_attributes(params[:qiwo_tag])
        format.html { redirect_to "/yo", notice: 'Se actualizó la información de tus suministros.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_tags/1
  # DELETE /qiwo_tags/1.json
  def destroy
    @qiwo_tag = QiwoTag.find(params[:id])
    @qiwo_tag.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_tags_url }
      format.json { head :no_content }
    end
  end
end
