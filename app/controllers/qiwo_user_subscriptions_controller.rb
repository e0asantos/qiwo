#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/qiwo_subscription.rb'
require '/usr/src/qiwo/app/models/qiwo_log_kart.rb'
require '/usr/src/qiwo/app/models/qlogs.rb'
require "conekta"
require '/usr/src/qiwo/app/controllers/notifications.rb'

class QiwoUserSubscriptionsController < ApplicationController
  protect_from_forgery :except => :create

  def self.userSuscriptionFinalDate(selfsession)
    #primero ver si esta loggeado
     if selfsession[:activeUser]==nil
      return nil
    end
    suscripcion=QiwoUserSubscription.where(:user_id=>selfsession[:activeUser].id).last
    if suscripcion!=nil
      #regresar la fecha
      numeroDeDias=0
      begin
        numeroDeDias=(((suscripcion.ends_on-DateTime.now)/3600)/24).ceil
        if numeroDeDias<0
          return nil
        end
        return numeroDeDias
      rescue Exception => e
        return nil
      end
      
    end
    return nil
  end

  def self.howManyEnterpriseAllowed(selfsession)
    #este metodo determina el numero de empresas que puede agregar un usuario
     if selfsession[:activeUser]==nil
      return 0
    end
    suscripcion=QiwoUserSubscription.where(:user_id=>selfsession[:activeUser].id).where(['ends_on >= ?', DateTime.now]).last
    if suscripcion!=nil
      #regresar el numero permitido
      #por si ya no existe el plan
      planActual=QiwoSubscription.where(:id=>suscripcion.subscription_type).last
      if planActual==nil
        return 0
      end
      return planActual.places_allowed-QiwoEnterprise.where(:user_owner=>selfsession[:activeUser].id).where("founded_date IS NOT NULL").length
    else
      #determinar cuantas empresas aun tenemos disponibles en modo gratuito
      #primero buscar modo gratuito
      modoGratis=QiwoSubscription.where(:price=>["0","0.0","0.00"]).last
      if modoGratis==nil
        return 0
      end
      return modoGratis.places_allowed-QiwoEnterprise.where(:user_owner=>selfsession[:activeUser].id).where("founded_date IS NOT NULL").length
    end
    return 0
  end

  def self.howManyProductsAllowed(selfsession)
    #este metodo determina el numero de empresas que puede agregar un usuario
     if selfsession[:activeUser]==nil
      return 0
    end
    suscripcion=QiwoUserSubscription.where(:user_id=>selfsession[:activeUser].id).where(['ends_on >= ?', DateTime.now]).last
    if suscripcion!=nil
      planActual=QiwoSubscription.where(:id=>suscripcion.subscription_type).last
      if planActual==nil
        return 0
      end
      empresas=QiwoEnterprise.where(:user_owner=>selfsession[:activeUser].id).where("founded_date IS NOT NULL").index_by(&:id).keys
      #regresar los que aun se pueden
      return planActual.products_allowed-QiwoProduct.where(:business_source=>empresas).length
    else
      modoGratis=QiwoSubscription.where(:price=>["0","0.0","0.00"]).last
      if modoGratis==nil
        return 0
      end
      empresas=QiwoEnterprise.where(:user_owner=>selfsession[:activeUser].id).where("founded_date IS NOT NULL").index_by(&:id).keys
      return modoGratis.products_allowed-QiwoProduct.where(:business_source=>empresas).length
    end
    return 0
  end

  def self.userSuscriptionName(selfsession)
    #primero ver si esta loggeado
     if selfsession[:activeUser]==nil
      return nil
    end
    suscripcion=QiwoUserSubscription.where(:user_id=>selfsession[:activeUser].id).last
    planElegido=QiwoSubscription.where(:id=>suscripcion.subscription_type).last
    if suscripcion!=nil and planElegido!=nil
      #regresar la fecha
      return planElegido.subscription_name
    end
    return ""
  end

  def self.userSuscriptionPlan(selfsession)
    #primero ver si esta loggeado
     if selfsession[:activeUser]==nil
      return nil
    end
    suscripcion=QiwoUserSubscription.where(:user_id=>selfsession[:activeUser].id).last
    
    if suscripcion!=nil
      planElegido=QiwoSubscription.where(:id=>suscripcion.subscription_type).last
      #regresar la fecha
      return planElegido
    end
    return nil
  end

  def userCancelSuscription
    
  end
  def incomingFromOpenPay
    #a diferencia de conekta aqui es por parametros y cargo-creado no pagado
    if params[:type]=="charge.created"
      #buscamos la referencia y la escalamos
      puts "SOLO PAYMENT_METHOD:"+params[:transaction][:payment_method][:reference].inspect
      # suscritoPagado=QiwoLogKart.where("openpay_stores='"+params[:transaction][:payment_method][:reference]+"'").last
      # if suscritoPagado!=nil
      #   #subimos de nivel la compra
      #   fuenteProducto=QiwoProduct.where(:id=>suscritoPagado.qiwo_product_id).last
      #   suscritoPagado.final_sale=(finalProductPriceNoTax(suscritoPagado.id)*suscritoPagado.quantity)
      #   suscritoPagado.final_tax=getTax(suscritoPagado.final_sale)
      #   suscritoPagado.final_total=finalProductPrice(suscritoPagado.id)*suscritoPagado.quantity
      #   suscritoPagado.final_gain=(suscritoPagado.final_sale-(fuenteProducto.raw_price*suscritoPagado.quantity))
      #   suscritoPagado.kart_state=2
      #   suscritoPagado.save
      #   #reducimos el stock
      #   fuenteProducto.stock_items=fuenteProducto.stock_items-suscritoPagado.quantity
      #   if fuenteProducto.stock_items<0
      #     fuenteProducto.stock_items=0
      #   end
      #   if fuenteProducto.stock_items==0
      #     #notificamos que no tenemos stock
      #     Notifications.notificateNoStock(session,suscritoPagado.store_id)
      #   end
      #   fuenteProducto.save
      # end
      # Notifications.notificatePayment(session,suscritoPagado.id)
      # sistemaComenta=QiwoReview.new(:sale_id=>suscritoPagado.id,:user_id=>11,:review=>"Hola se detecto un deposito en uno de los productos que tenías pendiente :), ponte en contacto con la otra parte para determinar el cierre de la venta")
      # sistemaComenta.save

      generalPayment(params[:transaction][:payment_method][:reference],params[:transaction])
    elsif params[:type]=="charge.succeeded"
      generalPayment(params[:transaction][:order_id],params[:transaction][:order_id])
    end
      
    #notificamos de la venta a ambas partes

    render :nothing => true, :status => 200
  end

  def incomingFromPaypal
    puts "INCOMING FROM PAYPAL"
    puts params.inspect
    render :nothing => true, :status => 200
  end

  def incomingFromConekta
    puts "INCOMING FROM CONEKTA"
    
    #este es un objeto de pago, recomiendo guardar
    #{"data"=>{"object"=>{"id"=>"52b4c9b3d7e1a0c3e1000041", "livemode"=>false, "created_at"=>1387579827, "status"=>"paid", "currency"=>"MXN", "description"=>"Nivel EmpresarialPara empresas", "reference_id"=>nil, "failure_code"=>nil, "failure_message"=>nil, "monthly_installments"=>nil, "object"=>"charge", "amount"=>38900, "fee"=>1655, "payment_method"=>{"name"=>"uno dos", "exp_month"=>"12", "exp_year"=>"15", "auth_code"=>"425339", "object"=>"card_payment", "last4"=>"4242", "brand"=>"visa"}, "details"=>{"name"=>"Andres Santos", "phone"=>"0000000000", "email"=>"none@none.com", "line_items"=>nil}}, "previous_attributes"=>{"payment_method"=>{}}}, "livemode"=>false, "created_at"=>1387579830, "type"=>"charge.paid", "id"=>"52b4c9b6d7e1a03803000371", "object"=>nil, "controller"=>"qiwo_user_subscriptions", "action"=>"incomingFromConekta", "format"=>"json", "qiwo_user_subscription"=>{"id"=>"52b4c9b6d7e1a03803000371"}}
    begin
      nLog=Qlogs.new
      nLog.modulo="conekta"
      nLog.id_ref=params[:data][:object][:id]
      nLog.id_status=params[:type]
      nLog.save
    puts params[:data][:object][:id]+"============"+params[:type]
    #en nuestro caso solo nos interesa cuando una suscripcion se pago correctamente
    if params[:type]=="subscription.paid"
      #hay que enviarle un mail al usuario de que se pago
      #buscar el id de la suscripcion para ver a quien le corresponde y marcarla con fecha de fin

      suscritoPagado=QiwoUserSubscription.find_by_payment_ref(params[:data][:object][:id])
      if suscritoPagado!=nil
        begin
          sendMailByID([suscritoPagado.user_id],"suscripcion recibida")
          Notifications.sendInboxToId([suscritoPagado.user_id],"Pago detectado de tu suscripción, ya pedimos a nuestros chimpances que te enviaran un mail.")  
        rescue Exception => e
          
        end
        
        suscripcionPointer=QiwoSubscription.find_by_id(suscritoPagado.subscription_type)
        suscritoPagado.amount_paid=suscripcionPointer.price
        #calcular meses

        suscritoPagado.ends_on=DateTime.now+suscripcionPointer.months_active.to_i.months
        suscritoPagado.save
      end
      
      #se pago la suscripcion, entonces buscar la cantidad que pago y la nueva fecha de vencimiento

    elsif params[:type]=="charge.paid"
      generalPayment(params[:data][:object][:id],params[:data][:object][:reference_id])
    end
    rescue Exception => e
      puts "ERRRRRRRRRRRROOOOOORR==>"+e.message+" "+e.backtrace.inspect
    end 
    
    render :nothing => true, :status => 200
  end

  def generalPayment(paymentRef,secondRef)
    #alguien pago algo
      suscritoPagado=QiwoLogKart.where("oxxo='"+paymentRef+"' or banorte='"+paymentRef+"' or conekta='"+paymentRef+"' or openpay_stores='"+paymentRef+"' or inreference='"+secondRef.to_s+"'")
      #buscamos la orden
      #order_state = 10 cancelado
      #order_state = 12 pagado con tarjeta
      #order_state = 11 pagado con efectivo
      #order_state = 13 cerrado (quiere decir ya no la vemos)
      ordenes=QiwoOrder.where("conekta='"+paymentRef+"' or openpay='"+paymentRef+"'").last
      if ordenes!=nil
        #avanzar a status de pagado
        ordenes.order_state=12
        ordenes.save
      end
      puts "INTENTO DE PAGO:"+suscritoPagado.inspect
      if suscritoPagado.length==1
        suscritoPagado=suscritoPagado[0]
        if suscritoPagado.qiwo_product_id==-1
          suscritoPagado.kart_state=12
        else
          puts "ONE------------------------"
          fuenteProducto=QiwoProduct.where(:id=>suscritoPagado.qiwo_product_id).last
          suscritoPagado.final_sale=finalProductPriceNoTax(suscritoPagado.id)*suscritoPagado.quantity
          suscritoPagado.final_tax=getTax(suscritoPagado.final_sale)
          suscritoPagado.final_total=finalProductPrice(suscritoPagado.id)*suscritoPagado.quantity
          suscritoPagado.final_gain=(suscritoPagado.final_sale-(fuenteProducto.raw_price*suscritoPagado.quantity))
          suscritoPagado.discount=fuenteProducto.discount_amount
          if suscritoPagado.kart_state==1 and foliosId().index(suscritoPagado.qiwo_product_id)!=nil
            agregarFolios(suscritoPagado.id)
          end
          if suscritoPagado.kart_state==1 and foliosEnviosId().index(suscritoPagado.qiwo_product_id)!=nil
            agregarFoliosGuia(suscritoPagado.id)
          end
          suscritoPagado.kart_state=2
          #hay que verificar .que tipo de producto es, hay que saber si son folios de facturacion electronica para asignarlos

        end
        #aqui hay que determinar el precio que costo y el iva que obtuvo, asi como la ganancia

        suscritoPagado.save
        modificarStock=QiwoProduct.find_by_id(suscritoPagado.qiwo_product_id)
        modificarStock.stock_items=modificarStock.stock_items-suscritoPagado.quantity
        if modificarStock.stock_items<=0
          modificarStock.stock_items=0
          modificarStock.save  
          #notificar si llegamos a cero
          Notifications.notificateNoStock(session,suscritoPagado.store_id)
        end
        
        puts "TRATANDO DE NOTIFICAR:"+suscritoPagado.id.to_s
        Notifications.notificatePayment(session,suscritoPagado.id)
        sistemaComenta=QiwoReview.new(:sale_id=>suscritoPagado.id,:user_id=>11,:review=>"Hola se detecto un deposito en uno de los productos que tenías pendiente :), ponte en contacto con la otra parte para determinar el cierre de la venta")
        sistemaComenta.save
      elsif suscritoPagado.length>1
        suscritoPagado.each do |singlePayment|
          puts "TWO------------------------"
          fuenteProducto=QiwoProduct.where(:id=>singlePayment.qiwo_product_id).last
          singlePayment.final_sale=finalProductPriceNoTax(singlePayment.id)*singlePayment.quantity
          singlePayment.final_tax=getTax(singlePayment.final_sale)
          singlePayment.final_total=finalProductPrice(singlePayment.id)*singlePayment.quantity
          singlePayment.final_gain=(singlePayment.final_sale-(fuenteProducto.raw_price*singlePayment.quantity))
          if singlePayment.kart_state==1 and foliosId().index(singlePayment.qiwo_product_id)!=nil
            agregarFolios(singlePayment.id)
          end
          if singlePayment.kart_state==1 and foliosEnviosId().index(singlePayment.qiwo_product_id)!=nil
            agregarFoliosGuia(singlePayment.id)
          end
          singlePayment.kart_state=2
          singlePayment.discount=fuenteProducto.discount_amount
          singlePayment.save
          modificarStock=QiwoProduct.find_by_id(singlePayment.qiwo_product_id)
          modificarStock.stock_items=modificarStock.stock_items-singlePayment.quantity
          if modificarStock.stock_items<=0
            modificarStock.stock_items=0
            modificarStock.save  
            #notificar si llegamos a cero
            Notifications.notificateNoStock(session,singlePayment.store_id)
          end
        end
      end
  end
  def paySubscription
    #crear una tarjeta
    # customer = Conekta::Customer.create({
    #     :name => "James Howlett",
    #     :email => "james.howlett@forces.gov",
    #     :phone => "55-5555-5555",
    #     :cards => ["tok_8kZwafM8IcN23Nd9"],
    #     :plan => "gold-plan"
    #   })

    # #vamos a crear un cliente
    # customer = Conekta::Customer.create({
    #     :name => "James Howlett",
    #     :email => "james.howlett@forces.gov",
    #     :phone => "55-5555-5555",
    #     :cards => ["tok_8kZwafM8IcN23Nd9"],
    #     :plan => "gold-plan"
    #   })
    begin
      charge = Conekta::Charge.create({
      "currency"=>"MXN",
      "amount"=> 1000,
      "description"=>"Stogies",
      "reference_id"=>"9839-wolf_pack",
      "card"=> "4772132802645605"
    })
    rescue Exception => e
      charge="ERROR"
    end
    
    puts charge.inspect
    render :nothing => true, :status => 200
  end
  # GET /qiwo_user_subscriptions
  # GET /qiwo_user_subscriptions.json
  def index
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    Conekta.api_key=conektaKeyPrivate()
    @qiwo_subscription = QiwoSubscription.new
    @qiwo_subscriptions = QiwoSubscription.all
    @qiwo_user_subscriptions = QiwoUserSubscription.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_user_subscriptions }
    end
  end

  # GET /qiwo_user_subscriptions/1
  # GET /qiwo_user_subscriptions/1.json
  def show
    @qiwo_user_subscription = QiwoUserSubscription.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_user_subscription }
    end
  end

  # GET /qiwo_user_subscriptions/new
  # GET /qiwo_user_subscriptions/new.json
  def new
    @qiwo_user_subscription = QiwoUserSubscription.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_user_subscription }
    end
  end

  # GET /qiwo_user_subscriptions/1/edit
  def edit
    @qiwo_user_subscription = QiwoUserSubscription.find(params[:id])
    render layout: false 
  end

  # POST /qiwo_user_subscriptions
  # POST /qiwo_user_subscriptions.json
  def create
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    #para lo que viene de conekta Parameters: {"conektaTokenId"=>"tok_nrFF7cT2PoEEhYgqK"}
    @qiwo_user_subscription = QiwoUserSubscription.new
    if params[:conektaTokenId]!=nil
      @qiwo_user_subscription.subscription_type=params[:subscription_type]
      @qiwo_user_subscription.user_id=session[:activeUser].id
      @qiwo_user_subscription.payment_gateway="Conekta"
      #para evitar nulos y errores en las tarjetas con la fecha ponemos una fecha normal
      @qiwo_user_subscription.ends_on=DateTime.now
      @qiwo_user_subscription.amount_paid=0
    end
    begin
      sendMailToAdmin('Este email se envió a los administradores por que se detecto un intento de suscripción de pago.')
      sendMailByIdSmallTemplate([session[:activeUser].id],"Procesando pago de suscripción","El pago demora unos minutos","Tu pago esta siendo resguardado por nuestros guardias de seguridad apoyados por perros altamente entrenados para proteger tu información, si en los próximos 2 a 6 minutos no recibes un correo de confirmación de pago es probable que no se haya realizado tu pago.")
    rescue Exception => e
      
    end
    Conekta.api_key=conektaKeyPrivate()
    nombreCompleto=" "
    if session[:activeUser].name!=nil
      nombreCompleto+=session[:activeUser].name+" "
    end
    if session[:activeUser].last_name!=nil
      nombreCompleto+=session[:activeUser].last_name
    end
    customer = Conekta::Customer.create({
      name: nombreCompleto,
      email: "none@none.com",
      phone: "0000000000",
      cards: [params[:conektaTokenId]]}
    )
    susc=nil
    if isInTestMode()==true
    susc=customer.create_subscription({
    plan: "qiwo-plan-19"
    })
  else 
    susc=customer.create_subscription({
    plan: "qiwo-plan-"+params[:subscription_type]
    })
  end
    @qiwo_user_subscription.payment_ref=susc.id
    #guardar la suscripcion y de esa forma sabemos a quien pertenece
    respond_to do |format|
      if @qiwo_user_subscription.save
        format.html { redirect_to "/qiwo_user_subscriptions", notice: 'La suscripcion a Qiwo se genero exitosamente.' }
        format.json { render json: @qiwo_user_subscription, status: :created, location: @qiwo_user_subscription }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_user_subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_user_subscriptions/1
  # PUT /qiwo_user_subscriptions/1.json
  def update
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_user_subscription = QiwoUserSubscription.find(params[:id])

    respond_to do |format|
      if @qiwo_user_subscription.update_attributes(params[:qiwo_user_subscription])
        format.html { redirect_to @qiwo_user_subscription, notice: 'Qiwo user subscription was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_user_subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_user_subscriptions/1
  # DELETE /qiwo_user_subscriptions/1.json
  def destroy
    if session[:activeUser].sys_rol==1
      @qiwo_user_subscription = QiwoUserSubscription.find(params[:id])
      @qiwo_user_subscription.destroy

      respond_to do |format|
        format.html { redirect_to qiwo_user_subscriptions_url }
        format.json { head :no_content }
      end
    end
  end


end
