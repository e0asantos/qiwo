#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/qiwo_log_kart.rb'
require '/usr/src/qiwo/app/models/qiwo_product.rb'
require '/usr/src/qiwo/app/models/qiwo_enterprise.rb'
require '/usr/src/qiwo/app/models/qiwo_review.rb'
require '/usr/src/qiwo/app/models/sys_user.rb'
require '/usr/src/qiwo/app/models/telegram.rb'
require '/usr/src/qiwo/app/models/api_token_user_interface.rb'
require 'xmpp4r_facebook'
require 'xmpp4r/roster'
require 'fb_graph'
require "uuidtools"
require "open3"
require 'fileutils'
require 'mandrill'
class Notifications

  def self.sendInboxToId(iDList,mensaje)
    token=Api_Token_User_Interface.where(:sys_user=>11,:api_interface=>2).last

    iDList.each do |singleList|

      direccionString=Sys_User.find_by_id(singleList)
      if direccionString!=nil
        cuerpoMensaje='Hola '+direccionString.name.to_s+', '+mensaje
        puts cuerpoMensaje.inspect
        puts cuerpoMensaje.inspect
        id = '-100005654102627@chat.facebook.com'
        if direccionString.user_id!=nil
          
        
            to = '-'+direccionString.user_id+'@chat.facebook.com'
          #buscar producto
            body = cuerpoMensaje
            subject = 'no titulo'
            message = Jabber::Message.new to, body
            # message.subject = subject
            
            # client = Jabber::Client.new Jabber::JID.new(id)
            # client.connect

            # client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)
            
            # client.send message
            # client.close
            emailTemplate=Mailtemplate.where(:usage=>"comment").last.content.sub! '--CONTENT--', cuerpoMensaje
              Notifications.sendMailToAddressFromTemplate([Sys_User.find_by_id(singleList).email],emailTemplate)
          else
            #analizar si es un telefono de telegram
            Notifications.sendTelegramToUser([singleList],cuerpoMensaje)
          end
        end
    end

  end

	def self.sendInfoToInbox(session,productid)
		qiwoUser=session[:activeUser]
    token=Api_Token_User_Interface.where(:sys_user=>11,:api_interface=>2).last
      if qiwoUser!=nil
          puts "enviando mensaje"
          currentSearch = QiwoProduct.find(productid)
          puts currentSearch.inspect
          cuerpoMensaje='Hola '+session[:activeUser].name.to_s+' la info es, '+currentSearch.name.to_s+' de $'+currentSearch.sale_price.to_s+' en '+QiwoEnterprise.find_by_id(currentSearch.business_source).location.to_s+' Qiwo MX'
          puts cuerpoMensaje.inspect
          puts cuerpoMensaje.inspect
          id = '-100005654102627@chat.facebook.com'
          if session[:activeUser].user_id!=nil
            
          
            to = '-'+session[:activeUser].user_id+'@chat.facebook.com'
          #buscar producto
            body = cuerpoMensaje
            subject = 'no titulo'
            # message = Jabber::Message.new to, body
            # message.subject = subject
            
            # client = Jabber::Client.new Jabber::JID.new(id)
            # client.connect

            # client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)
            
            # client.send message
            # client.close
            emailTemplate=Mailtemplate.where(:usage=>"comment").last.content.sub! '--CONTENT--', cuerpoMensaje
            Notifications.sendMailToAddressFromTemplate([session[:activeUser].email],emailTemplate)
          else
            #analizar telegram
            Notifications.sendTelegramToUser([session[:activeUser].id],cuerpoMensaje)
          end
		end
	end

	def self.sendGeneralInfo(session,message)
		qiwoUser=session[:activeUser]
    token=Api_Token_User_Interface.where(:sys_user=>11,:api_interface=>2).last
      if qiwoUser!=nil
          puts "enviando mensaje"
          cuerpoMensaje='Hola '+session[:activeUser].name+' la información es la siguiente, '+message+' Qiwo MX'
          puts cuerpoMensaje.inspect
          id = '-100005654102627@chat.facebook.com'
          if session[:activeUser].user_id!=nil
            to = '-'+session[:activeUser].user_id+'@chat.facebook.com'
          #buscar producto
            body = cuerpoMensaje
            subject = 'no titulo'
            # message = Jabber::Message.new to, body
            # message.subject = subject
            
            # client = Jabber::Client.new Jabber::JID.new(id)
            # client.connect

            # client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)
            
            # client.send message
            # client.close
            emailTemplate=Mailtemplate.where(:usage=>"comment").last.content.sub! '--CONTENT--', cuerpoMensaje
            Notifications.sendMailToAddressFromTemplate([session[:activeUser].email],emailTemplate)
          else
            #analizar telegram
            Notifications.sendTelegramToUser([session[:activeUser].id],cuerpoMensaje)
          end
		end
	end

  def self.notificateNoStock(session,idTienda)
    qiwoUser=session[:activeUser]
    token=Api_Token_User_Interface.where(:sys_user=>11,:api_interface=>2).last
      if qiwoUser!=nil
        #vamos a buscar el dueño de la tienda
        owner=QiwoEnterprise.find_by_id(idTienda)
        cargarUsuario=Sys_User.find_by_id(owner.user_owner)
          puts "enviando mensaje"
          cuerpoMensaje='Hola '+cargarUsuario.name+', parece que uno de tus productos se ha quedado sin stock sin embargo aun puedes recibir pagos.  Qiwo MX'
          puts cuerpoMensaje.inspect
          id = '-100005654102627@chat.facebook.com'
          if cargarUsuario.user_id!=nil
            
          
            to = '-'+cargarUsuario.user_id+'@chat.facebook.com'
          #buscar producto
            body = cuerpoMensaje
            subject = 'No stock'
            # message = Jabber::Message.new to, body
            # message.subject = subject
            
            # client = Jabber::Client.new Jabber::JID.new(id)
            # client.connect

            # client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)
            
            # client.send message
            # client.close
            emailTemplate=Mailtemplate.where(:usage=>"generic").last.content.sub! '--CONTENT--', cuerpoMensaje
            emailTemplate=emailTemplate.sub! '--TITLE--', "¡Sin stock!"
            Notifications.sendMailToAddressFromTemplate([session[:activeUser].email],emailTemplate)
          else
            #analizar telegram
            Notifications.sendTelegramToUser([cargarUsuario.id],cuerpoMensaje)
          end
    end
  end

	def self.newCommentOnProduct(session,mensaje)
		qiwoUser=session[:activeUser]
    token=Api_Token_User_Interface.where(:sys_user=>11,:api_interface=>2).last
      if qiwoUser!=nil
          puts "enviando mensaje"
          buscarProducto=QiwoProduct.find_by_id(mensaje)
          notificarUsuarios=QiwoReview.select(:user_id).where(:product_id=>mensaje).index_by(&:user_id).keys
          notificarUsuarios.delete(session[:activeUser].id)
          buscarNegocio=QiwoEnterprise.find_by_id(buscarProducto.business_source)
          propietario=Sys_User.find_by_id(buscarNegocio.user_owner)
          cuerpoMensaje='Hola '+propietario.name+' recibiste un comentario en '+buscarProducto.name+' que tienes a la venta. Qiwo MX'
          puts cuerpoMensaje.inspect
          id = '-100005654102627@chat.facebook.com'
          puts "pre-notify email"
          if notificarUsuarios.length==0
            #enviar al propietario
            puts "notify-length="+notificarUsuarios.length.to_s
            if propietario.user_id!=nil
              if propietario.email==nil
                puts "USUARIO SIN EMAIL"
                return
              end
              puts "post-notify email:"+propietario.email
            
              to = '-'+propietario.user_id.to_s+'@chat.facebook.com'
            #buscar producto
              body = cuerpoMensaje
              subject = 'no titulo'
              # message = Jabber::Message.new to, body
              # message.subject = subject
              
              # client = Jabber::Client.new Jabber::JID.new(id)
              # client.connect

              # client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)
              
              # client.send message
              # client.close
              # enviamos mail
              emailTemplate=Mailtemplate.where(:usage=>"comment").last.content.sub! '--CONTENT--', cuerpoMensaje
              Notifications.sendMailToAddressFromTemplate([propietario.email],emailTemplate)
            else
              #telegram
              Notifications.sendTelegramToUser([propietario.id],cuerpoMensaje)
            end
          else
            notificarUsuarios.each do |singleNotificar|
              cuerpoMensaje='Hola '+Sys_User.find_by_id(singleNotificar).name+' recibiste un comentario en '+buscarProducto.name+' que tienes a la venta. Qiwo MX'
              puts id
              if Sys_User.find_by_id(singleNotificar).user_id!=nil
                puts "few-notify"
                to = '-'+Sys_User.find_by_id(singleNotificar).user_id.to_s+'@chat.facebook.com'
                puts to
              #buscar producto
                body = cuerpoMensaje
                subject = 'no titulo'
                # message = Jabber::Message.new to, body
                # message.subject = subject
                
                # client = Jabber::Client.new Jabber::JID.new(id)
                # client.connect

                # client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)
                
                # client.send message
                # client.close
                emailTemplate=Mailtemplate.where(:usage=>"comment").last.content.sub! '--CONTENT--', cuerpoMensaje
                Notifications.sendMailToAddressFromTemplate([Sys_User.find_by_id(singleNotificar).email],emailTemplate)
              else
                #telegram
                Notifications.sendTelegramToUser([singleNotificar],cuerpoMensaje)
              end
            end
          end
		end
	end

	def self.newCommentOnSale(session,mensaje)
		qiwoUser=session[:activeUser]
      if qiwoUser!=nil
        token=Api_Token_User_Interface.where(:sys_user=>11,:api_interface=>2).last
          puts "enviando mensaje"
          notificarUsuarios=QiwoReview.select(:user_id).where(:sale_id=>mensaje).index_by(&:user_id).keys
          notificarUsuarios.delete(session[:activeUser].id)

          #buscar propietario
          
          comprador=Sys_User.find_by_id(QiwoLogKart.find_by_id(mensaje).user_id)
          buscarProducto=QiwoProduct.find_by_id(QiwoLogKart.find_by_id(mensaje).qiwo_product_id)
          buscarNegocio=QiwoEnterprise.find_by_id(buscarProducto.business_source)
          propietario=Sys_User.find_by_id(buscarNegocio.user_owner)
          if notificarUsuarios.length==0
            #verificar si el usuario que comento es el mismo propietario
            if comprador.user_id==session[:activeUser]
              #enviar a propietario
              notificarUsuarios.push(propietario.id)
            else
              notificarUsuarios.push(comprador.id)
            end
            
          end
          #tambien descomentar los comentarios de qiwo
          puts "PERSONAS QUE COMENTARON:"+notificarUsuarios.inspect
          
          id = '-100005654102627@chat.facebook.com'
          
          #en caso de que aun no haya comentarios

          if notificarUsuarios.length==0
          	return
          end
          if Sys_User.find_by_id(notificarUsuarios[0]).user_id!=nil
            to = '-'+Sys_User.find_by_id(notificarUsuarios[0]).user_id.to_s+'@chat.facebook.com'
            cuerpoMensaje='Hola '+Sys_User.find_by_id(notificarUsuarios[0]).name+' recibiste un comentario en una de tus compras. Qiwo MX'
            puts cuerpoMensaje.inspect
            #buscar producto
              body = cuerpoMensaje
              subject = 'no titulo'

              emailTemplate=Mailtemplate.where(:usage=>"comment").last.content.sub! '--CONTENT--', cuerpoMensaje
              Notifications.sendMailToAddressFromTemplate([Sys_User.find_by_id(notificarUsuarios[0]).email],emailTemplate)
              # message = Jabber::Message.new to, body
              # message.subject = subject
              
              # client = Jabber::Client.new Jabber::JID.new(id)
              # client.connect

              # client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)
              
              # client.send message
              # client.close
          else
            #telegram
            Notifications.sendTelegramToUser([notificarUsuarios[0]],'Hola '+Sys_User.find_by_id(notificarUsuarios[0]).name+' recibiste un comentario en una de tus compras. Qiwo MX')
          end
		end
	end

  def self.sendTelegramToUser(arrayUsers,mensaje)
    #el arrayUsers es una lista de id de usuarios
    puts "Enviando TELEGRAM "+mensaje.inspect
    arrayUsers.each do |singleUser|
      #el objeto usuario tiene el numero en el campo mail
      usuario=Sys_User.find_by_id(singleUser)
      if usuario!=nil
        if usuario.user_id==nil and usuario.email.index("+")!=nil
         stdin, stdout, stderr = Open3.popen3('/usr/src/qiwo/telegram-bot/tg/bin/telegram-cli -k /usr/src/qiwo/telegram-bot/tg/tg-server.pub -D') #Open3.popen3('/usr/src/qiwo/tg-master/telegram') 
          resultado=".."
          conta=1
          # while(conta<6)
          #   resultado=stdout.gets
          #   puts conta.to_s+"---"+resultado
          #   conta+=1
          # end
          resultado="..................."
          #+524448573202

          # stdin.puts("add_contact +524448573202 "+fakename+" "+fakelast)4443076957
          stdin.puts("msg "+Telegram.find_by_phone(usuario.email).alias+" "+mensaje) 
        end
      end
    end
  end

  def self.sendTelegramToPhone(arrayPhones,mensaje)
    #arrayPhones es una lista de telefonos
    arrayPhones.each do |singlePhone|
      stdin, stdout, stderr = Open3.popen3('/usr/src/qiwo/telegram-bot/tg/bin/telegram-cli -k /usr/src/qiwo/telegram-bot/tg/tg-server.pub -D') #Open3.popen3('/usr/src/qiwo/tg-master/telegram') 
      resultado=".."
      conta=1
      # while(conta<6)
      #   resultado=stdout.gets
      #   puts conta.to_s+"---"+resultado
      #   conta+=1
      # end
      resultado="..................."
      #+524448573202

      # stdin.puts("add_contact +524448573202 "+fakename+" "+fakelast)4443076957
      stdin.puts("msg "+Telegram.find_by_phone(singlePhone).alias+" "+mensaje)
    end
  end
  def self.sendMailToAddressFromTemplate(adresses,htmlLayout)
    direcciones=[]
    adresses.each do |singleList|
      direcciones.push({"email"=>singleList,"name"=>"Cliente Qiwo","type"=>"to"})    
    end
    mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
              message = {"html"=>htmlLayout,
                   "text"=>"",
                   "subject"=>"Atención a clientes Qiwo",
                   "from_email"=>"enviofactura@cfdi.ws",
                   "from_name"=>"Qiwo - Clientes",
                   "to"=>direcciones,
                   "important"=>false,
                   "track_opens"=>nil,
                   "track_clicks"=>nil,
                   "auto_text"=>nil,
                   "auto_html"=>nil,
                   "inline_css"=>nil,
                   "url_strip_qs"=>nil,
                   "preserve_recipients"=>nil,
                   "view_content_link"=>nil,
                   "tracking_domain"=>nil,
                   "signing_domain"=>nil,
                   "return_path_domain"=>nil,
                   "merge"=>true,
                   "global_merge_vars"=>[{"name"=>"merge1", "content"=>"merge1 content"}],
                   "merge_vars"=>[],
                   "tags"=>["password-resets"],
                   "attachments"=>
                      [],
                   "images"=>
                      []}
                  async = false
                  ip_pool = "Main Pool"
                  send_at = DateTime.now-2.hours
                  result = mandrill.messages.send message, async, ip_pool, send_at
                  puts "MANDRIL===>"
                  puts result
                  puts "<=====MANDRIL"
  end
	def self.notificatePayment(session,mensaje)
		qiwoUser=session[:activeUser]
    token=Api_Token_User_Interface.where(:sys_user=>11,:api_interface=>2).last
		puts "DENTRO DE NOTIFICATE:"+qiwoUser.inspect
      
          puts "enviando mensaje"

          notificarUsuarios=[]
          notificarUsuariosID=[]
          comprador=Sys_User.find_by_id(QiwoLogKart.find_by_id(mensaje).user_id)
          buscarProducto=QiwoProduct.find_by_id(QiwoLogKart.find_by_id(mensaje).qiwo_product_id)
          buscarNegocio=QiwoEnterprise.find_by_id(buscarProducto.business_source)
          propietario=Sys_User.find_by_id(buscarNegocio.user_owner)
          cuerpoMensaje='Hola se detecto un deposito en uno de los productos que tenías pendiente :), ponte en contacto con la otra parte para determinar el cierre de la venta'
          puts cuerpoMensaje.inspect
          notificarUsuarios.push(comprador.user_id)
          notificarUsuarios.push(propietario.user_id)
          notificarUsuarios.each do |notificarID|
            if notificarID!=nil
  	          id = '-100005654102627@chat.facebook.com'
  	          to = '-'+notificarID+'@chat.facebook.com'
  	          #buscar producto
  	            body = cuerpoMensaje
  	            subject = 'no titulo'
  	            # message = Jabber::Message.new to, body
  	            # message.subject = subject
  	            
  	            # client = Jabber::Client.new Jabber::JID.new(id)
  	            # client.connect

  	            # client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)
  	            
  	            # client.send message
               #  client.close
               emailTemplate=Mailtemplate.where(:usage=>"payment").last.content.sub! '--CONTENT--', cuerpoMensaje
              Notifications.sendMailToAddressFromTemplate([Sys_User.find_by_user_id(notificarID).email],emailTemplate)
              else
                #telegram

                Notifications.sendTelegramToUser([Sys_User.find_by_user_id(notificarID).id],cuerpoMensaje)
              end
	        end
	end
end