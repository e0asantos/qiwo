#!/bin/env ruby
# encoding: utf-8
class QiwoItemTagsController < ApplicationController
  # GET /qiwo_item_tags
  # GET /qiwo_item_tags.json
  def index
    @qiwo_item_tag=QiwoItemTag.new
    @qiwo_item_tags = QiwoItemTag.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_item_tags }
    end
  end

  # GET /qiwo_item_tags/1
  # GET /qiwo_item_tags/1.json
  def show
    @qiwo_item_tag = QiwoItemTag.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_item_tag }
    end
  end

  # GET /qiwo_item_tags/new
  # GET /qiwo_item_tags/new.json
  def new
    @qiwo_item_tag = QiwoItemTag.new

    respond_to do |format|
      format.html { render layout: false }# new.html.erb
      format.json { render json: @qiwo_item_tag }
    end
  end

  # GET /qiwo_item_tags/1/edit
  def edit
    @qiwo_item_tag = QiwoItemTag.find(params[:id])
    render :layout => false
  end

  # POST /qiwo_item_tags
  # POST /qiwo_item_tags.json
  def create
    @qiwo_item_tag = QiwoItemTag.new(params[:qiwo_item_tag])

    respond_to do |format|
      if @qiwo_item_tag.save
        format.html { redirect_to @qiwo_item_tag, notice: 'Se creo una URL' }
        format.json { render json: @qiwo_item_tag, status: :created, location: @qiwo_item_tag }
      else
        redirect_to "/qiwo_item_tags"
        format.json { render json: @qiwo_item_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_item_tags/1
  # PUT /qiwo_item_tags/1.json
  def update
    @qiwo_item_tag = QiwoItemTag.find(params[:id])

    respond_to do |format|
      if @qiwo_item_tag.update_attributes(params[:qiwo_item_tag])
        format.html { redirect_to @qiwo_item_tag, notice: 'Qiwo item tag was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_item_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_item_tags/1
  # DELETE /qiwo_item_tags/1.json
  def destroy
    @qiwo_item_tag = QiwoItemTag.find(params[:id])
    @qiwo_item_tag.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_item_tags_url }
      format.json { head :no_content }
    end
  end
end
