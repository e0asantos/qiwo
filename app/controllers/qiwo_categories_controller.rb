#!/bin/env ruby
# encoding: utf-8
class QiwoCategoriesController < ApplicationController
  # GET /qiwo_categories
  # GET /qiwo_categories.json
  def index
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    #primero traer los insumos
    insumos=QiwoTag.where(:id=>session[:activeUser].id).last
    #ahora recorrer los insumos y usar la busqueda
    @qiwo_categories = QiwoCategory.all
    if insumos!=nil
      if insumos.tag_name!=nil and insumos.tag_name!=""
        @qiwo_providers=globalQiwoSearch(insumos.tag_name.split(",").join(" "))      
      else
        @qiwo_providers=[]
      end
    else
      @qiwo_providers=[]
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_categories }
    end
  end

  # GET /qiwo_categories/1
  # GET /qiwo_categories/1.json
  def show
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_category = QiwoCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_category }
    end
  end

  # GET /qiwo_categories/new
  # GET /qiwo_categories/new.json
  def new
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_category = QiwoCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_category }
    end
  end

  # GET /qiwo_categories/1/edit
  def edit
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_category = QiwoCategory.find(params[:id])
  end

  # POST /qiwo_categories
  # POST /qiwo_categories.json
  def create
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_category = QiwoCategory.new(params[:qiwo_category])

    respond_to do |format|
      if @qiwo_category.save
        format.html { redirect_to @qiwo_category, notice: 'Qiwo category was successfully created.' }
        format.json { render json: @qiwo_category, status: :created, location: @qiwo_category }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_categories/1
  # PUT /qiwo_categories/1.json
  def update
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_category = QiwoCategory.find(params[:id])

    respond_to do |format|
      if @qiwo_category.update_attributes(params[:qiwo_category])
        format.html { redirect_to @qiwo_category, notice: 'Qiwo category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_categories/1
  # DELETE /qiwo_categories/1.json
  def destroy
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_category = QiwoCategory.find(params[:id])
    @qiwo_category.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_categories_url }
      format.json { head :no_content }
    end
  end
end
