#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/qiwo_enterprise.rb'
require '/usr/src/qiwo/app/models/qiwo_tag.rb'
require '/usr/src/qiwo/app/models/qiwo_measure_unit.rb'
require '/usr/src/qiwo/app/models/qiwo_log_kart.rb'
require 'koala'
class QiwoProductsController < ApplicationController
  protect_from_forgery :except => [:applyDiscount,:shareProduct]

  def getProductsAsToken
    productos=currentUserProducts()
    tokens=[]
    productos.each do |singleProduct|
      tokens.push(["{{qiwo-product="+singleProduct.to_s+"}}",QiwoProduct.find_by_id(singleProduct).name,QiwoProduct.find_by_id(singleProduct).name])
    end
    respond_to do |format|
      format.html { render :json => tokens.to_json }
      format.xml  { render :xml => tokens.to_json }
      format.json { render :json => tokens.to_json }
    end
  end
  def shareProduct
    miproducto=params[:prdef]
    paramId=params[:pagid].to_s
    texto=params[:para]
    token=Api_Token_User_Interface.where(:sys_user=>session[:activeUser].id,:api_interface=>2).last
    @graph = Koala::Facebook::API.new(token.token)
    objeto=nil
    #buscar producto
    prd=QiwoProduct.where(:id=>miproducto).last
    enuri=QiwoEnterprise.where(:id=>prd.business_source).last
    if paramId==1 or paramId=="1"
      objeto=@graph.put_wall_post("http://www.qiwo.mx/searches/"+enuri.unique_uri+" Ahora disponible en Qiwo venta de "+prd.name+" "+texto)
    else
      #@graph.put_wall_post(texto,paramId)
      objeto=@graph.put_object(paramId, "feed", :message => "Ahora disponible en Qiwo http://www.qiwo.mx/searches/"+enuri.unique_uri+" venta de "+prd.name+" "+texto)
    end
    #creamos un elemento de enlace con fb
    fblink=QiwoFbLikes.new
    fblink.fb_object_id=objeto["id"]
    fblink.qiwo_product_id=miproducto
    fblink.save
    productos="ok"
    respond_to do |format|
      format.html { render :json => productos.to_json }
      format.xml  { render :xml => productos.to_json }
      format.json { render :json => productos.to_json }
    end
  end

  def applyDiscount
    puts params.inspect
    puts "changeShippingPrice was executed"
    #primero ver si trae el nombre del campo por shipping-cost
    if params[:name].index("discount-cost")!=nil
      #obtener los numeros
      indiceDeProducto=params[:name][14..-1]
      puts "INDICE:"+indiceDeProducto
      #ahora accesar a la session actual
      #con la sesion determinamos si este usuario
      #es propietario de ese producto
      empresas=QiwoEnterprise.select(:id).where(:user_owner=>session[:activeUser].id)
      mis_negocios=empresas.index_by(&:id).keys
      #ahora buscar producto
      producto=QiwoProduct.find_by_id(indiceDeProducto.to_i)
      if producto!=nil
        puts "HAY PRODUCTO"
        #verificar si pertenece
        if mis_negocios.index(producto.business_source)!=nil
          puts "SI PERTENECE AL USUARIO"
          #ahora cambiamos el valor en la venta
          producto.discount_amount=params[:value]
          producto.save
        end
      end
      puts session[:activeUser].name
    end
    render :nothing => true, :status => 200
  end

  # GET /qiwo_products
  # GET /qiwo_products.json
  def index
      if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    token=Api_Token_User_Interface.where(:sys_user=>session[:activeUser].id,:api_interface=>2).last
    @graph = nil

    #crear lista de paginas
    @listaDePaginas=[]
    @yoAdministro=[]
    # if token!=nil
    #   if token.token!=nil
    #     @graph=Koala::Facebook::API.new(token.token)
    #     @listaDePaginas=@graph.get_object("me/accounts")
    #     @yoAdministro=@graph.get_object("me/likes")
        
    #   end
    # end
    
    
    if session[:activeUser].sys_rol==1
      @qiwo_products = QiwoProduct.paginate(:page => params[:page], :per_page => 20).order("created_at DESC")
    else
      misNegocios=QiwoEnterprise.where(:user_owner=>session[:activeUser].id).paginate(:page => params[:page], :per_page => 20).order("created_at DESC")
      misNegociosIndex=misNegocios.index_by(&:id).keys
      @qiwo_products=QiwoProduct.where(:business_source=>misNegociosIndex).paginate(:page => params[:page], :per_page => 20).order("created_at DESC")
    end
    misProductosIndex=@qiwo_products.index_by(&:id).keys
    puts misProductosIndex.inspect
    # likesobjeto=QiwoFbLikes.where(:qiwo_product_id=>misProductosIndex)
    likesobjeto = []
    puts likesobjeto.inspect
    @likesarray=[]
    # likesobjeto.each do |singleLike|
    #   if @likesarray[singleLike.qiwo_product_id]==nil
    #     @likesarray[singleLike.qiwo_product_id]=0
    #   end
    #   if token!=nil
    #     if token.token!=nil
    #       # uno=@graph.get_object(singleLike.fb_object_id+"/likes?summary=1")
    #       # uno.raw_response["summary"]["total_count"].to_i
    #       # @likesarray[singleLike.qiwo_product_id]+=uno.raw_response["summary"]["total_count"].to_i    
    #     end
        
    #   end
      
    # end
    @qiwo_product = QiwoProduct.new
    @qiwo_tag=QiwoTag.new
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_products }
    end
  end

  # GET /qiwo_products/1
  # GET /qiwo_products/1.json
  def show
    @qiwo_product = QiwoProduct.find(params[:id])
    redirect_to "qiwo_products"
    return
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_product }
    end
  end

  # GET /qiwo_products/new
  # GET /qiwo_products/new.json
  def new
    @qiwo_product = QiwoProduct.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_product }
    end
  end

  # GET /qiwo_products/1/edit
  def edit
    @qiwo_product = QiwoProduct.find(params[:id])
    render layout: false
  end

  # POST /qiwo_products
  # POST /qiwo_products.json
  def create
    #verificar cuantos productos permitidos se pueden agregar
    if QiwoUserSubscriptionsController.howManyProductsAllowed(session)>0
      @qiwo_product = QiwoProduct.new(params[:qiwo_product])
      if @qiwo_product.stock_items<0
        @qiwo_product.stock_items=@qiwo_product.stock_items*-1
      end
        puts "creating new product"
        puts @qiwo_product.inspect
        puts "end inspect"
     respond_to do |format|
       if @qiwo_product.save
         format.html { redirect_to "/qiwo_products", notice: 'Se creó un nuevo producto correctamente.' }
         format.json { render json: @qiwo_product, status: :created, location: @qiwo_product }
       else
         format.html { render action: "new" }
         format.json { render json: @qiwo_product.errors, status: :unprocessable_entity }
       end
     end
   else
    respond_to do |format|
         format.html { redirect_to "/qiwo_products", notice: 'La suscripción actual no permite agregar nuevos productos.' }
         format.json { render json: @qiwo_product, status: :created, location: @qiwo_product }
    end
  end
  end

  # PUT /qiwo_products/1
  # PUT /qiwo_products/1.json
  def update
    @qiwo_product = QiwoProduct.find(params[:id])
    if @qiwo_product.stock_items<0
      @qiwo_product.stock_items=@qiwo_product.stock_items*-1
    end
    respond_to do |format|
      if @qiwo_product.update_attributes(params[:qiwo_product])
        format.html { redirect_to "/qiwo_products", notice: 'Se actualizó correctamente el producto.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_products/1
  # DELETE /qiwo_products/1.json
  def destroy
    @qiwo_product = QiwoProduct.find(params[:id])
    @qiwo_product.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_products_url }
      format.json { head :no_content }
    end
  end
end
