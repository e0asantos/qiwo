#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/qiwo_log_kart.rb'
require '/usr/src/qiwo/app/models/sys_user.rb'
require '/usr/src/qiwo/app/controllers/notifications.rb'
class QiwoReviewsController < ApplicationController
  # GET /qiwo_reviews
  # GET /qiwo_reviews.json
  def index
    @qiwo_reviews = QiwoReview.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_reviews }
    end
  end

  # GET /qiwo_reviews/1
  # GET /qiwo_reviews/1.json
  def show
    @qiwo_review = QiwoReview.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_review }
    end
  end

  # GET /qiwo_reviews/new
  # GET /qiwo_reviews/new.json
  def new
    @qiwo_review = QiwoReview.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_review }
    end
  end

  # GET /qiwo_reviews/1/edit
  def edit
    @qiwo_review_d=params[:id]
    @qiwo_review = QiwoReview.find_by_sale_id(params[:id])
    puts "review:"+@qiwo_review.inspect
    render layout: false
  end

  # POST /qiwo_reviews
  # POST /qiwo_reviews.json
  def create
      if session[:activeUser]!=nil
        @qiwo_review = QiwoReview.new(params[:qiwo_review])
        puts "COMENTARIO:"+params[:commentonsale]
        if params[:commentonsale]=="yes"
          puts "SI COMENTARIO:"+@qiwo_review.product_id.inspect
          @qiwo_review.sale_id=@qiwo_review.product_id
          @qiwo_review.product_id=nil
        end
        @qiwo_review.user_id=session[:activeUser].id
        respond_to do |format|
          if @qiwo_review.save
            if params[:commentonsale]=="yes"
                Notifications.newCommentOnSale(session,@qiwo_review.sale_id)
            else
                Notifications.newCommentOnProduct(session,@qiwo_review.product_id)
            end
            format.html { redirect_to @qiwo_review, notice: 'Qiwo review was successfully created.' }
            format.json { render json: @qiwo_review, status: :created, location: @qiwo_review }
          else
            format.html { render action: "new" }
            format.json { render json: @qiwo_review.errors, status: :unprocessable_entity }
          end
        end
    end
  end

  # PUT /qiwo_reviews/1
  # PUT /qiwo_reviews/1.json
  def update
    @qiwo_review = QiwoReview.find(params[:id])

    respond_to do |format|
      if @qiwo_review.update_attributes(params[:qiwo_review])
        format.html { redirect_to @qiwo_review, notice: 'Qiwo review was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_review.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_reviews/1
  # DELETE /qiwo_reviews/1.json
  def destroy
    @qiwo_review = QiwoReview.find(params[:id])
    @qiwo_review.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_reviews_url }
      format.json { head :no_content }
    end
  end
end
