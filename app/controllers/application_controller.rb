#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/qiwo_image.rb'
require '/usr/src/qiwo/app/models/qiwo_enterprise.rb'
require '/usr/src/qiwo/app/models/sat_document.rb'
require 'fm_timbrado_cfdi'
require 'mail'
require 'mandrill'
require 'savon'
require "uuidtools"
require 'curb'

class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :getTax
  helper_method :conektaFormat
  helper_method :finalProductPrice
  helper_method :finalProductPriceNoTax
  helper_method :requestRing
  helper_method :currentUserEnterprises
  helper_method :conFacturaElectronica
  helper_method :hackVerificationBiz
  helper_method :hackAttempt
  helper_method :foliosId
  helper_method :foliosEnviosId
  helper_method :agregarFolios
  helper_method :superAttachGuide
  helper_method :agregarFoliosGuia
  helper_method :puntoDeVentaAutorizado
  helper_method :listadoPuntoDeVenta
  helper_method :conektaKeyPublic
  helper_method :conektaKeyPrivate
  helper_method :isInTestMode
  helper_method :isInTestModeNumber

  def getRedpackUser
    if isInTestMode()==true
      return 85
    else
      return 85
    end
  end

  def getRedpackPIN
    if isInTestMode()==true
      return 'QA tf+edKE4WTniXVrBpeEhnqgjNdTDKnGAuEPsF8zJzzk='
    else
      return 'PROD tf+edKE4WTniXVrBpeEhnqgjNdTDKnGAuEPsF8zJzzk='
    end
    
  end

  def LocationWkhtmltopdf
    return '/usr/bin/xvfb-run --server-args="-screen 0, 1024x768x24" /usr/bin/wkhtmltopdf'
  end

  def superAttachGuide(addressSource,addressDestination,shippingGuide,contenido,valor,logKart,logOrder)
    puts "----- TRYING SHIPPING --------"
    puts shippingGuide
    puts "----- TRYING SHIPPING --------"
    client = Savon.client(:wsdl=> "http://ws.redpack.com.mx/RedpackAPI_WS/services/RedpackWS?wsdl",:log=>false)
    
    respuesta='<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> 
      <SOAP-ENV:Body> 
        <ns:predocumentacion xmlns:ns="http://ws.redpack.com"> 
          <ns:PIN>'+getRedpackPIN()+'</ns:PIN> 
          <ns:idUsuario>'+getRedpackUser().to_s+'</ns:idUsuario> 
          <ns:guias> 
            <ax21:auxiliar xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:claveDex xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:consignatario xmlns:ax21="http://vo.redpack.com/xsd"> 
               <ax21:calle>'+addressDestination.calle+'</ax21:calle> 
              <ax21:ciudad>'+addressDestination.municipio+'</ax21:ciudad> 
              <ax21:codigoPostal>'+addressDestination.mycp+'</ax21:codigoPostal> 
              <ax21:colonia_Asentamiento>'+addressDestination.colonia+'</ax21:colonia_Asentamiento> 
              <ax21:contacto>'+addressDestination.comercial_name+'</ax21:contacto> 
              <ax21:email>'+addressDestination.email_sales+'</ax21:email> 
              <ax21:estado>'+addressDestination.estado+'</ax21:estado> 
              <ax21:iata xsi:nil="true" /> 
              <ax21:nombre_Compania>ninguna</ax21:nombre_Compania> 
              <ax21:numeroExterior>'+addressDestination.numexterior+'</ax21:numeroExterior>'
              if addressDestination.numinterior!=nil
                respuesta=respuesta+'<ax21:numeroInterior>'+addressDestination.numinterior+'</ax21:numeroInterior> '
              else
                respuesta=respuesta+'<ax21:numeroInterior xsi:nil="true" /> '
              end
              respuesta=respuesta+'<ax21:pais>'+addressDestination.pais+'</ax21:pais> 
              <ax21:referenciaUbicacion xsi:nil="true" /> 
              <ax21:telefonos xmlns:ax21="http://vo.redpack.com/xsd" >
                <ax21:LADA></ax21:LADA>
                <ax21:extension></ax21:extension>
                <ax21:telefono>'+addressDestination.telephones+'</ax21:telefono>
              </ax21:telefonos>  
            </ax21:consignatario> 
            <ax21:costoSeguro xmlns:ax21="http://vo.redpack.com/xsd">NaN</ax21:costoSeguro> 
            <ax21:cotizaciones xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:fechaDocumentacion xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:fechaSituacion xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:flag xmlns:ax21="http://vo.redpack.com/xsd">0</ax21:flag> 
            <ax21:guiaAsegurada xmlns:ax21="http://vo.redpack.com/xsd">false</ax21:guiaAsegurada> 
            <ax21:moneda xmlns:ax21="http://vo.redpack.com/xsd">MXN</ax21:moneda> 
            <ax21:numeroDeGuia xmlns:ax21="http://vo.redpack.com/xsd">'+shippingGuide+'</ax21:numeroDeGuia> 
            <ax21:observaciones xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:paquetes xmlns:ax21="http://vo.redpack.com/xsd"> 
              <ax21:alto>29</ax21:alto> 
              <ax21:ancho>29</ax21:ancho> 
              <ax21:auxiliar xsi:nil="true" /> 
              <ax21:consecutivo>1</ax21:consecutivo> 
              <ax21:descripcion>'+contenido+'</ax21:descripcion> 
              <ax21:detallesRastreo xsi:nil="true" /> 
              <ax21:formatoEtiqueta xsi:nil="true" /> 
              <ax21:largo>29</ax21:largo> 
              <ax21:peso>5</ax21:peso> 
            </ax21:paquetes> 
            <ax21:personaRecibio xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:pruebaDeEnterga xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:referencia xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:remitente xmlns:ax21="http://vo.redpack.com/xsd"> 
              <ax21:calle>'+addressSource.calle+'</ax21:calle> 
              <ax21:ciudad>'+addressSource.municipio+'</ax21:ciudad> 
              <ax21:codigoPostal>'+addressSource.mycp+'</ax21:codigoPostal> 
              <ax21:colonia_Asentamiento>'+addressSource.colonia+'</ax21:colonia_Asentamiento> 
              <ax21:contacto>'+addressSource.comercial_name+'</ax21:contacto> 
              <ax21:email>'+addressSource.email_sales+'</ax21:email> 
              <ax21:estado>'+addressSource.estado+'</ax21:estado> 
              <ax21:iata xsi:nil="true" /> 
              <ax21:nombre_Compania>ninguna</ax21:nombre_Compania> 
              <ax21:numeroExterior>'+addressSource.numexterior+'</ax21:numeroExterior>'
              if addressSource.numinterior!=nil
                respuesta=respuesta+'<ax21:numeroInterior> '+addressSource.numinterior+'</ax21:numeroInterior>'
              else
                respuesta=respuesta+'<ax21:numeroInterior xsi:nil="true" /> '
              end
              
             respuesta=respuesta+'<ax21:pais>'+addressSource.pais+'</ax21:pais>
              <ax21:referenciaUbicacion xsi:nil="true" /> 
              <ax21:telefonos xmlns:ax21="http://vo.redpack.com/xsd" >
                <ax21:LADA></ax21:LADA>
                <ax21:extension></ax21:extension>
                <ax21:telefono>'+addressSource.telephones+'</ax21:telefono>
              </ax21:telefonos> 
            </ax21:remitente> 
            <ax21:resultadoConsumoWS xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:situacion xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:tipoCambio xmlns:ax21="http://vo.redpack.com/xsd">NaN</ax21:tipoCambio> 
            <ax21:tipoEntrega xmlns:ax21="http://vo.redpack.com/xsd"> 
              <ax21:auxiliar xsi:nil="true" /> 
              <ax21:descripcion xsi:nil="true" /> 
              <ax21:equivalencia>DOM</ax21:equivalencia> 
              <ax21:id>2</ax21:id> 
            </ax21:tipoEntrega> 
            <ax21:tipoEnvio xmlns:ax21="http://vo.redpack.com/xsd"> 
              <ax21:auxiliar xsi:nil="true" /> 
              <ax21:descripcion xsi:nil="true" /> 
              <ax21:equivalencia>PAQ</ax21:equivalencia> 
              <ax21:id>1</ax21:id> 
            </ax21:tipoEnvio> 
            <ax21:tipoIdentificacion xsi:nil="true" xmlns:ax21="http://vo.redpack.com/xsd" /> 
            <ax21:tipoServicio xmlns:ax21="http://vo.redpack.com/xsd"> 
              <ax21:auxiliar xsi:nil="true" /> 
              <ax21:descripcion>EXPRESS</ax21:descripcion>
              <ax21:equivalencia xsi:nil="true" /> 
              <ax21:id>1</ax21:id> 
            </ax21:tipoServicio> 
            <ax21:valorDeclarado xmlns:ax21="http://vo.redpack.com/xsd">'+valor.to_s+'</ax21:valorDeclarado> 
          </ns:guias> 
        </ns:predocumentacion> 
      </SOAP-ENV:Body> 
    </SOAP-ENV:Envelope>'


    respuestaWSDL=client.call(:predocumentacion,xml:respuesta)
    postRespuestaWSDL=respuestaWSDL.body[:predocumentacion_response][:return][:resultado_consumo_ws]
    puts postRespuestaWSDL
    puts postRespuestaWSDL[:descripcion]
    mensajeARegresar="Redpack:"+postRespuestaWSDL[:descripcion]
    if (postRespuestaWSDL[:estatus]==1 or postRespuestaWSDL[:estatus]=="1") and (postRespuestaWSDL[:gravedad]==3 or postRespuestaWSDL[:gravedad]=="3")
      puts "PREDOCUEMENTADA CORRECTAMENTE"
      # ahora hay que crear la etiqueta de impresion
      respuestaWSDLLabel=client.call(:obtener_formato_de_impresion,xml:respuesta)
      # buscamos la guia le ponemos el usuario a donde va y le adjuntamos la guia
      objetoGuia=ShippingGuide.where(:id_from=>addressSource.user_owner,:real_guide=>shippingGuide,:gtype=>isInTestModeNumber()).last
      if objetoGuia!=nil
        if logKart!=-1
          objetoGuia.qiwo_log_kart_id=logKart
          ssale=QiwoLogKart.find_by_id(logKart)
          if ssale!=nil
            ssale.shipping_guide=shippingGuide
            ssale.save
          end
        end
        if logOrder!=-1
          objetoGuia.qiwo_order_id=logOrder
          sorder=QiwoLogKart.where(:qiwo_order_id=>logOrder)
          sorder.each do |singleSorder|
            singleSorder.shipping_guide=shippingGuide
            singleSorder.save
          end
          
        end
        if logKart==-1 and logOrder==-1
          objetoGuia.id_to=-1
        else
          objetoGuia.id_to=addressDestination.user_owner
        end
        
        objetoGuia.label_guide=respuestaWSDLLabel.body[:obtener_formato_de_impresion_response][:return][:paquetes][:formato_etiqueta]
        objetoGuia.status_guide=UUIDTools::UUID.random_create.to_s()
        objetoGuia.save
        mensajeARegresar="La guia de rastreo se generó correctamente, a tu cliente y a ti enviamos un inbox con el número, adicionalmente aparece ya visible en la compra para que lo puedan consultar en cualquier momento."
         Notifications.sendInboxToId([addressDestination.user_owner,addressSource.user_owner]," se generó un número de rastreo "+shippingGuide+" de redpack que puedes consultar en Qiwo ó Redpack PDF http://qiwo.mx/qiwo_log_karts/descargarPDF?facturaId="+objetoGuia.status_guide+", atte Qiwo MX")    
      else
        mensajeARegresar="No se encontró la guia en el sistema, ponte en contacto con Qiwo para solucionar el problema."
      end
      
    end

    return mensajeARegresar

    # aqui generamos la etiqueta y la guardamos
    # puts respuestaWSDL.body[:predocumentacion_response][:return][:resultado_consumo_ws]
    # respuestaWSDLLabel=client.call(:obtener_formato_de_impresion,xml:respuesta)
    # respuestaWSDLLabel.body[:obtener_formato_de_impresion_response][:return][:paquetes][:formato_etiqueta]

  end

  def listadoPuntoDeVenta
    @negociosPuntoDeVenta=[]
    #primero ver el permiso si acepta el punto de venta
    planWithSalePoint=QiwoUserSubscription.where(:user_id=>session[:activeUser].id).where(['ends_on >= ?', DateTime.now])
    puts "PLAN_SALE"
    puts session[:activeUser].id
    puts DateTime.now
    puts planWithSalePoint.inspect
    puts "PLAN_SALE"
    if planWithSalePoint.length>0
      #si tiene un plan activo, ahora determinar si el plan o los planes tienen negocios con punto de venta
      planWithSalePoint.each do |singlePlanWithSalePoint|
        currentPlant=QiwoSubscription.where(:id=>singlePlanWithSalePoint.subscription_type).where("sale_point>0").last
        if currentPlant!=nil
          #entonces si tiene permisos de punto de venta, agregar el objeto de empresa
          @negociosPuntoDeVenta=@negociosPuntoDeVenta.concat(currentUserEnterprises())
          puts "TIENE PUNTO DE VENTA 1"
          puts @negociosPuntoDeVenta.inspect
          puts "-----------------------"
        end
      end
    end
    #ahora verificar si tiene permisos de otra tienda para cobrar como empleado 
    empleadoDe=QiwoEmployee.where(:employee_fb_id=>session[:activeUser].user_id)
    #trae los objetos de tienda, ahora verificar que tenga un plan activo con el punto de venta
    miPatronIndice=empleadoDe.index_by(&:employee_enterprise).keys
    #cargadas las empresas verificar que sus dueños tengan plan activo
    ownerEnterprise=QiwoEnterprise.where(:id=>miPatronIndice)
    miPatronEmpresaIndice=ownerEnterprise.index_by(&:user_owner).keys
    planesConSalePoint=QiwoSubscription.where("sale_point>0")
    planesConSalePointIndice=planesConSalePoint.index_by(&:id).keys
    miPatronPlanWithSalePoint=QiwoUserSubscription.where(:user_id=>miPatronEmpresaIndice,:subscription_type=>planesConSalePointIndice).where(['ends_on >= ?', DateTime.now])
    miPatronPlanWithSalePointIndice=miPatronPlanWithSalePoint.index_by(&:user_id).keys
    finalMiPatronWithSalePoint=ownerEnterprise.where(:user_owner=>miPatronPlanWithSalePointIndice)
    puts "patron"
    puts finalMiPatronWithSalePoint.inspect
    if finalMiPatronWithSalePoint!=nil and finalMiPatronWithSalePoint.length>0
      @negociosPuntoDeVenta=@negociosPuntoDeVenta.concat(finalMiPatronWithSalePoint.index_by(&:id).keys).uniq!
    end
    puts "TIENE PUNTO DE VENTA 2"
    puts @negociosPuntoDeVenta.inspect
    puts "-----------------------"
    return @negociosPuntoDeVenta
  end

  def puntoDeVentaAutorizado
    @negociosPuntoDeVenta=listadoPuntoDeVenta()
    if @negociosPuntoDeVenta.length>0
      return true
    else
      return false
    end
  end

  def conFacturaElectronica
    #buscamos las empresas y las regresamos que ya se les registro la documentación con factura electronica
    usanFacturaElectronica=QiwoEnterprise.select("qiwo_enterprises.name").joins(:sat_documents).select("sat_documents.id,sat_documents.updated_at,sat_documents.apitoken").where("sat_documents.user_id='"+session[:activeUser].id.to_s+"' and is_active=1")
    #@mayoresVentas=QiwoProduct.where(:business_source=>misNegociosIndex).joins(:qiwo_log_karts).salesMade.last(10)
    puts "¡¡¡¡¡¡¡¡¡¡¡¡¡¡"
    puts usanFacturaElectronica.inspect
    puts "¡¡¡¡¡¡¡¡¡¡¡¡¡¡"
    return usanFacturaElectronica
  end

  def agregarFolios(idDeVenta)
    #hay que buscar el id de la venta y el propietario y a su vez las empresas para crearles los folios
    ventaFolios=QiwoLogKart.find_by_id(idDeVenta)
    producto=QiwoProduct.find_by_id(ventaFolios.qiwo_product_id)
    empresas=QiwoEnterprise.select(:id).where(:user_owner=>ventaFolios.user_id)
    misNegocios=empresas.index_by(&:id).keys
      #todavia no esta pagado pero son folios, activarlos
      #hay que ver si las empresas del usuario ya tienen contadores, en caso contrrio, crealos
    misNegocios.each do |singleEnterprise|
      #buscar los contadores y sumarle
      
      contadori=QiwoCounter.where(:qiwo_enterprise_id=>singleEnterprise).last
      if contadori==nil
        #no existe entonces creamos el contador
        contador=QiwoCounter.new
        contador.qiwo_enterprise_id=singleEnterprise
        contador.facturas_disponibles=(producto.weight_number.to_i*ventaFolios.quantity)
        contador.save
      else
        #existe el contador y sumamos los folios permitidos
        contadori.facturas_disponibles=contadori.facturas_disponibles+(producto.weight_number.to_i*ventaFolios.quantity)
        contadori.save
      end
    end
  end

  def agregarFoliosGuia(idDeVenta)
    ventaFolios=QiwoLogKart.find_by_id(idDeVenta)
    producto=QiwoProduct.find_by_id(ventaFolios.qiwo_product_id)
    # en el caso de los folios de envios se detecta la compra y se etiqueta con el usuario
    # buscamos si estamos en modo prueba
    guiaSinUsar=nil
    if isInTestMode()==false
      guiaSinUsar=ShippingGuide.where(:id_from=>nil,:gtype=>1).first(ventaFolios.quantity)
    else 
      guiaSinUsar=ShippingGuide.where(:id_from=>nil,:gtype=>0).first(ventaFolios.quantity)
    end
    guiaSinUsar.each do |singleGuia|
      singleGuia.id_from=ventaFolios.user_id
      # singleGuia.qiwo_log_kart_id=ventaFolios.id
      singleGuia.save  
    end
    

    # empresas=QiwoEnterprise.select(:id).where(:user_owner=>ventaFolios.user_id)
  end

  def hackAttempt(source)
    #este metodo se ejecuta en todas las areas donde se cree que hay intentos de hacking
  end

  def foliosId
    #este metodo regresa los id de los productos que son considerados folios de factura electronica
    return [71]
  end

  def foliosEnviosId
    return [114,115]
  end

  def getUserAndEmployees
    # este metodo trae los usuarios que pueden realizar cambios a mis ventas
    usuariosConMovimientos=[session[:activeUser].id]
    empresas=currentUserEnterprises()
    # puts "======>"+empresas.inspect
    misEmpleados=QiwoEmployee.where(:employee_enterprise=>empresas).index_by(&:employee_fb_id).keys
    # puts "misEmpleados=====>"+misEmpleados.inspect
    # aqui hay que intercambiar el fb_id por el id de base de datos para poder saber quien vendió
    misEmpleadosIndice=Sys_User.where(:user_id=>misEmpleados).index_by(&:id).keys
    usuariosConMovimientos=usuariosConMovimientos.concat(misEmpleadosIndice)
    return usuariosConMovimientos
  end

  def currentUserEnterprises
    empresas=QiwoEnterprise.select(:id).where(:user_owner=>session[:activeUser].id)
    mis_negocios=empresas.index_by(&:id).keys
    return mis_negocios
  end

  def currentUserProducts
    iproductos=QiwoProduct.where(:business_source=>currentUserEnterprises())
    misProductosIndex=iproductos.index_by(&:id).keys
    return misProductosIndex
  end

  def hackVerificationBiz(bizid)
    #primero verificamos si esta loggeado
    if session[:activeUser]==nil
      redirect_to "http://qiwo.mx/grant_permission.html"
    end
    permitidas=QiwoEnterprise.where(:id=>bizid,:user_owner=>session[:activeUser].id).last
    if permitidas==nil
      return false
    else
      return true
    end
  end

  def isInTestMode
    return true
  end
  def isInTestModeNumber
    if isInTestMode()==true
      return 0
    else
      return 1
    end
  end
  def facturacionWSDL
    if isInTestMode()==true
      return 'https://t2demo.facturacionmoderna.com/timbrado/wsdl'
    else
      return 'https://t2.facturacionmoderna.com/timbrado/wsdl'
    end
  end
  def facturacionUser
    if isInTestMode()==true
      return 'UsuarioPruebasWS'
    else
      return 'CME130327GG1'
    end
  end
  def facturacionPwd
    if isInTestMode()==true
      return 'b9ec2afa3361a59af4b4d102d3f704eabdf097d4'
    else
      return '386a78f88b825efd2b7026e86f54947882e8110b'
    end
  end
  def conektaKeyPrivate
    #estos son los de prueba
    if isInTestMode()==true
      return 'key_GrwhaWmPPgXv1eTT'  
    else 
      return 'key_5U4CVGj2PSCCrSNg'
    end

    #productivo
    #
  end

  def conektaKeyPublic
    #estos son los de prueba
    if isInTestMode()==true
      return 'key_O8q11LLYy19LjpQv'
    else
     return 'key_d5svpyqrag9ZFdh9'
    end 

    #produccion
    #
  end

  def requestRing
    FmTimbradoCfdi.configurar do |config|
      config.user_id = 'user_id'
      config.user_pass = 'password'
      config.namespace = 'https://t2demo.facturacionmoderna.com/timbrado/soap'
      config.endpoint = 'https://t2demo.facturacionmoderna.com/timbrado/soap'
      config.fm_wsdl = 'https://t2demo.facturacionmoderna.com/timbrado/wsdl'
      config.log = 'path_to_log'
      config.ssl_verify_mode = true
    end # configurar
  end


  def qiwoLogOut
    if session[:activeUser]!=nil
      session[:activeUser]=nil
    end
    redirect_to "/"
  end

  def getTax(cantidad)
    return cantidad*0.16
  end
  def conektaFormat(cantidadFloat)
    #primero multiplicamos por 100
    formato=cantidadFloat*100
    formato=formato.to_i
    return formato
  end

  def finalProductPrice(logKartId)
    #este metodo ayuda a determinar el precio con iva y mayoritario de los productos
    compra=QiwoLogKart.where(:id=>logKartId).last
    producto=QiwoProduct.where(:id=>compra.qiwo_product_id).last
    precio=0

    if compra.quantity>=producto.major_quantity and producto.major_quantity>0
      #tiene precio de mayoreo
      precio=producto.major_price
    else
      precio=producto.sale_price
    end

    #aplicar descuento
    precio=precio-producto.discount_amount
    #verificar si tiene costo de envio
    precio+=compra.shipping_cost
    #verificar si este articulo tiene iva
    if producto.tax==1
      precio=precio+getTax(precio)
    end
    
    #
    return precio
  end
  def sendMailByIdSmallTemplate(iDList,titulo,subtitulo,mensaje)
    direcciones=[]
    iDList.each do |singleList|
      direccionString=Sys_User.find_by_id(singleList)
      if direccionString.email!=nil
        if direccionString.email.index("@")!=nil
          direcciones.push({"email"=>direccionString.email,"name"=>"Cliente Qiwo","type"=>"to"})    
        else
          #telegram
          Notifications.sendTelegramToPhone([direccionString.email],titulo+" "+mensaje)
        end
        
      end
      
    end
    mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
              message = {"html"=>smallTemplate(titulo,subtitulo,mensaje),
                   "text"=>"Se recibió su pago",
                   "subject"=>"Qiwo - Información",
                   "from_email"=>"enviofactura@cfdi.ws",
                   "from_name"=>"Qiwo - ventas",
                   "to"=>direcciones,
                   "important"=>false,
                   "track_opens"=>nil,
                   "track_clicks"=>nil,
                   "auto_text"=>nil,
                   "auto_html"=>nil,
                   "inline_css"=>nil,
                   "url_strip_qs"=>nil,
                   "preserve_recipients"=>nil,
                   "view_content_link"=>nil,
                   "tracking_domain"=>nil,
                   "signing_domain"=>nil,
                   "return_path_domain"=>nil,
                   "merge"=>true,
                   "global_merge_vars"=>[{"name"=>"merge1", "content"=>"merge1 content"}],
                   "merge_vars"=>
                      [{"rcpt"=>"recipient.email@example.com",
                          "vars"=>[{"name"=>"merge2", "content"=>"merge2 content"}]}],
                   "tags"=>["password-resets"],
                   "attachments"=>
                      [],
                   "images"=>
                      [{"type"=>"image/png", "name"=>"IMAGECID", "content"=>"ZXhhbXBsZSBmaWxl"}]}
                  async = false
                  ip_pool = "Main Pool"
                  send_at = DateTime.now-2.hours
                  result = mandrill.messages.send message, async, ip_pool, send_at
  end
  def sendMailByID(iDList,message)
    #el iDList es una lista de usuarios, vamos a recorrerla
    direcciones=[]
    iDList.each do |singleList|
      direccionString=Sys_User.find_by_id(singleList)
      if direccionString.email!=nil
        #para evitar errores con telegram
        if direccionString.email.index("@")!=nil
          direcciones.push({"email"=>direccionString.email,"name"=>"Cliente Qiwo","type"=>"to"})    
        else
          #telegram
          if message==nil
            Notifications.sendTelegramToPhone([direccionString.email],"Se detecto un pago en uno de los productos de tu cuenta en Qiwo")
          else
            Notifications.sendTelegramToPhone([direccionString.email],message)
          end
        end
        
      end
      
    end
    mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
              message = {"html"=>paymentDetectedHTML(),
                   "text"=>"Se recibió su pago",
                   "subject"=>"Qiwo - pago recibido",
                   "from_email"=>"enviofactura@cfdi.ws",
                   "from_name"=>"Qiwo - ventas",
                   "to"=>direcciones,
                   "important"=>false,
                   "track_opens"=>nil,
                   "track_clicks"=>nil,
                   "auto_text"=>nil,
                   "auto_html"=>nil,
                   "inline_css"=>nil,
                   "url_strip_qs"=>nil,
                   "preserve_recipients"=>nil,
                   "view_content_link"=>nil,
                   "tracking_domain"=>nil,
                   "signing_domain"=>nil,
                   "return_path_domain"=>nil,
                   "merge"=>true,
                   "global_merge_vars"=>[{"name"=>"merge1", "content"=>"merge1 content"}],
                   "merge_vars"=>
                      [{"rcpt"=>"recipient.email@example.com",
                          "vars"=>[{"name"=>"merge2", "content"=>"merge2 content"}]}],
                   "tags"=>["password-resets"],
                   "attachments"=>
                      [],
                   "images"=>
                      [{"type"=>"image/png", "name"=>"IMAGECID", "content"=>"ZXhhbXBsZSBmaWxl"}]}
                  async = false
                  ip_pool = "Main Pool"
                  send_at = DateTime.now-2.hours
                  result = mandrill.messages.send message, async, ip_pool, send_at
  end

  def sendMailToAdmin(message)
    direcciones=[{"email"=>'andres.santos@consultware.mx',
                          "name"=>"Cliente Qiwo Factura",
                          "type"=>"to"},{"email"=>'jonathan.santos@consultware.mx',
                          "name"=>"Cliente Qiwo Factura",
                          "type"=>"to"},{"email"=>'asb.studios@gmail.com',
                          "name"=>"Cliente Qiwo Factura",
                          "type"=>"to"}]
    mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
              message = {"html"=>smallTemplate('Intento de suscripción','Verificar pago',message),
                   "text"=>"Se genero su factura electronica, la puede descargar en dos formatos",
                   "subject"=>"Factura electronica",
                   "from_email"=>"enviofactura@cfdi.ws",
                   "from_name"=>"Qiwo - factura",
                   "to"=>direcciones,
                   "important"=>false,
                   "track_opens"=>nil,
                   "track_clicks"=>nil,
                   "auto_text"=>nil,
                   "auto_html"=>nil,
                   "inline_css"=>nil,
                   "url_strip_qs"=>nil,
                   "preserve_recipients"=>nil,
                   "view_content_link"=>nil,
                   "tracking_domain"=>nil,
                   "signing_domain"=>nil,
                   "return_path_domain"=>nil,
                   "merge"=>true,
                   "global_merge_vars"=>[{"name"=>"merge1", "content"=>"merge1 content"}],
                   "merge_vars"=>
                      [{"rcpt"=>"recipient.email@example.com",
                          "vars"=>[{"name"=>"merge2", "content"=>"merge2 content"}]}],
                   "tags"=>["password-resets"],
                   "attachments"=>
                      [],
                   "images"=>
                      [{"type"=>"image/png", "name"=>"IMAGECID", "content"=>"ZXhhbXBsZSBmaWxl"}]}
                  async = false
                  ip_pool = "Main Pool"
                  send_at = DateTime.now-2.hours
                  result = mandrill.messages.send message, async, ip_pool, send_at
  end

  def sendMailToAddress(adresses,message)
    mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
              message = {"html"=>smallTemplate('Qiwo noticias','Lea cuidadosamente las noticias de Qiwo','Noticia'),
                   "text"=>"Se genero su factura electronica, la puede descargar en dos formatos",
                   "subject"=>"Atención a clientes Qiwo",
                   "from_email"=>"enviofactura@cfdi.ws",
                   "from_name"=>"Qiwo - Clientes",
                   "to"=>
                      [{"email"=>@qiwo_enterprise.email_sales,
                          "name"=>"Cliente Qiwo Suscripcion",
                          "type"=>"to"},{"email"=>@qiwo_enterprise.email_buy,
                          "name"=>"Cliente Qiwo Suscripcion",
                          "type"=>"to"}],
                   
                   "important"=>false,
                   "track_opens"=>nil,
                   "track_clicks"=>nil,
                   "auto_text"=>nil,
                   "auto_html"=>nil,
                   "inline_css"=>nil,
                   "url_strip_qs"=>nil,
                   "preserve_recipients"=>nil,
                   "view_content_link"=>nil,
                   "tracking_domain"=>nil,
                   "signing_domain"=>nil,
                   "return_path_domain"=>nil,
                   "merge"=>true,
                   "global_merge_vars"=>[{"name"=>"merge1", "content"=>"merge1 content"}],
                   "merge_vars"=>
                      [{"rcpt"=>"recipient.email@example.com",
                          "vars"=>[{"name"=>"merge2", "content"=>"merge2 content"}]}],
                   "tags"=>["password-resets"],
                   "attachments"=>
                      [{"type"=>"text/plain",
                          "name"=>"myfile.txt",
                          "content"=>"ZXhhbXBsZSBmaWxl"}],
                   "images"=>
                      [{"type"=>"image/png", "name"=>"IMAGECID", "content"=>"ZXhhbXBsZSBmaWxl"}]}
                  async = false
                  ip_pool = "Main Pool"
                  send_at = DateTime.now-2.hours
                  result = mandrill.messages.send message, async, ip_pool, send_at
  end

  def sendMailToAddressFromTemplate(adresses,htmlLayout)
    direcciones=[]
    adresses.each do |singleList|
      direcciones.push({"email"=>singleList,"name"=>"Cliente Qiwo","type"=>"to"})    
    end
    mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
              message = {"html"=>htmlLayout,
                   "text"=>"",
                   "subject"=>"Atención a clientes Qiwo",
                   "from_email"=>"enviofactura@cfdi.ws",
                   "from_name"=>"Qiwo - Clientes",
                   "to"=>direcciones,
                   "important"=>false,
                   "track_opens"=>nil,
                   "track_clicks"=>nil,
                   "auto_text"=>nil,
                   "auto_html"=>nil,
                   "inline_css"=>nil,
                   "url_strip_qs"=>nil,
                   "preserve_recipients"=>nil,
                   "view_content_link"=>nil,
                   "tracking_domain"=>nil,
                   "signing_domain"=>nil,
                   "return_path_domain"=>nil,
                   "merge"=>true,
                   "global_merge_vars"=>[{"name"=>"merge1", "content"=>"merge1 content"}],
                   "merge_vars"=>
                      [{"rcpt"=>"recipient.email@example.com",
                          "vars"=>[{"name"=>"merge2", "content"=>"merge2 content"}]}],
                   "tags"=>["password-resets"],
                   "attachments"=>
                      [{"type"=>"text/plain",
                          "name"=>"myfile.txt",
                          "content"=>"ZXhhbXBsZSBmaWxl"}],
                   "images"=>
                      [{"type"=>"image/png", "name"=>"IMAGECID", "content"=>"ZXhhbXBsZSBmaWxl"}]}
                  async = false
                  ip_pool = "Main Pool"
                  send_at = DateTime.now-2.hours
                  result = mandrill.messages.send message, async, ip_pool, send_at
  end

  def finalProductPriceNoTax(logKartId)
    #este metodo ayuda a determinar el precio con iva y mayoritario de los productos
    compra=QiwoLogKart.where(:id=>logKartId).last
    producto=QiwoProduct.where(:id=>compra.qiwo_product_id).last
    precio=0
    if compra.quantity>=producto.major_quantity and producto.major_quantity>0
      #tiene precio de mayoreo
      precio=producto.major_price
    else
      precio=producto.sale_price
    end
    #aplicar descuento
    precio=precio-producto.discount_amount
    #verificar si tiene costo de envio
    precio+=compra.shipping_cost
    
    
    #
    return precio
  end

  def globalQiwoSearch(internalSearchId)
  #antes de proceder hay que verificar si se busca por negocio
    empresa=QiwoEnterprise.find_by_unique_uri(internalSearchId)
    if empresa!=nil
      #buscar productos de la empresa
      @productSearch=QiwoProduct.where(:business_source=>empresa.id,:is_pos=>0)
    else
      searchSeed=internalSearchId.split(" ")
      searchSeeds=[]
          searchSeed.each do |singleSearchSeed|
              puts ">>>>>"+singleSearchSeed.inspect
              if singleSearchSeed=='es' or
                  singleSearchSeed=='de' or
                  singleSearchSeed=='por' or
                  singleSearchSeed=='para' or
                  singleSearchSeed=='el' or
                  singleSearchSeed=='la' or
                  singleSearchSeed=='los' or
                  singleSearchSeed=='las' or
                  next
              end
              
              if singleSearchSeed[-2,2]=="es"
                  #probablmente sea plural
  #                searchSeeds.push("or")
                  searchSeeds.push(singleSearchSeed[0..-2])
              elsif searchSeed[-1,1]=="s"
  #                searchSeeds.push("or")
                  searchSeeds.push(singleSearchSeed[0..-1])
              end
  #            searchSeeds.push("or")
              searchSeeds.push(singleSearchSeed)
          end
        if searchSeeds.length==0
            puts ">>>>>"+searchSeed[0].inspect
              
              if searchSeed[0][-2,2]=="es"
                  #probablmente sea plural
  #                searchSeeds.push("or")
                  searchSeeds.push("%"+searchSeed[0][0..-3]+"%")
              elsif searchSeed[0][-1,1]=="s"
  #                searchSeeds.push("or")
                  searchSeeds.push("%"+searchSeed[0][0..-2]+"%")
              end
  #            searchSeeds.push("or")
              searchSeeds.push("%"+searchSeed[0]+"%")
        end
      @search = nil#Search.find(params[:id])
      #se busca en la tabla de productos
  #    @productSearch=QiwoProduct.where("name like '%"+params[:id]+"%' or description like '%"+params[:id]+"%'").where(['ends_on >= ?', DateTime.now])
      puts "------------------product search-----------------------"
      puts searchSeeds.inspect
      @productSearch=QiwoProduct.where("is_pos=0 and name like '"+searchSeeds.join("' or name like '")+"' or description like '"+searchSeeds.join("' or description like '")+"' or keywords like '"+searchSeeds.join("' or keywords like '")+"'").where(['ends_on >= ?', DateTime.now]).where(:search_enabled=>1)
    end
    return @productSearch
end

def smallTemplate(titulo,subtitulo,mensaje)
#   tituloMail="Intento de suscripción"
# subtituloMail="Verificar pago"
# contenido="Este email se envió a los administradores por que se detecto un intento de suscripción de pago."
tituloMail=titulo
subtituloMail=subtitulo
contenido=mensaje
  contenidoMail='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <!-- NAME: 1 COLUMN -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>*|MC:SUBJECT|*</title>
        
        <!--[if gte mso 6]>
        <style>
            table.mcnFollowContent {width:100% !important;}
            table.mcnShareContent {width:100% !important;}
        </style>
        <![endif]-->
    <style type="text/css">
    body,#bodyTable,#bodyCell{
      height:100% !important;
      margin:0;
      padding:0;
      width:100% !important;
    }
    table{
      border-collapse:collapse;
    }
    img,a img{
      border:0;
      outline:none;
      text-decoration:none;
    }
    h1,h2,h3,h4,h5,h6{
      margin:0;
      padding:0;
    }
    p{
      margin:1em 0;
      padding:0;
    }
    a{
      word-wrap:break-word;
    }
    .ReadMsgBody{
      width:100%;
    }
    .ExternalClass{
      width:100%;
    }
    .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
      line-height:100%;
    }
    table,td{
      mso-table-lspace:0pt;
      mso-table-rspace:0pt;
    }
    #outlook a{
      padding:0;
    }
    img{
      -ms-interpolation-mode:bicubic;
    }
    body,table,td,p,a,li,blockquote{
      -ms-text-size-adjust:100%;
      -webkit-text-size-adjust:100%;
    }
    #bodyCell{
      padding:20px;
    }
    .mcnImage{
      vertical-align:bottom;
    }
    .mcnTextContent img{
      height:auto !important;
    }
  /*
  @tab Page
  @section background style
  @tip Set the background color and top border for your email. You may want to choose colors that match your companys branding.
  */
    body,#bodyTable{
      /*@editable*/background-color:#F2F2F2;
    }
  /*
  @tab Page
  @section background style
  @tip Set the background color and top border for your email. You may want to choose colors that match your companys branding.
  */
    #bodyCell{
      /*@editable*/border-top:0;
    }
  /*
  @tab Page
  @section email border
  @tip Set the border for your email.
  */
    #templateContainer{
      /*@editable*/border:0;
    }
  /*
  @tab Page
  @section heading 1
  @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
  @style heading 1
  */
    h1{
      /*@editable*/color:#606060 !important;
      display:block;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:40px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:-1px;
      margin:0;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section heading 2
  @tip Set the styling for all second-level headings in your emails.
  @style heading 2
  */
    h2{
      /*@editable*/color:#404040 !important;
      display:block;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:26px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:-.75px;
      margin:0;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section heading 3
  @tip Set the styling for all third-level headings in your emails.
  @style heading 3
  */
    h3{
      /*@editable*/color:#606060 !important;
      display:block;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:18px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:-.5px;
      margin:0;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section heading 4
  @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
  @style heading 4
  */
    h4{
      /*@editable*/color:#808080 !important;
      display:block;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      margin:0;
      /*@editable*/text-align:left;
    }
  /*
  @tab Preheader
  @section preheader style
  @tip Set the background color and borders for your emails preheader area.
  */
    #templatePreheader{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
    }
  /*
  @tab Preheader
  @section preheader text
  @tip Set the styling for your emails preheader text. Choose a size and color that is easy to read.
  */
    .preheaderContainer .mcnTextContent,.preheaderContainer .mcnTextContent p{
      /*@editable*/color:#606060;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:11px;
      /*@editable*/line-height:125%;
      /*@editable*/text-align:left;
    }
  /*
  @tab Preheader
  @section preheader link
  @tip Set the styling for your emails header links. Choose a color that helps them stand out from your text.
  */
    .preheaderContainer .mcnTextContent a{
      /*@editable*/color:#606060;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  /*
  @tab Header
  @section header style
  @tip Set the background color and borders for your emails header area.
  */
    #templateHeader{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
    }
  /*
  @tab Header
  @section header text
  @tip Set the styling for your emails header text. Choose a size and color that is easy to read.
  */
    .headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
      /*@editable*/color:#606060;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:15px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  /*
  @tab Header
  @section header link
  @tip Set the styling for your emails header links. Choose a color that helps them stand out from your text.
  */
    .headerContainer .mcnTextContent a{
      /*@editable*/color:#6DC6DD;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  /*
  @tab Body
  @section body style
  @tip Set the background color and borders for your emails body area.
  */
    #templateBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
    }
  /*
  @tab Body
  @section body text
  @tip Set the styling for your emails body text. Choose a size and color that is easy to read.
  */
    .bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
      /*@editable*/color:#606060;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:15px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  /*
  @tab Body
  @section body link
  @tip Set the styling for your emails body links. Choose a color that helps them stand out from your text.
  */
    .bodyContainer .mcnTextContent a{
      /*@editable*/color:#6DC6DD;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  /*
  @tab Footer
  @section footer style
  @tip Set the background color and borders for your emails footer area.
  */
    #templateFooter{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
    }
  /*
  @tab Footer
  @section footer text
  @tip Set the styling for your emails footer text. Choose a size and color that is easy to read.
  */
    .footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
      /*@editable*/color:#606060;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:11px;
      /*@editable*/line-height:125%;
      /*@editable*/text-align:left;
    }
  /*
  @tab Footer
  @section footer link
  @tip Set the styling for your emails footer links. Choose a color that helps them stand out from your text.
  */
    .footerContainer .mcnTextContent a{
      /*@editable*/color:#606060;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  @media only screen and (max-width: 480px){
    body,table,td,p,a,li,blockquote{
      -webkit-text-size-adjust:none !important;
    }

} @media only screen and (max-width: 480px){
    body{
      width:100% !important;
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    td[id=bodyCell]{
      padding:10px !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnTextContentContainer]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnBoxedTextContentContainer]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcpreview-image-uploader]{
      width:100% !important;
      display:none !important;
    }

} @media only screen and (max-width: 480px){
    img[class=mcnImage]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnImageGroupContentContainer]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageGroupContent]{
      padding:9px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageGroupBlockInner]{
      padding-bottom:0 !important;
      padding-top:0 !important;
    }

} @media only screen and (max-width: 480px){
    tbody[class=mcnImageGroupBlockOuter]{
      padding-bottom:9px !important;
      padding-top:9px !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnCaptionTopContent],table[class=mcnCaptionBottomContent]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnCaptionLeftTextContentContainer],table[class=mcnCaptionRightTextContentContainer],table[class=mcnCaptionLeftImageContentContainer],table[class=mcnCaptionRightImageContentContainer],table[class=mcnImageCardLeftTextContentContainer],table[class=mcnImageCardRightTextContentContainer]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardLeftImageContent],td[class=mcnImageCardRightImageContent]{
      padding-right:18px !important;
      padding-left:18px !important;
      padding-bottom:0 !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardBottomImageContent]{
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardTopImageContent]{
      padding-top:18px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardLeftImageContent],td[class=mcnImageCardRightImageContent]{
      padding-right:18px !important;
      padding-left:18px !important;
      padding-bottom:0 !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardBottomImageContent]{
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardTopImageContent]{
      padding-top:18px !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnCaptionLeftContentOuter] td[class=mcnTextContent],table[class=mcnCaptionRightContentOuter] td[class=mcnTextContent]{
      padding-top:9px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnCaptionBlockInner] table[class=mcnCaptionTopContent]:last-child td[class=mcnTextContent]{
      padding-top:18px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnBoxedTextContentColumn]{
      padding-left:18px !important;
      padding-right:18px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnTextContent]{
      padding-right:18px !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section template width
  @tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesnt work for you, set the width to 300px instead.
  */
    table[id=templateContainer],table[id=templatePreheader],table[id=templateHeader],table[id=templateBody],table[id=templateFooter]{
      /*@tab Mobile Styles
@section template width
@tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesnt work for you, set the width to 300px instead.*/max-width:600px !important;
      /*@editable*/width:100% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section heading 1
  @tip Make the first-level headings larger in size for better readability on small screens.
  */
    h1{
      /*@editable*/font-size:24px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section heading 2
  @tip Make the second-level headings larger in size for better readability on small screens.
  */
    h2{
      /*@editable*/font-size:20px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section heading 3
  @tip Make the third-level headings larger in size for better readability on small screens.
  */
    h3{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section heading 4
  @tip Make the fourth-level headings larger in size for better readability on small screens.
  */
    h4{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Boxed Text
  @tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    table[class=mcnBoxedTextContentContainer] td[class=mcnTextContent],td[class=mcnBoxedTextContentContainer] td[class=mcnTextContent] p{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Preheader Visibility
  @tip Set the visibility of the emails preheader on small screens. You can hide it to save space.
  */
    table[id=templatePreheader]{
      /*@editable*/display:block !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Preheader Text
  @tip Make the preheader text larger in size for better readability on small screens.
  */
    td[class=preheaderContainer] td[class=mcnTextContent],td[class=preheaderContainer] td[class=mcnTextContent] p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:115% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Header Text
  @tip Make the header text larger in size for better readability on small screens.
  */
    td[class=headerContainer] td[class=mcnTextContent],td[class=headerContainer] td[class=mcnTextContent] p{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Body Text
  @tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    td[class=bodyContainer] td[class=mcnTextContent],td[class=bodyContainer] td[class=mcnTextContent] p{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section footer text
  @tip Make the body content text larger in size for better readability on small screens.
  */
    td[class=footerContainer] td[class=mcnTextContent],td[class=footerContainer] td[class=mcnTextContent] p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:115% !important;
    }

} @media only screen and (max-width: 480px){
    td[class=footerContainer] a[class=utilityLink]{
      display:block !important;
    }

}</style></head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN PREHEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader">
                                        <tr>
                                          <td valign="top" class="preheaderContainer" style="padding-top:9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="366" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:0;">
                        
                            Suscripciones, departamento de finanzas
                        </td>
                    </tr>
                </tbody></table>
                
                <table align="right" border="0" cellpadding="0" cellspacing="0" width="197" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:0;">
                        
                            <a href="*|ARCHIVE|*" target="_blank">View this email in your browser</a>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END PREHEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                        <tr>
                                            <td valign="top" class="headerContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">
                                
                                    
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/360fbf8f7d5ec95b6b5cf3da3/images/1779304_478475825598129_1910178108_n.png" width="300" style="max-width:300px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                        <tr>
                                            <td valign="top" class="bodyContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1>'+tituloMail+'</h1>

<h3>'+subtituloMail+'</h3>

<p>'+contenido+'</p>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateFooter">
                                        <tr>
                                            <td valign="top" class="footerContainer" style="padding-bottom:9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            Consultware de México S de RL de CV<br>
&nbsp;Agustin Vera No.1415 Int A2, Col. Del valle<br>
&nbsp;C.P. 78200 San Luis Potosí, S.L.P., México<br>
&nbsp;Tel S.L.P: +52 (444) 585-2985 Ext.: 350<br>
&nbsp;Web: www.consultware.mx &nbsp;<br>
<br>

<br>
<br>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>'
return contenidoMail
end

def paymentDetectedHTML
  return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <!-- NAME: 1 COLUMN -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>*|MC:SUBJECT|*</title>
        
        <!--[if gte mso 6]>
        <style>
            table.mcnFollowContent {width:100% !important;}
            table.mcnShareContent {width:100% !important;}
        </style>
        <![endif]-->
    <style type="text/css">
    body,#bodyTable,#bodyCell{
      height:100% !important;
      margin:0;
      padding:0;
      width:100% !important;
    }
    table{
      border-collapse:collapse;
    }
    img,a img{
      border:0;
      outline:none;
      text-decoration:none;
    }
    h1,h2,h3,h4,h5,h6{
      margin:0;
      padding:0;
    }
    p{
      margin:1em 0;
      padding:0;
    }
    a{
      word-wrap:break-word;
    }
    .ReadMsgBody{
      width:100%;
    }
    .ExternalClass{
      width:100%;
    }
    .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
      line-height:100%;
    }
    table,td{
      mso-table-lspace:0pt;
      mso-table-rspace:0pt;
    }
    #outlook a{
      padding:0;
    }
    img{
      -ms-interpolation-mode:bicubic;
    }
    body,table,td,p,a,li,blockquote{
      -ms-text-size-adjust:100%;
      -webkit-text-size-adjust:100%;
    }
    #bodyCell{
      padding:20px;
    }
    .mcnImage{
      vertical-align:bottom;
    }
    .mcnTextContent img{
      height:auto !important;
    }
  /*
  @tab Page
  @section background style
  @tip Set the background color and top border for your email. You may want to choose colors that match your company s branding.
  */
    body,#bodyTable{
      /*@editable*/background-color:#F2F2F2;
    }
  /*
  @tab Page
  @section background style
  @tip Set the background color and top border for your email. You may want to choose colors that match your company s branding.
  */
    #bodyCell{
      /*@editable*/border-top:0;
    }
  /*
  @tab Page
  @section email border
  @tip Set the border for your email.
  */
    #templateContainer{
      /*@editable*/border:0;
    }
  /*
  @tab Page
  @section heading 1
  @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
  @style heading 1
  */
    h1{
      /*@editable*/color:#606060 !important;
      display:block;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:40px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:-1px;
      margin:0;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section heading 2
  @tip Set the styling for all second-level headings in your emails.
  @style heading 2
  */
    h2{
      /*@editable*/color:#404040 !important;
      display:block;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:26px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:-.75px;
      margin:0;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section heading 3
  @tip Set the styling for all third-level headings in your emails.
  @style heading 3
  */
    h3{
      /*@editable*/color:#606060 !important;
      display:block;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:18px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:-.5px;
      margin:0;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section heading 4
  @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
  @style heading 4
  */
    h4{
      /*@editable*/color:#808080 !important;
      display:block;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      margin:0;
      /*@editable*/text-align:left;
    }
  /*
  @tab Preheader
  @section preheader style
  @tip Set the background color and borders for your email s preheader area.
  */
    #templatePreheader{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
    }
  /*
  @tab Preheader
  @section preheader text
  @tip Set the styling for your email s preheader text. Choose a size and color that is easy to read.
  */
    .preheaderContainer .mcnTextContent,.preheaderContainer .mcnTextContent p{
      /*@editable*/color:#606060;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:11px;
      /*@editable*/line-height:125%;
      /*@editable*/text-align:left;
    }
  /*
  @tab Preheader
  @section preheader link
  @tip Set the styling for your email s header links. Choose a color that helps them stand out from your text.
  */
    .preheaderContainer .mcnTextContent a{
      /*@editable*/color:#606060;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  /*
  @tab Header
  @section header style
  @tip Set the background color and borders for your email s header area.
  */
    #templateHeader{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
    }
  /*
  @tab Header
  @section header text
  @tip Set the styling for your email s header text. Choose a size and color that is easy to read.
  */
    .headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
      /*@editable*/color:#606060;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:15px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  /*
  @tab Header
  @section header link
  @tip Set the styling for your email s header links. Choose a color that helps them stand out from your text.
  */
    .headerContainer .mcnTextContent a{
      /*@editable*/color:#6DC6DD;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  /*
  @tab Body
  @section body style
  @tip Set the background color and borders for your email s body area.
  */
    #templateBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
    }
  /*
  @tab Body
  @section body text
  @tip Set the styling for your email s body text. Choose a size and color that is easy to read.
  */
    .bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
      /*@editable*/color:#606060;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:15px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  /*
  @tab Body
  @section body link
  @tip Set the styling for your email s body links. Choose a color that helps them stand out from your text.
  */
    .bodyContainer .mcnTextContent a{
      /*@editable*/color:#6DC6DD;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  /*
  @tab Footer
  @section footer style
  @tip Set the background color and borders for your email s footer area.
  */
    #templateFooter{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
    }
  /*
  @tab Footer
  @section footer text
  @tip Set the styling for your email s footer text. Choose a size and color that is easy to read.
  */
    .footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
      /*@editable*/color:#606060;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:11px;
      /*@editable*/line-height:125%;
      /*@editable*/text-align:left;
    }
  /*
  @tab Footer
  @section footer link
  @tip Set the styling for your email s footer links. Choose a color that helps them stand out from your text.
  */
    .footerContainer .mcnTextContent a{
      /*@editable*/color:#606060;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  @media only screen and (max-width: 480px){
    body,table,td,p,a,li,blockquote{
      -webkit-text-size-adjust:none !important;
    }

} @media only screen and (max-width: 480px){
    body{
      width:100% !important;
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    td[id=bodyCell]{
      padding:10px !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnTextContentContainer]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnBoxedTextContentContainer]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcpreview-image-uploader]{
      width:100% !important;
      display:none !important;
    }

} @media only screen and (max-width: 480px){
    img[class=mcnImage]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnImageGroupContentContainer]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageGroupContent]{
      padding:9px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageGroupBlockInner]{
      padding-bottom:0 !important;
      padding-top:0 !important;
    }

} @media only screen and (max-width: 480px){
    tbody[class=mcnImageGroupBlockOuter]{
      padding-bottom:9px !important;
      padding-top:9px !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnCaptionTopContent],table[class=mcnCaptionBottomContent]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnCaptionLeftTextContentContainer],table[class=mcnCaptionRightTextContentContainer],table[class=mcnCaptionLeftImageContentContainer],table[class=mcnCaptionRightImageContentContainer],table[class=mcnImageCardLeftTextContentContainer],table[class=mcnImageCardRightTextContentContainer]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardLeftImageContent],td[class=mcnImageCardRightImageContent]{
      padding-right:18px !important;
      padding-left:18px !important;
      padding-bottom:0 !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardBottomImageContent]{
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardTopImageContent]{
      padding-top:18px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardLeftImageContent],td[class=mcnImageCardRightImageContent]{
      padding-right:18px !important;
      padding-left:18px !important;
      padding-bottom:0 !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardBottomImageContent]{
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnImageCardTopImageContent]{
      padding-top:18px !important;
    }

} @media only screen and (max-width: 480px){
    table[class=mcnCaptionLeftContentOuter] td[class=mcnTextContent],table[class=mcnCaptionRightContentOuter] td[class=mcnTextContent]{
      padding-top:9px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnCaptionBlockInner] table[class=mcnCaptionTopContent]:last-child td[class=mcnTextContent]{
      padding-top:18px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnBoxedTextContentColumn]{
      padding-left:18px !important;
      padding-right:18px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=mcnTextContent]{
      padding-right:18px !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section template width
  @tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesn t work for you, set the width to 300px instead.
  */
    table[id=templateContainer],table[id=templatePreheader],table[id=templateHeader],table[id=templateBody],table[id=templateFooter]{
      /*@tab Mobile Styles
@section template width
@tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesn t work for you, set the width to 300px instead.*/max-width:600px !important;
      /*@editable*/width:100% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section heading 1
  @tip Make the first-level headings larger in size for better readability on small screens.
  */
    h1{
      /*@editable*/font-size:24px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section heading 2
  @tip Make the second-level headings larger in size for better readability on small screens.
  */
    h2{
      /*@editable*/font-size:20px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section heading 3
  @tip Make the third-level headings larger in size for better readability on small screens.
  */
    h3{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section heading 4
  @tip Make the fourth-level headings larger in size for better readability on small screens.
  */
    h4{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Boxed Text
  @tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    table[class=mcnBoxedTextContentContainer] td[class=mcnTextContent],td[class=mcnBoxedTextContentContainer] td[class=mcnTextContent] p{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Preheader Visibility
  @tip Set the visibility of the email s preheader on small screens. You can hide it to save space.
  */
    table[id=templatePreheader]{
      /*@editable*/display:block !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Preheader Text
  @tip Make the preheader text larger in size for better readability on small screens.
  */
    td[class=preheaderContainer] td[class=mcnTextContent],td[class=preheaderContainer] td[class=mcnTextContent] p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:115% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Header Text
  @tip Make the header text larger in size for better readability on small screens.
  */
    td[class=headerContainer] td[class=mcnTextContent],td[class=headerContainer] td[class=mcnTextContent] p{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Body Text
  @tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    td[class=bodyContainer] td[class=mcnTextContent],td[class=bodyContainer] td[class=mcnTextContent] p{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section footer text
  @tip Make the body content text larger in size for better readability on small screens.
  */
    td[class=footerContainer] td[class=mcnTextContent],td[class=footerContainer] td[class=mcnTextContent] p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:115% !important;
    }

} @media only screen and (max-width: 480px){
    td[class=footerContainer] a[class=utilityLink]{
      display:block !important;
    }

}</style></head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN PREHEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader">
                                        <tr>
                                          <td valign="top" class="preheaderContainer" style="padding-top:9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="366" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:0;">
                        
                            <a href="http://www.qiwo.mx" target="_blank"><img align="none" height="50" src="https://gallery.mailchimp.com/360fbf8f7d5ec95b6b5cf3da3/images/1779304_478475825598129_1910178108_n.png" style="width: 50px; height: 50px;" width="50"></a>
                        </td>
                    </tr>
                </tbody></table>
                
                <table align="right" border="0" cellpadding="0" cellspacing="0" width="197" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:0;">
                        
                            <p>Qiwo departamento de ventas</p>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END PREHEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                        <tr>
                                            <td valign="top" class="headerContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">
                                
                                    
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/360fbf8f7d5ec95b6b5cf3da3/images/Security_Guard_icon.png" width="96" style="max-width:96px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                        <tr>
                                            <td valign="top" class="bodyContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1>¡Gracias por usar Qiwo!</h1>

<h3>Bienvenido a la nueva forma de negocios en México</h3>

<p>Nuestros guardias de seguridad han confirmado que se realizo un pago seguro y se pudo corroborar el deposito, cualquier duda, soporte o sugerencia puedes comunicarte con nosotros a hola@qiwo.mx</p>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateFooter">
                                        <tr>
                                            <td valign="top" class="footerContainer" style="padding-bottom:9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <span style="color: #606060;font-family: helvetica;font-size: 11px;line-height: 13.75px;">Consultware de México S de RL de CV</span><br style="line-height: 15.619999885559082px;color: #606060;font-family: Helvetica;font-size: 11px;">
<span style="color: #606060;font-family: helvetica;font-size: 11px;line-height: 13.75px;">Agustin Vera No.1415 Int A2, Col. Del valle</span><br style="line-height: 15.619999885559082px;color: #606060;font-family: Helvetica;font-size: 11px;">
<span style="color: #606060;font-family: helvetica;font-size: 11px;line-height: 13.75px;">C.P. 78200 San Luis Potosí, S.L.P., México</span><br>
<span style="color: #606060;font-family: helvetica;font-size: 11px;line-height: 13.75px;">Tel S.L.P: +52 (444) 585-2985</span><br style="line-height: 15.619999885559082px;color: #606060;font-family: Helvetica;font-size: 11px;">
<span style="color: #606060;font-family: helvetica;font-size: 11px;line-height: 13.75px;">&nbsp;Web: www.consultware.mx &nbsp;</span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
'
  
end
  
end
