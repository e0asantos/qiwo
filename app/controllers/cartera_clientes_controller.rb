#!/bin/env ruby
# encoding: utf-8
class CarteraClientesController < ApplicationController
  # GET /cartera_clientes
  # GET /cartera_clientes.json
  def index
    @cartera_clientes = CarteraCliente.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @cartera_clientes }
    end
  end

  # GET /cartera_clientes/1
  # GET /cartera_clientes/1.json
  def show
    @cartera_cliente = CarteraCliente.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @cartera_cliente }
    end
  end

  # GET /cartera_clientes/new
  # GET /cartera_clientes/new.json
  def new
    @cartera_cliente = CarteraCliente.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @cartera_cliente }
    end
  end

  # GET /cartera_clientes/1/edit
  def edit
    @cartera_cliente = CarteraCliente.find(params[:id])
  end

  # POST /cartera_clientes
  # POST /cartera_clientes.json
  def create
    @cartera_cliente = CarteraCliente.new(params[:cartera_cliente])

    respond_to do |format|
      if @cartera_cliente.save
        format.html { redirect_to @cartera_cliente, notice: 'Cartera cliente was successfully created.' }
        format.json { render json: @cartera_cliente, status: :created, location: @cartera_cliente }
      else
        format.html { render action: "new" }
        format.json { render json: @cartera_cliente.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /cartera_clientes/1
  # PUT /cartera_clientes/1.json
  def update
    @cartera_cliente = CarteraCliente.find(params[:id])

    respond_to do |format|
      if @cartera_cliente.update_attributes(params[:cartera_cliente])
        format.html { redirect_to @cartera_cliente, notice: 'Cartera cliente was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @cartera_cliente.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cartera_clientes/1
  # DELETE /cartera_clientes/1.json
  def destroy
    @cartera_cliente = CarteraCliente.find(params[:id])
    @cartera_cliente.destroy

    respond_to do |format|
      format.html { redirect_to cartera_clientes_url }
      format.json { head :no_content }
    end
  end
end
