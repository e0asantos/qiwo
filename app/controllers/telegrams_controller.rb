#!/bin/env ruby
# encoding: utf-8
require "uuidtools"
require "open3"
require 'fileutils'
class TelegramsController < ApplicationController

  protect_from_forgery :except => [:create,:show,:validateTelegram,:ping]
  # GET /telegrams
  # GET /telegrams.json
  def index

    @telegrams = Telegram.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @telegrams }
    end
  end

  def ping

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: "pong".to_json, layout: false }
    end
    
  end

  # GET /telegrams/1
  # GET /telegrams/1.json
  def show
    @telegram = Telegram.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @telegram }
    end
  end

  # GET /telegrams/new
  # GET /telegrams/new.json
  def new
    @telegram = Telegram.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @telegram }
    end
  end

  # GET /telegrams/1/edit
  def edit
    @telegram = Telegram.find(params[:id])
  end

  def validateTelegram
    usuario=Sys_User.where(:email=>session[:mtelegram],:password=>params[:xcode]).last
    if usuario != nil
      session[:activeUser]=usuario
    end
    puts usuario
    respond_to do |format|
      format.json { render json: usuario.to_json,layout: false}
    end
  end

  def validateTelegram_bak
    #validar si esta guardado un telefono
    respuesta="login_error"
    if session[:mtelegram]!=nil
      @telegram=Telegram.where(:phone=>session[:mtelegram],:auth_code=>params[:xcode]).last
      if @telegram!=nil
        #crear usuario o cargar usuario
        respuesta="login_ok"
        #hay que buscar el usuario por el telefono, el telefono es como el mail
        usuario=Sys_User.where(:email=>session[:mtelegram]).last
        # si el usuario no existe hay que crearlo
        if usuario==nil
          usuario=Sys_User.new
          usuario.name=session[:nombreReal].split(" ")[0]
          if session[:nombreReal].split(" ").length>1
            usuario.last_name=session[:nombreReal].split(" ")[1]
          end
          usuario.email=session[:mtelegram]
          usuario.avatar=@telegram.telegram_img
          usuario.save
        end
        usuario.avatar=@telegram.telegram_img
        usuario.save
        # si no es nil entonces no hay problema, lo cargamos en la sesion
        session[:activeUser]=usuario
      else
        respuesta="login_error"
      end
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: respuesta, layout: false }
    end
  end

  def emailLogin
    @telegram = Telegram.new
    usuario=Sys_User.where(:email=>params[:moreless]).last
    session[:mtelegram]=params[:moreless]
    puts usuario
    respond_to do |format|
      format.json { render json: @telegram.to_json,layout: false}
    end
  end

  # POST /telegrams
  # POST /telegrams.json
  def create
    @telegram = Telegram.new
    # @telegram.phone=params[:moreless]
    # @telegram.save
    
    o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
    fakename=(0...5).map { o[rand(o.length)] }.join
    fakelast="uno"
    authcode=UUIDTools::UUID.random_create.to_s()[0..4]
    #busquemos si ya lo teniamos
    @telegram=Telegram.where(:phone=>params[:moreless]).last
    session[:mtelegram]=params[:moreless]
    if @telegram!=nil
      fakename=@telegram.alias[0..4]
    else
      @telegram = Telegram.new
      @telegram.phone=params[:moreless]
      @telegram.alias=fakename+"_"+fakelast
      
    end
    @telegram.telegram_img="http://qiwo.mx/assets/telegram/small_unknown.png"
    @telegram.auth_code=authcode
    @telegram.save
    
    


    # stdin, stdout, stderr = Open3.popen3('/usr/src/qiwo/telegram-bot/tg/bin/telegram-cli -k /usr/src/qiwo/telegram-bot/tg/tg-server.pub -D') 
    resultado=".."
    conta=1
    # while(conta<5)
    #   resultado=stdout.gets
    #   puts conta.to_s+"---"+resultado
    #   conta+=1
    # end
    resultado="..................."
    #+524448573202

    # stdin.puts("add_contact +524448573202 "+fakename+" "+fakelast)4443076957
    # stdin.puts("add_contact "+params[:moreless]+" "+fakename+" "+fakelast)
    #stdin.puts("add_contact +528180829368 juan juanito")
    #perdemos la primera linea
    # puts stdout.gets
    # estadoContacto=stdout.gets.strip
    estadoContacto = "not added"

    if estadoContacto.index("not added")==nil
      @telegram.telegram_img="http://qiwo.mx/assets/telegram/small_unknown.png"
      @telegram.auth_code=authcode
      @telegram.save
      # stdin.puts("msg "+fakename+"_"+fakelast+" Estas usando tu cuenta de Telegram para ingresar a Qiwo, ingresa el siguiente codigo de letras cuando se te pida: "+authcode)
      #lo agregamos y ahora procedemos a obtener su informacion
      # puts "0."+stdout.gets
      session[:mtelegram]=params[:moreless]
      # stdin.puts("user_info "+fakename+"_"+fakelast)
      # puts "1."+stdout.gets
      # puts "--->START"
      # puts "2."+stdout.gets
      # nombreReal=stdout.gets
      # puts "NOMBRE_REAL:"+nombreReal
      # #nombre completo
      # puts "3."+nombreReal
      #telefono
      session[:nombreReal]=nombreReal
      # puts "5."+stdout.gets
      # #foto
      # foto=stdout.gets.strip
      # puts "6."+foto.inspect
      # foto=stdout.gets.strip
      # foto=stdout.gets.strip
      # foto=stdout.gets.strip
      # if foto!="0"
      #   array=Dir.entries('/root/.telegram/downloads/')
      #   archivo=nil
      #   array.each do |singleFile|
      #     if singleFile.index(foto)!=nil
      #       puts "archivo"
      #       filename  = File.basename('/root/.telegram/downloads/'+singleFile,".*")
      #       FileUtils.mv('/root/.telegram/downloads/'+singleFile, '/usr/src/qiwo/public/assets/telegram/'+singleFile)
      #       session[:telegram_image]="http://qiwo.mx/assets/telegram/"+singleFile
      #       @telegram.telegram_img="http://qiwo.mx/assets/telegram/"+singleFile
      #       @telegram.save
      #     end
      #   end
      # end
      
      
    else
      # y borramos el ultimo

      respond_to do |format|
          format.json { render json: "NOT_CORRECT",layout: false }
          return
      end
    end




    respond_to do |format|
      if @telegram.save
        format.json { render json: @telegram, status: :created, location: @telegram,layout: false }
      else
        format.html { render action: "new",layout: false }
        format.json { render json: @telegram.errors, status: :unprocessable_entity,layout: false }
      end
    end
  end


 def telegram_actions
    @toMessage=Telegram.where(:processed=>0)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @toMessage.to_json, layout: false }
    end


  end
  # PUT /telegrams/1
  # PUT /telegrams/1.json
  def update
    @telegram = Telegram.find(params[:id])

    respond_to do |format|
      if @telegram.update_attributes(params[:telegram])
        format.html { redirect_to @telegram, notice: 'Telegram was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @telegram.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /telegrams/1
  # DELETE /telegrams/1.json
  def destroy
    @telegram = Telegram.find(params[:id])
    @telegram.destroy

    respond_to do |format|
      format.html { redirect_to telegrams_url }
      format.json { head :no_content }
    end
  end
end
