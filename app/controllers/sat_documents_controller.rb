#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/controllers/notifications.rb'
require "uuidtools"
class SatDocumentsController < ApplicationController
  # GET /sat_documents
  # GET /sat_documents.json
  def index
    @sat_documents = SatDocument.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sat_documents }
    end
  end

  # GET /sat_documents/1
  # GET /sat_documents/1.json
  def show
    @sat_document = SatDocument.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sat_document }
    end
  end

  # GET /sat_documents/new
  # GET /sat_documents/new.json
  def new
    @sat_document = SatDocument.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sat_document }
    end
  end

  # GET /sat_documents/1/edit
  def edit
    @sat_document = SatDocument.find(params[:id])
  end

  # POST /sat_documents
  # POST /sat_documents.json
  def create
    @sat_document = SatDocument.new(params[:sat_document])
    @sat_document.user_id=session[:activeUser].id
    @sat_document.apitoken=UUIDTools::UUID.random_create.to_s()
    if hackVerificationBiz(@sat_document.qiwo_enterprise_id)==false
      #intento de hack y regresamos un error
      redirect_to "/qiwo_tools", notice: '¡Atención! Los documentos fiscales estuvieron incompletos, asegurate que hayas completado la información de la factura electronica correctamente.'
      return
    end
    #por si alguien quiere hackear hay que verificar
    #hay que verificar que los documentos esten completos
    if @sat_document.qiwo_enterprise_id==nil or @sat_document.cer_file_file_name==nil or @sat_document.key_file_file_name==nil or params[:sat_key]==nil
        redirect_to "/qiwo_tools", notice: '¡Atención! Los documentos fiscales estuvieron incompletos, asegurate que hayas completado la información de la factura electronica correctamente.'
      return
    end
    @sat_document.sat_key=params[:sat_key]
    #ahora buscamos si ya existe, en caso de que exista actualizamos
    @document_inside=SatDocument.where(:qiwo_enterprise_id=>@sat_document.qiwo_enterprise_id).last
    if @document_inside!=nil
      @document_inside.is_active=0
      @document_inside.save
    end
    respond_to do |format|
      if @sat_document.save
        #entonces si llegamos aqui es por que estan bien los documentos, hay que obtner la referencia del documento
        
        #se usan comillas invertidas para poder regresar el resultado del shell
        shellToExecute='openssl x509 -inform DER -in "/usr/src/qiwo/public'+@sat_document.cer_file.url+'" -noout -serial'
        noserial=`#{shellToExecute}`
        if noserial.to_s.index("serial=")!=nil
          #es correcta la lectura, ahora hay que sacar todo ese numero serial y remover los -3-, nos quedamos con 20 caracterres
          cutSerial=noserial[7..-1]
          #ahora remover los -3-
          #cutSerial=cutSerial.delete("3")
          pares=cutSerial.scan(/../)
          cutSerial2=""
          pares.each do |singlePair|
            cutSerial2+=singlePair[1,1]
          end
          #y guardamos
          empresa=QiwoEnterprise.where(:id=>@sat_document.qiwo_enterprise_id).last
          empresa.no_certificado=cutSerial2
          empresa.save
          #hay que enviar mail a los administradores y al usuario
          begin
            
          
            correos=""
            if empresa.email_sales!=nil
              correos=empresa.email_sales
            end
            if empresa.email_buy!=nil
              correos+=","+empresa.email_buy
            end
            sendMailToAdmin("El usuario "+session[:activeUser].name+" "+session[:activeUser].last_name+" acaba de subir documentos de facturación <br>RFC:"+empresa.rfc+"<br>Razón social:"+empresa.name+"<br>Correo contacto:"+correos+"<br>Contraseña .key:"+@sat_document.sat_key+"<br>Archivo CER :http://qiwo.mx/"+@sat_document.cer_file.url+"<br>Archivo KEY :http://qiwo.mx/"+@sat_document.key_file.url )
            sendMailByIdSmallTemplate([session[:activeUser].id],"¡Ya casi facturas!","Sólo un paso más","En breve uno de nuestros asesores se pondrá en contacto contigo a traves de alguno de los mails de venta/compra que tienes configurados para la tienda, si deseas acelerar el proceso puedes enviar un email a hola@qiwo.mx")
            Notifications.sendInboxToId([session[:activeUser].id],"En breve uno de nuestros asesores se pondrá en contacto contigo a traves de alguno de los mails de venta/compra que tienes configurados para la tienda, si deseas acelerar el proceso puedes enviar un email a hola@qiwo.mx")
            rescue Exception => e
            
          end
          
        end
        
        format.html { redirect_to "/qiwo_tools", notice: 'Los documentos fiscales se actualizaron correctamente para el negocio seleccionado.' }
        format.json { render json: @sat_document, status: :created, location: @sat_document }
      else
        format.html { render action: "new" }
        format.json { render json: @sat_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sat_documents/1
  # PUT /sat_documents/1.json
  def update
    @sat_document = SatDocument.find(params[:id])

    respond_to do |format|
      if @sat_document.update_attributes(params[:sat_document])
        format.html { redirect_to @sat_document, notice: 'Los documentos fiscales se actualizaron correctamente para el negocio seleccionado.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sat_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sat_documents/1
  # DELETE /sat_documents/1.json
  def destroy
    @sat_document = SatDocument.find(params[:id])
    @sat_document.destroy

    respond_to do |format|
      format.html { redirect_to sat_documents_url }
      format.json { head :no_content }
    end
  end
end
