#!/bin/env ruby
# encoding: utf-8
# require '/hacks/lineAccess/app/models/user.rb'

require '/usr/src/qiwo/app/models/sys_user.rb'
require '/usr/src/qiwo/app/models/api_token_user_interface.rb'
require '/usr/src/qiwo/app/models/sys_user_phone.rb'
require '/usr/src/qiwo/app/models/api_interface.rb'
class AuthenticationsController < ApplicationController
  # GET /authentications
  # GET /authentications.json
  def index
    puts "______________INDEX___________"
    @authentications = Api_Token_User_Interface.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @authentications }
    end
  end

  # GET /authentications/1
  # GET /authentications/1.json
  def show
    puts "______________SHOW___________"
    @authentication = Api_Token_User_Interface.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @authentication }
    end
  end

  # GET /authentications/new
  # GET /authentications/new.json
  def new
    puts "______________NEW___________"
    @authentication = Api_Token_User_Interface.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @authentication }
    end
  end

  # GET /authentications/1/edit
  def edit
    puts "______________EDIT___________"
    @authentication = Api_Token_User_Interface.find(params[:id])
  end

  # POST /authentications
  # POST /authentications.json
  # def create
  #   puts "______________CREATE___________"
  #   @authentication = Authentication.new(params[:authentication])

  #   respond_to do |format|
  #     if @authentication.save
  #       format.html { redirect_to @authentication, notice: 'Authentication was successfully created.' }
  #       format.json { render json: @authentication, status: :created, location: @authentication }
  #     else
  #       format.html { render action: "new" }
  #       format.json { render json: @authentication.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PUT /authentications/1
  # PUT /authentications/1.json
  def update
    puts "______________UPDATE___________"
    @authentication = Api_Token_User_Interface.find(params[:id])

    respond_to do |format|
      if @authentication.update_attributes(params[:authentication])
        format.html { redirect_to @authentication, notice: 'Authentication was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @authentication.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /authentications/1
  # DELETE /authentications/1.json
  def destroy
    puts "______________DESTROY_____________"
    @authentication = Api_Token_User_Interface.find(params[:id])
    @authentication.destroy

    respond_to do |format|
      format.html { redirect_to authentications_url }
      format.json { head :no_content }
    end
  end
  def create  

    auth = request.env["omniauth.auth"]
    puts "----------auth-from-facebook---------"
    puts auth.credentials.token
    puts auth.uid
    puts auth.info.image
    puts auth
    puts session[:activeUser].inspect
    if auth.provider == "facebook"
      
      puts auth.extra.raw_info.email
      #guardamos token
      usuario=Sys_User.find_by_user_id(auth.uid)
      
      if usuario == nil
        #crear usuario
        usuario=Sys_User.new
        usuario.sys_rol=2
        end
        usuario.user_id=auth.uid
        usuario.name=auth.extra.raw_info.first_name
        usuario.last_name=auth.extra.raw_info.last_name
        usuario.email=auth.extra.raw_info.email
        usuario.avatar=auth.info.image

        
        usuario.save
        #guardar telefono
        if session[:phone]!=nil
        userPhone=Sys_User_Phone.find_by_number(session[:phone])
        if userPhone==nil
          #crear objeto telefono
          userPhone=Sys_User_Phone.new
          userPhone.sys_phone_type=1
          userPhone.number=session[:phone]
          userPhone.sys_user=usuario.id
          userPhone.save
        end
        end
        #usuario.phonenumber=session[:phone]
        #usuario.token=auth.credentials.token
        

        autenticacion=Api_Token_User_Interface.new
        #buscar el provider
        apiInterface=Api_Interface.find_by_name(auth.provider)
        #autenticacion.provider=auth.provider
        autenticacion.api_interface=apiInterface.id
        autenticacion.token=auth.credentials.token
        autenticacion.sys_user=usuario.id
        autenticacion.save
        puts "***USER CREATED****"
      
      apiInterface=Api_Interface.find_by_name(auth.provider)
      updateAuth=Api_Token_User_Interface.where("api_interface='"+apiInterface.id.to_s+"' and sys_user='"+usuario.id.to_s+"'")
      if updateAuth.length>0
        #tomamos el primero
        firstQuery=updateAuth[0]
        firstQuery.token=auth.credentials.token
        firstQuery.save
        #acutalizamos token
        puts "***UPDATING FACEBOOK****"
      end
      #creamos sesion de facebook
      session[:activeUser] = usuario
      puts session[:activeUser].inspect
    elsif auth.provider == "evernote"
      puts "*****EVERNOTE*****"
      #recuperamos usuario de fb y enlazamos ambas cuentas
      apiInterface=Api_Interface.find_by_name(auth.provider)
      autenticacion=Api_Token_User_Interface.new
      autenticacion.api_interface=apiInterface.id
      autenticacion.token=auth.credentials.token
      autenticacion.sys_user=session[:activeUser].id
      autenticacion.save
    elsif auth.provider=="twitter"
      puts "*****TWITTER******"
      apiInterface=Api_Interface.find_by_name(auth.provider)
      autenticacion=Api_Token_User_Interface.new
      autenticacion.api_interface=apiInterface.id
      autenticacion.token=auth.credentials.token
      autenticacion.sys_user=session[:activeUser].id
      autenticacion.token_secret=auth.credentials.secret
      autenticacion.save
    elsif auth.provider=="google_oauth2"
      puts "***** GOOGLE ******"
      apiInterface=Api_Interface.find_by_name("google")
      autenticacion=Api_Token_User_Interface.new
      autenticacion.api_interface=apiInterface.id
      autenticacion.token=auth.credentials.token
      autenticacion.token_secret=auth.credentials.refresh_token
      autenticacion.sys_user=session[:activeUser].id
      autenticacion.expires=auth.credentials.expires_at
      autenticacion.save
      puts "+++++ google +++++"
    end
    redirect_to "/dashboard"
    # render :text => auth.inspect
    puts "----------auth----------"
    
end
end
