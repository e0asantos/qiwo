#!/bin/env ruby
# encoding: utf-8
class NfcRequestsController < ApplicationController
  # GET /nfc_requests
  # GET /nfc_requests.json
  def index
    @nfc_requests = NfcRequest.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @nfc_requests }
    end
  end

  # GET /nfc_requests/1
  # GET /nfc_requests/1.json
  def show
    @nfc_request = NfcRequest.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @nfc_request }
    end
  end

  # GET /nfc_requests/new
  # GET /nfc_requests/new.json
  def new
    @nfc_request = NfcRequest.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @nfc_request }
    end
  end

  # GET /nfc_requests/1/edit
  def edit
    @nfc_request = NfcRequest.find(params[:id])
  end

  # POST /nfc_requests
  # POST /nfc_requests.json
  def create
    @nfc_request = NfcRequest.new(params[:nfc_request])

    respond_to do |format|
      if @nfc_request.save
        format.html { redirect_to @nfc_request, notice: 'Nfc request was successfully created.' }
        format.json { render json: @nfc_request, status: :created, location: @nfc_request }
      else
        format.html { render action: "new" }
        format.json { render json: @nfc_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /nfc_requests/1
  # PUT /nfc_requests/1.json
  def update
    @nfc_request = NfcRequest.find(params[:id])

    respond_to do |format|
      if @nfc_request.update_attributes(params[:nfc_request])
        format.html { redirect_to @nfc_request, notice: 'Nfc request was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @nfc_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nfc_requests/1
  # DELETE /nfc_requests/1.json
  def destroy
    @nfc_request = NfcRequest.find(params[:id])
    @nfc_request.destroy

    respond_to do |format|
      format.html { redirect_to nfc_requests_url }
      format.json { head :no_content }
    end
  end
end
