#!/bin/env ruby
# encoding: utf-8
class PaymentsController < ApplicationController
  def index
    @payments=Payment.all
    request = Paypal::Express::Request.new(
      :username   => "asb.studios_api1.gmail.com",
      :password   => "ZHGEFBACCVCQ8G6E",
      :signature  => "AFcWxV21C7fd0v3bYYYRCpSSRl31A8QoANQfjk5IzpOAu-mCRUbdvn8O"
    )
    puts request.inspect
    payment_request = Paypal::Payment::Request.new(
      :currency_code => :MXN,   # if nil, PayPal use USD as default
      :description   => "alguna descripcion",    # item description
      :quantity      => 1,      # item quantity
      :amount        => 50.00,   # item value
    )
    payment_request = Paypal::Payment::Request.new(
  :currency_code => :JPY, # if nil, PayPal use USD as default
  :amount        => 1,
  :items => [{
    :name => "venta de algo",
    :description => "descr de la venta",
    :amount => "1",
    :category => :Digital
  }]
)
    puts payment_request.inspect
    response = request.setup(
  payment_request,
  "http://www.qiwo.mx/callBack.json",
  "http://www.qiwo.mx/callBack.json",
  :no_shipping => true
)
    puts "POP_UP======>"+response.popup_uri
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_log_karts }
    end
  end
  def successCallBack
    puts params.inspect
  end
  def cancelCallBack
    puts params.inspect
  end
  def show
    @payment = Payment.find_by_identifier! params[:id]
  end

  def create
    payment = Payment.create! params[:payment]
    payment.setup!(
      success_payments_url,
      cancel_payments_url
    )
    if payment.popup?
      redirect_to payment.popup_uri
    else
      redirect_to payment.redirect_uri
    end
  end

  def destroy
    Payment.find_by_identifier!(params[:id]).unsubscribe!
    redirect_to root_path, notice: 'Recurring Profile Canceled'
  end

  def success
    handle_callback do |payment|
      puts "success!!!!"
    end
  end

  def cancel
    handle_callback do |payment|
      puts "CANCEL!!!!"
    end
  end

  private

  def handle_callback
    payment = Payment.find_by_token! params[:token]
    @redirect_uri = yield payment
    if payment.popup?
      render :close_flow, layout: false
    else
      redirect_to @redirect_uri
    end
  end

  def paypal_api_error(e)
    redirect_to root_url, error: e.response.details.collect(&:long_message).join('<br />')
  end

end