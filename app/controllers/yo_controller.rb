#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/qiwo_log_kart.rb'
require '/usr/src/qiwo/app/models/qiwo_review.rb'
require '/usr/src/qiwo/app/models/qiwo_log_visit.rb'
require '/usr/src/qiwo/app/models/qiwo_log_search.rb'
require '/usr/src/qiwo/app/models/api_token_user_interface.rb'
require '/usr/src/qiwo/app/models/sys_user.rb'
require 'iso_country_codes'
require 'json'
require 'koala'
require 'sys/filesystem'
require 'xmpp4r_facebook'
require 'xmpp4r/roster'
require 'fb_graph'
class YoController < ApplicationController
  protect_from_forgery except: :qtinbo

  def reassignPlan
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    if session[:activeUser].sys_rol==1
      usuario=params[:usuario]
      plan=params[:plan]
      suscripcion=QiwoUserSubscription.where(:user_id=>usuario,:subscription_type=>plan).last
      if suscripcion==nil
        suscripcion=QiwoUserSubscription.new
        suscripcion.user_id=usuario
        suscripcion.save
        suscripcion.payment_gateway="none"
        suscripcion.amount_paid=0
      end
      suscripcion.subscription_type=plan
      suscripcion.ends_on=Date.today+QiwoSubscription.where(:id=>plan).last.days_active.to_i
      suscripcion.save
      respond_to do |format|
        format.html { render :json => "OK".to_json }
        format.xml  { render :xml => "OK".to_json }
        format.json { render :json => "OK".to_json }
      end
    else
      redirect_to "/grant_permission.html"
      return
    end
  end

  def qtinbo
    Notifications.sendGeneralInfo(session," no se pudo generar el código de barras, comentalo con tu vendedor para que acuerden otra forma de pago")
    respond_to do |format|
        format.html { render :json => "OK".to_json }
        format.xml  { render :xml => "OK".to_json }
        format.json { render :json => "OK".to_json }
      end
  end

  def reassignCFDI
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    if session[:activeUser].sys_rol==1
      usuario=params[:usuario]
      folios=params[:folios]
      # buscamos las empresas del usuario y reescribimos sus folios
      empresas=QiwoEnterprise.where(:user_owner=>usuario)
      empresasId=empresas.index_by(&:id).keys
      cambio=QiwoCounter.where(:qiwo_enterprise_id=>empresasId)
      cambio.each do |singleCounter|
        singleCounter.facturas_disponibles=folios
        singleCounter.save
      end
       respond_to do |format|
        format.html { render :json => "OK".to_json }
        format.xml  { render :xml => "OK".to_json }
        format.json { render :json => "OK".to_json }
      end
    else
      redirect_to "/grant_permission.html"
      return
    end
  end

  def createGuides
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    if session[:activeUser].sys_rol==1
      # hay que tomar los rangos y crear las guias, las guias de prueba entran en juego solo cuando qiwo esta en modo de prueba
      inicial=params[:initialGuide].to_i
      fin=params[:endGuide].to_i
      while inicial<fin+1
        guia=ShippingGuide.where(:label_guide=>inicial).last
        if guia==nil
          guia=ShippingGuide.new
          guia.real_guide=inicial
          guia.status_guide=0
          if params[:optionsRadios]=="test"
            guia.gtype=0  
          else
            guia.gtype=1
          end
          guia.save
          
        end
        inicial=inicial+1
      end

    end
    redirect_to "/yo"
  end

  def sendMessageToAll
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    if session[:activeUser].sys_rol==1
      #usamos token de qiwo
      token=Api_Token_User_Interface.where(:sys_user=>11,:api_interface=>2).last
      puts token.inspect
      if token!=nil
        @graph = Koala::Facebook::API.new(token.token)
        friends=@graph.get_connections "me", "friends"
          friends.each do |singleFriend|
            puts singleFriend['id'] 
            #enviar a andres mensaje de prueba
          cuerpoMensaje=params[:idmessage]
          puts cuerpoMensaje.inspect
          id = '-100005654102627@chat.facebook.com'
            to = '-'+singleFriend['id']+'@chat.facebook.com'
          #buscar producto
            body = cuerpoMensaje
            subject = 'no titulo'
            message = Jabber::Message.new to, body
            message.subject = subject
            
            client = Jabber::Client.new Jabber::JID.new(id)
            client.connect

            client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)
            
            client.send message
            client.close
          end 
      end
      
    end
    render :nothing => true, :status => 200
  end

  def getCalendar
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    eventos=[]
    #primero cargamos la fundacion de las empresas
    misNegocios=QiwoEnterprise.where(:user_owner=>session[:activeUser].id)
    misNegociosIndex=misNegocios.index_by(&:id).keys
    misNegocios.each do |singleBiz|
      if singleBiz.is_shipping!=1
        singleEvent=Hash.new
      singleEvent= { :title=>"Inscrito:"+singleBiz.name,:start=>singleBiz.created_at.to_s,:backgroundColor=>'#4b8df8'  }
      eventos.push(singleEvent)  
      end
      
    end
    #ahora obtener los productos
    misProductos=QiwoProduct.where(:business_source=>misNegociosIndex)
    misProductosIndex=misProductos.index_by(&:id).keys
    misProductos.each do |singleProduct|
      singleEvent=Hash.new
      singleEvent= { :title=>"Inscrito:"+singleProduct.name,:start=>singleProduct.created_at.to_s,:backgroundColor=>'#35aa47'  }
      eventos.push(singleEvent)
      #tambien hay que introducir la fecha de vencimiento de la busqueda
      singleEvent= { :title=>"Vencimiento:"+singleProduct.name,:start=>singleProduct.ends_on.to_s,:backgroundColor=>'#e02222'  }
      eventos.push(singleEvent)
    end
    #ahora obtener cotizaciones
    misVentas=QiwoLogKart.where(:qiwo_product_id=>misProductosIndex)
    misVentas.each do |singleSale|
      singleEvent=Hash.new
      if singleSale.kart_state==1
        singleEvent= { :title=>"Posible cliente para:"+QiwoProduct.find_by_id(singleSale.qiwo_product_id).name,:start=>singleSale.created_at.to_s,:backgroundColor=>'#555555'  }
        eventos.push(singleEvent)
      elsif singleSale.kart_state==2
        singleEvent= { :title=>"Pago recibido:"+QiwoProduct.find_by_id(singleSale.qiwo_product_id).name,:start=>singleSale.created_at.to_s,:backgroundColor=>'#555555'  }
        eventos.push(singleEvent)
      elsif singleSale.kart_state==3
        singleEvent= { :title=>"Venta completa:"+QiwoProduct.find_by_id(singleSale.qiwo_product_id).name,:start=>singleSale.created_at.to_s,:backgroundColor=>'#ffb848'  }
        eventos.push(singleEvent)
      end 
    end
    #por ultimo traer la suscripcion
    misSuscripciones=QiwoUserSubscription.where(:user_id=>session[:activeUser].id)
    misSuscripciones.each do |singleSuscription|
        eachSuscription=Hash.new
        eachSuscription={ :title=>"suscripcion:",:start=>singleSuscription.created_at.to_s,:backgroundColor=>'#852b99',:end=>singleSuscription.ends_on.to_s  }
        eventos.push(eachSuscription)
    end
    

    puts "EVENTOS:"+ eventos.to_json
    respond_to do |format|
      format.html { render :json => eventos.to_json }
      format.xml  { render :xml => eventos.to_json }
      format.json { render :json => eventos.to_json }
    end
  end
  # GET /yos
  # GET /yos.json
  def index
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    if session[:activeUser].sys_rol==1
      @yos = Yo.all
      @maparaw=QiwoLogVisit.last(200)
      @mapa=Hash.new
      stat = Sys::Filesystem.stat("/")
      fecha_inicio=Date.new(Date.today.year,Date.today.mon,1)  #Date.today-30
      fecha_fin=fecha_inicio+30
      @visitas=QiwoLogSearch.where(:created_at=>fecha_inicio..fecha_fin)
      @visitasraw=Hash.new
      @visitas.each do |singleVisit|
         if @visitasraw[singleVisit.created_at.day.to_s+"_"+singleVisit.created_at.month.to_s]==nil
             @visitasraw[singleVisit.created_at.day.to_s+"_"+singleVisit.created_at.month.to_s]=1
          else
             @visitasraw[singleVisit.created_at.day.to_s+"_"+singleVisit.created_at.month.to_s]=@visitasraw[singleVisit.created_at.day.to_s+"_"+singleVisit.created_at.month.to_s]+1
          end
      end
      @mb_available = stat.block_size * stat.blocks_available / 1024 / 1024
      @maparaw.each do |singleMap|
        begin
          if singleMap.country.downcase!='vietnam'
              iso=IsoCountryCodes.search_by_name(singleMap.country.downcase)
              if @mapa[iso[0].alpha2.downcase]==nil
                  @mapa[iso[0].alpha2.downcase]=1
              else 
                  @mapa[iso[0].alpha2.downcase]=@mapa[iso[0].alpha2.downcase]+1
              end
          end
        rescue Exception => e
          
        end
          
      end
      #tambien hay que cargar los ultimos 20 usuarios nuevos
      @recentUsers=Sys_User.all.last(40).reverse
      #verificar los clientes
      @qiwoClientsSubscriptions=QiwoUserSubscription.all
      @qiwoClients=[]
      # @qiwoClients["name"]=[] Hash.new
      # @qiwoClients["email"]=[]
      # @qiwoClients["active"]=[]
      @qiwoClientsSubscriptions.each do |singleSuscription|
        cliente=Sys_User.where(:id=>singleSuscription.user_id).last
        objeto=Hash.new
        objeto["name"]=cliente.name
        if cliente.last_name!=nil
          objeto["name"]=objeto["name"]+" "+cliente.last_name
        end
        objeto["email"]=cliente.email
        objeto["active"]=singleSuscription.ends_on>Time.now ? "yes" : "no"
        planusuario=QiwoSubscription.where(:id=>singleSuscription.subscription_type).last
        if planusuario!=nil
          objeto["plan"]=planusuario.subscription_name  
          objeto["details"]=planusuario.places_allowed.to_s+" Negocios/"+planusuario.products_allowed.to_s+" productos"
        else
          objeto["plan"]="INEXISTENTE"
          objeto["details"]="0 Negocios/0 productos"
        end
        
        @qiwoClients.push(objeto)
        #ahora verificar que las fechas sean correctas
      end
    end
    #aqui recorremos todas las ventas que tengan pago confirmado
    misNegocios=QiwoEnterprise.where(:user_owner=>session[:activeUser].id)
    misNegociosIndex=misNegocios.index_by(&:id).keys
    @gananciasGlobales=QiwoLogKart.select("sum(final_gain) as my_gain").where(:store_id=>misNegociosIndex).last
    #con los negocios podemos determinar el numero de comentarios
    misProductos=QiwoProduct.where(:business_source=>misNegociosIndex)
    misProductosIndex=misProductos.index_by(&:id).keys
    @openComments=QiwoReview.where(:product_id=>misProductosIndex).count
    #ordenes abiertas
    @openOrders=QiwoLogKart.where(:store_id=>misNegociosIndex,:kart_state=>1).count
    #ordenes completas
    @completedOrders=QiwoLogKart.where(:store_id=>misNegociosIndex).where("kart_state>2").count
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @yos }
    end
  end

  # GET /yos/1
  # GET /yos/1.json
  def show
    @yo = Yo.find(params[:id])

    respond_to do |format|
      format.html {render layout: false}# show.html.erb
      format.json { render json: @yo }
    end
  end

  # GET /yos/new
  # GET /yos/new.json
  def new
    @yo = Yo.new

    respond_to do |format|
      format.html{render layout: false} # new.html.erb
      format.json { render json: @yo }
    end
  end

  # GET /yos/1/edit
  def edit
    @yo = Yo.find(params[:id])
    render layout: false
  end

  # POST /yos
  # POST /yos.json
  def create
    @yo = Yo.new(params[:yo])

    respond_to do |format|
      if @yo.save
        format.html { redirect_to @yo, notice: 'Yo was successfully created.' }
        format.json { render json: @yo, status: :created, location: @yo }
      else
        format.html { render action: "new" }
        format.json { render json: @yo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /yos/1
  # PUT /yos/1.json
  def update
    # aqui se actualiza solo la portada del blog
    usuario=Sys_User.find_by_id(session[:activeUser].id)
    usuario.blogfrontpage=params[:yo][:userid]
    usuario.blogname=params[:yo][:id]
    usuario.save
    puts "BLLLLOOOOGGGG"
    puts params[:yo][:userid]
    puts "BLLLLOOOOGGGG"

    respond_to do |format|
      if usuario.save
        format.html { redirect_to "/blogs", notice: 'Se actualizó la imagen de portada de tu blog.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: "/blogs", status: :unprocessable_entity }
      end
    end
  end

  # DELETE /yos/1
  # DELETE /yos/1.json
  def destroy
    @yo = Yo.find(params[:id])
    @yo.destroy

    respond_to do |format|
      format.html { redirect_to yos_url }
      format.json { head :no_content }
    end
  end
end
