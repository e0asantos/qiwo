#!/bin/env ruby
# encoding: utf-8
require '/usr/src/qiwo/app/models/qiwo_subscription.rb'
require '/usr/src/qiwo/app/controllers/notifications.rb'
class QiwoToolsController < ApplicationController

  def updateMerchantTools
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    paypal_tool=QiwoTool.where(:user_id=>session[:activeUser].id,:toolname=>"paypal").last
    paypal_tool.apikey=params[:ppaluser]
    paypal_tool.apisecret=params[:ppalpassword]
    paypal_tool.signature=params[:ppalsignature]
    paypal_tool.save
    conekta_tool=QiwoTool.where(:user_id=>session[:activeUser].id,:toolname=>"conekta").last
    conekta_tool.apikey=params[:nektapub]
    conekta_tool.apisecret=params[:nektapri]
    conekta_tool.save
    conekta_tool=QiwoTool.where(:user_id=>session[:activeUser].id,:toolname=>"openpay").last
    conekta_tool.signature=params[:opayid]
    conekta_tool.apikey=params[:opaypub]
    conekta_tool.apisecret=params[:opaypri]
    conekta_tool.save
    conekta_tool=QiwoTool.where(:user_id=>session[:activeUser].id,:toolname=>"aftership").last
    conekta_tool.apisecret=params[:aftership]
    conekta_tool.save
    firstdata_tool=QiwoTool.where(:user_id=>session[:activeUser].id,:toolname=>"firstdata").last
    firstdata_tool.apikey=params[:fd_apikey]
    firstdata_tool.apisecret=params[:fd_apisecret]
    firstdata_tool.signature=params[:fd_sharedsecret]
    firstdata_tool.store_id=params[:fd_storeid]
    firstdata_tool.save
    render :nothing => true, :status => 200
  end

  # GET /qiwo_tools
  # GET /qiwo_tools.json
  def index
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    #hay que buscar las empresas que ya tienen documentos de factura electronica
    @empresasConFactura=conFacturaElectronica()
    
    if QiwoTool.where(:user_id=>session[:activeUser].id,:toolname=>"openpay").length==0
      openpay_tool=QiwoTool.new(:user_id=>session[:activeUser].id,:toolname=>"openpay")
      openpay_tool.save  
    end
    if QiwoTool.where(:user_id=>session[:activeUser].id,:toolname=>"paypal").length==0
      conekta_tool=QiwoTool.new(:user_id=>session[:activeUser].id,:toolname=>"paypal")
      conekta_tool.save  
    end
    if QiwoTool.where(:user_id=>session[:activeUser].id,:toolname=>"conekta").length==0
      paypal_tool=QiwoTool.new(:user_id=>session[:activeUser].id,:toolname=>"conekta")
      paypal_tool.save  
    end
    if QiwoTool.where(:user_id=>session[:activeUser].id,:toolname=>"aftership").length==0
      aftership_tool=QiwoTool.new(:user_id=>session[:activeUser].id,:toolname=>"aftership")
      aftership_tool.save  
    end
    if QiwoTool.where(:user_id=>session[:activeUser].id,:toolname=>"firstdata").length==0
      firstdata_tool=QiwoTool.new(:user_id=>session[:activeUser].id,:toolname=>"firstdata")
      firstdata_tool.save  
    end
    #hay que verificar si el usuario ya tiene los registros de las herramientas, de no ser asi, crearlas al entrar
    @qiwo_tools = QiwoTool.where(:user_id=>session[:activeUser].id)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_tools }
    end
  end

  # GET /qiwo_tools/1
  # GET /qiwo_tools/1.json
  def show
    @qiwo_tool = QiwoTool.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_tool }
    end
  end

  # GET /qiwo_tools/new
  # GET /qiwo_tools/new.json
  def new
    @qiwo_tool = QiwoTool.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_tool }
    end
  end

  # GET /qiwo_tools/1/edit
  def edit
    @qiwo_tool = QiwoTool.find(params[:id])
  end

  # POST /qiwo_tools
  # POST /qiwo_tools.json
  def create
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_tool = QiwoTool.new(params[:qiwo_tool])

    respond_to do |format|
      if @qiwo_tool.save
        format.html { redirect_to @qiwo_tool, notice: 'Qiwo tool was successfully created.' }
        format.json { render json: @qiwo_tool, status: :created, location: @qiwo_tool }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_tool.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_tools/1
  # PUT /qiwo_tools/1.json
  def update
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_tool = QiwoTool.find(params[:id])

    respond_to do |format|
      if @qiwo_tool.update_attributes(params[:qiwo_tool])
        format.html { redirect_to @qiwo_tool, notice: 'Qiwo tool was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_tool.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_tools/1
  # DELETE /qiwo_tools/1.json
  def destroy
    @qiwo_tool = QiwoTool.find(params[:id])
    @qiwo_tool.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_tools_url }
      format.json { head :no_content }
    end
  end
end
