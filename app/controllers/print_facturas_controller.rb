#!/bin/env ruby
# encoding: utf-8
require "base64"
require 'xmlsimple'
class PrintFacturasController < ApplicationController
  helper_method :numero_a_palabras
  # GET /print_facturas
  # GET /print_facturas.json
  def index
    @print_facturas = PrintFactura.all

    # respond_to do |format|
    #   format.html # index.html.erb
    #   format.json { render json: @print_facturas }
    # end
    render layout: false
  end

  # GET /print_facturas/1
  # GET /print_facturas/1.json
  def show
    #buscamos la factura por string de factura
    @print_factura = QiwoFacturas.find_by_token(params[:id])
    #primero vamos a abrir el xml
    doc=Base64.decode64(@print_factura.cfdi)
    @xml=XmlSimple.xml_in(doc)
    puts @xml["Emisor"][0]["nombre"].inspect
    #cargamos el nombre comercial de la empresa
    if @print_factura.qiwo_log_kart_id!=nil
      #buscamos la compra individual
      @compra=QiwoLogKart.where(:id=>@print_factura.qiwo_log_kart_id)
      @nombre_comercial=QiwoEnterprise.find_by_id(@compra[0].store_id).comercial_name
    else
      #buscamos la compra en forma de orden
      @compra=QiwoLogKart.where(:qiwo_order_id=>@print_factura.qiwo_order_id)
      @nombre_comercial=QiwoEnterprise.find_by_id(@compra[0].store_id).comercial_name
    end
    
    #hay que cargar las ventas

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @print_factura }
    # end
     render layout: false
  end

  def numero_a_palabras(numero)
  de_tres_en_tres = numero.to_i.to_s.reverse.scan(/\d{1,3}/).map{|n| n.reverse.to_i}
 
  millones = [
    {true => nil, false => nil},
    {true => 'millón', false => 'millones'},
    {true => "billón", false => "billones"},
    {true => "trillón", false => "trillones"}
  ]
 
  centena_anterior = 0
  contador = -1
  palabras = de_tres_en_tres.map do |numeros|
    contador += 1
    if contador%2 == 0
      centena_anterior = numeros
      [centena_a_palabras(numeros), millones[contador/2][numeros==1]].compact if numeros > 0
    elsif centena_anterior == 0
      [centena_a_palabras(numeros), "mil", millones[contador/2][false]].compact if numeros > 0
    else
      [centena_a_palabras(numeros), "mil"] if numeros > 0
    end
  end
 
  palabras.compact.reverse.join(' ')
end
 
def centena_a_palabras(numero)
  especiales = {
    11 => 'once', 12 => 'doce', 13 => 'trece', 14 => 'catorce', 15 => 'quince',
    10 => 'diez', 20 => 'veinte', 100 => 'cien'
  }
  if especiales.has_key?(numero)
    return especiales[numero]
  end
 
  centenas = [nil, 'ciento', 'doscientos', 'trescientos', 'cuatrocientos', 'quinientos', 'seiscientos', 'setecientos', 'ochocientos', 'novecientos']
  decenas = [nil, 'dieci', 'veinti', 'treinta', 'cuarenta', 'cincuenta', 'sesenta', 'setenta', 'ochenta', 'noventa']
  unidades = [nil, 'un', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve']
 
  centena, decena, unidad = numero.to_s.rjust(3,'0').scan(/\d/).map{|i| i.to_i}
 
  palabras = []
  palabras << centenas[centena]
 
  if especiales.has_key?(decena*10 + unidad)
    palabras << especiales[decena*10 + unidad]
  else
    tmp = "#{decenas[decena]}#{' y ' if decena > 2 && unidad > 0}#{unidades[unidad]}"
    palabras << (tmp.blank? ? nil : tmp)
  end
 
  palabras.compact.join(' ')
end

  # GET /print_facturas/new
  # GET /print_facturas/new.json
  def new
    @print_factura = PrintFactura.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @print_factura }
    end
  end

  # GET /print_facturas/1/edit
  def edit
    @print_factura = PrintFactura.find(params[:id])
  end

  # POST /print_facturas
  # POST /print_facturas.json
  def create
    @print_factura = PrintFactura.new(params[:print_factura])

    respond_to do |format|
      if @print_factura.save
        format.html { redirect_to @print_factura, notice: 'Print factura was successfully created.' }
        format.json { render json: @print_factura, status: :created, location: @print_factura }
      else
        format.html { render action: "new" }
        format.json { render json: @print_factura.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /print_facturas/1
  # PUT /print_facturas/1.json
  def update
    @print_factura = PrintFactura.find(params[:id])

    respond_to do |format|
      if @print_factura.update_attributes(params[:print_factura])
        format.html { redirect_to @print_factura, notice: 'Print factura was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @print_factura.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /print_facturas/1
  # DELETE /print_facturas/1.json
  def destroy
    @print_factura = PrintFactura.find(params[:id])
    @print_factura.destroy

    respond_to do |format|
      format.html { redirect_to print_facturas_url }
      format.json { head :no_content }
    end
  end
end
