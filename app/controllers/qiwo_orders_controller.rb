#!/bin/env ruby
# encoding: utf-8
class QiwoOrdersController < ApplicationController
  # GET /qiwo_orders
  # GET /qiwo_orders.json
  def index
    if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @qiwo_enterprise=QiwoEnterprise.new
    # vamos a borrar las ordenes vacias
    # ordenesTodas=QiwoOrder.all
    # ordenesTodas.each do |singleOrderAll|
    #   contenidos=QiwoLogKart.where(:qiwo_order_id=>singleOrderAll.id)
    #   if contenidos.length==0
    #     singleOrderAll.destroy
    #   end
    # end
    @qiwo_orders = QiwoOrder.where(:salesforce_user_id=>getUserAndEmployees()).paginate(:page => params[:page], :per_page => 20).order("created_at DESC")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_orders }
    end
  end

  # GET /qiwo_orders/1
  # GET /qiwo_orders/1.json
  def show
    @qiwo_order = QiwoOrder.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_order }
    end
  end

  # GET /qiwo_orders/new
  # GET /qiwo_orders/new.json
  def new
    @qiwo_order = QiwoOrder.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_order }
    end
  end

  # GET /qiwo_orders/1/edit
  def edit
    @qiwo_order = QiwoOrder.find(params[:id])
  end

  # POST /qiwo_orders
  # POST /qiwo_orders.json
  def create
    @qiwo_order = QiwoOrder.new(params[:qiwo_order])

    respond_to do |format|
      if @qiwo_order.save
        format.html { redirect_to @qiwo_order, notice: 'Qiwo order was successfully created.' }
        format.json { render json: @qiwo_order, status: :created, location: @qiwo_order }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_orders/1
  # PUT /qiwo_orders/1.json
  def update
    @qiwo_order = QiwoOrder.find(params[:id])

    respond_to do |format|
      if @qiwo_order.update_attributes(params[:qiwo_order])
        format.html { redirect_to @qiwo_order, notice: 'Qiwo order was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_orders/1
  # DELETE /qiwo_orders/1.json
  def destroy
    @qiwo_order = QiwoOrder.find(params[:id])
    @qiwo_order.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_orders_url }
      format.json { head :no_content }
    end
  end
end
