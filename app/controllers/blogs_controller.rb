require 'securerandom'
require 'nokogiri'
class BlogsController < ApplicationController
  # GET /blogs
  # GET /blogs.json
  def index
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @blogs = Blog.all

    if session[:activeUser].sys_rol==1
      @blogs = Blog.where("authors is not null").paginate(:page => params[:page], :per_page => 10).order("created_at DESC")
    else
      @blogs = Blog.where("authors is not null and authors=="+session[:activeUser].id.to_s+"").paginate(:page => params[:page], :per_page => 10).order("created_at DESC")
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @blogs }
    end
  end

  # GET /blogs/1
  # GET /blogs/1.json
  def show

    if params.has_key?(:nombre)
    @blog = Blog.where(:uri=>params[:nombre]).last
    else
      @blog = Blog.find(params[:id])
      if @blog==nil
          redirect_to "/"
          return
      end
    end
    respond_to do |format|
      if params.has_key?(:nombre)
      format.html# show.html.erb
    else
format.html {render layout: false}# show.html.erb
    end
      format.json { render json: @blog }
    end
  end

  # GET /blogs/new
  # GET /blogs/new.json
  def new
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @blog = Blog.new

    respond_to do |format|
      format.html {render layout:false}# new.html.erb
      format.json { render json: @blog }
    end
  end

  # GET /blogs/1/edit
  def edit
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @blog = Blog.find(params[:id])
    if @blog.authors.to_i!=session[:activeUser].id
      redirect_to "/grant_permission.html"
        return
    end
    render layout: false
  end

  # POST /blogs
  # POST /blogs.json
  def create
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end

    @blog = Blog.new(params[:blog])
    @blog.authors=session[:activeUser].id
    if @blog.uri==nil or @blog.uri==""
      @blog.uri=@blog.title.gsub(/[^0-9a-z ]/i, '').downcase
      @blog.uri=@blog.title.gsub(" ", '-')  
      # antes de guardar veridicar q no exista
      if Blog.where(:uri=>@blog.uri).length>0
        @blog.uri=@blog.uri+SecureRandom.hex[0..4]
      end
    end
    
    respond_to do |format|
      if @blog.save
        format.html { redirect_to @blog, notice: 'Blog was successfully created.',layout: false }
        format.json { render json: @blog, status: :created, location: @blog }
      else
        format.html { render action: "new",layout: false }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  def frontpage
    respond_to do |format|
      
        format.html {render layout:false} 
        format.json { render json: "ok" }
      
    end
  end

  # PUT /blogs/1
  # PUT /blogs/1.json
  def update
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @blog = Blog.find(params[:id])
    if @blog.authors.to_i!=session[:activeUser].id
      redirect_to "/grant_permission.html"
        return
    end
    if @blog.uri==nil or @blog.uri==""
      @blog.uri=@blog.title.gsub(/[^0-9a-z ]/i, '').downcase
      @blog.uri=@blog.title.gsub(" ", '-')  
      # antes de guardar veridicar q no exista
      if Blog.where(:uri=>@blog.uri).length>0
        @blog.uri=@blog.uri+SecureRandom.hex[0..4]
      end
    end
    respond_to do |format|
      if @blog.update_attributes(params[:blog])
        format.html { redirect_to @blog, notice: 'Blog was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit",layout: false }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /blogs/1
  # DELETE /blogs/1.json
  def destroy
     if session[:activeUser]==nil
        redirect_to "/grant_permission.html"
        return
    end
    @blog = Blog.find(params[:id])
    if @blog.authors.to_i!=session[:activeUser].id
      redirect_to "/grant_permission.html"
        return
    end
    @blog.destroy

    respond_to do |format|
      format.html { redirect_to blogs_url }
      format.json { head :no_content }
    end
  end
end
