#!/bin/env ruby
# encoding: utf-8
require 'koala'
class QiwoEmployeesController < ApplicationController
  protect_from_forgery :except => [:lookInFacebookEmployees,:listEmployees,:assignEmployee,:fireEmployee]

  def fireEmployee
    employeeRef=params[:empref]
    misNegocios=QiwoEnterprise.where(:user_owner=>session[:activeUser].id)
    misNegociosIndex=misNegocios.index_by(&:id).keys
    activo=QiwoEmployee.where(:id=>employeeRef,:employee_enterprise=>misNegociosIndex).last
    if activo!=nil
      activo.is_active=0
      activo.save
    end
    productos="ok"
    respond_to do |format|
      format.html { render :json => productos.to_json }
      format.xml  { render :xml => productos.to_json }
      format.json { render :json => productos.to_json }
    end
  end
  def assignEmployee
    storeId=params[:stype]
    userId=params[:fbtype]
    #buscamos si ya existe
    yaexiste=QiwoEmployee.where(:employee_fb_id=>userId,:employee_enterprise=>storeId).last
    if yaexiste==nil
      empleado=QiwoEmployee.new
      empleado.employee_fb_id=userId
      empleado.employee_enterprise=storeId
      empleado.save
    end

    productos="ok"
    respond_to do |format|
      format.html { render :json => productos.to_json }
      format.xml  { render :xml => productos.to_json }
      format.json { render :json => productos.to_json }
    end
  end

  def lookInFacebookEmployees
    friendName=params[:q]
    token=Api_Token_User_Interface.where(:sys_user=>session[:activeUser].id,:api_interface=>2).last
    @graph = Koala::Facebook::API.new(token.token)
    productos={:movies=>[],:total=>3}
    searchParam=params[:q]
    peliculas=productos[:movies]
    friends=@graph.get_connections "me", "friends"
    friends.each do |singleFriend|
      if singleFriend["name"].downcase.index(friendName.downcase)!=nil
        peliculas.push(:id=>singleFriend["id"],:title=>singleFriend["name"])
        puts singleFriend["name"]
      end
    end
    # @qiwo_products.each do |singleProduct|
    #   peliculas.push({:id=>singleProduct.id,:title=>singleProduct.name})
    # end

     #productos[:movies]=[{:id=>1,:title=>"juan carlos"},{:id=>1,:title=>"lee brimelow"},{:id=>1,:title=>"pedro paramo"},{:id=>1,:title=>"el topo"}]
    productos[:movies]=peliculas
    respond_to do |format|
      format.html { render :json => productos.to_json }
      format.xml  { render :xml => productos.to_json }
      format.json { render :json => productos.to_json }
    end
  end

  def listEmployees
    token=Api_Token_User_Interface.where(:sys_user=>session[:activeUser].id,:api_interface=>2).last
    @graph = Koala::Facebook::API.new(token.token)
    misNegocios=QiwoEnterprise.where(:user_owner=>session[:activeUser].id)
    misNegociosIndex=misNegocios.index_by(&:id).keys
    empleados=QiwoEmployee.where(:employee_enterprise=>misNegociosIndex,:is_active=>1)
    empleadosInfo=[]
    empleados.each do |singleEmployee|
      #primero buscarlo quizas aun no se registra
      estaRegistrado=Sys_User.where(:user_id=>singleEmployee.employee_fb_id).last
      if estaRegistrado!=nil
        #ya se registro entonces obtener sus datos
        #calcular ventas generadas este mes
        fecha=Date.new(Date.today.year,Date.today.mon,1)
        fechaFin=fecha+1.month
        suma=QiwoLogKart.select("sum(final_total) as ganancia").where(['updated_at <= ?',fechaFin]).where(['updated_at >= ?', fecha]).where(:user_id=>estaRegistrado.id).where(:store_id=>misNegociosIndex).last
        if suma==nil or suma.ganancia==nil
          suma='0'
        else
          suma=suma.ganancia
        end
        empleadosInfo.push({:name=>estaRegistrado.name+" "+estaRegistrado.last_name,:store=>misNegocios.where(:id=>singleEmployee.employee_enterprise).last.name,:pic=>estaRegistrado.avatar,:sales=>suma,:mref=>singleEmployee.id})
      else
        #si no se ha registrado buscar el nombre en fb y decir que no se ha registrado
        empleadosInfo.push({:name=>@graph.get_object(singleEmployee.employee_fb_id)["name"]+" (No inscrito en Qiwo)",:store=>misNegocios.where(:id=>singleEmployee.employee_enterprise).last.name,:pic=>@graph.get_picture(singleEmployee.employee_fb_id),:sales=>'0',:mref=>singleEmployee.id})
      end
    end
    respond_to do |format|
      format.html { render :json => empleadosInfo.to_json }
      format.xml  { render :xml => empleadosInfo.to_json }
      format.json { render :json => empleadosInfo.to_json }
    end
  end
  # GET /qiwo_employees
  # GET /qiwo_employees.json
  def index
    token=Api_Token_User_Interface.where(:sys_user=>session[:activeUser].id,:api_interface=>2).last
    @graph = Koala::Facebook::API.new(token.token)
    @misNegocios=QiwoEnterprise.where(:user_owner=>session[:activeUser].id)
    @misNegociosIndex=@misNegocios.index_by(&:id).keys
    @qiwo_employees = QiwoEmployee.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qiwo_employees }
    end
  end

  # GET /qiwo_employees/1
  # GET /qiwo_employees/1.json
  def show
    @qiwo_employee = QiwoEmployee.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qiwo_employee }
    end
  end

  # GET /qiwo_employees/new
  # GET /qiwo_employees/new.json
  def new
    @qiwo_employee = QiwoEmployee.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qiwo_employee }
    end
  end

  # GET /qiwo_employees/1/edit
  def edit
    @qiwo_employee = QiwoEmployee.find(params[:id])
  end

  # POST /qiwo_employees
  # POST /qiwo_employees.json
  def create
    @qiwo_employee = QiwoEmployee.new(params[:qiwo_employee])

    respond_to do |format|
      if @qiwo_employee.save
        format.html { redirect_to @qiwo_employee, notice: 'Qiwo employee was successfully created.' }
        format.json { render json: @qiwo_employee, status: :created, location: @qiwo_employee }
      else
        format.html { render action: "new" }
        format.json { render json: @qiwo_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qiwo_employees/1
  # PUT /qiwo_employees/1.json
  def update
    @qiwo_employee = QiwoEmployee.find(params[:id])

    respond_to do |format|
      if @qiwo_employee.update_attributes(params[:qiwo_employee])
        format.html { redirect_to @qiwo_employee, notice: 'Qiwo employee was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qiwo_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qiwo_employees/1
  # DELETE /qiwo_employees/1.json
  def destroy
    @qiwo_employee = QiwoEmployee.find(params[:id])
    @qiwo_employee.destroy

    respond_to do |format|
      format.html { redirect_to qiwo_employees_url }
      format.json { head :no_content }
    end
  end
end
