class SatDocument < ActiveRecord::Base
	belongs_to :qiwo_enterprises
  attr_accessible :cer_file, :qiwo_enterprise_id, :id, :key_file, :user_id,:cer_file_file_name,:key_file_file_name,:is_active,:sat_key,:apitoken
  has_attached_file :cer_file
  has_attached_file :key_file
end
