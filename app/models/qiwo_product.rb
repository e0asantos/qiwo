class QiwoProduct < ActiveRecord::Base
  has_many :qiwo_log_karts 

  attr_accessible :description, :id, :image_id, :location_store, :name, :raw_price, :sale_price, :stock_items,:business_source,:image_id_file_name,:sku,:warehouse,:measure_units,:measure_deep,:ends_on,:unique_uri,:search_enabled,:discount_amount
  attr_accessible :measure_width,:measure_height,:keywords,:major_price,:major_quantity,:weight_number,:total_sales,:tax,:is_pos,:shipping_cost,:nfcTicket
  attr_accessible :image_one_file_name,:image_one
  attr_accessible :image_two_file_name,:image_two
  attr_accessible :image_three_file_name,:image_three
  has_attached_file :image_id, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/usr/src/qiwo/app/assets/images/:style/missing.png"
  has_attached_file :image_one, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  has_attached_file :image_two, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  has_attached_file :image_three, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  def total_sales=(total_sales)
    write_attribute(:total_sales, total_sales)
  end

  def total_sales
    return read_attribute(:total_sales)
  end

  scope :salesMade, ->(conteo = 0) {joins(:qiwo_log_karts).
    select('qiwo_products.*,count(qiwo_log_karts.id) as total_sales').
    group('qiwo_products.id').
    having('count(qiwo_log_karts.id) > '+conteo.to_s).order("total_sales DESC")
}

end
