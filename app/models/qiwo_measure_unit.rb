class QiwoMeasureUnit < ActiveRecord::Base
  attr_accessible :id, :label_es

  def unit_display
    "#{tech_id} - #{label_es}"
  end
end
