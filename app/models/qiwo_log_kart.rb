class QiwoLogKart < ActiveRecord::Base
	belongs_to :qiwo_products
	has_many :qiwo_facturas
  attr_accessible :cotizacion_number, :coupon_id, :facebook_id, :id, :ip_user,:qiwo_product_id, :store_id, :user_id,:paypal,:conekta,:oxxo,:banorte,:shipping_cost,:shipping_guide,:qiwo_order_id,:final_gain,:final_sale,:final_tax,:final_total,:openpay_stores,:discount,:inreference
end
