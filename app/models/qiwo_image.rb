class QiwoImage < ActiveRecord::Base
  attr_accessible :allowed, :fron_page, :id, :image_url, :user_id,:image_url_file_name
  has_attached_file :image_url, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/usr/src/qiwo/app/assets/images/:style/missing.png"
end
