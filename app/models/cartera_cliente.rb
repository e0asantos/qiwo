class CarteraCliente < ActiveRecord::Base
  attr_accessible :contacto_email, :contacto_name, :is_client, :location_lat, :location_lon, :negocio, :next_visit, :observaciones, :telefono, :visitado
end
